-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2016 at 03:51 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+08:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dcis_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_year`
--

CREATE TABLE IF NOT EXISTS `academic_year` (
  `academic_year_id` int(11) NOT NULL,
  `academic_year` varchar(9) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic_year`
--

INSERT INTO `academic_year` (`academic_year_id`, `academic_year`, `start_date`, `end_date`) VALUES
(1, '2015-2016', '2015-03-01', '2016-04-01'),
(2, '2016-2017', '2016-02-24', '2017-02-15');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL,
  `school_id` varchar(25) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `phone` int(11) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `school_id`, `firstname`, `middlename`, `lastname`, `dob`, `phone`, `photo`) VALUES
(1, 'admin', 'Mary Jane', 'G', 'Sabellano', '2015-12-01', 94312347, '');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE IF NOT EXISTS `attendance` (
  `attendance_id` int(11) NOT NULL,
  `class_member_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `callout`
--

CREATE TABLE IF NOT EXISTS `callout` (
  `callout_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `type` enum('info','warning','danger') NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(255) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `deadline` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `callout`
--

INSERT INTO `callout` (`callout_id`, `class_id`, `type`, `title`, `content`, `active`, `deadline`, `created_at`, `modified_at`) VALUES
(1, 20, 'danger', 'e', 'e', '1', '0000-00-00 00:00:00', '2016-02-23 00:06:27', '2016-02-23 01:00:08'),
(2, 20, 'warning', 'b', 'b', '1', '0000-00-00 00:00:00', '2016-02-23 00:06:32', '2016-02-23 00:06:32'),
(3, 20, 'info', 'd', 'd', '1', '0000-00-00 00:00:00', '2016-02-23 00:06:36', '2016-02-23 00:59:54'),
(4, 20, 'danger', 'red', 'b', '1', '0000-00-00 00:00:00', '2016-02-23 10:06:19', '2016-02-23 10:06:25'),
(5, 20, 'info', 'a', 'a', '1', '0000-00-00 00:00:00', '2016-02-23 10:08:17', '2016-02-23 10:08:17'),
(6, 20, 'info', 'b', 'b', '1', '0000-00-00 00:00:00', '2016-02-23 10:08:23', '2016-02-23 10:08:23'),
(7, 20, 'info', 'c', 'c', '1', '0000-00-00 00:00:00', '2016-02-23 10:08:27', '2016-02-23 10:08:27'),
(8, 20, 'info', 'd', 'd', '1', '0000-00-00 00:00:00', '2016-02-23 10:08:43', '2016-02-23 10:08:43'),
(9, 20, 'info', 'e', 'e', '1', '0000-00-00 00:00:00', '2016-02-23 10:08:50', '2016-02-23 10:08:50'),
(10, 20, 'warning', 'f', 'f', '1', '0000-00-00 00:00:00', '2016-02-23 10:08:56', '2016-02-23 10:08:56'),
(11, 20, 'info', 'g', 'g', '1', '0000-00-00 00:00:00', '2016-02-23 10:09:19', '2016-02-23 10:09:19'),
(12, 20, 'info', 'h', 'h', '1', '0000-00-00 00:00:00', '2016-02-23 10:10:03', '2016-02-23 10:10:03'),
(13, 20, 'info', 'i', 'i', '1', '0000-00-00 00:00:00', '2016-02-23 10:10:07', '2016-02-23 10:10:07'),
(14, 20, 'info', 'j', 'j', '1', '0000-00-00 00:00:00', '2016-02-23 10:10:10', '2016-02-23 10:10:10'),
(15, 20, 'info', 'k', 'k', '1', '0000-00-00 00:00:00', '2016-02-23 10:10:14', '2016-02-23 10:10:14'),
(16, 20, 'info', 'l', 'l', '1', '0000-00-00 00:00:00', '2016-02-23 10:10:17', '2016-02-23 10:10:17'),
(17, 20, 'info', 'm', 'm', '1', '0000-00-00 00:00:00', '2016-02-23 10:10:21', '2016-02-23 10:10:21'),
(18, 20, 'info', 'n', 'n', '1', '0000-00-00 00:00:00', '2016-02-23 10:10:24', '2016-02-23 10:10:24'),
(19, 20, 'danger', 'o', 'o', '1', '0000-00-00 00:00:00', '2016-02-23 10:10:28', '2016-02-23 10:13:36'),
(20, 20, 'warning', 'p', 'p', '1', '0000-00-00 00:00:00', '2016-02-23 10:13:16', '2016-02-23 10:15:36');

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `class_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `instructor_id` varchar(25) NOT NULL,
  `course_id` int(11) NOT NULL,
  `group_number` int(11) NOT NULL,
  `max_absences` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `semester_id` int(11) NOT NULL,
  `class_grade_sf` float DEFAULT '0' COMMENT 'Total grade of class so far.'
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`class_id`, `room_id`, `instructor_id`, `course_id`, `group_number`, `max_absences`, `active`, `semester_id`, `class_grade_sf`) VALUES
(1, 5, 'instructor', 2, 4, 7, 1, 1, 0),
(18, 1, 'instructor', 1, 3, 1, 1, 1, 0),
(19, 3, 'instructor2', 3, 1, 1, 1, 1, 0),
(20, 1, 'instructor', 4, 1, 1, 1, 1, 37.5),
(21, 2, 'instructor', 10, 2, 1, 1, 1, 0),
(22, 4, 'instructor', 11, 5, 1, 1, 1, 0),
(23, 2, 'instructor', 12, 2, 1, 1, 1, 0),
(24, 1, 'instructor', 13, 1, 1, 1, 1, 0),
(25, 3, 'instructor', 14, 6, 1, 1, 1, 0),
(26, 3, 'instructor2', 16, 4, 5, 1, 1, 0),
(27, 1, 'instructor', 15, 3, 7, 1, 1, 0),
(28, 4, 'instructor2', 81, 1, 6, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `class_calendar`
--

CREATE TABLE IF NOT EXISTS `class_calendar` (
  `calendar_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class_chatbox`
--

CREATE TABLE IF NOT EXISTS `class_chatbox` (
  `convo_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `school_id` varchar(25) NOT NULL,
  `username` varchar(25) NOT NULL,
  `content` varchar(255) NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_chatbox`
--

INSERT INTO `class_chatbox` (`convo_id`, `class_id`, `school_id`, `username`, `content`, `active`, `created_at`, `modified_at`) VALUES
(130, 20, 'admin', 'Mary Jane Sabellano', 'Testing lang', '1', '2016-02-03 08:12:20', '2016-02-03 08:12:20'),
(131, 20, 'admin', 'Mary Jane Sabellano', 'Testing nasad bi', '1', '2016-02-03 08:13:18', '2016-02-03 08:13:18'),
(132, 20, '12104744', 'Joseph Vincent Lao', 'Testing sad ko ha', '1', '2016-02-03 08:14:41', '2016-02-03 08:14:41'),
(133, 20, 'admin', 'Mary Jane Sabellano', 'Nice ka!', '1', '2016-02-03 08:14:53', '2016-02-03 08:14:53'),
(134, 20, 'admin', 'Mary Jane Sabellano', 'Testing nasda kooo', '1', '2016-02-03 14:18:09', '2016-02-03 14:18:09'),
(135, 20, '12104744', 'Joseph Vincent Lao', 'Hahahahahaha', '1', '2016-02-03 14:18:15', '2016-02-03 14:18:15'),
(136, 20, 'instructor', 'Angie Ceniza', 'Testing lang', '1', '2016-02-04 10:01:40', '2016-02-04 10:01:40'),
(137, 20, 'instructor', 'Angie Ceniza', 'Testing po', '1', '2016-02-04 10:42:28', '2016-02-04 10:42:28'),
(138, 20, '12104744', 'Joseph Vincent Lao', 'Testing lang', '1', '2016-02-04 10:49:43', '2016-02-04 10:49:43'),
(139, 20, 'instructor', 'Angie Ceniza', 'Testing nasad ni', '1', '2016-02-04 10:49:52', '2016-02-04 10:49:52'),
(140, 20, 'instructor', 'Angie Ceniza', 'testing nasad', '1', '2016-02-04 10:50:27', '2016-02-04 10:50:27'),
(141, 20, 'instructor', 'Angie Ceniza', 'Testing nasaad', '1', '2016-02-04 10:54:05', '2016-02-04 10:54:05'),
(142, 20, '12104744', 'Joseph Vincent Lao', 'Sure diha ooy', '1', '2016-02-04 10:54:17', '2016-02-04 10:54:17'),
(143, 20, 'instructor', 'Angie Ceniza', '4564564', '1', '2016-02-04 10:55:43', '2016-02-04 10:55:43'),
(144, 20, '12104744', 'Joseph Vincent Lao', '345345345352', '1', '2016-02-04 10:56:19', '2016-02-04 10:56:19'),
(145, 20, 'instructor', 'Angie Ceniza', 'rtrrrrt', '1', '2016-02-04 10:57:16', '2016-02-04 10:57:16'),
(146, 20, 'instructor', 'Angie Ceniza', 'wqeqweqweqweqwe', '1', '2016-02-04 11:03:47', '2016-02-04 11:03:47'),
(147, 20, '12104744', 'Joseph Vincent Lao', '123123123123', '1', '2016-02-04 11:03:56', '2016-02-04 11:03:56'),
(148, 20, 'instructor', 'Angie Ceniza', '5634563', '1', '2016-02-04 11:04:46', '2016-02-04 11:04:46'),
(149, 20, '12104744', 'Joseph Vincent Lao', '34563224', '1', '2016-02-04 11:04:53', '2016-02-04 11:04:53'),
(150, 20, 'admin', 'Mary Jane Sabellano', 'Hello po', '1', '2016-02-04 11:22:21', '2016-02-04 11:22:21'),
(151, 20, 'instructor', 'Angie Ceniza', 'Hi miss', '1', '2016-02-04 11:22:31', '2016-02-04 11:22:31'),
(152, 20, '12104744', 'Joseph Vincent Lao', 'Hahahahahaha', '1', '2016-02-04 11:22:55', '2016-02-04 11:22:55'),
(153, 20, 'admin', 'Mary Jane Sabellano', 'Awooot', '1', '2016-02-04 11:23:13', '2016-02-04 11:23:13'),
(154, 20, '12104744', 'Joseph Vincent Lao', 'Testing lang', '1', '2016-02-06 19:23:13', '2016-02-06 19:23:13'),
(155, 20, 'admin', 'Mary Jane Sabellano', 'Testing sad', '1', '2016-02-06 19:23:28', '2016-02-06 19:23:28'),
(156, 20, 'admin', 'Mary Jane Sabellano', 'tryertyghjh ', '1', '2016-02-07 12:02:02', '2016-02-07 12:02:02'),
(157, 20, 'admin', 'Mary Jane Sabellano', 'rtyrtyrtyry', '1', '2016-02-07 13:43:29', '2016-02-07 13:43:29'),
(158, 20, 'admin', 'Mary Jane Sabellano', 'werwerwer', '1', '2016-02-07 13:45:08', '2016-02-07 13:45:08'),
(159, 20, 'admin', 'Mary Jane Sabellano', '54645645', '1', '2016-02-07 13:47:01', '2016-02-07 13:47:01'),
(160, 20, 'admin', 'Mary Jane Sabellano', 'gfdgdfgd', '1', '2016-02-07 13:47:36', '2016-02-07 13:47:36'),
(161, 20, 'admin', 'Mary Jane Sabellano', '45645', '1', '2016-02-07 13:48:15', '2016-02-07 13:48:15'),
(162, 20, 'admin', 'Mary Jane Sabellano', 'dfgasdfsd sdfsdfsd', '1', '2016-02-07 13:48:46', '2016-02-07 13:48:46'),
(163, 20, 'admin', 'Mary Jane Sabellano', '567ghfg ', '1', '2016-02-07 13:51:14', '2016-02-07 13:51:14'),
(164, 20, 'admin', 'Mary Jane Sabellano', 'gdfgfd sdfgdg dfg dfgdfg', '1', '2016-02-07 13:52:03', '2016-02-07 13:52:03'),
(165, 20, 'admin', 'Mary Jane Sabellano', '5464 dfgfdg ''', '1', '2016-02-07 13:53:17', '2016-02-07 13:53:17'),
(166, 20, 'admin', 'Mary Jane Sabellano', 'rty', '1', '2016-02-07 13:53:27', '2016-02-07 13:53:27'),
(167, 20, 'admin', 'Mary Jane Sabellano', 'rtyhfg fghfgh', '1', '2016-02-07 13:53:52', '2016-02-07 13:53:52'),
(168, 20, 'admin', 'Mary Jane Sabellano', 'hgdfgdf dfgdfg', '1', '2016-02-07 13:54:54', '2016-02-07 13:54:54'),
(169, 20, '12103895', 'Lyster Paul Astudillo', 'test', '1', '2016-02-12 07:18:16', '2016-02-12 07:18:16'),
(170, 20, 'admin', 'Mary Jane Sabellano', 'Testing chat', '1', '2016-02-18 18:42:07', '2016-02-18 18:42:07');

-- --------------------------------------------------------

--
-- Table structure for table `class_member`
--

CREATE TABLE IF NOT EXISTS `class_member` (
  `class_member_id` int(11) NOT NULL,
  `student_id` varchar(25) NOT NULL,
  `class_id` int(11) NOT NULL,
  `total_absences` int(11) NOT NULL DEFAULT '0',
  `status_attendance` enum('Warning','Readmission','Dropped') NOT NULL,
  `cm_grade_sf` float NOT NULL DEFAULT '0' COMMENT 'Grade of student so far.',
  `status_grade` enum('Warning','Fail','Pass') NOT NULL,
  `seat-row` int(11) NOT NULL,
  `seat-column` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_member`
--

INSERT INTO `class_member` (`class_member_id`, `student_id`, `class_id`, `total_absences`, `status_attendance`, `cm_grade_sf`, `status_grade`, `seat-row`, `seat-column`) VALUES
(5, '12104744', 20, 0, '', 25, '', 0, 0),
(6, '12102719', 20, 0, '', 25, '', 0, 0),
(11, '12105455', 20, 0, 'Warning', 25, 'Warning', 0, 0),
(12, '12103895', 20, 0, 'Warning', 25, 'Warning', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `class_schedule`
--

CREATE TABLE IF NOT EXISTS `class_schedule` (
  `class_schedule_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `day` enum('M','T','W','TH','F','S','SU') NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_schedule`
--

INSERT INTO `class_schedule` (`class_schedule_id`, `class_id`, `day`, `start_time`, `end_time`) VALUES
(1, 26, 'M', '12:00:00', '01:30:00'),
(2, 26, 'W', '12:00:00', '01:30:00'),
(3, 27, 'M', '12:30:00', '01:30:00'),
(4, 27, 'W', '12:30:00', '01:30:00'),
(5, 28, 'M', '01:30:00', '02:30:00'),
(6, 28, 'W', '01:30:00', '02:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `class_settings`
--

CREATE TABLE IF NOT EXISTS `class_settings` (
  `class_settings_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `tile_color` varchar(25) NOT NULL DEFAULT 'grey',
  `tile_icon` varchar(25) NOT NULL DEFAULT 'fa-cog',
  `enable_latest_activity` enum('0','1') NOT NULL DEFAULT '1',
  `enable_callouts` enum('1','0') NOT NULL DEFAULT '1',
  `enable_chatbox` enum('1','0') NOT NULL DEFAULT '1',
  `enable_file_upload` enum('1','0') NOT NULL DEFAULT '1',
  `stud_edit_post` enum('1','0') NOT NULL DEFAULT '1',
  `stud_delete_post` enum('1','0') NOT NULL DEFAULT '1',
  `enable_internal_notif` enum('1','0') NOT NULL DEFAULT '1',
  `enable_external_notif` enum('1','0') NOT NULL DEFAULT '1',
  `notify_via_email` enum('1','0') NOT NULL DEFAULT '1',
  `notify_via_sms` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_settings`
--

INSERT INTO `class_settings` (`class_settings_id`, `class_id`, `tile_color`, `tile_icon`, `enable_latest_activity`, `enable_callouts`, `enable_chatbox`, `enable_file_upload`, `stud_edit_post`, `stud_delete_post`, `enable_internal_notif`, `enable_external_notif`, `notify_via_email`, `notify_via_sms`) VALUES
(3, 20, 'red', 'fa-desktop', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(4, 1, 'red', 'fa-cog', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(5, 18, 'green', 'fa-cog', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(6, 19, 'blue', 'fa-cog', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(7, 21, 'purple', 'fa-cog', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(8, 22, 'grey', 'fa-cog', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(9, 23, 'red', 'fa-cog', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(10, 24, 'blue', 'fa-cog', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(11, 25, 'green', 'fa-cog', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(18, 27, 'grey', 'fa-cog', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(19, 28, 'grey', 'fa-cog', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `comment_id` int(11) NOT NULL,
  `discussion_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `school_id` varchar(25) NOT NULL,
  `username` varchar(55) NOT NULL,
  `content` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `edited` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`comment_id`, `discussion_id`, `class_id`, `school_id`, `username`, `content`, `created_at`, `modified_at`, `active`, `edited`) VALUES
(1, 1, 20, 'admin', 'Mary Jane Sabellano', 'test', '2016-02-12 17:35:26', '2016-02-12 17:35:26', 1, '0'),
(2, 2, 20, '12104744', 'Joseph Vincent Lao', 'Comment ko', '2016-02-18 17:39:32', '2016-02-18 17:39:32', 1, '0'),
(3, 3, 20, '12104744', 'Joseph Vincent Lao', 'Comment haha', '2016-02-18 17:40:45', '2016-02-18 17:40:45', 1, '0'),
(4, 3, 20, 'admin', 'Mary Jane Sabellano', 'Comment oy', '2016-02-18 17:42:04', '2016-02-18 17:42:04', 1, '0'),
(5, 2, 20, 'admin', 'Mary Jane Sabellano', 'Comment hahaha 123', '2016-02-18 17:42:49', '2016-02-18 17:44:32', 1, '1'),
(6, 2, 20, '12104744', 'Joseph Vincent Lao', 'asdsada dasdad', '2016-02-18 17:46:08', '2016-02-18 17:46:08', 1, '0'),
(7, 4, 20, '12104744', 'Joseph Vincent Lao', 'Testing comment dapat mag una nani.', '2016-02-18 18:13:24', '2016-02-18 18:13:24', 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `consultation`
--

CREATE TABLE IF NOT EXISTS `consultation` (
  `consultation_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `instructor_id` int(11) NOT NULL,
  `consultation_date` date NOT NULL,
  `consultation_topic` varchar(50) NOT NULL,
  `consultation_content` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultation`
--

INSERT INTO `consultation` (`consultation_id`, `class_id`, `instructor_id`, `consultation_date`, `consultation_topic`, `consultation_content`) VALUES
(1, 0, 2, '2016-02-10', 'This is a test', 'This is a very long content. This is a very long content. This is a very long content. This is a very long content. This is a very long content. This is a very long content. This is a very long content. This is a very long content .This is a very long content'),
(2, 0, 1, '2016-02-23', 'Capstone Project', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop p');

-- --------------------------------------------------------

--
-- Table structure for table `consultees`
--

CREATE TABLE IF NOT EXISTS `consultees` (
  `consultation_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultees`
--

INSERT INTO `consultees` (`consultation_id`, `student_id`) VALUES
(2, 5),
(2, 7),
(1, 5),
(1, 6),
(1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `course_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `course_code` varchar(10) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `req_year` int(11) NOT NULL,
  `req_semester` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `program_id`, `course_code`, `course_title`, `req_year`, `req_semester`) VALUES
(1, 1, 'ICT209', 'Capstone Proposal', 4, 1),
(2, 1, 'ICT146', 'Capstone Project', 4, 2),
(3, 1, 'ICT126', 'Multimedia Production', 2, 1),
(4, 1, 'ICT110', 'Introduction to Computer Science and Programming', 1, 1),
(10, 1, 'ICT300', 'CMS Framework', 3, 1),
(11, 1, 'ICT137', 'Quality Assurance', 3, 2),
(12, 1, 'ICT212', 'PHP Something', 2, 1),
(13, 1, 'ICT131', 'Database Systems I', 3, 1),
(14, 1, 'ICT122', 'Multimedia Basics with Image Processing', 2, 1),
(15, 1, 'ICT111', 'ICT Fundamentals', 1, 1),
(16, 1, 'ICT116', 'Advanced Programming A', 1, 2),
(18, 1, 'ICT118', 'Web Page Design', 1, 2),
(25, 1, 'ICT135', 'Oral Communication for ICT II', 3, 2),
(28, 1, 'ICT139', 'Professional Ethics for ICT', 3, 2),
(35, 1, 'ICT121', 'Advanced Programming B', 2, 1),
(36, 1, 'ICT123', 'Introduction to Management', 2, 1),
(38, 1, 'ICT132', 'Data Comm. & Networking', 3, 1),
(39, 1, 'ICT133', 'Presentation Skillz', 3, 1),
(49, 1, 'ICT117', 'Software Application', 1, 2),
(53, 1, 'ICT128', 'Business Processes', 2, 2),
(56, 1, 'ICT134', 'Oral Communication for ICT I', 3, 1),
(58, 1, 'ICT136', 'Database Systems II', 3, 2),
(59, 1, 'ICT138', 'Network Management and Security', 3, 2),
(61, 1, 'ICT141', 'Web Application Development', 4, 1),
(62, 1, 'ICT142', 'Internship/OJT/Practicum', 4, 1),
(63, 1, 'ICT127', 'Lab Technician''s Course', 2, 2),
(64, 2, 'IT110', 'Introduction to Computer Science & Programming', 1, 1),
(65, 2, 'IT11', 'Computer Operations', 1, 1),
(66, 2, 'IT116', 'Advanced Programming', 1, 2),
(67, 2, 'IT121', 'Data Structures I', 2, 1),
(68, 2, 'IT126', 'Data Structures II', 2, 2),
(69, 2, 'IT127', 'File Organization and Processing', 2, 2),
(70, 2, 'IT128', 'Multimedia Systems', 2, 2),
(71, 2, 'IT130', 'Database Management Systems I', 3, 1),
(72, 2, 'IT131', 'Personal Computer Technology', 3, 1),
(73, 2, 'IT132', 'Presentation Skills in IT', 3, 1),
(74, 2, 'IT133', 'Introduction to Accounting for IT', 3, 1),
(75, 2, 'IT134', 'Object-Oriented Technology', 3, 1),
(76, 2, 'IT135', 'Introduction to Management', 3, 1),
(77, 2, 'IT136', 'Database Management Systems II', 3, 2),
(78, 2, 'IT137', 'Computer Systems Organization with Assembly Language', 3, 2),
(79, 2, 'IT138', 'Data Communication & Networking', 3, 2),
(80, 2, 'IT139', 'Web Applications Development', 3, 2),
(81, 2, 'IT140', 'Software Engineering I', 4, 1),
(82, 2, 'IT141', 'Operating Systems', 4, 1),
(83, 2, 'IT142', 'Ethics for the IT Profession', 4, 1),
(84, 2, 'IT143', 'Quality Consciousness, Habits and Processes', 4, 1),
(85, 2, 'IT144', 'Systems Resource Management', 4, 1),
(86, 2, 'IT150', 'IT Practicum', 4, 1),
(87, 2, 'IT145', 'Software Engineering II', 4, 2),
(88, 2, 'IT146', 'Management Information System', 4, 2),
(89, 2, 'IT147', 'Quality Management and Processes', 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `department_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `department_code` varchar(10) NOT NULL,
  `department_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `school_id`, `department_code`, `department_name`) VALUES
(1, 1, 'DCIS', 'Department of Computer and Information Sciences');

-- --------------------------------------------------------

--
-- Table structure for table `discussion`
--

CREATE TABLE IF NOT EXISTS `discussion` (
  `discussion_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `school_id` varchar(25) NOT NULL,
  `username` varchar(25) NOT NULL,
  `discussion_content` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` enum('1','0') NOT NULL DEFAULT '1',
  `edited` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discussion`
--

INSERT INTO `discussion` (`discussion_id`, `class_id`, `school_id`, `username`, `discussion_content`, `created_at`, `modified_at`, `active`, `edited`) VALUES
(1, 20, 'admin', 'Mary Jane Sabellano', 'Testing post hahaha<br>', '2016-02-12 17:35:19', '2016-02-18 17:22:48', '1', '1'),
(2, 20, '12104744', 'Joseph Vincent Lao', 'Testing nasad ni ahihi', '2016-02-18 17:34:20', '2016-02-18 17:34:20', '1', '0'),
(3, 20, 'admin', 'Mary Jane Sabellano', 'Post ko hahaha', '2016-02-18 17:39:58', '2016-02-18 17:39:58', '1', '0'),
(4, 20, '12104744', 'Joseph Vincent Lao', 'Post sad ko naunsa', '2016-02-18 17:40:17', '2016-02-18 17:40:17', '1', '0'),
(5, 20, '12104744', 'Joseph Vincent Lao', 'Testing post dapat kani nasad una', '2016-02-18 18:13:39', '2016-02-18 18:13:39', '1', '0'),
(6, 20, 'admin', 'Mary Jane Sabellano', 'Testing post dapat kani nasad mag una haha', '2016-02-18 18:38:11', '2016-02-18 18:38:11', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(250) NOT NULL,
  `event_details` text NOT NULL,
  `event_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `calendar_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE IF NOT EXISTS `grade` (
  `grade_id` int(11) NOT NULL,
  `history_id` int(11) NOT NULL,
  `class_member_id` int(11) NOT NULL,
  `rubrics_formula_id` int(11) NOT NULL,
  `score` float NOT NULL,
  `total` int(11) NOT NULL,
  `grade_active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`grade_id`, `history_id`, `class_member_id`, `rubrics_formula_id`, `score`, `total`, `grade_active`) VALUES
(1, 1, 12, 1, 0, 100, '1'),
(2, 1, 5, 1, 0, 100, '1'),
(3, 1, 11, 1, 0, 100, '1'),
(4, 1, 6, 1, 0, 100, '1'),
(5, 2, 12, 2, 50, 50, '1'),
(6, 2, 5, 2, 50, 50, '1'),
(7, 2, 11, 2, 50, 50, '1'),
(8, 2, 6, 2, 50, 50, '1'),
(9, 3, 12, 2, 50, 100, '1'),
(10, 3, 5, 2, 50, 100, '1'),
(11, 3, 11, 2, 50, 100, '1'),
(12, 3, 6, 2, 50, 100, '1');

-- --------------------------------------------------------

--
-- Table structure for table `grade_history`
--

CREATE TABLE IF NOT EXISTS `grade_history` (
  `grade_history_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `formula_id` int(11) NOT NULL,
  `activity_title` varchar(50) NOT NULL,
  `perfect_score` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ghistory_active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_history`
--

INSERT INTO `grade_history` (`grade_history_id`, `class_id`, `formula_id`, `activity_title`, `perfect_score`, `created_at`, `ghistory_active`) VALUES
(1, 20, 1, 'Quiz 1', 0, '2016-02-22 15:26:52', '1'),
(2, 20, 2, 'Assignment 1', 50, '2016-02-22 16:36:06', '1'),
(3, 20, 2, 'Assignment 2', 100, '2016-02-22 16:36:16', '1');

-- --------------------------------------------------------

--
-- Table structure for table `grade_rubrics`
--

CREATE TABLE IF NOT EXISTS `grade_rubrics` (
  `formula_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `percent` float NOT NULL,
  `active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_rubrics`
--

INSERT INTO `grade_rubrics` (`formula_id`, `class_id`, `type`, `percent`, `active`) VALUES
(1, 20, 'Quiz', 25, '0'),
(2, 20, 'Assignment', 25, '1');

-- --------------------------------------------------------

--
-- Table structure for table `holiday`
--

CREATE TABLE IF NOT EXISTS `holiday` (
  `id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `allDay` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `holiday`
--

INSERT INTO `holiday` (`id`, `title`, `start`, `end`, `allDay`) VALUES
(3, 'Capstone Deadline', '2016-02-23 00:00:00', '2016-02-23 00:00:00', 'true'),
(4, 'Capstone Defense', '2016-03-04 00:00:00', '2016-03-05 00:00:00', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `instructor`
--

CREATE TABLE IF NOT EXISTS `instructor` (
  `instructor_id` int(11) NOT NULL,
  `school_id` varchar(25) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(155) NOT NULL,
  `phone` int(11) NOT NULL,
  `degree` varchar(25) NOT NULL,
  `started` date NOT NULL,
  `status` enum('Part-time','Full-time') NOT NULL,
  `load_units` int(11) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instructor`
--

INSERT INTO `instructor` (`instructor_id`, `school_id`, `firstname`, `middlename`, `lastname`, `dob`, `email`, `phone`, `degree`, `started`, `status`, `load_units`, `photo`) VALUES
(1, 'instructor', 'Angie', '', 'Ceniza', '2015-12-01', '@gmail.com', 12345, 'something', '2015-12-01', 'Full-time', 27, ''),
(2, 'instructor2', 'Joan', '', 'Tero', '2015-12-01', 'joantero@gmail.com', 123456, 'BSAd', '2015-12-02', 'Full-time', 27, '');

-- --------------------------------------------------------

--
-- Table structure for table `message_actual`
--

CREATE TABLE IF NOT EXISTS `message_actual` (
  `msg_id` int(11) NOT NULL,
  `msg_map_id` int(11) NOT NULL,
  `msg_group` int(11) NOT NULL,
  `msg_author_id` varchar(20) NOT NULL,
  `msg_subject` text NOT NULL,
  `msg_body` text NOT NULL,
  `msg_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `msg_type_s` enum('sent','draft','trash') NOT NULL DEFAULT 'sent',
  `msg_active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_actual`
--

INSERT INTO `message_actual` (`msg_id`, `msg_map_id`, `msg_group`, `msg_author_id`, `msg_subject`, `msg_body`, `msg_created_at`, `msg_type_s`, `msg_active`) VALUES
(1, 1, 0, '12102719', 'test', 'test', '2016-02-20 00:14:54', 'sent', '1'),
(2, 2, 0, '12102719', 'a', 'a', '2016-02-20 00:23:12', 'sent', '1'),
(3, 3, 0, '12102719', 'c', 'c', '2016-02-20 00:31:31', 'sent', '1'),
(4, 4, 0, 'admin', 'test', '', '2016-02-22 20:39:53', 'sent', '1'),
(5, 5, 0, 'admin', 'test', '<h4>HEADER</h4><b>To Do:</b><br><ul><li>Go out</li><li>Drink</li><li>Eat</li></ul>', '2016-02-22 20:54:37', 'sent', '1'),
(6, 6, 0, 'admin', 'Testing WYSIWG', '<h2>This is a header. </h2><b>Bold</b><i>. Italic</i><u>. Underline</u>.<br><b>Checklist.</b><br><ul><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li></ul>', '2016-02-22 22:16:09', 'sent', '1'),
(7, 7, 0, 'admin', 'Testing WYSIWG', '<h2>This is a header. </h2><b>Bold</b><i>. Italic</i><u>. Underline</u>.<br><b>Checklist.</b><br><ul><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li></ul>', '2016-02-22 22:16:09', 'sent', '1');

-- --------------------------------------------------------

--
-- Table structure for table `message_map`
--

CREATE TABLE IF NOT EXISTS `message_map` (
  `msgmap_id` int(11) NOT NULL,
  `msgmap_recipient_id` varchar(20) NOT NULL,
  `msgmap_type_r` enum('inbox','trash','draft') NOT NULL DEFAULT 'inbox',
  `msgmap_isRead` enum('0','1') NOT NULL DEFAULT '0',
  `msgmap_active` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_map`
--

INSERT INTO `message_map` (`msgmap_id`, `msgmap_recipient_id`, `msgmap_type_r`, `msgmap_isRead`, `msgmap_active`) VALUES
(1, 'admin', 'inbox', '1', '1'),
(2, 'admin', 'inbox', '1', '1'),
(3, 'admin', 'inbox', '1', '1'),
(4, '12102719', 'inbox', '1', '1'),
(5, '12102719', 'inbox', '1', '1'),
(6, '12102719', 'inbox', '0', '1'),
(7, '12104744', 'inbox', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `notification_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_log`
--

CREATE TABLE IF NOT EXISTS `profile_log` (
  `log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE IF NOT EXISTS `programs` (
  `program_id` int(11) NOT NULL,
  `deparment_id` int(11) NOT NULL,
  `program_code` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`program_id`, `deparment_id`, `program_code`) VALUES
(1, 1, 'BSICT'),
(2, 1, 'BSIT');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `room_id` int(11) NOT NULL,
  `room_number` int(11) NOT NULL,
  `room_type` enum('Classroom','Lab') NOT NULL,
  `building` varchar(50) NOT NULL,
  `floor` int(11) NOT NULL,
  `campus` enum('DT','TC','SC','NC') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`room_id`, `room_number`, `room_type`, `building`, `floor`, `campus`) VALUES
(1, 443, 'Lab', 'LB', 4, 'TC'),
(2, 442, 'Lab', 'LB', 4, 'TC'),
(3, 485, 'Classroom', 'LB', 4, 'TC'),
(4, 486, 'Classroom', 'LB', 4, 'TC'),
(5, 469, 'Lab', 'LB', 4, 'TC');

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE IF NOT EXISTS `school` (
  `school_id` int(11) NOT NULL,
  `school_code` varchar(10) NOT NULL,
  `school_name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`school_id`, `school_code`, `school_name`) VALUES
(1, 'SAS', 'School of Arts and Sciences');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE IF NOT EXISTS `semester` (
  `semester_id` int(11) NOT NULL,
  `semester` varchar(30) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `academic_year_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`semester_id`, `semester`, `start_date`, `end_date`, `academic_year_id`) VALUES
(1, '2nd Semester', '2015-10-01', '2016-04-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `staff_id` int(11) NOT NULL,
  `school_id` varchar(25) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `phone` int(11) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `school_id`, `firstname`, `middname`, `lastname`, `dob`, `phone`, `photo`) VALUES
(1, 'staff', 'Jovie', '', 'Baliguat', '2015-12-01', 123456, '');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(11) NOT NULL,
  `school_id` varchar(25) NOT NULL,
  `program_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(155) NOT NULL,
  `phone` int(11) DEFAULT NULL,
  `year_level` int(11) NOT NULL,
  `load_units` int(11) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `school_id`, `program_id`, `firstname`, `middlename`, `lastname`, `dob`, `email`, `phone`, `year_level`, `load_units`, `photo`) VALUES
(5, '12102719', 1, 'Devlin', 'Baraga', 'Pajaron', '2015-12-01', 'devlinpajaron@gmail.com', NULL, 4, 27, ''),
(6, '12103895', 1, 'Lyster Paul', 'Tagalog', 'Astudillo', '2015-12-01', 'lysterpaul@gmail.com', NULL, 4, 27, ''),
(7, '12104744', 1, 'Joseph Vincent', 'Evangelista', 'Lao', '2015-12-01', 'josephvincent@gmail.com', NULL, 4, 27, ''),
(8, '12105455', 1, 'Jun', 'Navarro', 'Mitsui', '2015-12-01', '', NULL, 4, 27, '');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `task_id` int(11) NOT NULL,
  `school_id` varchar(25) NOT NULL,
  `color` varchar(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`task_id`, `school_id`, `color`, `title`, `content`, `created_at`, `modified_at`, `active`) VALUES
(1, 'admin', 'red', 'red from blue', 'red from blue', '2016-02-18 16:41:00', '2016-02-19 23:54:00', '0'),
(2, 'admin', 'purple', 'blue na ni siya', 'blue', '2016-02-18 16:49:07', '2016-02-22 20:56:34', '1'),
(3, 'admin', 'grey', 'y to p', 'time y to p', '2016-02-18 16:49:51', '2016-02-19 23:51:26', '1'),
(4, 'admin', 'green', '4', '4', '2016-02-19 22:41:31', '2016-02-19 23:51:26', '1'),
(5, 'admin', 'purple', '5', '5', '2016-02-19 22:42:20', '2016-02-19 23:59:12', '0'),
(6, 'admin', 'purple', '3', '3', '2016-02-19 22:42:48', '2016-02-21 14:10:43', '0'),
(7, 'admin', 'grey', '4', '4', '2016-02-19 22:43:29', '2016-02-19 23:52:59', '0'),
(8, '12102719', 'blue', 'test', 'test', '2016-02-19 22:59:21', '2016-02-19 23:51:26', '1'),
(9, 'admin', 'blue', 'de', 'de', '2016-02-19 23:49:50', '2016-02-19 23:51:33', '0'),
(10, 'admin', 'red', 'g', 'jnj', '2016-02-21 14:14:25', '2016-02-22 20:56:16', '1'),
(11, 'admin', 'purple', 'Test', 'bbb', '2016-02-22 21:31:50', '2016-02-22 21:32:05', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL,
  `school_id` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL DEFAULT '/../assets/admin/pages/media/profile/avatar.png',
  `user_role` enum('student','instructor','staff','admin') NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `school_id`, `password`, `photo`, `user_role`, `active`) VALUES
(1, 'admin', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '/../assets/admin/pages/media/profile/avatar.png', 'admin', 1),
(2, 'staff', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '/../assets/admin/pages/media/profile/avatar.png', 'staff', 1),
(3, 'instructor', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '/../assets/admin/pages/media/profile/avatar.png', 'instructor', 1),
(4, '12102719', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '/../assets/admin/pages/media/profile/avatar.png', 'student', 1),
(5, '12104744', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '/../assets/admin/pages/media/profile/avatar.png', 'student', 1),
(6, '12103895', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '/../assets/admin/pages/media/profile/avatar.png', 'student', 1),
(7, '12105455', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '/../assets/admin/pages/media/profile/avatar.png', 'student', 1),
(8, 'instructor2', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '/../assets/admin/pages/media/profile/avatar.png', 'instructor', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_permission`
--

CREATE TABLE IF NOT EXISTS `user_permission` (
  `user_permission_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `permission` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_preference`
--

CREATE TABLE IF NOT EXISTS `user_preference` (
  `preference_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `displayDOB` enum('0','1') NOT NULL,
  `displayPhone` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_year`
--
ALTER TABLE `academic_year`
  ADD PRIMARY KEY (`academic_year_id`), ADD UNIQUE KEY `academic_year_ak_1` (`academic_year`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`), ADD KEY `admin_user` (`school_id`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`attendance_id`), ADD KEY `attendance_class_record` (`class_member_id`);

--
-- Indexes for table `callout`
--
ALTER TABLE `callout`
  ADD PRIMARY KEY (`callout_id`), ADD KEY `callout_class` (`class_id`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`class_id`), ADD KEY `class_course` (`course_id`), ADD KEY `class_rooms` (`room_id`), ADD KEY `class_semester` (`semester_id`), ADD KEY `class_instructor` (`instructor_id`);

--
-- Indexes for table `class_calendar`
--
ALTER TABLE `class_calendar`
  ADD PRIMARY KEY (`calendar_id`), ADD KEY `class_calendar_class` (`class_id`);

--
-- Indexes for table `class_chatbox`
--
ALTER TABLE `class_chatbox`
  ADD PRIMARY KEY (`convo_id`), ADD KEY `private_messaging_instructor` (`class_id`), ADD KEY `private_messaging_student` (`content`);

--
-- Indexes for table `class_member`
--
ALTER TABLE `class_member`
  ADD PRIMARY KEY (`class_member_id`), ADD KEY `class_record_class` (`class_id`), ADD KEY `class_record_student` (`student_id`);

--
-- Indexes for table `class_schedule`
--
ALTER TABLE `class_schedule`
  ADD PRIMARY KEY (`class_schedule_id`), ADD KEY `class_days_class` (`class_id`);

--
-- Indexes for table `class_settings`
--
ALTER TABLE `class_settings`
  ADD PRIMARY KEY (`class_settings_id`), ADD KEY `class_settings` (`class_id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`comment_id`), ADD KEY `comment_discussion` (`discussion_id`), ADD KEY `comment_user` (`school_id`), ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `consultation`
--
ALTER TABLE `consultation`
  ADD PRIMARY KEY (`consultation_id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`course_id`), ADD KEY `course_programs` (`program_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`), ADD KEY `department_school` (`school_id`);

--
-- Indexes for table `discussion`
--
ALTER TABLE `discussion`
  ADD PRIMARY KEY (`discussion_id`), ADD KEY `discussion_class` (`class_id`), ADD KEY `discussion_user` (`school_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`), ADD KEY `event_class_calendar` (`calendar_id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`grade_id`), ADD KEY `grade_class_member` (`class_member_id`), ADD KEY `grade_grade_rubrics` (`rubrics_formula_id`), ADD KEY `fk_grade_to_history` (`history_id`);

--
-- Indexes for table `grade_history`
--
ALTER TABLE `grade_history`
  ADD PRIMARY KEY (`grade_history_id`), ADD KEY `history_formula` (`formula_id`), ADD KEY `grade_history` (`class_id`);

--
-- Indexes for table `grade_rubrics`
--
ALTER TABLE `grade_rubrics`
  ADD PRIMARY KEY (`formula_id`), ADD KEY `grade_formula_class` (`class_id`);

--
-- Indexes for table `holiday`
--
ALTER TABLE `holiday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`instructor_id`), ADD UNIQUE KEY `school_id` (`school_id`);

--
-- Indexes for table `message_actual`
--
ALTER TABLE `message_actual`
  ADD PRIMARY KEY (`msg_id`), ADD KEY `fk_msgactual_user` (`msg_author_id`), ADD KEY `fk_msgactual_msgmap` (`msg_map_id`);

--
-- Indexes for table `message_map`
--
ALTER TABLE `message_map`
  ADD PRIMARY KEY (`msgmap_id`), ADD KEY `fk_msgmap_user` (`msgmap_recipient_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`notification_id`), ADD KEY `notification_class` (`class_id`);

--
-- Indexes for table `profile_log`
--
ALTER TABLE `profile_log`
  ADD PRIMARY KEY (`log_id`), ADD KEY `profile_log_user` (`user_id`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`program_id`), ADD KEY `programs_department` (`deparment_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`school_id`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`semester_id`), ADD KEY `semester_academic_year` (`academic_year_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`), ADD KEY `staff_user` (`school_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`student_id`), ADD UNIQUE KEY `student_ak_1` (`school_id`), ADD KEY `student_programs` (`program_id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`task_id`), ADD KEY `task_class` (`school_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `school_id` (`school_id`);

--
-- Indexes for table `user_permission`
--
ALTER TABLE `user_permission`
  ADD PRIMARY KEY (`user_permission_id`), ADD UNIQUE KEY `school_id` (`user_id`);

--
-- Indexes for table `user_preference`
--
ALTER TABLE `user_preference`
  ADD PRIMARY KEY (`preference_id`), ADD KEY `user_preference_user` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_year`
--
ALTER TABLE `academic_year`
  MODIFY `academic_year_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `attendance_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `callout`
--
ALTER TABLE `callout`
  MODIFY `callout_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `class_calendar`
--
ALTER TABLE `class_calendar`
  MODIFY `calendar_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `class_chatbox`
--
ALTER TABLE `class_chatbox`
  MODIFY `convo_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT for table `class_member`
--
ALTER TABLE `class_member`
  MODIFY `class_member_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `class_schedule`
--
ALTER TABLE `class_schedule`
  MODIFY `class_schedule_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `class_settings`
--
ALTER TABLE `class_settings`
  MODIFY `class_settings_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `consultation`
--
ALTER TABLE `consultation`
  MODIFY `consultation_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `discussion`
--
ALTER TABLE `discussion`
  MODIFY `discussion_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `grade_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `grade_history`
--
ALTER TABLE `grade_history`
  MODIFY `grade_history_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `grade_rubrics`
--
ALTER TABLE `grade_rubrics`
  MODIFY `formula_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `holiday`
--
ALTER TABLE `holiday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `instructor`
--
ALTER TABLE `instructor`
  MODIFY `instructor_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `message_actual`
--
ALTER TABLE `message_actual`
  MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `message_map`
--
ALTER TABLE `message_map`
  MODIFY `msgmap_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile_log`
--
ALTER TABLE `profile_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `program_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `school_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `semester_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_permission`
--
ALTER TABLE `user_permission`
  MODIFY `user_permission_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_preference`
--
ALTER TABLE `user_preference`
  MODIFY `preference_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
ADD CONSTRAINT `admin_user` FOREIGN KEY (`school_id`) REFERENCES `user` (`school_id`);

--
-- Constraints for table `attendance`
--
ALTER TABLE `attendance`
ADD CONSTRAINT `attendance_class_record` FOREIGN KEY (`class_member_id`) REFERENCES `class_member` (`class_member_id`);

--
-- Constraints for table `callout`
--
ALTER TABLE `callout`
ADD CONSTRAINT `callout_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`);

--
-- Constraints for table `class`
--
ALTER TABLE `class`
ADD CONSTRAINT `class_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`),
ADD CONSTRAINT `class_instructor` FOREIGN KEY (`instructor_id`) REFERENCES `instructor` (`school_id`),
ADD CONSTRAINT `class_rooms` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`),
ADD CONSTRAINT `class_semester` FOREIGN KEY (`semester_id`) REFERENCES `semester` (`semester_id`);

--
-- Constraints for table `class_calendar`
--
ALTER TABLE `class_calendar`
ADD CONSTRAINT `class_calendar_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`);

--
-- Constraints for table `class_chatbox`
--
ALTER TABLE `class_chatbox`
ADD CONSTRAINT `fk_class_convo` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`);

--
-- Constraints for table `class_member`
--
ALTER TABLE `class_member`
ADD CONSTRAINT `class_record_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`),
ADD CONSTRAINT `class_record_student` FOREIGN KEY (`student_id`) REFERENCES `student` (`school_id`);

--
-- Constraints for table `class_schedule`
--
ALTER TABLE `class_schedule`
ADD CONSTRAINT `class_days_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`);

--
-- Constraints for table `class_settings`
--
ALTER TABLE `class_settings`
ADD CONSTRAINT `class_settings` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`);

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
ADD CONSTRAINT `comment_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`),
ADD CONSTRAINT `comment_discussion` FOREIGN KEY (`discussion_id`) REFERENCES `discussion` (`discussion_id`),
ADD CONSTRAINT `comment_user` FOREIGN KEY (`school_id`) REFERENCES `user` (`school_id`);

--
-- Constraints for table `course`
--
ALTER TABLE `course`
ADD CONSTRAINT `course_programs` FOREIGN KEY (`program_id`) REFERENCES `programs` (`program_id`);

--
-- Constraints for table `department`
--
ALTER TABLE `department`
ADD CONSTRAINT `department_school` FOREIGN KEY (`school_id`) REFERENCES `school` (`school_id`);

--
-- Constraints for table `discussion`
--
ALTER TABLE `discussion`
ADD CONSTRAINT `discussion_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`),
ADD CONSTRAINT `discussion_user` FOREIGN KEY (`school_id`) REFERENCES `user` (`school_id`);

--
-- Constraints for table `event`
--
ALTER TABLE `event`
ADD CONSTRAINT `event_class_calendar` FOREIGN KEY (`calendar_id`) REFERENCES `class_calendar` (`calendar_id`);

--
-- Constraints for table `grade`
--
ALTER TABLE `grade`
ADD CONSTRAINT `fk_grade_to_history` FOREIGN KEY (`history_id`) REFERENCES `grade_history` (`grade_history_id`),
ADD CONSTRAINT `grade_class_member` FOREIGN KEY (`class_member_id`) REFERENCES `class_member` (`class_member_id`),
ADD CONSTRAINT `grade_grade_rubrics` FOREIGN KEY (`rubrics_formula_id`) REFERENCES `grade_rubrics` (`formula_id`);

--
-- Constraints for table `grade_history`
--
ALTER TABLE `grade_history`
ADD CONSTRAINT `grade_history` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`),
ADD CONSTRAINT `history_formula` FOREIGN KEY (`formula_id`) REFERENCES `grade_rubrics` (`formula_id`);

--
-- Constraints for table `grade_rubrics`
--
ALTER TABLE `grade_rubrics`
ADD CONSTRAINT `grade_formula_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`);

--
-- Constraints for table `instructor`
--
ALTER TABLE `instructor`
ADD CONSTRAINT `instructor_user` FOREIGN KEY (`school_id`) REFERENCES `user` (`school_id`);

--
-- Constraints for table `message_actual`
--
ALTER TABLE `message_actual`
ADD CONSTRAINT `fk_msgactual_msgmap` FOREIGN KEY (`msg_map_id`) REFERENCES `message_map` (`msgmap_id`),
ADD CONSTRAINT `fk_msgactual_user` FOREIGN KEY (`msg_author_id`) REFERENCES `user` (`school_id`);

--
-- Constraints for table `message_map`
--
ALTER TABLE `message_map`
ADD CONSTRAINT `fk_msgmap_user` FOREIGN KEY (`msgmap_recipient_id`) REFERENCES `user` (`school_id`);

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
ADD CONSTRAINT `notification_class` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`);

--
-- Constraints for table `profile_log`
--
ALTER TABLE `profile_log`
ADD CONSTRAINT `profile_log_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `programs`
--
ALTER TABLE `programs`
ADD CONSTRAINT `programs_department` FOREIGN KEY (`deparment_id`) REFERENCES `department` (`department_id`);

--
-- Constraints for table `semester`
--
ALTER TABLE `semester`
ADD CONSTRAINT `semester_academic_year` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`academic_year_id`);

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
ADD CONSTRAINT `staff_user` FOREIGN KEY (`school_id`) REFERENCES `user` (`school_id`);

--
-- Constraints for table `student`
--
ALTER TABLE `student`
ADD CONSTRAINT `student_programs` FOREIGN KEY (`program_id`) REFERENCES `programs` (`program_id`),
ADD CONSTRAINT `student_user` FOREIGN KEY (`school_id`) REFERENCES `user` (`school_id`);

--
-- Constraints for table `task`
--
ALTER TABLE `task`
ADD CONSTRAINT `task_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `user` (`school_id`);

--
-- Constraints for table `user_permission`
--
ALTER TABLE `user_permission`
ADD CONSTRAINT `user_permission` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `user_preference`
--
ALTER TABLE `user_preference`
ADD CONSTRAINT `user_preference_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
