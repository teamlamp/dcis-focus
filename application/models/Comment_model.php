<?php
class Comment_model extends CI_Model {

   public $school_id;
   private $password;
   public $modified_at;

   public function __construct(){
        parent::__construct();
   }

  public function getPostComment($school_id, $date){
    $sql = 'SELECT * FROM class LEFT JOIN course on course.course_id = class.course_id LEFT JOIN comment on comment.class_id = class.class_id LEFT JOIN user on user.school_id = comment.school_id WHERE Date(comment.modified_at) = "'.$date.'" AND comment.active = 1 ORDER BY comment.modified_at DESC';
    $query = $this->db->query($sql);   
    

    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

   public function getComments(){ // display all comments in a class
    $this->db->select("*");
    $this->db->where("active", 1);
    $this->db->from('post_comments');
    $this->db->join('users', 'post_comments.user_id = users.user_id', 'union');
    $this->db->order_by("post_comments.created_at", "asc");
    $query = $this->db->get();
    return $query->result();
    
   }

   public function getOne(){ // display only one discussion using post_id

   }

   public function addcomment($data){ 
    $this->db->insert('post_comments', $data);
   }

   public function updatecomment($post_id,$data){
    $this->db->set('updated_at', 'DATE_ADD(NOW(), INTERVAL 1 MINUTE)', FALSE);
    $this->db->where('comment_id', $post_id);
    $this->db->update('post_comments', $data);

   }

   public function deletecomment($id,$data){ // set discussion.active to 0
    $this->db->where('comment_id', $id);
    $this->db->update('post_comments', $data);
   }

   // teacher side

   public function admingetAll(){

   }

  public function getownlatestcomment($id,$post_id){
    $this->db->select("*");
    $this->db->where("active", 1);
    $this->db->where("post_id", $post_id);
    $this->db->where("user_id", $id);
    $this->db->from('post_comments');
    //$this->db->join('users', 'post_comments.user_id = users.user_id');
    $this->db->order_by("post_comments.comment_id", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if ($query->num_rows() == 1) {               
    return $query->result();
    }
    else {
    return false;
    }
    
   
  }

}