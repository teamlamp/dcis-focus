<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function add_image($data)
	{
		$this->db->select('*');
		$this->db->from('image');
		$this->db->where('user_id', $this->session->userdata('school_id'));
		$query = $this->db->get();

		if ($query->num_rows() == 1)
	  	{
	    	$this->db->where('user_id', $this->session->userdata('school_id'));
			$this->db->delete('image');

			$this->db->insert('image',$data);
	  	}

  		else
  		{ 
    		$this->db->insert('image',$data);
  		}

	}
}