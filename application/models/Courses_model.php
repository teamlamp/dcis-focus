<?php
class Courses_model extends CI_Model {

   public function __construct(){
        parent::__construct();
   }

public $program_id;
public $course_code;
public $course_title;
public $req_year;
public $req_semester;

   public function getAll(){
    $this->db->select('*');
    $this->db->from('course');
    $this->db->join('programs','course.program_id = programs.program_id','left');
    $this->db->order_by('course_code','asc');
    $query = $this->db->get();
    return $query->result_array();
   }

    public function getAllCourses(){
    $this->db->select('*');
    $this->db->from('course');
    $this->db->join('programs','course.program_id = programs.program_id','left');
    $this->db->order_by('course_code','asc');
    $query = $this->db->get();
    return $query->result();
   }

  public function addCourse()
  {
    $query = $this->db->insert('course', $this);
    return $query;
  }

  public function checkIfCourseAlreadyExists($course_code){
    $this->db->where('course_code', $course_code);
    $query = $this->db->get('course');

    if ($query->num_rows() == 1)
      {
        return true;
      }

      else
      { 
        return false;
      }
  }


}

