<?php
class Timeline_model extends CI_Model {

   public function __construct(){
        parent::__construct();
   }

  public function getPostFeed($school_id, $date){
    $sql = 'SELECT * FROM class LEFT JOIN course on course.course_id = class.course_id LEFT JOIN discussion on discussion.class_id = class.class_id LEFT JOIN user on user.school_id = discussion.school_id WHERE Date(discussion.modified_at) = "'.$date.'" AND discussion.active = "1" ORDER BY discussion.modified_at DESC';
    $query = $this->db->query($sql);   
    

    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

}
