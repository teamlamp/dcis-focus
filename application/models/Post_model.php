<?php
class Post_model extends CI_Model {

   public $school_id;
   private $password;
   public $modified_at;

   public function __construct(){
        parent::__construct();
   }

   public function getAll(){ // display all discussions of a class
        $this->db->select("*");
        $this->db->where("active", 1);
        $this->db->from('posts');
        $this->db->join('users', 'posts.user_id = users.user_id', 'union');
        $this->db->order_by("posts.post_id", "desc");
        $this->db-> limit(5);
        $query = $this->db->get();
        return $query->result();
    
   }

   public function getOne(){ // display only one discussion using post_id
        $this->db->select("*");
        $this->db->where("active", 1);
        $this->db->from('posts');
        $this->db->join('users', 'posts.user_id = users.user_id', 'union');
        $this->db->order_by("posts.post_id", "desc");
        $this->db-> limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {               
        return $query->result();
        }
        else {
        return false;
        }

   }

   public function addpost($data){ 
        $this->db->insert('posts', $data);
   }

   public function updatepost($post_id,$data){
        $this->db->set('updated_at', 'DATE_ADD(NOW(), INTERVAL 1 MINUTE)', FALSE);
        $this->db->where('post_id', $post_id);
        $this->db->update('posts', $data);

   }

   public function deletepost($id,$data){ // set discussion.active to 0
        $this->db->where('post_id', $id);
        $this->db->update('posts', $data);
   }

   // teacher side

   public function admingetAll(){

   }

   public function getownlatestpost($id){
    $this->db->select("*");
    $this->db->where("posts.active", 1);
    $this->db->where("posts.user_id", $id);
    $this->db->from('posts');
    $this->db->join('users', 'posts.user_id = users.user_id');
    $this->db->order_by("posts.post_id", "desc");
    $this->db->limit(1);
    $query = $this->db->get();
    if ($query->num_rows() == 1) {               
    return $query->result();
    }
    else {
    return false;
    }
   }

}