<?php
class Grades_model extends CI_Model {

   public $school_id;
   private $password;
   public $modified_at;

   public function __construct(){
        parent::__construct();
   }

   public function savegrades($data){
      $this->db->insert_batch('grade',$data);
   }

   public function updateSingleGrade($data){
      extract($data);
      $this->db->where('grade_id', $grade_id);
      $this->db->update('grade', array('score' => $score));    
   }

   public function updateAllGrades($data){
      $this->db->update_batch('grade', $data, 'grade_id');
   }

   public function getgrade(){
      $this->db->select('*');
      $this->db->from('grades');
      $this->db->order_by('grade_id','desc');
      $this->db->limit(1);
      $query = $this->db->get();
      return $query->result();
   }

   public function recordHistory($data){
      $this->db->insert('grade_history',$data);
   }

   public function getHistory($id){
      $this->db->select('*');
      $this->db->from('grade_history'); 
      $this->db->join('grade_rubrics', 'grade_rubrics.formula_id=grade_history.formula_id', 'left');
      $this->db->where('grade_history.class_id', $id);  
      $this->db->where('grade_history.perfect_score !=','0');    
      $this->db->where('grade_rubrics.active','1');   
      $query = $this->db->get(); 
      if($query->num_rows() != 0){
         return $query->result_array();
      }else{
         return false;
      }
   }

   public function getLastHistory(){
      $last_row=$this->db->select('grade_history_id')->order_by('grade_history_id',"desc")->limit(1)->get('grade_history')->row();
      return $last_row;
   }

   public function oneHistory($course_code,$group_number,$history_id){
      $this->db->select('*');
      $this->db->from('grade');
      $this->db->join('grade_history','grade.history_id = grade_history.grade_history_id');
      $this->db->join('class','grade_history.class_id = class.class_id');
      $this->db->join('course','class.course_id = course.course_id');
      $this->db->join('class_member','class_member.class_member_id = grade.class_member_id');
      $this->db->join('student','student.school_id = class_member.student_id');
      $this->db->join('grade_rubrics','grade_rubrics.formula_id = grade.rubrics_formula_id');
      $this->db->where('course.course_code',$course_code);
      $this->db->where('class.group_number', $group_number);
      $this->db->where('grade.history_id', $history_id);
      $this->db->where('grade_history.perfect_score !=','0');
      $this->db->group_by('grade.grade_id');
      $query = $this->db->get();
      if($query->num_rows() != 0){
         return $query->result_array();
      }else{
         return false;
      }
   }

   public function getStudentGrades($school_id, $class_id){
      $this->db->select('*');
      $this->db->from('grade');
      $this->db->join('class_member','class_member.class_member_id = grade.class_member_id');
      $this->db->join('grade_history', 'grade_history.grade_history_id = grade.history_id');
      $this->db->join('student', 'student.school_id = class_member.student_id');
      $this->db->join('grade_rubrics', 'grade_history.formula_id = grade_rubrics.formula_id');
      $this->db->where('class_member.student_id', $school_id);
      $this->db->where('class_member.class_id', $class_id);
      $this->db->where('grade.grade_active','1');
      $this->db->where('grade_history.perfect_score !=','0');
      $this->db->order_by('grade_history.created_at', 'ASC');
      $query = $this->db->get();
      if($query->num_rows() != 0){
         return $query->result_array();
      }else{
         return false;
      }
   }

   public function allGrades($id){
      $query = $this->db->query('SELECT class_member.class_member_id, student.lastname, student.firstname, grade.score, grade.grade_id, grade_history.activity_title, grade.rubrics_formula_id, grade_history.grade_history_id
                                 FROM `grade`
                                 JOIN `grade_history` ON `grade`.`history_id` = `grade_history`.`grade_history_id`
                                 JOIN `class` ON `grade_history`.`class_id` = `class`.`class_id`
                                 JOIN `course` ON `class`.`course_id` = `course`.`course_id`
                                 JOIN `class_member` ON `class_member`.`class_member_id` = `grade`.`class_member_id`
                                 JOIN `student` ON `student`.`school_id` = `class_member`.`student_id`
                                 JOIN `grade_rubrics` ON `grade_rubrics`.`formula_id` = `grade`.`rubrics_formula_id`
                                 WHERE `grade_history`.`class_id` = '.$id.' AND `grade_history`.`perfect_score` != "0"
          ORDER BY grade.rubrics_formula_id ASC, grade_history.grade_history_id ASC, student.lastname ASC, student.firstname ASC');
                                 //ORDER BY grade.grade_id ASC');
      if($query->num_rows() != 0){
         return $query->result_array();
      }else{
         return false;
      }
   }

   public function getStudentFinalGrade(){
      
   }

   public function addStudentGradesTot($class_id){
      $query = $this->db->query('SELECT g.class_member_id,SUM(g.score*(gr.percent/100)) as cm_grade_sf FROM grade g
                                 INNER JOIN grade_rubrics gr ON g.rubrics_formula_id = gr.formula_id
                                 INNER JOIN class_member cm ON g.class_member_id = cm.class_member_id
                                 INNER JOIN grade_history gh ON g.history_id = gh.grade_history_id
                                 INNER JOIN student s ON cm.student_id = s.school_id
                                 WHERE cm.class_id = '.$class_id.' 
                                 GROUP BY g.class_member_id
                                 ORDER BY g.rubrics_formula_id ASC,g.grade_id ASC');
      if($query->num_rows() != 0){
         return $query->result_array();
      }else{
         return false;
      }
   }   

   public function updateStudentGradesTot($data){
      $this->db->update_batch('class_member', $data, 'class_member_id');
   }

   public function getLastActivity($id){
     $query =  $this->db->query('SELECT grade_history_id as last_act_id, activity_title, formula_id, perfect_score FROM `grade_history` 
                                 WHERE class_id = '.$id.'
                                 ORDER BY grade_history_id DESC LIMIT 1');
      if($query->num_rows() != 0){
         return $query->result_array();
      }else{
         return false;
      }
   }

   public function countPass($id){
      $query = $this->db->query('SELECT COUNT(g.score) as pass FROM grade g 
                                 INNER JOIN grade_history gh ON g.history_id = gh.grade_history_id
                                 WHERE g.score >= (gh.perfect_score / 2) AND gh.grade_history_id = '.$id.'');
      if($query->num_rows() != 0){
         return $query->result_array();
      }else{
         return false;
      }
   }

   public function countFail($id){
      $query = $this->db->query('SELECT COUNT(g.score) as fail FROM grade g 
                                 INNER JOIN grade_history gh ON g.history_id = gh.grade_history_id
                                 WHERE g.score < (gh.perfect_score / 2) AND gh.grade_history_id = '.$id.'');
      if($query->num_rows() != 0){
         return $query->result_array();
      }else{
         return false;
      }
   }

   public function setZeroToMissedActs($class_member_id,$history_id,$rubrics_formula_id,$total){
      $query = $this->db->query('INSERT INTO grade (history_id,class_member_id,rubrics_formula_id,score,total) 
                                 VALUES ('.$history_id.','.$class_member_id.','.$rubrics_formula_id.',0,'.$total.')');
   }

   // Get passing and failing chance of a student
   public function passfailChance($school_id, $class_id){
      $query = $this->db->query("SELECT g.class_member_id,SUM(g.score*(gr.percent/100)) as cm_grade, c.class_grade_sf as c_grade FROM grade g
                                 INNER JOIN grade_rubrics gr ON g.rubrics_formula_id = gr.formula_id
                                 INNER JOIN class_member cm ON g.class_member_id = cm.class_member_id
                                 INNER JOIN grade_history gh ON g.history_id = gh.grade_history_id
                                 INNER JOIN student s ON cm.student_id = s.school_id
                                 INNER JOIN class c ON cm.class_id = c.class_id
                                 WHERE cm.class_id = '".$class_id."' and cm.student_id = '".$school_id."' AND g.grade_active = '1'");
      if($query->num_rows() != 0){
         return $query->result_array();
      }else{
         return false;
      }
   }

   public function delGrades($data){
      $this->db->set('score', '0');
      $this->db->where_in('rubrics_formula_id',$data);
      $this->db->update('grade');
   }

   public function delRH($data){
      $this->db->set('perfect_score', '0');
      $this->db->where_in('formula_id',$data);
      $this->db->update('grade_history');
   }
}