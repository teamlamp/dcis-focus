<?php
class Classes_model extends CI_Model {

   public $school_id;
   private $password;
   public $modified_at;

   public function __construct(){
        parent::__construct();
   }

  public function getAllClasses(){ // admin & staff. get all classes
    $this->db->select('*');
    $this->db->from('class'); 
    $this->db->join('course', 'course.course_id=class.course_id', 'left');
    $this->db->join('rooms', 'rooms.room_id=class.room_id', 'left');
    //$this->db->join('instructor', 'instructor.instructor_id=class.instructor_id', 'left');
    $this->db->join('class_settings','class.class_id = class_settings.class_id');
    $this->db->where('class.active', '1');
    $this->db->order_by('course.course_code','asc');         
    $query = $this->db->get(); 
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function getHandledClasses($school_id){ // instructor. get all handled classes
    $this->db->select('*');
    $this->db->from('class'); 
    $this->db->join('course', 'course.course_id=class.course_id', 'left');
    $this->db->join('rooms', 'rooms.room_id=class.room_id', 'left');
    $this->db->join('instructor', 'instructor.instructor_id=class.instructor_id', 'left');
    $this->db->join('class_settings','class.class_id = class_settings.class_id');
    $this->db->where('class.active','1');
    $this->db->where('class.instructor_id',$school_id);
    $this->db->order_by('course.course_code','asc');         
    $query = $this->db->get(); 
    if($query->num_rows() != 0) {
      return $query->result_array();
    } else {
      return false;
    }
  }

  public function getEnrolledClasses($student_id){ // student. get all enrolled classes
    $this->db->select('*');
    $this->db->from('class'); 
    $this->db->join('course', 'course.course_id=class.course_id', 'left');
    $this->db->join('rooms', 'rooms.room_id=class.room_id', 'left');
    //$this->db->join('instructor', 'instructor.instructor_id=class.instructor_id', 'left');
    $this->db->join('class_member', 'class_member.class_id=class.class_id', 'left');
    $this->db->join('class_settings','class.class_id = class_settings.class_id');
    $this->db->where('class.active','1');
    $this->db->where('class_member.student_id',$student_id);
    $this->db->order_by('course.course_code','asc');         
    $query = $this->db->get(); 
    if($query->num_rows() != 0) {
      return $query->result_array();
    } else {
      return false;
    }
  }

  public function getOneClass($course_code, $group_number){
    $this->db->select('*');
    $this->db->from('class'); 
    $this->db->join('course', 'course.course_id=class.course_id', 'left');
    $this->db->join('rooms', 'rooms.room_id=class.room_id', 'left');
    $this->db->join('instructor', 'instructor.school_id=class.instructor_id', 'left');
    $this->db->join('image', 'image.user_id=instructor.school_id', 'left');
    $this->db->join('class_settings', 'class.class_id = class_settings.class_id');
    $this->db->where('course.course_code', $course_code); 
    $this->db->where('class.group_number', $group_number);        
    $query = $this->db->get(); 
    if($query->num_rows() != 0) {
      return $query->result_array();
    } else {
      return false;
    }

  }

    public function getClass($class_id){
    $this->db->select('*');
    $this->db->from('class'); 
    $this->db->join('class_member', 'class_member.class_id = class.class_id');
    $this->db->join('student', 'student.student_id = class_member.student_id');
    $query = $this->db->get(); 
    if($query->num_rows() != 0) {
      return $query->result_array();
    } else {
      return false;
    }

  }
  
  public function getCallOuts($course_code, $group_number){
    $this->db->select('*');
    $this->db->from('callout');
    $this->db->join('class', 'class.class_id=callout.class_id', 'left'); 
    $this->db->join('course', 'course.course_id=class.course_id', 'left');
    $this->db->join('rooms', 'rooms.room_id=class.room_id', 'left');
    $this->db->where('course.course_code', $course_code); 
    $this->db->where('class.group_number', $group_number);  
    $this->db->order_by('callout.created_at', 'desc');     
    $query = $this->db->get(); 
    if($query->num_rows() != 0)
    {
      return $query->result_array();
    }
    else
    {
      return false;
    }
  }

  public function saveCallOut($data){
    $this->db->insert('callout', $data);
  }

  function importClassList($class_id)
  {
    $fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
    


    $this->db->select('*');
    $this->db->from('class_member');
    $this->db->where('class_id', $class_id);
    $this->db->order_by('class_member_id', 'desc');
    $this->db->limit(1);
    $query = $this->db->get();

$row = 0;
$col = 0;

foreach($query->result() as $x){
  $row = $x->row;
  $col = $x->column;
}

if($row != NULL && $col != NULL)
{
  if($col++ == 10){
    $row++;
    $col = 1;
  }

  else{
    $col++;
    $row = $row;
  }
}
else{
  $row = 1;
$col = 1; 
}




    while($csv_line = fgetcsv($fp,1024)) 
    {
      for ($i = 0, $j = count($csv_line); $i < $j; $i++) 
      {
        $insert_csv = array();
        $insert_csv['student_id'] = $csv_line[0];
        //$insert_csv['class_id'] = $this->class_id;
      }
      if($col == 10){
        $col=1;
        $row+=1;
      }
      $data = array(
      'student_id' => $insert_csv['student_id'] ,
      'class_id' => $class_id,
      'row' => $row,
      'column' => $col
      );
      
      if ($check = $this->checkIfStudentAlreadyExistsInClass($insert_csv['student_id'], $class_id) == false)
      {
         $data['insert_students']=$this->db->insert('class_member', $data);
      }
      else
      {
        fclose($fp) or die("can't close file");
        $data['error'] = "This student already exists in this class.";
        return $data;
      }

      $col++;

    }
    
    fclose($fp) or die("can't close file");
    $data['success']="success";
    return $data;
  }

  public function addClassMemberManually($students, $id)
  {


    $this->db->select('*');
    $this->db->from('class_member');
    $this->db->where('class_id', $id);
    $this->db->order_by('class_member_id', 'desc');
    $this->db->limit(1);
    $query = $this->db->get();

$row = 1;
$col = 1; 

    for($i=0; $i < count($students); $i++)
    {
        if($col == 10)
        {
            $col=1;
            $row++;
        }else{
          $col++;
        }
        
        $data = array(
        'student_id' => $students[$i] ,
        'class_id' => $id,
        'row' => $row,
        'column' => $col
        );


      if ($check = $this->checkIfStudentAlreadyExistsInClass($students[$i], $id) == false)
      {
         $data['insert_students']=$this->db->insert('class_member', $data);
      }
      else
      {
        $data['error'] = "This student already exists in this class.";
        return $data;
      }

    }

    $data['success']="success";
    return $data;
  }



  public function checkIfStudentAlreadyExistsInClass($student_id, $class_id){
    $this->db->where('student_id', $student_id);
    $this->db->where('class_id', $class_id);
    $query = $this->db->get('class_member');

      if ($query->num_rows() == 1)
      {
        return true;
      }

      else
      { 
        return false;
      }

  }

  public function addNewClass($data){
    $query = $this->db->insert('class', $data);
    
    if ($query)
    {
     $id = $this->db->insert_id(); 
      if ($add = $this->addClassSchedule($id) == true)
      {
        if($settings = $this->addDefaultClassSettings($id)== true)
        {
          return true;
        }
      }

      else
      {
        return false;
      }
    }
    
    else{
      return false;
    }
  }

  function addDefaultClassSettings($id){
    $data = array(
      'class_id' => $id);
    $query = $this->db->insert('class_settings', $data);
  
  if ($query){
  return true; }
  else{
    return false;
    }
  }

  function addClassSchedule($id) {      
    $data =array();
    
    for($i=0; $i < count($_POST['day']); $i++)
    {
      $data[$i] = array(
           'day' => $_POST['day'][$i], 
           'start_time' => $_POST['start_time'][$i], 
           'end_time' => $_POST['end_time'][$i], 
           'class_id' => $id
           );
      }

    $query = $this->db->insert_batch('class_schedule', $data);
    if ($query){
      return true;
    }
    else
    {
      return false;
    }

  }


public function checkIfClassExists($course_id, $group_number)
{
  $this->db->where('course_id', $course_id);
  $this->db->where('group_number', $group_number);

  $query = $this->db->get('class');

  if ($query->num_rows() == 1)
  {
    return true;
  }

  else
  { 
    return false;
  }
}

  function removeClassMember($student_id, $class_id){
    $this->db->where('student_id', $student_id);
    $this->db->where('class_id', $class_id);
    $query = $this->db->delete('class_member');

    return $query;
  }

  function getClassMembers($class_id) {
    $this->db->where('class_id', $class_id);
    $query = $this->db->get('class_member');

    return $query->result_array();
  }

    function getAllClassMembers($class_id) {

   
    $this->db->where('class_id', $class_id);
    $this->db->join('student', 'student.school_id = class_member.student_id');
    $this->db->join('image', 'image.user_id=student.school_id', 'left');
    $query = $this->db->get('class_member');

    return $query->result();
  }

  function getAttendanceStatus($class_id)
  {
    date_default_timezone_set("Asia/Manila");
    $this->db->select('*');
    $this->db->from('class');
    $this->db->where('class.class_id', $class_id);
    $this->db->join('class_member', 'class_member.class_id = class.class_id');
    $this->db->join('attendance', 'attendance.class_member_id = class_member.class_member_id');
    $this->db->where('attendance.date_today', mdate('%Y-%m-%d'));
    
    $query = $this->db->get();

    return $query->result();

  }

  function getClassMembersName($class_id) {
    $this->db->where('class_id', $class_id);
    $this->db->join('student', 'student.school_id = class_member.student_id');
    $query = $this->db->get('class_member');

    return $query->result_array();
  }

  public function saveClassPref($a,$b,$c,$d,$e,$f){
    $this->db->query("UPDATE class_settings SET tile_color='".$b."', tile_icon='".$c."', enable_latest_activity='".$d."', enable_callouts='".$e."', enable_chatbox='".$f."'  WHERE class_id = '".$a."'");
  }

  public function updateSeatPlan($data)
  {
    foreach(json_decode($data) as $value){
      $value->classID;
        $this->db->where('class_id', $value->classID);
        $this->db->where('student_id', $value->studentID);
        $dataC = array(
            'row' => $value->row,
            'column' => $value->col
        );
        $query = $this->db->update('class_member',$dataC);
    }
      
      return $data;  

  }

  public function checkAttendance($member_id, $type){
date_default_timezone_set("Asia/Manila");
$datestring = "%Y-%m-%d %h:%i:s";

$this->db->where('class_member_id', $member_id);
$this->db->where('date_today', mdate("%Y-%m-%d"));
$query = $this->db->get('attendance');

if ($query->num_rows() == 1)
  {
  $dataC = array(
      'type' => $type,
      'time_checked' => mdate($datestring));
    $this->db->where('class_member_id', $member_id);
$this->db->where('date_today', mdate("%Y-%m-%d"));
    $data = $this->db->update('attendance', $dataC);
  }

  else
  { 
    $dataC = array(
      'class_member_id' => $member_id,
      'type' => $type,
      'date_today' => mdate("%Y-%m-%d"),
      'time_checked' => mdate($datestring));

    $data = $this->db->insert('attendance', $dataC);
  }

return $data;
    
  
  }

public function countAbsences($s)
{
  $this->db->select('*');
  $this->db->from('attendance');
  $this->db->join('class_member, attendance.class_member_id = class_member.class_member_id', 'left');
  $this->db->where('type', "Absent");
  $this->db->where('class_member_id', $id);
  $query = $this->db->get();

  $a = $query->num_rows();

  $this->db->select('*');
  $this->db->from('attendance');
  $this->db->join('class_member, attendance.class_member_id = class_member.class_member_id', 'left');
  $this->db->where('type', "Late");
  $this->db->where('class_member_id', $id);
  $query = $this->db->get();

  $l = $query->num_rows() / 3;

  $ret = $a + $l;
  return $ret;
}

public function getClassMember($id)
{
  $this->db->select('*');
  $this->db->from('class_member');
  $this->db->where('student_id', $this->session->userdata('school_id'));
  $this->db->where('class_id', $id);
  $query = $this->db->get(); 
    if($query->num_rows() != 0)
    {
      return $query->result_array();
    }
    else
    {
      return false;
    }

}



public function getStudentAbsences($id){
$this->db->select('*');
$this->db->from('attendance');
$this->db->where('class_member_id', $id);
$this->db->where('type', "Absent");
$query = $this->db->get();
if($query->num_rows() != 0)
    {
      return $query->result();
    }
    else
    {
      return false;
    }

}

public function getStudentLates($id){
  $this->db->select('*');
$this->db->from('attendance');
$this->db->where('class_member_id', $id);
$this->db->where('type', "Late");
$query = $this->db->get();
if($query->num_rows() != 0)
    {
      return $query->result();
    }
    else
    {
      return false;
    }

}



}

