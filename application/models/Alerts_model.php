<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Alerts_model extends CI_Model {


public function countNotifications()
{
	$this->db->select('*');
	$this->db->from('alert');
	$this->db->where('to_user_id', $this->session->userdata('user_id'));
	$query = $this->db->get();

	return $query->num_rows();
}


}

?>