<?php
class Students_model extends CI_Model {

  public function __construct(){
    parent::__construct();
  }

  public function getAllStudents(){
    $query = $this->db->get('student');
    return $query->result();
  }

  public function getStudentsList($id){
    $this->db->select('*');
    $this->db->from('class_member'); 
    $this->db->join('student', 'class_member.student_id=student.school_id');
    $this->db->join('class','class.class_id = class_member.class_id');
    //$this->db->join('attendance','attendance.class_id = class_member.class_id');
    $this->db->where('class_member.class_id', $id);  
    $this->db->order_by('student.lastname', 'asc');       
    $query = $this->db->get(); 
    if($query->num_rows() != 0){
      return $query->result_array();
    } else {
      return false;
    }
  }  
  public function cmWithGrades($id){
    $query = $this->db->query('SELECT g.class_member_id FROM grade g 
                                INNER JOIN grade_history gh ON g.history_id = gh.grade_history_id WHERE class_id = '.$id.'
                                GROUP BY class_member_id');
    if($query->num_rows() != 0){
      return $query->result_array();
    } else {
      return false;
    }
  }

  public function countStudents($class_id){
    $query = $this->db->query("SELECT COUNT(*) FROM class_member WHERE class_id = '.$class_id.'");
    if($query->num_rows() != 0){
      return $query->result_array();
    } else {
      return false;
    }
  }

  public function getStudentData($school_id, $class_id){
    $query = $this->db->query('SELECT * FROM class_member cm INNER JOIN class c ON cm.class_id = c.class_id WHERE cm.student_id = '.$school_id.' AND cm.class_id = '.$class_id.'');
    if($query->num_rows() != 0){
      return $query->result_array();
    } else {
      return false;
    }
  }

}

