<?php
class User_model extends CI_Model {


   public $school_id;
   private $password;
   public $modified_at;

   public function __construct(){
        parent::__construct();
   }

   public function checkUserExist($school_id) {
        $this->db->select('user_id');
        $this->db->from('user');        // Get school_id if given school_id exist in the database
        $this->db->where('school_id', $school_id);
        $this->db-> limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
          return true;
        } else {
          return false;
        }
   }

   public function login($school_id,$password){
        $this->db->select('user_id, school_id, password, user_role');
        $this->db->from('user');             // Checks if validated school_id and has matching password in the database
        $this->db->where('school_id', $school_id);
        $this->db->where('password', sha1($password));
        $this->db-> limit(1);
        $query = $this -> db -> get();

        if($query -> num_rows() == 1) {
          return $query->result();
        } else {
          return false;
        }
   }

   // ngano inyo man ni gi comment out? naa namoy para query sa photo?
  public function getUserInfo($school_id,$user_role){
    $query = $this->db->query("SELECT user.photo as photo, image.img_name as image, image.ext as ext, ".$user_role.".firstname as firstname,
     ".$user_role.".lastname as lastname FROM user INNER JOIN ".$user_role." 
     ON ".$user_role.".school_id = user.school_id
     LEFT JOIN image ON image.user_id = user.school_id
                               WHERE user.school_id = '".$school_id."'");

    return $query->row_array();
  }

 // public function getUserInfo($school_id,$user_role){
 //    $query = $this->db->query("SELECT ".$user_role.".firstname as firstname, ".$user_role.".lastname as lastname FROM user INNER JOIN ".$user_role." ON ".$user_role.".school_id = user.school_id
 //                               WHERE user.school_id = '".$school_id."'");
 //    return $query->row_array();
 //  }

  // naka session na ang user_role sa logged_in user if ever mao na inyo pasabot ani
  public function getUserRole($school_id){
    $this->db->select('user_role');
    $this->db->from('user');
    $this->db->where('school_id', $school_id);
    $query = $this->db->get(); 
    if($query->num_rows() != 0){
      return $query->result_array();
    } else {
      return false;
    }
  }

  // for profile displaying. lahi2 ug columns and info to display ang each user so kuwang pa ni ug teacher, staff, admin
  public function getStudentProfile($school_id){
    $this->db->select('*');
    $this->db->from('student');
    $this->db->join('user','user.school_id = student.school_id');
    $this->db->join('programs','programs.program_id = student.program_id');
    $this->db->where('student.school_id', $school_id);
    $query = $this->db->get(); 
    if($query->num_rows() != 0){
      return $query->result_array();
    } else {
      return false;
    }
  }

  public function getInstructorProfile($school_id){
    $this->db->select('*');
    $this->db->from('instructor');
    $this->db->join('user','user.school_id = instructor.school_id');
    $this->db->where('instructor.school_id', $school_id);
    $query = $this->db->get(); 
    if($query->num_rows() != 0){
      return $query->result_array();
    } else {
      return false;
    }
  }

  public function getAdminProfile($school_id){
    $this->db->select('*');
    $this->db->from('admin');
    $this->db->join('user','user.school_id = admin.school_id');
    $this->db->where('admin.school_id', $school_id);
    $query = $this->db->get(); 
    if($query->num_rows() != 0){
      return $query->result_array();
    } else {
      return false;
    }
  }

  public function getStaffProfile($school_id){
    $this->db->select('*');
    $this->db->from('staff');
    $this->db->join('user','user.school_id = staff.school_id');
    $this->db->where('staff.school_id', $school_id);
    $query = $this->db->get(); 
    if($query->num_rows() != 0){
      return $query->result_array();
    } else {
      return false;
    }
  }



function importUsers()
  {
    $fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
    
    while($csv_line = fgetcsv($fp,1024)) 
    {
      for ($i = 0, $j = count($csv_line); $i < $j; $i++) 
      {
        $insert_csv = array();
        $insert_csv['school_id'] = $csv_line[0];
        $insert_csv['password'] = $csv_line[0][1];
      }

      $data = array(
      'school_id' => $insert_csv['school_id'] ,
      'password' => $insert_csv['password']
      );
         
      $data['insert_users']=$this->db->insert('user', $data);
    }
    
    fclose($fp) or die("can't close file");
    $data['success']="success";
    return $data;
  }

  // Auto suggest in typing a recipient at send a message
  public function findUser($findthis,$school_id){
    $query = $this->db->query("SELECT u.school_id, ur.firstname, ur.lastname FROM user u
                              INNER JOIN  (   
                                            SELECT 'admin' as user_role, school_id, firstname, lastname FROM admin
                                              UNION ALL
                                            SELECT 'staff' as user_role, school_id, firstname, lastname FROM staff
                                              UNION ALL
                                            SELECT 'instructor' as user_role, school_id, firstname, lastname FROM instructor
                                              UNION ALL
                                            SELECT   'student' as user_role, school_id, firstname, lastname FROM student   
                                          ) ur
                              ON ur.school_id = u.school_id
                              WHERE (u.school_id LIKE '%".$findthis."%' OR 
                              ur.firstname LIKE '%".$findthis."%' OR 
                              ur.lastname LIKE '%".$findthis."%') and
                              u.school_id != '".$school_id."'
                              ORDER BY ur.lastname ASC
                              LIMIT 5");
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function getUserInfoSenderEx($school_id,$msg_id){
    $query = $this->db->query("SELECT u.school_id, ur.firstname, ur.lastname FROM user u
                              INNER JOIN  (   
                                            SELECT 'admin' as user_role, school_id, firstname, lastname FROM admin
                                              UNION ALL
                                            SELECT 'staff' as user_role, school_id, firstname, lastname FROM staff
                                              UNION ALL
                                            SELECT 'instructor' as user_role, school_id, firstname, lastname FROM instructor
                                              UNION ALL
                                            SELECT   'student' as user_role, school_id, firstname, lastname FROM student   
                                          ) ur
                              ON ur.school_id = u.school_id
                              INNER JOIN message_actual ma ON ma.msg_author_id = u.school_id
                              WHERE ma.msg_author_id = '".$school_id."' AND ma.msg_map_id = '".$msg_id."'");
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
    $str = $this->db->last_query();
    echo "Sender";
    echo $str;
  }

  public function getUserInfoRecipientEx($school_id,$msg_id){
    $query = $this->db->query("SELECT u.school_id, ur.firstname, ur.lastname FROM user u
                              INNER JOIN  (   
                                            SELECT 'admin' as user_role, school_id, firstname, lastname FROM admin
                                              UNION ALL
                                            SELECT 'staff' as user_role, school_id, firstname, lastname FROM staff
                                              UNION ALL
                                            SELECT 'instructor' as user_role, school_id, firstname, lastname FROM instructor
                                              UNION ALL
                                            SELECT   'student' as user_role, school_id, firstname, lastname FROM student   
                                          ) ur
                              ON ur.school_id = u.school_id
                              INNER JOIN message_map mp ON mp.msgmap_recipient_id = u.school_id
                              WHERE mp.msgmap_recipient_id = '".$school_id."' AND mp.msgmap_id = '".$msg_id."'");
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
    $str = $this->db->last_query();
    echo "recipient";
    echo $str;
  }


}