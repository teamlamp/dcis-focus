<?php
class Semesters_model extends CI_Model {

	public $semester_id;
	public $semester;
	public $academic_year_id;
	public $start_date;
	public $end_date;

	public function loadSemesters()
	{
		$query = $this->db->get('semester');
		return $query;
	}

	public function addSemester()
	{
		$query = $this->db->insert('semester', $this);
		return $query;
	}

	public function updateSemester()
	{
		$this->db->where('semester_id', $this->semester_id);
		$query=$this->db->update('semester',$this);
		return $query;

	}

	public function deleteSemester()
	{

		$query = $this->db->delete('semester',array('semester_id'=>$this->semester_id));
		return $query;
	}

	public function getSemester(){
		
		$this->db->where('start_date >=', $log_date);
		$this->db->where('end_date <=', $log_date);
		$query = $this->db->get('semester');
		return $query->row_array();
	}

	public function getOneSemester()
	{
		$query = $this->db->get_where('semester',array('semester_id' => $this->semester_id));
		return $query->row_array();
	}

	public function getAcademicYearSemesters()
	{
		$this->db->where('academic_year_id', $this->academic_year_id);
		$query = $this->db->get('semester');
		return $query->result();
	}

	public function getCurrentSemester(){
		date_default_timezone_set("Asia/Manila");


		$this->db->select('semester_id');
		$this->db->from('semester');
		$this->db->where('start_date <=', mdate('%Y-%m-%d'));
		$this->db->where('end_date >=', mdate('%Y-%m-%d'));
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{

			foreach ($query->result_array() as $row)
				{
				   return $row['semester_id'];				   
				}			
		}

		else
		{
			return false;
		}

	}

	public function checkIfSemesterAlreadyExists($semester, $academic_year_id)
	{
		$this->db->where('academic_year_id', $academic_year_id);
		$this->db->where('semester', $semester);
		$query = $this->db->get('semester');

		if ($query->num_rows() == 1)
	    {
	       return true;
	    }
		
		else
	  	{ 
			return false;
		}
	}


} //End