<?php
class Programs_model extends CI_Model {

   public function __construct(){
        parent::__construct();
   }

   public function getAllPrograms(){
    $query = $this->db->get('programs');
    return $query->result();
   }


public function addNewProgram($data){
	$query = $this->db->insert('programs', $data);
	return $query;
}

public function deleteProgram(){
	$query = $this->db->delete('programs',array('program_id'=>$this->program_id));
    return $query;
}

public function updateProgram($data){
	$this->db->where('program_id', $this->program_id);
    $query=$this->db->update('programs',$this);
    return $query;
}

public function getOneProgram()
  {
    $query = $this->db->get_where('programs',array('program_id' => $this->program_id));
    return $query->row_array();
  }
  
public function checkIfProgramAlreadyExists($program_code)
{
    $this->db->where('program_code', $program_code);
    $query = $this->db->get('programs');

    if ($query->num_rows() == 1)
    {
      return true;
    }

    else
    { 
      return false;
    }
}

}

