<?php
class Exports_model extends CI_Model {

  public function __construct(){
    parent::__construct();
  }

      public function getAllConsultation() {
      $this->db->select('*');
      $this->db->from('consultation');
      $this->db->join('class', 'consultation.class_id = class.class_id','left');
      $this->db->join('course', 'class.course_id = course.course_id','left');
      //$this->db->join('consultees', 'consultees.consultation_id = consultation.consultation_id', 'left');
      $this->db->group_by('consultation.consultation_id');
      $query = $this->db->get(); 
      if($query->num_rows() != 0) {
          return $query->result();
      }
      else {
          return false;
      }
    }

public function getAllConsultationByInstructor() {
  $instructor_id = $this->session->userdata('user_id');
      $this->db->select('*');
      $this->db->from('consultation');
      $this->db->join('class', 'consultation.class_id = class.class_id','left');
      $this->db->join('course', 'class.course_id = course.course_id','left');
      $this->db->where('consultation.instructor_id', $instructor_id);
      $query = $this->db->get();
      if($query->num_rows() != 0)
      {
        return $query->result();
      }
      else
      {
          return false;
      }
    }

    public function getAllConsultationByStudent() {
      $this->db->select('*');
      $this->db->from('consultees');
      $this->db->join('consultation', 'consultation.consultation_id = consultees.consultation_id');
      $this->db->join('class', 'consultation.class_id = class.class_id','left');
      $this->db->join('course', 'class.course_id = course.course_id','left');
      $this->db->where(array('consultees.student_id' => $this->session->userdata('school_id')));
      $query = $this->db->get(); 
      if($query->num_rows() != 0) {
          return $query->result_array();
      }
      else
      {
          return false;
      }
    }

public function getAllConsultees(){
  $this->db->select('*');
  $this->db->from('consultees');
  $this->db->join('student', 'consultees.student_id = student.student_id','left');
  $query = $this->db->get(); 
      if($query->num_rows() != 0) {
          return $query->result();
      }
      else {
          return false;
      }
} 


}

