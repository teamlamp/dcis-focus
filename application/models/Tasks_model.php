<?php
class Tasks_model extends CI_Model {


  public function __construct(){
    parent::__construct();
  }
  

  public function saveTask($data){
    $this->db->insert('task', $data);
    return $this->db->insert_id();
  }

  public function getMyTassks($school_id){
    $this->db->select('*');
    $this->db->from('task');
    $this->db->where('school_id', $school_id);
    $this->db->where('active','1');
    $this->db->order_by('created_at', 'desc');
    $query = $this->db->get(); 
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function getDelTasks($school_id){
    $this->db->select('*');
    $this->db->from('task');
    $this->db->where('school_id', $school_id);
    $this->db->where('active','0');
    $this->db->order_by('created_at', 'desc');
    $query = $this->db->get(); 
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function countTasks($school_id){
    $query = $this->db->query("SELECT COUNT(*) as taskcount FROM task WHERE school_id = '".$school_id."' AND active ='1'");
    if($query->num_rows() != 0){
      return $query->row_array();
    } else { 
      return false;
    }
  }

  public function delTask($school_id,$task_id){
    $this->db->query("UPDATE task SET active = '0' WHERE task_id = '".$task_id."' AND school_id = '".$school_id."'");
  }

  public function delTaskPerma($school_id,$task_id){
    $this->db->query("UPDATE task SET active = '2' WHERE task_id = '".$task_id."' AND school_id = '".$school_id."'");
  }

  public function getTask($school_id,$task_id){
    $query = $this->db->query("SELECT * FROM task WHERE school_id = '".$school_id."' AND task_id = '".$task_id."' AND active = '1'");
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function editTask($task_id,$color,$title,$content){
    $this->db->query("UPDATE task SET color = '".$color."', title = '".$title."', content = '".$content."' WHERE task_id = '".$task_id."'");
  }
}

