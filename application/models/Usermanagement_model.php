<?php
class Usermanagement_model extends CI_Model {


   	public function __construct(){
    	parent::__construct();
   	}

   	public function getAllStudents () {
   		$this->db->select('*');
   		$this->db->from('student');
      $this->db->join('user', 'user.user_id = student.student_id');
   		$this->db->join('programs', 'student.program_id = programs.program_id', 'left');
      $this->db->where('user.active = 1');
   		$this->db->order_by("student.lastname", "asc");
      $query = $this->db->get(); 
    	if($query->num_rows() != 0) {
      		return $query->result_array();
    	}
    	else {
      		return false;
    	}
   	}
    public function getAllInstructors () {
      $this->db->select('*');
      $this->db->from('instructor');
      $this->db->order_by("instructor.lastname", "asc");
      $query = $this->db->get(); 
      if($query->num_rows() != 0) {
          return $query->result_array();
      }
      else {
          return false;
      }
    }

public function confirmPassword($password)
{
  $this->db->where('school_id', $this->session->userdata('school_id'));
  $this->db->where('password', sha1($password));
  $query = $this->db->get('user');
    
    if ($query->num_rows() == 1)
    {
      return true;
    }
    else
    {
      return false;
    }

}

  function changePassword()
  {   
    $this->db->where('school_id',$this->session->userdata('school_id'));
    $this->db->set('password',sha1($this->input->post('new_password')));
    $query=$this->db->update('user');   

      if ($query)
      {        
          return true;
      }

      else
      {
          return "Wrong Old Password";
      }

  }

function importUsers()
  {
    $fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
    
    while($csv_line = fgetcsv($fp,1024)) 
    {
      for ($i = 0, $j = count($csv_line); $i < $j; $i++) 
      {
        $insert_csv = array();
        $insert_csv['school_id'] = $csv_line[0];
        $insert_csv['password'] = sha1($csv_line[1]);
        $insert_csv['user_role'] = $csv_line[2];
      }

      $data = array(
      'school_id' => $insert_csv['school_id'] ,
      'password' => $insert_csv['password'] ,
      'user_role' => $insert_csv['user_role']
      );
      
      if ($check = $this->checkIfUserAlreadyExists($insert_csv['school_id']) == false)
      {   
        $data['insert_users']=$this->db->insert('user', $data);
        $id = $this->db->insert_id();

        //create a profile for newly added students 
        if($insert_csv['user_role'] == "student")
        {
          $data2 = array(
            'student_id' => $id,
             'school_id' => $insert_csv['school_id'],
            'program_id' => 3
          );
          
          $data['insert_profile'] = $this->db->insert('student', $data2);
            
        }
          
        else
        {
        //create a profile for newly added instructors      
          $data2 = array(
          'instructor_id' => $id,
          'school_id' => $insert_csv['school_id']
          );
          
          $data['insert_profile'] = $this->db->insert('instructor', $data2);
        }
      }//end - if user does not exist in DB
      
      else
      {
        fclose($fp) or die("can't close file");
        $data['error'] = "This user already exists in the database.";
        return $data;
      }

    }
    
    fclose($fp) or die("can't close file");
    $data['success']="success";
    return $data;
  }


  public function checkIfUserAlreadyExists($school_id){
    $this->db->where('school_id', $school_id);
    $query = $this->db->get('user');

      if ($query->num_rows() == 1)
      {
        return true;
      }

      else
      { 
        return false;
      }

  }



public function editStudentProfile($data)
{
  $this->db->where('school_id', $this->session->userdata('school_id'));
  $this->db->set($data);
  $query = $this->db->update('student');
  
  return $query;
}

public function editInstructorProfile($data)
{
  
}

public function deactivateStudent($id, $data) {
  $this->db->where('school_id',$id);
  $query = $this->db->update('user',$data);

}

public function addNewStudent($data, $data2){
  if($add = $this->db->insert('user', $data))
  {
    $query = $this->db->insert('student', $data2);
  }
  return $query;
}

public function addNewInstructor($data, $data2){
if($add = $this->db->insert('user', $data))
{
  $query = $this->db->insert('instructor', $data2);
}
return $query;
}




}
?>