<?php
class Calendar_model extends CI_Model {


   	public function __construct(){
    	parent::__construct();
   	}

   	public function getAllYears () {
   		$this->db->select('*');
   		$this->db->from('academic_year');
   		$query = $this->db->get(); 
    	if($query->num_rows() != 0) {
      		return $query->result_array();
    	}
    	else {
      		return false;
    	}
   	}

    public function getSemester () {

      $this->db->select('*');
      $this->db->from('semester');
      $query = $this->db->get();
      if($query->num_rows() != 0) {
        return $query->result_array();
      }
      else {
        return false;
      }
    }

    public function getHoliday () {
      $this->db->select('*');
      $this->db->from('holiday');
      $query = $this->db->get();
      if($query->num_rows() != 0) {
        return $query->result_array();
      }
      else {
        return false;
      }
    }

    public function addHoliday($data) {
      $query = $this->db->insert('holiday', $data);
      if ($query){
        return true; }
      else{
        return false;
      }
    }

    public function updateHoliday($id,$data) {
      $this->db->where('id', $id);
      $query = $this->db->update('holiday', $data);
      if ($query){
        return true;
      }
      else {
        return false;
      }
    }
    public function deleteHoliday($id) {
      $this->db->where('id',$id);
      $query = $this->db->delete('holiday');
      if ($query){
        return true;
      }
      else {
        return false;
      }
      }

    public function getClass ($class_id) {
      $this->db->select('*');
      $this->db->from('class_calendar');
      $this->db->where('class_id',$class_id);
      $query = $this->db->get();
      if($query->num_rows() != 0) {
        return $query->result_array();
      }
      else {
        return false;
      }
    }

    public function addEvent($data) {
      $query = $this->db->insert('class_calendar', $data);
      if ($query){
        return true; }
      else{
        return false;
      }
    }
    public function updateEvent($id,$class_id,$data) {
      $this->db->where('calendar_id', $id);
      $this->db->where('class_id', $class_id);
      $query = $this->db->update('class_calendar', $data);
      if ($this->db->affected_rows()){
        return true;
      }
      else {
        return false;
      }
    }
    public function deleteEvent($id) {
      $this->db->where('calendar_id',$id);
      $query = $this->db->delete('class_calendar');
      if ($this->db->affected_rows()){
        return true;
      }
      else {
        return false;
      }
    }
}

?>