<?php
class Consultation_model extends CI_Model {

	public $consultation_id;
  public $class_id;
  public $instructor_id;
  public $consultation_date;
  public $consultation_topic;
  public $consultation_content;



  //public $school_id;
  //private $password;
  //public $modified_at;

   	public function __construct(){
    	parent::__construct();
   	}

   	public function getAllConsultation() {
   		$this->db->select('*');
   		$this->db->from('consultation');
      $this->db->join('class', 'consultation.class_id = class.class_id','left');
      $this->db->join('course', 'class.course_id = course.course_id','left');
   		$this->db->join('consultees', 'consultees.consultation_id = consultation.consultation_id', 'left');
   		$this->db->group_by('consultation.consultation_id');
   		$query = $this->db->get(); 
    	if($query->num_rows() != 0) {
      		return $query->result_array();
    	}
    	else {
      		return false;
    	}
   	}


    public function getAllConsultationByInstructor($instructor_id) {
      $this->db->select('*');
      $this->db->from('consultation');
      $this->db->join('class', 'consultation.class_id = class.class_id','left');
      $this->db->join('course', 'class.course_id = course.course_id','left');
      $this->db->where('consultation.instructor_id', $instructor_id);
      $query = $this->db->get(); 
      if($query->num_rows() != 0) {
          return $query->result_array();
      }
      else {
          return false;
      }
    }

    public function getAllConsultationByStudent() {
      $this->db->select('*');
      $this->db->from('consultees');
      $this->db->join('consultation', 'consultation.consultation_id = consultees.consultation_id');
      $this->db->join('class', 'consultation.class_id = class.class_id','left');
      $this->db->join('course', 'class.course_id = course.course_id','left');
      $this->db->where(array('consultees.student_id' => $this->session->userdata('school_id')));
      $query = $this->db->get(); 
      if($query->num_rows() != 0) {
          return $query->result_array();
      }
      else {
          return false;
      }
    }


   	public function getRow($id) {
   		$this->db->select('consultation_topic');
   		$this->db->from('consultation');
   		$this->db->where('consultation_id ='.$id);
   		$query = $this->db->get();
   		if($query->num_rows() != 0) {
      		return $query->result_array();
    	}
    	else {
      		return false;
    	}
   	}


public function addNewConsultation(){

  $query = $this->db->insert('consultation', $this);
  
  if ($query)
  {
    return $this->db->insert_id();// return latest consultation_id
  }
  
  else{
    return false;
  }
}


  function addConsultees($id)
  {      
    $data =array();
    
    for($i=0; $i < count($_POST['consultees']); $i++)
    {
      $data[$i] = array(
           'student_id' => $_POST['consultees'][$i], 
           'consultation_id' => $id
           );
      }

    $query = $this->db->insert_batch('consultees', $data);
    return $query;

  }


public function getConsultees($x){
  $this->db->select('student.school_id, student.firstname, student.lastname, consultees.consultation_id');
  $this->db->from('consultees');
  $this->db->join('student', 'student.school_id = consultees.student_id');
  $this->db->where('consultation_id', $x);
  $query = $this->db->get(); 
      if($query->num_rows() != 0) {
          return $query->result_array();
      }
      else {
          return false;
      }
} 

  public function deleteConsultees($id) {

    $this->db->where('consultation_id', $id);
    $this->db->delete('consultees');

  }

  public function deleteConsultation($id) {
    $this->db->where('consultation_id', $id);
    $this->db->delete('consultation');
  }

  public function searchClass() {
    $findthis = strip_tags(trim($_GET['q'])); 
    $this->data['recipients'] = $this->User_model->findUser($findthis,$this->data['school_id']);

    if($this->data['recipients']){
      foreach ($this->data['recipients'] as $key => $value) {
        $name = $value['firstname']." ".$value['lastname'];
        $data[] = array('id' => $value['school_id'], 'text' => $name);        
      } 
    } else {
       $data[] = array('id' => '0', 'text' => 'No user found.');
    }

    echo json_encode($data);
  }
  public function getInstructorID($school_id) {
    $this->db->select('instructor_id');
    $this->db->from('instructor');
    $this->db->where('school_id',$school_id);
    $query = $this->db->get();
    $ret = $query->row();
    if($ret){
      return $ret->instructor_id;
    } else {
      return false;
    }
  }

  public function getInstructorSchoolID($id) {
    $this->db->select('school_id');
    $this->db->from('instructor');
    $this->db->where('instructor_id',$id);
    $query = $this->db->get();
    $ret = $query->row();
    if($ret){
      return $ret->school_id;
    } else {
      return false;
    }
  }

  public function getConsultationClasses($id){ // instructor. get all handled classes
    $this->db->select('*');
    $this->db->from('class'); 
    $this->db->join('course', 'course.course_id = class.course_id', 'left');
    $this->db->join('instructor', 'instructor.school_id = class.instructor_id', 'left');
    $this->db->where('class.active','1');
    $this->db->where('instructor.school_id',$id);
    $this->db->order_by('course.course_code','asc');         
    $query = $this->db->get(); 
    if($query->num_rows() != 0) {
      return $query->result_array();
    } else {
      return false;
    }
  }

}
?>