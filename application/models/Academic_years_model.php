<?php
class Academic_years_model extends CI_Model {

	public $academic_year_id;
	public $academic_year;
	public $start_date;
	public $end_date;

	public function loadAllAcademicYears()
	{
		$this->db->order_by('academic_year_id', 'desc');
		$query = $this->db->get('academic_year');
		return $query->result();
	}

	public function getCurrentAcademicYear(){
		date_default_timezone_set("Asia/Manila");


		$this->db->select('academic_year_id');
		$this->db->from('academic_year');
		$this->db->where('start_date <=', mdate('%Y-%m-%d'));
		$this->db->where('end_date >=', mdate('%Y-%m-%d'));
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			foreach ($query->result_array() as $row)
				{
				   return $row['academic_year_id'];
				   
				}

		}

		else
		{
			return false;
		}

	}

	public function addAcademicYear()
	{
		$query = $this->db->insert('academic_year', $this);
		return $query;
	}

	public function checkIfAcademicYearExists($academic_year){
		$this->db->where('academic_year', $academic_year);
		$query = $this->db->get('academic_year');

		  if ($query->num_rows() == 1)
		  {
		    return true;
		  }

		  else
		  { 
		    return false;
		  }
	}

	public function updateAcademicYear()
	{
		$this->db->where('academic_year_id', $this->academic_year_id);
		$query=$this->db->update('academic_year',$this);
		return $query;

	}

	public function deleteAcademicYear()
	{

		$query = $this->db->delete('academic_year',array('academic_year_id'=>$this->academic_year_id));
		return $query;
	}

	public function getOneAcademicYear()
	{
		$query = $this->db->get_where('academic_year',array('academic_year_id' => $this->academic_year_id));
		return $query->row_array();
	}

	public function getAcademicYear()
	{
		$query = $this->db->get_where('academic_year',array('	academic_year_id' => $this->academic_year_id));
		return $query->result();
	}


} //End