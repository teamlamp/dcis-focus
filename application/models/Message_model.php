<?php
class Message_model extends CI_Model {

  public function setRecipient($recipient){
    $this->db->query("INSERT INTO message_map (msgmap_recipient_id) VALUES ('".$recipient."')"); 
    return $this->db->insert_id();
  }

  public function setMessageActual($msgmap_id,$sender,$subject,$content){
    $this->db->query("INSERT INTO message_actual (msg_map_id,msg_author_id,msg_subject,msg_body) 
                      VALUES ('".$msgmap_id."','".$sender."','".$subject."','".$content."')");    
  }

  public function setRecipientDraft($recipient){
    $this->db->query("INSERT INTO message_map (msgmap_recipient_id,msgmap_type_r) VALUES ('".$recipient."','draft')"); 
    return $this->db->insert_id();
  }

  public function setMessageActualDraft($msgmap_id,$sender,$subject,$content,$msg_group){
    $this->db->query("INSERT INTO message_actual (msg_map_id,msg_author_id,msg_subject,msg_body,msg_type_s,msg_group) 
                      VALUES ('".$msgmap_id."','".$sender."','".$subject."','".$content."','draft','".$msg_group."')");
  }

  public function getInboxMessages($school_id){
    $query = $this->db->query("SELECT u.school_id, ur.firstname, ur.lastname, mp.*,ma.* FROM user u
                              INNER JOIN  (   
                                  SELECT 'admin' as user_role, school_id, firstname, lastname FROM admin
                                  UNION ALL
                                  SELECT 'staff' as user_role, school_id, firstname, lastname FROM staff
                                  UNION ALL
                                  SELECT 'instructor' as user_role, school_id, firstname, lastname FROM instructor
                                  UNION ALL
                                  SELECT   'student' as user_role, school_id, firstname, lastname FROM student   
                              ) ur
                              ON ur.school_id = u.school_id
                              INNER JOIN message_actual ma ON ma.msg_author_id = u.school_id
                              INNER JOIN message_map mp ON mp.msgmap_id = ma.msg_map_id
                              WHERE mp.msgmap_recipient_id = '".$school_id."' AND mp.msgmap_type_r = 'inbox' AND msgmap_active = '1'
                              ORDER BY mp.msgmap_id DESC");
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function getSentboxMessages($school_id){
    $query = $this->db->query("SELECT u.school_id, ur.firstname, ur.lastname, mp.*,ma.* FROM user u
                              INNER JOIN  (   
                                  SELECT 'admin' as user_role, school_id, firstname, lastname FROM admin
                                  UNION ALL
                                  SELECT 'staff' as user_role, school_id, firstname, lastname FROM staff
                                  UNION ALL
                                  SELECT 'instructor' as user_role, school_id, firstname, lastname FROM instructor
                                  UNION ALL
                                  SELECT   'student' as user_role, school_id, firstname, lastname FROM student   
                              ) ur
                              ON ur.school_id = u.school_id
                              INNER JOIN message_map mp ON mp.msgmap_recipient_id = u.school_id
                              INNER JOIN message_actual ma ON ma.msg_map_id = mp.msgmap_id
                              WHERE ma.msg_author_id = '".$school_id."' AND ma.msg_type_s = 'sent' AND msg_active = '1'
                              ORDER BY ma.msg_id DESC");
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function getDraftMessages($school_id){
    $query = $this->db->query("SELECT u.school_id, ur.firstname, ur.lastname, mp.*,ma.* FROM user u
                              INNER JOIN  (   
                                  SELECT 'admin' as user_role, school_id, firstname, lastname FROM admin
                                  UNION ALL
                                  SELECT 'staff' as user_role, school_id, firstname, lastname FROM staff
                                  UNION ALL
                                  SELECT 'instructor' as user_role, school_id, firstname, lastname FROM instructor
                                  UNION ALL
                                  SELECT   'student' as user_role, school_id, firstname, lastname FROM student   
                              ) ur
                              ON ur.school_id = u.school_id
                              INNER JOIN message_map mp ON mp.msgmap_recipient_id  = u.school_id
                              INNER JOIN message_actual ma ON ma.msg_id = mp.msgmap_id
                              WHERE ma.msg_author_id = '".$school_id."' AND ma.msg_type_s = 'draft'
                              ORDER BY ma.msg_id DESC");
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function getTrashMessages($school_id){
    $query = $this->db->query("SELECT u.school_id, ur.firstname, ur.lastname, mp.*,ma.* FROM user u
                              INNER JOIN  (   
                                  SELECT 'admin' as user_role, school_id, firstname, lastname FROM admin
                                  UNION ALL
                                  SELECT 'staff' as user_role, school_id, firstname, lastname FROM staff
                                  UNION ALL
                                  SELECT 'instructor' as user_role, school_id, firstname, lastname FROM instructor
                                  UNION ALL
                                  SELECT   'student' as user_role, school_id, firstname, lastname FROM student   
                              ) ur
                              ON ur.school_id = u.school_id
                              INNER JOIN message_actual ma ON ma.msg_author_id = u.school_id
                              INNER JOIN message_map mp ON mp.msgmap_id = ma.msg_id
                              WHERE CASE 
                                WHEN mp.msgmap_recipient_id = '".$school_id."' THEN mp.msgmap_type_r = 'trash' AND msgmap_active = '1'
                                WHEN ma.msg_author_id = '".$school_id."' THEN ma.msg_type_s = 'trash' AND msg_active = '1'
                              END
                              ORDER BY ma.msg_id DESC");
    if($query->num_rows() != 0) {
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function getOneMessage($school_id,$id){
    $query = $this->db->query("SELECT * FROM message_actual ma 
                              INNER JOIN message_map mp ON mp.msgmap_id = ma.msg_map_id
                              WHERE CASE 
                                WHEN ma.msg_author_id = '".$school_id."' THEN ma.msg_active = '1'
                                WHEN mp.msgmap_recipient_id = '".$school_id."' THEN mp.msgmap_active = '1'
                              END
                              AND ma.msg_map_id = '".$id."'");
    if($query->num_rows() != 0){
      return $query->row_array();
    } else { 
      return false;
    }
  }

  public function messageIsRead($school_id,$id){
    $query = $this->db->query("UPDATE message_map SET msgmap_isRead = '1' WHERE msgmap_recipient_id = '".$school_id."' AND msgmap_id = '".$id."'");
  }

  public function deleteMessage($data,$col_name,$tbl_name,$m_type){
    $this->db->query("UPDATE ".$tbl_name." SET ".$col_name." = 'trash' WHERE ".$m_type." = '".$data."' ");    
  }

  public function deleteMessageForever($data,$col_name,$tbl_name,$m_type){
    $this->db->query("UPDATE ".$tbl_name." SET ".$col_name." = '0' WHERE ".$m_type." = '".$data."' ");    
  }

  public function getMessageGroup($school_id,$id){
    $query = $this->db->query("SELECT msg_group as msg_group FROM message_actual WHERE msg_author_id = '".$school_id."' AND msg_map_id = '".$id."'");
    if($query->num_rows() != 0){
      return $query->row_array();
    } else { 
      return false;
    }
  }

  public function getDraftRecipients($msg_group){
    $query = $this->db->query("SELECT u.school_id, ur.firstname, ur.lastname FROM user u
                              INNER JOIN  (   
                                  SELECT 'admin' as user_role, school_id, firstname, lastname FROM admin
                                  UNION ALL
                                  SELECT 'staff' as user_role, school_id, firstname, lastname FROM staff
                                  UNION ALL
                                  SELECT 'instructor' as user_role, school_id, firstname, lastname FROM instructor
                                  UNION ALL
                                  SELECT   'student' as user_role, school_id, firstname, lastname FROM student   
                              ) ur
                              ON ur.school_id = u.school_id
                              INNER JOIN message_map mp ON mp.msgmap_recipient_id = u.school_id
                              INNER JOIN message_actual ma ON ma.msg_map_id = mp.msgmap_id
                              WHERE ma.msg_group = '".$msg_group."' AND mp.msgmap_type_r = 'draft' AND ma.msg_type_s = 'draft'");
    if($query->num_rows() != 0){
      return $query->result_array();
    } else { 
      return false;
    }
  }

  public function getSubjectByGroup($msg_group){
    $query = $this->db->query("SELECT msg_subject from message_actual WHERE msg_group = '".$msg_group."'");
    if($query->num_rows() != 0){
      return $query->row_array();
    } else { 
      return false;
    }
  }

  public function getContentByGroup($msg_group){
    $query = $this->db->query("SELECT   msg_body from message_actual WHERE msg_group = '".$msg_group."'");
    if($query->num_rows() != 0){
      return $query->row_array();
    } else { 
      return false;
    }
  }

  public function setInboxtoDraft_r($school_id,$msg_group){
    $this->db->query("UPDATE message_map mp 
                      INNER JOIN message_actual ma ON ma.msg_map_id = mp.msgmap_id SET msgmap_type_r = 'inbox' 
                      WHERE ma.msg_group = '".$msg_group."' AND mp.msgmap_recipient_id = '".$school_id."'");
  }

  public function setSenttoDraft_s($school_id,$subject,$body,$recipient){
    $this->db->query("UPDATE message_actual as ma INNER JOIN message_map as mp ON mp.msgmap_id = ma.msg_map_id
                    SET ma.msg_type_s = 'sent', msg_subject = '".$subject."', msg_body = '".$body."'
                    WHERE ma.msg_author_id = '".$school_id."' AND mp.msgmap_recipient_id = '".$recipient."'");
  }

  public function countUnread($school_id){
    $query = $this->db->query("SELECT COUNT(*) as unreadcount FROM message_map WHERE msgmap_recipient_id = '".$school_id."' AND msgmap_active ='1' AND msgmap_isRead = '0' AND msgmap_type_r = 'inbox'");
    if($query->num_rows() != 0){
      return $query->row_array();
    } else { 
      return false;
    }
  }

// DO NOT DELETE THIS
/*
  public function getOneMessage($school_id,$id){
  $query = $this->db->query("SELECT u.school_id, ur.firstname, ur.lastname, pm.* FROM user u
                              INNER JOIN  (   
                                  SELECT 'admin' as user_role, school_id, firstname, lastname FROM admin
                                  UNION ALL
                                  SELECT 'staff' as user_role, school_id, firstname, lastname FROM staff
                                  UNION ALL
                                  SELECT 'instructor' as user_role, school_id, firstname, lastname FROM instructor
                                  UNION ALL
                                  SELECT   'student' as user_role, school_id, firstname, lastname FROM student   
                              ) ur
                              ON ur.school_id = u.school_id
                              INNER JOIN private_message pm ON pm.pm_sender = u.school_id OR pm.pm_recipient = u.school_id
                              WHERE (pm.pm_sender = '".$school_id."' OR pm.pm_recipient = '".$school_id."') AND pm.active = '1' AND pm.pm_id = '".$id."'");
  if($query->num_rows() != 0){
    return $query->result_array();
  } else { 
    return false;
  }
}
*/
}