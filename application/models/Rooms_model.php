<?php
class Rooms_model extends CI_Model {

   public function __construct(){
        parent::__construct();
   }

    public function getAllRooms(){
    $query = $this->db->get('rooms');
    return $query->result();
   }

}

