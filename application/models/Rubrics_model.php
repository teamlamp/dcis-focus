<?php
class Rubrics_model extends CI_Model {
  
  public function __construct(){
    parent::__construct();
  }

  public function getRubrics($id){
    $query = $this->db->query('SELECT gr.type, gr.percent,gr.formula_id FROM grade_rubrics gr WHERE gr.class_id = '.$id.' AND gr.active = "1" ORDER BY gr.formula_id ASC');
    if($query->num_rows() != 0){
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function saveRubrics($data){
    $this->db->insert_batch('grade_rubrics', $data);
  }

  public function deleteRubrics($data){
    $this->db->set('active', '0');
    $this->db->where_in('formula_id',$data);
    $this->db->update('grade_rubrics');
  }

  public function updateRubrics($data){
    $this->db->update_batch('grade_rubrics', $data, 'formula_id');
  }

  // For Class Record Purposes
  public function getActivities($id){
    $this->db->select('activity_title, grade_history_id, perfect_score, grade_history.formula_id');
    $this->db->from('grade_history');
    $this->db->join('grade_rubrics','grade_rubrics.formula_id = grade_history.formula_id');
    $this->db->where('grade_history.class_id', $id);
    $this->db->where('grade_history.perfect_score !=','0');
    $this->db->order_by('grade_history.formula_id','ASC');
    $this->db->order_by('grade_history.grade_history_id','ASC');
    $query = $this->db->get(); 
    if($query->num_rows() != 0){
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function classRecordrubrics($id){
    $query = $this->db->query('SELECT gr.type, gr.percent,gr.formula_id, COUNT(gh.activity_title) as countActivity 
                              FROM grade_rubrics gr 
                              INNER JOIN grade_history gh ON gh.formula_id = gr.formula_id WHERE gr.class_id = '.$id.' AND gh.perfect_score != "0" 
                              GROUP BY gh.formula_id ORDER BY gr.formula_id ASC');
    if($query->num_rows() != 0){
      return $query->result_array();
    }else{
      return false;
    }
  }

  public function addRubricsTotal(){
    $query = $this->db->query('SELECT gr.class_id, SUM(gh.perfect_score*(gr.percent/100)) as rubtot FROM grade_history gh 
                               INNER JOIN grade_rubrics gr ON gh.formula_id = gr.formula_id
                               GROUP BY gr.class_id');
    if($query->num_rows() != 0){
       return $query->result_array();
    }else{
       return false;
    }
  }

  public function storeRubTot($class_id, $rubtot){
    $query = $this->db->query('UPDATE class SET class_grade_sf = '.$rubtot.' WHERE class_id = '.$class_id.'');
  }

  public function getRubTot($id){
    $query = $this->db->query('SELECT class_grade_sf FROM class WHERE class_id = '.$id.'');
    if($query->num_rows() != 0){
       return $query->result_array();
    }else{
       return false;
    }
  }

  public function getRubDetails($id){
    $query = $this->db->query('SELECT * FROM grade_rubrics WHERE formula_id = '.$id.'');
    if($query->num_rows() != 0){
       return $query->result_array();
    }else{
       return false;
    }
  }

  public function updateSingleRH($data){
    extract($data);
    $this->db->where('grade_history_id', $grade_history_id);
    $this->db->update('grade_history', array('activity_title' => $activity_title));    
  }

  public function updateSingleTS($data){
    extract($data);
    $this->db->where('grade_history_id', $grade_history_id);
    $this->db->update('grade_history', array('perfect_score' => $perfect_score)); 
  }

  public function updateAllTS($y, $z){
    $this->db->query("UPDATE grade SET score='".$y."',total='".$y."' WHERE history_id='".$z."'");
  }

  public function updateTSOnly($y, $z){
    $this->db->query("UPDATE grade SET total='".$y."' WHERE history_id='".$z."'");
  }

  public function checkIfRubricHasGrades($formula_id){
    $query = $this->db->query("SELECT * FROM grade_history WHERE formula_id = '".$formula_id."'");
    if($query->num_rows() != 0){
       return true;
    }else{
       return false;
    }
  }



}