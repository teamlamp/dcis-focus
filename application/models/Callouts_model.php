<?php
class callouts_model extends CI_Model {

   public $school_id;
   private $password;
   public $modified_at;

   public function __construct(){
        parent::__construct();
   }
  
  public function getCallOuts($course_code, $group_number){
    $this->db->select('*');
    $this->db->from('callout');
    $this->db->join('class', 'class.class_id=callout.class_id', 'left'); 
    $this->db->join('course', 'course.course_id=class.course_id', 'left');
    $this->db->join('rooms', 'rooms.room_id=class.room_id', 'left');
    $this->db->where('course.course_code', $course_code); 
    $this->db->where('class.group_number', $group_number);  
    $this->db->where('callout.active','1');
    $this->db->order_by('callout.modified_at', 'desc');     
    $query = $this->db->get(); 
    if($query->num_rows() != 0)
    {
      return $query->result_array();
    }
    else
    {
      return false;
    }
  }

  public function saveCallOut($data){
    $this->db->insert('callout', $data);
    return $this->db->insert_id();
  }

  public function updatecallout($a,$b,$c,$d){
    $this->db->query("UPDATE callout SET type='".$b."', title='".$c."', content='".$d."' WHERE callout_id ='".$a."'");
    $e =  $this->db->last_query();
    echo $e;
  }
}

