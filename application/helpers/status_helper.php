<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{

  function checkAttendanceStatus($id, $class_id)
  {
    $ci = get_instance();
    $ci->load->database();

    $ci->db->select('*');
    $ci->db->from('class_member');
    $ci->db->where('student_id', $id);
    $ci->db->where('class_id', $class_id);
    $query = $ci->db->get();

    foreach($query->result() as $row)
    {
      $c_id = $row->class_member_id;
      $status = $row->status_attendance;
      $override = $row->is_overridden;
    }

    if($status === "Readmission")
    {
      $data = 1;
    }

    elseif ($status === "Warning")
    {
      //warning na siya
      $data = 2;
    }

    elseif($status === "Dropped")
    {
      $data = 3;
    }

    else
    {
      $data = 0;
    }
      return $data;
  }
}