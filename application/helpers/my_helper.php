<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function getClassSchedule($id)
    {
    	$ci = get_instance();
        //$id = $ci->session->userdata('user_id');

		
		$array = array('class_id' => $id);
		$ci->load->database();
		$query = $ci->db->select('day, start_time, end_time')
			->from('class_schedule')
				->where($array);
			
		$ret = $query->get();

	return $ret;
    }	
}