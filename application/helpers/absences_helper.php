<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
function countAbsences($id, $class_id)
{
$ci = get_instance();
$ci->load->database();

$ci->db->select('*');
$ci->db->from('class_member');
$ci->db->where('student_id', $id);
$ci->db->where('class_id', $class_id);
$query = $ci->db->get();

foreach($query->result() as $row){
  $c_id = $row->class_member_id;
}
  $ci->db->select('*');
  $ci->db->from('attendance');
  $ci->db->where('type', "Absent");
  $ci->db->where('class_member_id', $c_id);
  $query2 = $ci->db->get();

  $a = $query2->num_rows();

  $ci->db->select('*');
  $ci->db->from('attendance');
  $ci->db->where('type', "Late");
  $ci->db->where('class_member_id', $c_id);
  $query3 = $ci->db->get();

  $l = $query3->num_rows() / 3;
  $r = number_format($l);
  $ret = $a + $r;
  return $ret;
}
}