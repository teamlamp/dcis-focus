<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{

  function updateAttendanceStatus($id, $class_id, $abs, $max)
  {
 
    $ci = get_instance();
    $ci->load->database();

    $ci->db->select('*');
    $ci->db->from('class_member');
    $ci->db->where('student_id', $id);
    $ci->db->where('class_id', $class_id);
    $query = $ci->db->get();

    foreach($query->result() as $row)
    {
      $c_id = $row->class_member_id;
      $status = $row->status_attendance;
      $override = $row->is_overridden;
    }

    if($abs >= 3)
    {
      if($status != "Readmission")
      {
        if($override != 1)
        {          
          $data_up = array('status_attendance' => 'Readmission');
          $ci->db->where('class_member_id', $c_id);
          $ci->db->update('class_member', $data_up);
        }
        else
        {
          $data_up = array('status_attendance' => 'Warning');
          $ci->db->where('class_member_id', $c_id);
          $ci->db->update('class_member', $data_up);
        }
      }
    }

    elseif($max <= $abs)
    {
      $data_up = array('status_attendance' => 'Dropped');
      $ci->db->where('class_member_id', $c_id);
      $ci->db->update('class_member', $data_up);
    }

    else{
      $data_up = array('status_attendance' => 'Okay');
      $ci->db->where('class_member_id', $c_id);
      $ci->db->update('class_member', $data_up);
    }
  }
}