<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Error</title>
<!--<div style="padding-top:15%">  
  <img src="/../assets/error/error-404.jpg">
</div>-->

<style type="text/css">
@keyframes rotate360 {
  to { transform: rotate(360deg); }
}
#middle{
  left: -18%; /* Begin off-screen */
	position:fixed;
	animation: 50s rotate360 infinite linear;
}
</style>

<html lang="en">
<!-- BEGIN HEAD -->
<head>
<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="/../assets/admin/pages/css/error.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE STYLES -->
<link href="/../assets/admin/pages/css/blog.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md page-404-3" >
<div class="page-inner" id="earth">
	<a href="<?= base_url() ?>"><img src="/../assets/admin/pages/media/pages/earth.jpg" class="img-responsive" alt=""></a>
</div>
<div class="container error-404">
	<h1>404</h1>
	<h2>Houston, we have a problem.</h2>
	<h3>Matt Damon is lost again.</h3>
	<h5>Click earth to send him home.</h5>
</div>
<div class="container error-404">
	<div id="middle">
		<img src="/../assets/error/astronaut.png">
	</div>
</div>

<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<?php require_once 'application/views/includes/core_js.phtml';?> 
<!-- END CORE PLUGINS -->
	<?php require_once 'application/views/includes/theme_js.phtml';?> 
<script type="text/javascript">
// For astronaut animation
$(document).ready(function(){
	var randomtop;
	var astro = $('#middle');
	var width = $(document).width();
	var nwidth = width + (width * .25); // to make to disappear off screen
  function moveLeft() {
  	$(astro).css("top", "");
  	$(astro).css("top", "0");
    $(astro).animate({left: "-="+nwidth+""}, 10000, moveRight);
	}
	function moveRight() {
		$(astro).css("top", "");
		$(astro).css("top", "50");
	  $(astro).animate({left: "+="+nwidth+""}, 10000, moveLeft);
	}
	moveRight();

	function genRanTop(){
		var randomtop = Math.floor(Math.random() * (60 - 1 + 1)) + 1;
		console.log(randomtop);
		return randomtop;
	}

});	

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

</html>