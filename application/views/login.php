<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>DCIS | Login</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="/../assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
<link href="/../assets/admin/pages/css/login-soft.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md login">

<!-- BEGIN LOGO -->
<div class="logo">
	<a href="index.html">
	<img src="/../assets/admin/layout2/img/dcislogo2.png" alt="" style="width: 185px; height: 90px;"/>
	</a>
</div>
<!-- END LOGO -->

<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->

<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form class="login-form" action="<?= base_url('login') ?>" method="post">
		<h3 class="form-title">Login to your account</h3>
		
<?php if (isset($message)) {
foreach ($message as $prompt) { ?>
<div class="alert alert-danger display">
<button class="close" data-close="alert"></button>
<h4><?= $prompt->header ?></h4>
    <span><?= $prompt->message ?></span>
</div> 
<?php } }?>

		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Student ID</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="ID Number" name="school_id" autofocus="on" required/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
					<div class="input-icon right">
						<i class="fa fa-eye" id="password-eye" style="cursor:pointer!important;color:grey;"></i>
					</div>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="password" required/>
			</div>
		</div>
		<div class="form-actions">
			<label class="checkbox">
			<input type="checkbox" name="remember" value="1"/> Remember me </label>
			<button type="submit" name="submit" class="btn blue pull-right" id="user-login">
			Login <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
		<div class="login-options"> 
			<h4>Or you can also ...</h4>
			<!-- <div style="border-bottom: 1px dashed #eee"></div> -->
			<a class="btn green-meadow btn-block" href="http://ismis.usc.edu.ph" style="margin-top: 10px">
			Login to ISMIS <i class="fa fa-sign-in"></i>
			</a>
			<!-- <ul class="social-icons">
				<li>
					<a class="btn green" data-original-title="facebook" href="javascript:;">Login to ISMIS
					</a>
				</li>
				<li>
					<a class="facebook" data-original-title="facebook" href="javascript:;">
					</a>
				</li>
				<li>
					<a class="twitter" data-original-title="Twitter" href="javascript:;">
					</a>
				</li>
				<li>
					<a class="googleplus" data-original-title="Goole Plus" href="javascript:;">
					</a>
				</li>
				<li>
					<a class="linkedin" data-original-title="Linkedin" href="javascript:;">
					</a>
				</li>
			</ul> -->
		</div>
		<div class="forget-password" style="border-top: 1px dotted #eee">
<!-- 			<h4>Forgot your password ?</h4>
			<p>
				 no worries, click <a href="javascript:;" id="forget-password">
				here </a>
				to reset your password.
			</p> -->
			<p style="padding-top: 15px;"><a href="javascript:;" id="forget-password" class="font-grey-cararra"> I forgot my password </a></p>
		</div>
	</form>
	<!-- END LOGIN FORM -->

	<!-- BEGIN FORGOT PASSWORD FORM -->
	<form class="forget-form" action="index.html" method="post">
		<h3>Forget Password ?</h3>
		<p>
			 Enter your e-mail address below to reset your password.
		</p>
		<div class="form-group">
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
			</div>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn">
			<i class="m-icon-swapleft"></i> Back </button>
			<button type="submit" class="btn blue pull-right">
			Submit <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
	</form>
	<!-- END FORGOT PASSWORD FORM -->
</div>
<!-- END LOGIN -->

<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 2015 &copy; Team L.A.M.P - DCIS || Focus.
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/../assets/global/plugins/respond.min.js"></script>
<script src="/../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="/../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="/../assets/dcis/js/dcis.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/../assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="/../assets/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="/../assets/admin/pages/scripts/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
  Metronic.init(); // init metronic core components
Layout.init(); // init current layout
  Login.init();
  Demo.init();
       // init background slide images
       $.backstretch([
        "/../assets/admin/pages/media/bg/loginBG_3.jpg",
        "/../assets/admin/pages/media/bg/loginBG_1.jpg",
        "/../assets/admin/pages/media/bg/loginBG_4.jpg",
        "/../assets/admin/pages/media/bg/loginBG_5.jpg",
        "/../assets/admin/pages/media/bg/loginBG_7.jpg",
        "/../assets/admin/pages/media/bg/loginBG_8.jpg"
        ], {
          fade: 1000,
          duration: 8000
    }
    );
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>