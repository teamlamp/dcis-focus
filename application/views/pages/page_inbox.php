<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>DCIS | Pages - Messages</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet"/>
<!-- BEGIN:File Upload Plugin CSS files-->
<link href="../../assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet"/>
<link href="../../assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet"/>
<link href="../../assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
<!-- END:File Upload Plugin CSS files-->
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../assets/admin/pages/css/inbox.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<link href="../../assets/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="../../assets/global/css/plugins-md.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/pages/css/todo.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout2/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="../../assets/admin/layout2/css/themes/grey.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/layout2/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo ">

<!-- BEGIN HEADER -->
<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix">
</div>
<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN STYLE CUSTOMIZER -->
				<div class="theme-panel">
					<div class="toggler tooltips" data-container="body" data-placement="left" data-html="true" data-original-title="Click to open advance theme customizer panel">
						<i class="icon-settings"></i>
					</div>
					<div class="toggler-close">
						<i class="icon-close"></i>
					</div>
					<div class="theme-options">
						<div class="theme-option theme-colors clearfix">
							<span>
							THEME COLOR </span>
							<ul>
								<li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default">
								</li>
								<li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey">
								</li>
								<li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue">
								</li>
								<li class="color-dark tooltips" data-style="dark" data-container="body" data-original-title="Dark">
								</li>
								<li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light">
								</li>
							</ul>
						</div>
						<div class="theme-option">
							<span>
							Theme Style </span>
							<select class="layout-style-option form-control input-small">
								<option value="square" selected="selected">Square corners</option>
								<option value="rounded">Rounded corners</option>
							</select>
						</div>
						<div class="theme-option">
							<span>
							Layout </span>
							<select class="layout-option form-control input-small">
								<option value="fluid" selected="selected">Fluid</option>
								<option value="boxed">Boxed</option>
							</select>
						</div>
						<div class="theme-option">
							<span>
							Header </span>
							<select class="page-header-option form-control input-small">
								<option value="fixed" selected="selected">Fixed</option>
								<option value="default">Default</option>
							</select>
						</div>
						<div class="theme-option">
							<span>
							Top Dropdown</span>
							<select class="page-header-top-dropdown-style-option form-control input-small">
								<option value="light" selected="selected">Light</option>
								<option value="dark">Dark</option>
							</select>
						</div>
						<div class="theme-option">
							<span>
							Sidebar Mode</span>
							<select class="sidebar-option form-control input-small">
								<option value="fixed">Fixed</option>
								<option value="default" selected="selected">Default</option>
							</select>
						</div>
						<div class="theme-option">
							<span>
							Sidebar Style</span>
							<select class="sidebar-style-option form-control input-small">
								<option value="default" selected="selected">Default</option>
								<option value="compact">Compact</option>
							</select>
						</div>
						<div class="theme-option">
							<span>
							Sidebar Menu </span>
							<select class="sidebar-menu-option form-control input-small">
								<option value="accordion" selected="selected">Accordion</option>
								<option value="hover">Hover</option>
							</select>
						</div>
						<div class="theme-option">
							<span>
							Sidebar Position </span>
							<select class="sidebar-pos-option form-control input-small">
								<option value="left" selected="selected">Left</option>
								<option value="right">Right</option>
							</select>
						</div>
						<div class="theme-option">
							<span>
							Footer </span>
							<select class="page-footer-option form-control input-small">
								<option value="fixed">Fixed</option>
								<option value="default" selected="selected">Default</option>
							</select>
						</div>
					</div>
				</div>
				<!-- END STYLE CUSTOMIZER -->
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Messages
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-archive"></i>
							<a href="index.html">Messages</a>
							<i class="fa fa-angle-right"></i>
						</li>
<!-- 						<li>
							<a href="#">Pages</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">New Timeline</a>
						</li> -->
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
		<div style="margin-bottom: 10px;">
		<a class="btn btn-sm green">
		<i class="fa fa-pencil"></i> Compose
		</a>
		<a class="btn btn-sm blue">
		Inbox
		</a>
		<a class="btn btn-sm blue">
		Sent
		</a>
		<a class="btn btn-sm blue">
		Draft
		</a>
		<a class="btn btn-sm blue">
		Trash
		</a>
		</div>
<table class="table table-striped table-advance table-hover">
<thead>
<tr>
	<th colspan="3">
		
	</th>
	<th class="pagination-control" colspan="3">
	<div style="float:right;">
		<span class="pagination-info">
		1-30 of 789 </span>
		<a class="btn btn-sm blue">
		<i class="fa fa-angle-left"></i>
		</a>
		<a class="btn btn-sm blue">
		<i class="fa fa-angle-right"></i>
		</a>
		<div class="btn-group" style="float: right;">
			<a class="btn btn-sm blue dropdown-toggle" href="javascript:;" data-toggle="dropdown">
			Options <i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu">
				<li>
					<a href="javascript:;">
					<i class="fa fa-pencil"></i> Mark as Read </a>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-ban"></i> Spam </a>
				</li>
				<li class="divider">
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-trash-o"></i> Delete </a>
				</li>
			</ul>
		</div>
	</th>
	</div>
</tr>
</thead>
<tbody>
<tr class="unread" data-messageid="1">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 Petronas IT
	</td>
	<td class="view-message ">
		 New server for datacenter needed
	</td>
	<td class="view-message inbox-small-cells">
		<i class="fa fa-paperclip"></i>
	</td>
	<td class="view-message text-right">
		 16:30 PM
	</td>
</tr>
<tr class="unread" data-messageid="2">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 Daniel Wong
	</td>
	<td class="view-message">
		 Please help us on customization of new secure server
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 15
	</td>
</tr>
<tr data-messageid="3">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 John Doe
	</td>
	<td class="view-message">
		 Lorem ipsum dolor sit amet
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 15
	</td>
</tr>
<tr data-messageid="4">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 Facebook
	</td>
	<td class="view-message">
		 Dolor sit amet, consectetuer adipiscing
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 14
	</td>
</tr>
<tr data-messageid="5">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star inbox-started"></i>
	</td>
	<td class="view-message hidden-xs">
		 John Doe
	</td>
	<td class="view-message">
		 Lorem ipsum dolor sit amet
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 15
	</td>
</tr>
<tr data-messageid="6">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star inbox-started"></i>
	</td>
	<td class="view-message hidden-xs">
		 Facebook
	</td>
	<td class="view-message">
		 Dolor sit amet, consectetuer adipiscing
	</td>
	<td class="view-message inbox-small-cells">
		<i class="fa fa-paperclip"></i>
	</td>
	<td class="view-message text-right">
		 March 14
	</td>
</tr>
<tr data-messageid="7">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star inbox-started"></i>
	</td>
	<td class="view-message hidden-xs">
		 John Doe
	</td>
	<td class="view-message">
		 Lorem ipsum dolor sit amet
	</td>
	<td class="view-message inbox-small-cells">
		<i class="fa fa-paperclip"></i>
	</td>
	<td class="view-message text-right">
		 March 15
	</td>
</tr>
<tr data-messageid="8">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 Facebook
	</td>
	<td class="view-message view-message">
		 Dolor sit amet, consectetuer adipiscing
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 14
	</td>
</tr>
<tr data-messageid="9">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 John Doe
	</td>
	<td class="view-message view-message">
		 Lorem ipsum dolor sit amet
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 15
	</td>
</tr>
<tr data-messageid="10">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 Facebook
	</td>
	<td class="view-message view-message">
		 Dolor sit amet, consectetuer adipiscing
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 14
	</td>
</tr>
<tr data-messageid="11">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star inbox-started"></i>
	</td>
	<td class="view-message hidden-xs">
		 John Doe
	</td>
	<td class="view-message">
		 Lorem ipsum dolor sit amet
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 15
	</td>
</tr>
<tr data-messageid="12">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star inbox-started"></i>
	</td>
	<td class="hidden-xs">
		 Facebook
	</td>
	<td class="view-message">
		 Dolor sit amet, consectetuer adipiscing
	</td>
	<td class="view-message inbox-small-cells">
		<i class="fa fa-paperclip"></i>
	</td>
	<td class="view-message text-right">
		 March 14
	</td>
</tr>
<tr data-messageid="13">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 John Doe
	</td>
	<td class="view-message">
		 Lorem ipsum dolor sit amet
	</td>
	<td class="view-message inbox-small-cells">
		<i class="fa fa-paperclip"></i>
	</td>
	<td class="view-message text-right">
		 March 15
	</td>
</tr>
<tr data-messageid="14">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="hidden-xs">
		 Facebook
	</td>
	<td class="view-message view-message">
		 Dolor sit amet, consectetuer adipiscing
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 14
	</td>
</tr>
<tr data-messageid="15">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 John Doe
	</td>
	<td class="view-message">
		 Lorem ipsum dolor sit amet
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 15
	</td>
</tr>
<tr data-messageid="16">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 Facebook
	</td>
	<td class="view-message">
		 Dolor sit amet, consectetuer adipiscing
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 14
	</td>
</tr>
<tr data-messageid="17">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star inbox-started"></i>
	</td>
	<td class="view-message hidden-xs">
		 John Doe
	</td>
	<td class="view-message">
		 Lorem ipsum dolor sit amet
	</td>
	<td class="view-message inbox-small-cells">
	</td>
	<td class="view-message text-right">
		 March 15
	</td>
</tr>
<tr data-messageid="18">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 Facebook
	</td>
	<td class="view-message view-message">
		 Dolor sit amet, consectetuer adipiscing
	</td>
	<td class="view-message inbox-small-cells">
		<i class="fa fa-paperclip"></i>
	</td>
	<td class="view-message text-right">
		 March 14
	</td>
</tr>
<tr data-messageid="19">
	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	<td class="inbox-small-cells">
		<i class="fa fa-star"></i>
	</td>
	<td class="view-message hidden-xs">
		 John Doe
	</td>
	<td class="view-message">
		 Lorem ipsum dolor sit amet
	</td>
	<td class="view-message inbox-small-cells">
		<i class="fa fa-paperclip"></i>
	</td>
	<td class="view-message text-right">
		 March 15
	</td>
</tr>
</tbody>
</table>

</div>
</div>


				<!-- END PAGE CONTENT-->

		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->
		<!--Cooming Soon...-->
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	
	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->

</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->

<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN: Page level plugins -->
<script src="../../assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<!-- BEGIN:File Upload Plugin JS files-->
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="../../assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="../../assets/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="../../assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="../../assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script src="../../assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="../../assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="../../assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="../../assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="../../assets/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="../../assets/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="../../assets/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="../../assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="../../assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
    <script src="../../assets/global/plugins/jquery-file-upload/js/cors/jquery.xdr-transport.js"></script>
    <![endif]-->
<!-- END:File Upload Plugin JS files-->
<!-- END: Page level plugins -->
<script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="../../assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<script src="../../assets/admin/pages/scripts/inbox.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
   Inbox.init();
});
</script>
<!-- END JAVASCRIPTS -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37564768-1', 'keenthemes.com');
  ga('send', 'pageview');
</script>
</body>

<!-- END BODY -->
</html>