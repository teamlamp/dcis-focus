
					<div class="col-md-5">
						<!-- BEGIN Portlet PORTLET-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-speech"></i>
									<span class="caption-subject bold uppercase"> Record</span>
									<span class="caption-helper">Latest Activity</span>
								</div>
								<div class="actions">
									<a href="javascript:;" class="btn btn-circle btn-default">
									<i class="fa fa-gears"></i> Options </a>
									<a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
								</div>
							</div>
							<div class="portlet-body">
									<form  action="/classes/saverecord" method="POST" class="form_post">
								<div class="item">
									<div class="item-head">
										<div class="item-details">
											<img class="item-pic" src="../../assets/admin/layout3/img/avatar3.jpg">
											<a href="" class="item-name primary-link">12104744</a>
											<span class="item-label"><input type="number" name="score" placeholder="Input grade.." class="form-control" required></span>
										</div>
											<span class="item-label"><input type="number" name="overall" placeholder="Input overall grade.." class="form-control" min="1" required></span>
									</div>
									<div class="item-body">
									<div class="form-group form-md-line-input form-md-floating-label">
										<input type="submit" class="btn btn-circle btn-sm green-haze" style="margin-top:15px;margin-bottom:10px;" value="Save" id="save_post">
									</div>	
								</div>
							</div>
									</form>
						</div>
						</div>
						<!-- END Portlet PORTLET-->
					</div>