<!-- BEGIN CHANGE AVATAR -->
<div class="tab-pane" id="tab_1_3">
<hr style="margin-bottom:15px!important;margin-top:0px!important">
	<form action="<?php echo base_url('upload/do_upload'); ?>" method="POST" enctype="multipart/form-data" class="form-horizontal" role="form">
		<div class="form-body">

		<div class="form-group">
			<label class="control-label col-md-3">Photo Upload</label>
			<div class="col-md-9">
				<div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-new thumbnail" style="width: 400px; height: 250px;">
						<img src="http://www.placehold.it/400x250/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 400px; max-height: 250px;">
					</div>
					<div>
						<span class="btn green btn-file">
						<span class="fileinput-new">
						Select image to upload</span>
						<span class="fileinput-exists">
						Change </span>
						<input type="file" name="userfile">
						</span>
						<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
						Remove </a>
					</div>
				</div>
			</div>
		</div>

		<!-- <div class="form-group">
			<label class="control-label col-md-3"></label>
			<div class="col-md-9">
				<div class="note note-info">
					<p>Please select an image file to upload as your profile picture.</p>
				</div>
			</div>
		</div> -->

		<hr style="margin:0px!important">
		<div class="form-actions btntoright">
			<button type="submit" name="upload" class="btn green-haze">Save Changes</button>
			<button type="button" class="btn default">Cancel</button>
		</div>
		</div>
	</form>
</div>
<!-- END CHANGE AVATAR -->