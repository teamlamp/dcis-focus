						<div class="profile-content">
							<div class="row">
								<div class="col-md-12">
									<div class="portlet light">
										<div class="portlet-title tabbable-line">
											<ul class="nav nav-tabs" style="float: left;">
												<li class="active">
													<a href="#tab_1_1" data-toggle="tab">Personal Info</a>
												</li>
												<li>
													<a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
												</li>
												<li>
													<a href="#tab_1_3" data-toggle="tab">Change Password</a>
												</li>
												<li>
													<a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
												</li>
											</ul>
										</div>
										<div class="portlet-body">
											<div class="tab-content">
												<!-- PERSONAL INFO TAB -->
												<div class="tab-pane active" id="tab_1_1">
													<form  class="form-horizontal" role="form">
														<div class="form-body">
															
															<div class="form-group">
																<label class="col-md-3 control-label">First Name</label>
																<div class="col-md-9">
																	<input type="text" class="form-control" placeholder="Joseph Vincent">
																	<span class="help-block">
																	Change your firstname. </span>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label">Last Name</label>
																<div class="col-md-9">
																	<input type="text" class="form-control" placeholder="Lao">
																</div>
															</div>

															<div class="form-group">
																<label class="control-label col-md-3">Birthdate</label>
																<div class="col-md-3">
																	<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value=""/>
																	<span class="help-block">
																	Select date. </span>
																</div>
															</div>


															<div class="form-group">
																<label class="col-md-3 control-label">Email</label>
																<div class="col-md-9">
																	<div class="input-group">
																		<span class="input-group-addon">
																		<i class="fa fa-envelope"></i>
																		</span>
																		<input type="email" class="form-control" placeholder="Email Address">
																	</div>
																</div>
															</div>


															<div class="form-group">
																<label class="col-md-3 control-label">Mobile No.</label>
																<div class="col-md-9">
																	<div class="input-group">
																		<span class="input-group-addon">
																		<i class="fa fa-phone"></i>
																		</span>
																	<input type="text" class="form-control" placeholder="+63 9123456789">
																	</div>
																	<span class="help-block">
																	Optional. </span>
																</div>
															</div>
					
														<div class="form-actions right">
															<button type="submit" class="btn green-haze">Save Changes</button>
															<button type="button" class="btn default">Cancel</button>
														</div>
														</div>
													</form>
												</div>
												<!-- END PERSONAL INFO TAB -->
												<!-- CHANGE AVATAR TAB -->
												<div class="tab-pane" id="tab_1_2">
													<form  class="form-horizontal" role="form">
														<div class="form-body">
															
															<div class="form-group">
															<div class="fileinput fileinput-new" data-provides="fileinput">
																<div class="fileinput-new thumbnail" style=" margin-left:20px;width: 200px; height: 220px;">
																	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
																</div>
																<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 220px;">
																</div>
																<div style="margin-left:20px; margin-top:10px">
																	<span class="btn yellow btn-file">
																	<span class="fileinput-new">
																	Select image </span>
																	<span class="fileinput-exists">
																	Change </span>
																	<input type="file" name="...">
																	</span>
																	<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
																	Remove </a>
																</div>
															</div>
															<div class="clearfix margin-top-10" style="margin-left:20px">
																<span class="label label-danger">NOTE! </span>
																<span>&nbsp;Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
															</div>
														</div>
					
														<div class="form-actions right">
															<button type="submit" class="btn green-haze">Save Changes</button>
															<button type="button" class="btn default">Cancel</button>
														</div>
														</div>
													</form>
												</div>
												<!-- END CHANGE AVATAR TAB -->
												<!-- CHANGE PASSWORD TAB -->
												<div class="tab-pane" id="tab_1_3">
													<form  class="form-horizontal" role="form">
														<div class="form-body">
															
															<div class="form-group">
																<label class="col-md-3 control-label">Password</label>
																<div class="col-md-9">
																	<input type="password" class="form-control" placeholder="Enter current password">
																	<span class="help-block">
																	Password currently used. </span>
																</div>
															</div>

															<div class="form-group last password-strength">
																<label class="col-md-3 control-label">New Password</label>
																<div class="col-md-9">
																	<div class="input-group">
																		<span class="input-group-addon">
																		<i class="fa fa-unlock"></i>
																		</span>
																		<input type="password" class="form-control" name="password" id="password_strength" placeholder="Enter new password">
																	</div>
																	<span class="help-block">
																	Type a password to check its strength. </span>
																</div>
															</div>


															<div class="form-group">
																<label class="col-md-3 control-label">Retype New Password</label>
																<div class="col-md-9">
																	<div class="input-group">
																		<span class="input-group-addon">
																		<i class="fa fa-lock"></i>
																		</span>
																	<input type="password" class="form-control" placeholder="Please retype new password">
																	</div>
																</div>
															</div>
					
														<div class="form-actions right">
															<button type="submit" class="btn green-haze">Save Changes</button>
															<button type="button" class="btn default">Cancel</button>
														</div>
														</div>
													</form>
												</div>
												<!-- END CHANGE PASSWORD TAB -->
												<!-- PRIVACY SETTINGS TAB -->
												<div class="tab-pane" id="tab_1_4">
													<form action="#">
														<table class="table table-light table-hover">
														<tr>
															<td>
																<label>Enrolled Subjects </label> 
															</td>
															<td>
																<input type="checkbox" checked class="make-switch" data-size="small">
															</td>
														</tr>
														<tr>
															<td>
																<label>Email Address </label> 
															</td>
															<td>
																<input type="checkbox" checked class="make-switch" data-size="small">
															</td>
														</tr>
														<tr>
															<td>
																<label>Mobile Number </label> 
															</td>
															<td>
																<input type="checkbox" checked class="make-switch" data-size="small">
															</td>
														</tr>
														<tr>
															<td>
																<label>ID Number </label> 
															</td>
															<td>
																<input type="checkbox" checked class="make-switch" data-size="small">
															</td>
														</tr>
														<tr>
															<td>
																<label>Birthdate </label> 
															</td>
															<td>
																<input type="checkbox" checked class="make-switch" data-size="small">
															</td>
														</tr>
														</table>
														<!--end profile-settings-->
														<div class="margin-top-10">
															<a href="javascript:;" class="btn green-haze">
															Save Changes </a>
															<a href="javascript:;" class="btn default">
															Cancel </a>
														</div>
													</form>
												</div>
												<!-- END PRIVACY SETTINGS TAB -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>