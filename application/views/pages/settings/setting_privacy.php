<!-- BEGIN PRIVACY SETTING -->
<div class="tab-pane" id="tab_1_2">
<hr style="margin-bottom:15px!important;margin-top:0px!important">
	<div class="skin skin-square">
		<form class="form-horizontal" role="form">
			<div class="form-body">
			<p><small> You have the option to show or hide your personal information :</small></p>

			<div class="col-md-12" style="margin-bottom:15px;border-bottom:1px solid #eee">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-6 control-label">Show Enrolled Subjects</label>
							<div class="col-md-6">
								<input type="checkbox" checked class="make-switch" data-size="small" data-on-text="Yes" data-off-text="No">
							</div>
					</div>
					<div class="form-group">
						<label class="col-md-6 control-label">Show Email Address</label>
							<div class="col-md-6">
								<input type="checkbox" checked class="make-switch" data-size="small" data-on-text="Yes" data-off-text="No">
							</div>
					</div>
					<div class="form-group">
						<label class="col-md-6 control-label">Show Mobile Number</label>
							<div class="col-md-6">
								<input type="checkbox" checked class="make-switch" data-size="small" data-on-text="Yes" data-off-text="No">
							</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-6 control-label">Show ID Number</label>
							<div class="col-md-6">
								<input type="checkbox" checked class="make-switch" data-size="small" data-on-text="Yes" data-off-text="No">
							</div>
					</div>
					<div class="form-group">
						<label class="col-md-6 control-label">Show Year Level</label>
							<div class="col-md-6">
								<input type="checkbox" checked class="make-switch" data-size="small" data-on-text="Yes" data-off-text="No">
							</div>
					</div>
					<div class="form-group">
						<label class="col-md-6 control-label">Show Birthdate</label>
							<div class="col-md-6">
								<input type="checkbox" checked class="make-switch" data-size="small" data-on-text="Yes" data-off-text="No">
							</div>
					</div>
				</div>
			</div>
				<div class="form-actions btntoright">
					<button type="submit" class="btn green-haze">Save Changes</button>
					<button type="button" class="btn default">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- END PRIVACY SETTING -->