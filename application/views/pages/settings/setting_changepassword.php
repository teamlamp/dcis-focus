<!-- BEGIN CHANGE PASSWORD -->
<div class="tab-pane" id="tab_1_4">
<hr style="margin-bottom:15px!important;margin-top:0px!important">
	<form  class="form-horizontal" role="form">
	<div class="alert-message-change-password"><label class="message"></label></div>
		<div class="form-body">
		<div id="form_container" class="changePassword">
			<div class="message" style="display:none;"></div>
			<div class="form-group">
				<label class="col-md-3 control-label">Password</label>
				<div class="col-md-9">
					<div class="input-group">
						<span class="input-group-addon">
						<i class="fa fa-lock"></i>
						</span>
						<input type="password" name="old_password" class="form-control input-large old_password" placeholder="Enter current password">
					</div>
					<span class="help-block">
					Password currently used. </span>
				</div>
			</div>

			<div class="form-group last password-strength">
				<label class="col-md-3 control-label">New Password</label>
				<div class="col-md-9">
					<div class="input-group">
						<span class="input-group-addon">
						<i class="fa fa-unlock"></i>
						</span>
						<input type="password" class="form-control input-large new_password" name="new_password" id="password_strength" placeholder="Enter new password" maxlength="8">
					</div>
					<span class="help-block">
					Password must not be longer than 8 characters. </span>
				</div>
			</div>


			<div class="form-group">
				<label class="col-md-3 control-label">Retype New Password</label>
				<div class="col-md-9">
					<div class="input-group">
						<span class="input-group-addon">
						<i class="fa fa-lock"></i>
						</span>
					<input type="password" class="form-control input-large confirm_password" name="confirm_password" placeholder="Please retype new password">
					</div>
				</div>
			</div>

		<hr style="margin:0px!important">
		<div class="form-actions btntoright">
			<a href="javascript:void(0)" class="btn green-haze ajaxChangePass">Save Changes</a>
			<button type="button" class="btn default">Cancel</button>
		</div>
		</div>
		</div>
	</form>
</div>
<!-- END CHANGEPASSWORD -->
