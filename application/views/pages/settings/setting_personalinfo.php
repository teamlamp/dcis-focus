<!-- BEGIN PERSONAL INFO -->
<div class="tab-pane active" id="tab_1_1">
<hr style="margin-bottom:15px!important;margin-top:0px!important">
<?php

if($this->session->userdata('user_role') == "student"){
echo "<form action='".base_url('settings/editStudentProfile')."' method='POST' class='form-horizontal' role='form'>";
}
elseif($this->session->userdata('user_role') == "instructor"){
echo "<form action='".base_url('settings/editInstructorProfile')."' method='POST' class='form-horizontal' role='form'>";
}
if($this->session->userdata('user_role') == "admin"){
echo "<form action='".base_url('settings/editAdminProfile')."' method='POST' class='form-horizontal' role='form'>";
}
else{
echo "<form action='".base_url('settings/editStaffProfile')."' method='POST' class='form-horizontal' role='form'>";
}
?>
		<div class="form-body">
			
			<div class="form-group">
				<label class="col-md-3 control-label">First Name</label>
				<div class="col-md-9">
					<input type="text" name="firstname" class="form-control input-large" placeholder="Joseph Vincent">
					<span class="help-block">
					Change your firstname. </span>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Middle Name</label>
				<div class="col-md-9">
					<input type="text" name="middlename" class="form-control input-large" placeholder=" ">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Last Name</label>
				<div class="col-md-9">
					<input type="text" name="lastname" class="form-control input-large" placeholder="Lao">
				</div>
			</div>
<?php if($this->session->userdata('user_role') == "student") { ?>
			<div class="form-group">
				<label class="control-label col-md-3">Program Code</label>
				<div class="col-md-9">
					<select name="program_id" class="form-control ajaxPrograms select2me" data-placeholder="Select...">
					</select>
				</div>
			</div>
<?php } ?>

			<div class="form-group">
				<label class="control-label col-md-3">Birthdate</label>
				<div class="col-md-3">
					<div class="input-group">
					<span class="input-group-addon">
						<i class="fa fa-calendar"></i>
						</span>
					<input class="form-control form-control-inline input-large date-picker" name="dob" size="16" type="text" value=""/>
					</div>
					<span class="help-block">
					Select date. </span>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Email</label>
				<div class="col-md-9">
					<div class="input-group">
						<span class="input-group-addon">
						<i class="fa fa-envelope"></i>
						</span>
						<input type="email" name="email" class="form-control input-large" placeholder="josephvincentlao@gmail.com">
					</div>
				</div>
			</div>


			<div class="form-group">
				<label class="col-md-3 control-label">Mobile No.</label>
				<div class="col-md-9">
					<div class="input-group">
						<span class="input-group-addon">
						<i class="fa fa-phone"></i>
						</span>
					<input type="text" name="phone" class="form-control input-large" placeholder="+63 9123456789">
					</div>
					<span class="help-block">
					Optional. </span>
				</div>
			</div>

		<hr style="margin:0px!important">
		<div class="form-actions btntoright">
			<button type="submit" class="btn green-haze">Save Changes</button>
			<button type="button" class="btn default">Cancel</button>
		</div>
		</div>
	</form>
</div>
<!-- END PERSONAL INFO -->