			<div class="portlet light">
		<!-- STAT -->
		<div class="row list-separated profile-stat">
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="uppercase profile-stat-title">
					<?php 
					$total_units = 0;
					$ec = sizeof($enrolled_classes);
					for($i=0;$i<$ec;$i++){
						$total_units+=$enrolled_classes[$i]['units'];
					}
					?>
					<?= number_format($total_units,1) ?>
				</div>
				<div class="uppercase profile-stat-text">
					Total Units Enrolled
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="uppercase profile-stat-title">
					<?= $ec ?>
				</div>
				<div class="uppercase profile-stat-text">
					Major Subjects Enrolled
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-6">
				<div class="uppercase profile-stat-title">
					<?= $profile_data[0]['year_level'] ?>
				</div>
				<div class="uppercase profile-stat-text">
					Year Level
				</div>
			</div>
		</div>
		<div class="portlet-body">
		<div class="panel-group accordion" id="accordion1">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1">
					User Basic Information : </a>
					</h4>
				</div>
				<div id="collapse_1" class="panel-collapse collapse">
					<div class="panel-body">
							<span class="profile-desc-text"> <?= $profile_data[0]['program_title'] ?></span>
							<table border="0" class="table table-hover table-light">
								<tr>
									<td>Birth : </td>
									<td><?= date('F d, Y',strtotime($profile_data[0]['dob'])) ?></td>
								</tr>
								<tr>
									<td>Mobile # :</td>
									<td><?= $profile_data[0]['phone'] ?></td>
								</tr>
								<tr>
									<td>Email :</td>
									<td><?= $profile_data[0]['email'] ?></td>
								</tr>
							</table>

							<!-- <ul class="feeds">
								<li>
									<a href="javascript:;">
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<a title="" href="javascript:;" data-original-title="skype" class="social-icon social-icon-color skype"></a>
												</div>
												<div class="cont-col2">
													<div class="desc">
														josephlao
													</div>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li>
									<a href="javascript:;">
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<a href="javascript:;" data-original-title="Goole Plus" class="social-icon social-icon-color googleplus"></a>
												</div>
												<div class="cont-col2">
													<div class="desc">
														josephvincentlao@gmail.com
													</div>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li>
									<a href="javascript:;">
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<a href="javascript:;" data-original-title="facebook" class="social-icon social-icon-color facebook"></a>
												</div>
												<div class="cont-col2">
													<div class="desc">
														facebook.com/joseph.lao
													</div>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li>
									<a href="javascript:;">
										<div class="col1">
											<div class="cont">
												<div class="cont-col1">
													<a href="javascript:;" data-original-title="yahoo" class="social-icon social-icon-color yahoo"></a>
												</div>
												<div class="cont-col2">
													<div class="desc">
														josephlao@yahoo.com
													</div>
												</div>
											</div>
										</div>
									</a>
								</li>
							</ul> -->

					</div>
				</div>
			</div>
		</div>
	</div>

		<!-- END STAT -->
	</div>
	</div>
	<!-- END PORTLET MAIN -->
<!-- END PORTLET