<!-- BEGIN PORTLET -->
									<div class="portlet light">
										<div class="portlet-title tabbable-line">
											<div class="caption caption-md">
												<i class="icon-speech font-blue"></i>
												<span class="caption-subject font-blue bold uppercase">Feeds</span>
											</div>
											<ul class="nav nav-tabs">
												<li class="active">
													<a href="#tab_1_1" data-toggle="tab">
													Class Activities</a>
												</li>
												<li>
													<a href="#tab_1_2" data-toggle="tab">
													Class Callouts </a>
												</li>
											</ul>
										</div>
										<div class="portlet-body">
											<!--BEGIN TABS-->
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_1">
													<div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
												<div class="general-item-list">
													<div class="item">
														<div class="item-head">
															<div class="item-details">
																<img class="item-pic" src="../../assets/admin/layout3/img/avatar4.jpg">
																<a href="" class="item-name primary-link">Jun Mitsui</a>
																<span class="item-label">3 hrs ago</span>
															</div>
															<span class="item-status"><a href="" class="font-blue"><i class="fa fa-cube font-blue"></i> ICT146</a></span>
														</div>
														<div class="item-body">
															 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
														</div>
													</div>
													<div class="item">
														<div class="item-head">
															<div class="item-details">
																<img class="item-pic" src="../../assets/admin/layout3/img/avatar3.jpg">
																<a href="" class="item-name primary-link">Devlin Pajaron</a>
																<span class="item-label">5 hrs ago</span>
															</div>
															<span class="item-status"><a href="" class="font-green-haze"><i class="fa fa-video-camera font-green-haze"></i> ICT126</a></span>
														</div>
														<div class="item-body">
															 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat tincidunt ut laoreet.
														</div>
													</div>
													<div class="item">
														<div class="item-head">
															<div class="item-details">
																<img class="item-pic" src="../../assets/admin/layout3/img/avatar6.jpg">
																<a href="" class="item-name primary-link">Alley Berdin</a>
																<span class="item-label">8 hrs ago</span>
															</div>
															<span class="item-status"><a href="" class="font-yellow"><i class="fa fa-image font-yellow"></i> ICT122</a></span>
														</div>
														<div class="item-body">
															 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh.
														</div>
													</div>
													<div class="item">
														<div class="item-head">
															<div class="item-details">
																<img class="item-pic" src="../../assets/admin/layout3/img/avatar7.jpg">
																<a href="" class="item-name primary-link">Loyd Calatrava</a>
																<span class="item-label">12 hrs ago</span>
															</div>
															<span class="item-status"><a href="" class="font-red"><i class="fa fa-file-text-o font-red"></i> ICT201N</a></span>
														</div>
														<div class="item-body">
															 Consectetuer adipiscing elit Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
														</div>
													</div>
													<div class="item">
														<div class="item-head">
															<div class="item-details">
																<img class="item-pic" src="../../assets/admin/layout3/img/avatar9.jpg">
																<a href="" class="item-name primary-link">Kendrick Siao</a>
																<span class="item-label">2 days ago</span>
															</div>
															<span class="item-status"><a href="" class="font-purple"><i class="fa fa-star-half font-purple"></i> ICT110</a></span>
														</div>
														<div class="item-body">
															 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, ut laoreet dolore magna aliquam erat volutpat.
														</div>
													</div>
													<div class="item">
														<div class="item-head">
															<div class="item-details">
																<img class="item-pic" src="../../assets/admin/layout3/img/avatar8.jpg">
																<a href="" class="item-name primary-link">wtf asdf</a>
																<span class="item-label">3 days ago</span>
															</div>
															<span class="item-status"><a href="" class="font-purple"><i class="fa fa-star-half font-purple"></i> ICT110</a></span>
														</div>
														<div class="item-body">
															 Lorem ipsum dolor sit amet, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
														</div>
													</div>
													<div class="item">
														<div class="item-head">
															<div class="item-details">
																<img class="item-pic" src="../../assets/admin/layout3/img/avatar2.jpg">
																<a href="" class="item-name primary-link">hmmm owww</a>
																<span class="item-label">4 hrs ago</span>
															</div>
															<span class="item-status"><a href="" class="font-red"><i class="fa fa-file-text-o font-red"></i> ICT201N</a></span>
														</div>
														<div class="item-body">
															 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
														</div>
													</div>
												</div>
											</div>
												</div>
												<div class="tab-pane" id="tab_1_2">
													<div class="scroller" style="height: 337px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
														<ul class="feeds">
															<li>
																<a href="javascript:;">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-sm label-danger">
																				<i class="fa fa-bullhorn"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				 New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		 Just now
																	</div>
																</div>
																</a>
															</li>
															<li>
																<a href="javascript:;">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-sm label-danger">
																				<i class="fa fa-bullhorn"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				 New order received
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		 10 mins
																	</div>
																</div>
																</a>
															</li>
															<li>
																<a href="javascript:;">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-sm label-success">
																				<i class="fa fa-bullhorn"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				 New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		 Just now
																	</div>
																</div>
																</a>
															</li>
															<li>
																<a href="javascript:;">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-sm label-info">
																				<i class="fa fa-bullhorn"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				 New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		 Just now
																	</div>
																</div>
																</a>
															</li>
															<li>
																<a href="javascript:;">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-sm label-info">
																				<i class="fa fa-bullhorn"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				 New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		 Just now
																	</div>
																</div>
																</a>
															</li>
															<li>
																<a href="javascript:;">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-sm label-success">
																				<i class="fa fa-bullhorn"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				 New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		 Just now
																	</div>
																</div>
																</a>
															</li>
															<li>
																<a href="javascript:;">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-sm label-warning">
																				<i class="fa fa-bullhorn"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				 New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		 Just now
																	</div>
																</div>
																</a>
															</li>
															<li>
																<a href="javascript:;">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-sm label-success">
																				<i class="fa fa-bullhorn"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				 New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		 Just now
																	</div>
																</div>
																</a>
															</li>
															<li>
																<a href="javascript:;">
																<div class="col1">
																	<div class="cont">
																		<div class="cont-col1">
																			<div class="label label-sm label-danger">
																				<i class="fa fa-bullhorn"></i>
																			</div>
																		</div>
																		<div class="cont-col2">
																			<div class="desc">
																				 New user registered
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col2">
																	<div class="date">
																		 Just now
																	</div>
																</div>
																</a>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<!--END TABS-->
										</div>
									</div>
<!-- END PORTLET -->