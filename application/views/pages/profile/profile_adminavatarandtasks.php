<!-- BEGIN PROFILE SIDEBAR -->
<div class="profile-sidebar" style="width: 250px;">
	<!-- PORTLET MAIN -->
	<div class="portlet light profile-sidebar-portlet">
		<!-- SIDEBAR USERPIC -->
		<div class="profile-userpic">
			<img src="<?= $profile['photo'] ?>" class="img-responsive" alt="">
		</div>
		<!-- END SIDEBAR USERPIC -->
		<!-- SIDEBAR USER TITLE -->
		<div class="profile-usertitle">
			<div class="profile-usertitle-name">
				<?= $profile['firstname']." ".$profile['lastname']; ?>
			</div>
			<div class="profile-usertitle-job">
				<?= $profile['school_id']."<br>DCIS Department Chair" ?>
			</div>
		</div>
		<!-- END SIDEBAR USER TITLE -->
		<!-- SIDEBAR BUTTONS -->
		<div class="profile-userbuttons">
			<!-- <button type="button" class="btn btn-circle green-haze btn-sm">Follow</button> -->
			<a href="<?= base_url('message')?>" class="btn btn-circle btn-danger btn-sm">Message</a>
		</div>
		<!-- END SIDEBAR BUTTONS -->
		<!-- SIDEBAR MENU -->
		<div class="profile-usermenu">
		<!--
			<ul class="nav">
				<li class="active">
					<a href="extra_profile.html">
					<i class="icon-home"></i>
					Overview </a>
				</li>
				<li>
					<a href="extra_profile_account.html">
					<i class="icon-settings"></i>
					Account Settings </a>
				</li>
				<li>
					<a href="page_todo.html" target="_blank">
					<i class="icon-check"></i>
					Tasks </a>
				</li>
				<li>
					<a href="extra_profile_help.html">
					<i class="icon-info"></i>
					Help </a>
				</li>
			</ul>
		-->
		</div>
		<!-- END MENU -->
	</div>
	<!-- END PORTLET MAIN -->
	<!-- PORTLET MAIN -->
	<!--
	<div class="portlet light tasks-widget">
				<div class="portlet-title">
					<div class="caption caption-md">
						<i class="fa fa-tasks font-blue"></i>
						<span class="caption-subject font-blue bold uppercase">Tasks</span>
						<span class="caption-helper">6 notes</span>
					</div>
					<div class="tools">
						<a href="javascript:;" class="collapse">
						</a>
					</div>
					<div class="inputs">
						<div class="portlet-input input-small input-inline">
							<div class="input-icon right">
								<i class="icon-magnifier"></i>
								<input type="text" class="form-control form-control-solid" placeholder="search...">
							</div>
						</div>
					</div> 
				</div>
				<div class="portlet-body">
					<div class="task-content">
						<div class="scroller" style="height: 282px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
							START TASK LIST 
							<ul class="task-list">
								<li>
									<div class="task-checkbox">
										<input type="hidden" value="1" name="test"/>
										<input type="checkbox" class="liChild" value="2" name="test"/>
									</div>
									<div class="task-title">
										<span class="task-title-sp">
										Task Title Here</span>																</span>
									</div>
									<div class="task-config">
										<div class="task-config-btn btn-group">
											<a class="btn btn-xs default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
											<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													<i class="fa fa-pencil"></i> Edit </a>
												</li>
												<li>
													<a href="javascript:;">
													<i class="fa fa-trash-o"></i> Delete </a>
												</li>
											</ul>
										</div>
									</div>
								</li>
								<li>
									<div class="task-checkbox">
										<input type="checkbox" class="liChild" value=""/>
									</div>
									<div class="task-title">
										<span class="task-title-sp">
										Hold An Interview </span>
									</div>
									<div class="task-config">
										<div class="task-config-btn btn-group">
											<a class="btn btn-xs default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
											<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													<i class="fa fa-pencil"></i> Edit </a>
												</li>
												<li>
													<a href="javascript:;">
													<i class="fa fa-trash-o"></i> Delete </a>
												</li>
											</ul>
										</div>
									</div>
								</li>
								<li>
									<div class="task-checkbox">
										<input type="checkbox" class="liChild" value=""/>
									</div>
									<div class="task-title">
										<span class="task-title-sp">
										AirAsia Intranet System</span>
									</div>
									<div class="task-config">
										<div class="task-config-btn btn-group">
											<a class="btn btn-xs default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
											<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													<i class="fa fa-pencil"></i> Edit </a>
												</li>
												<li>
													<a href="javascript:;">
													<i class="fa fa-trash-o"></i> Delete </a>
												</li>
											</ul>
										</div>
									</div>
								</li>
								<li>
									<div class="task-checkbox">
										<input type="checkbox" class="liChild" value=""/>
									</div>
									<div class="task-title">
										<span class="task-title-sp">
										Technical Management Meeting </span>
									</div>
									<div class="task-config">
										<div class="task-config-btn btn-group">
											<a class="btn btn-xs default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
											<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													<i class="fa fa-pencil"></i> Edit </a>
												</li>
												<li>
													<a href="javascript:;">
													<i class="fa fa-trash-o"></i> Delete </a>
												</li>
											</ul>
										</div>
									</div>
								</li>
								<li>
									<div class="task-checkbox">
										<input type="checkbox" class="liChild" value=""/>
									</div>
									<div class="task-title">
										<span class="task-title-sp">
										Kick-off Company CRM Mobile App Development </span>
									</div>
									<div class="task-config">
										<div class="task-config-btn btn-group">
											<a class="btn btn-xs default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
											<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													<i class="fa fa-pencil"></i> Edit </a>
												</li>
												<li>
													<a href="javascript:;">
													<i class="fa fa-trash-o"></i> Delete </a>
												</li>
											</ul>
										</div>
									</div>
								</li>
								<li>
									<div class="task-checkbox">
										<input type="checkbox" class="liChild" value=""/>
									</div>
									<div class="task-title">
										<span class="task-title-sp">
										Prepare Commercial Offer For SmartVision Website Rewamp </span>
									</div>
									<div class="task-config">
										<div class="task-config-btn btn-group">
											<a class="btn btn-xs default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
											<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													<i class="fa fa-pencil"></i> Edit </a>
												</li>
												<li>
													<a href="javascript:;">
													<i class="fa fa-trash-o"></i> Delete </a>
												</li>
											</ul>
										</div>
									</div>
								</li>
								<li class="last-line">
									<div class="task-checkbox">
										<input type="checkbox" class="liChild" value=""/>
									</div>
									<div class="task-title">
										<span class="task-title-sp">
										KeenThemes Investment Discussion </span>
									</div>
									<div class="task-config">
										<div class="task-config-btn btn-group">
											<a class="btn btn-xs default" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
											<i class="fa fa-cog"></i><i class="fa fa-angle-down"></i>
											</a>
											<ul class="dropdown-menu pull-right">
												<li>
													<a href="javascript:;">
													<i class="fa fa-pencil"></i> Edit </a>
												</li>
												<li>
													<a href="javascript:;">
													<i class="fa fa-trash-o"></i> Delete </a>
												</li>
											</ul>
										</div>
									</div>
								</li>
							</ul>
							 END START TASK LIST 
						</div>
					</div>
					<div class="task-footer">
						<div class="btn-arrow-link pull-right">
							<a href="<?= base_url('tasks')?>">See All Tasks <i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			-->
			<!--END PORTLET MAIN-->
</div>
<!-- END BEGIN PROFILE SIDEBAR -->