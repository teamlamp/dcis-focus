<!-- BEGIN PORTLET -->
<div class="portlet light ">
<div class="portlet-title" style="height:38px!important">
	<div class="caption caption-md">
		<i class="icon-notebook font-blue"></i>
		<span class="caption-subject font-blue bold uppercase">Classes Handled</span>
		<span class="caption-helper">2nd Semester</span>
	</div>
	<div class="actions">
		<!-- <div class="btn-group btn-group-devided" data-toggle="buttons">
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm active">
			<input type="radio" name="options" class="toggle" id="option1">Today</label>
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
			<input type="radio" name="options" class="toggle" id="option2">Week</label>
			<label class="btn btn-transparent grey-salsa btn-circle btn-sm">
			<input type="radio" name="options" class="toggle" id="option2">Month</label>
		</div> -->
	</div>
</div>
<div class="portlet-body">
	<div class="table-scrollable table-scrollable-borderless">
		<table class="table table-hover table-light">
			<thead>
				<tr class="uppercase">
					<th colspan="2">Course Code</th>
					<th>Course Title</th>
					<th>Units</th>
					<th>Schedule</th>
					<th>Room</th>
				</tr>
			</thead>
			<?php foreach($handled_classes as $class){ ?>
			<tr>
				<td class="fit"><?= $class['course_code'] ?></td>
				<td><a href="javascript:;" class="primary-link">Grp <?= $class['group_number'] ?></a></td>
				<td><?= $class['course_title'] ?></td>
				<td><?= number_format($class['units'],1) ?></td>
				<td>MW 9:00-10:30</td>
				<td><?= $class['building']."".$class['room_number'] ?></td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>
</div>
<!-- END PORTLET -->