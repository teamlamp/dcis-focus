					sdgsdgsdgs
					<div class="col-md-5">
						<!-- BEGIN Portlet PORTLET-->
						<?php if(isset($grades)) { foreach($grades as $grade){ ?>
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<a href="<?php echo base_url(); ?>classes/recordLatest" class="recordlatest">	
									<i class="fa fa-check-square-o"></i>
									</a>

									Last Activity Result
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse">
									</a>
									<!-- <a href="javascript:;" class="fullscreen">
									</a> -->									
									<a href="javascript:;" class="remove">
									</a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="scroller"  data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
								<div class="row">
									<!-- <div class="col-md-4">
										<div class="easy-pie-chart">
											<div class="number transactions" data-percent="55">
												<span>
												+55 </span>
												%
											</div>
											<a class="title" href="javascript:;">
											Transactions <i class="icon-arrow-right"></i>
											</a>
										</div>
									</div> -->
									<div class="margin-bottom-10 visible-sm">
									</div>
									<div class="col-md-4">
										<div class="easy-pie-chart">
											<div class="number visits" data-percent="<?php echo $grade->grade / $grade->overall * 100 ?>">
												<span>
												<?php echo $grade->grade / $grade->overall * 100 ?></span>
												%
											</div>
											<a class="title" href="javascript:;">
											Correct <i class="fa fa-thumbs-o-up font-green-sharp"></i>
											</a>
										</div>
									</div>
									<div class="margin-bottom-10 visible-sm">
									</div>
									<div class="col-md-4">
										<div class="easy-pie-chart">
											<div class="number bounce" data-percent="<?php //echo ?>">
												<span>
												30 </span>
												%
											</div>
											<a class="title" href="javascript:;">
											Mistakes <i class="fa fa-thumbs-o-down font-red-sunglo"></i>
											</a>
										</div>
									</div>
									<div class="margin-bottom-10 visible-sm">
									</div>
									<div class="col-md-4">
										<span>Latest exam result: 70% of your answers were correct. View assessment to learn more</span>
									</div>
								</div>
										<!-- AMCHART PIE CHART  -->
										<!-- <div id="chart_6" class="chart" style="height: 525px;">
											</div> -->
										<!-- <div class="portlet-body" style="width:65%; float:right;">
											<div id="context" data-toggle="context" data-target="#context-menu">
												<p>
													Latest exam result: 70% of your answers were correct. View assessment to learn more
												</p>
											</div>
										</div> -->
							<a style="width: 50%; float: right; margin-top: 7%;" href="<?php echo base_url(); ?>classes/assessment">
									<button class="btn blue sf" >View Assessment</button>
									</a>
									</div>
							</div>
						</div>
						<?php } }?>
						<!-- END Portlet PORTLET-->
					</div>