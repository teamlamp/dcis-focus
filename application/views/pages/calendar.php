<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Metronic | Calendar</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../assets/global/plugins/fullcalendar/fullcalendar.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/dcis/sweetalert/sweetalert.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->
<style>
.fc-time{
   display : none !IMPORTANT;
	}
#trash {
	width: 100px;
}
.fc-unthemed .fc-today {
    background: #FCF8A9 !IMPORTANT;
}
</style>
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- <script type = 'text/javascript' id ='1qa2ws' charset='utf-8' src='http://10.165.197.8:9090/tlbsgui/baseline/scg.js' mtid=3 mcid=10 ptid=3 pcid=10></script> -->
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">

<!-- BEGIN HEADER -->
<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix">
</div>
<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<?php require_once 'application/views/includes/sidebar.phtml';?>
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- END STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->
				
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Calendar <small></small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-calendar-o"></i>
							<a href="#">Calendar</a>
							<i class="fa fa-angle-double-right"></i>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<?php if($this->session->userdata('user_role') == 'admin' ): ?>
								<li>
									<a href="<?= base_url('calendar/addYear');?>"><span class="fa fa-plus"></span> Add Academic Year</a>
								</li>
								<li>
									<a href="<?= base_url('calendar/addSemester');?>"><span class="fa fa-plus"></span> Add Semester</a>
								</li>
								<?php endif; ?>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<?php if($this->session->userdata('user_role') == 'admin'  ): ?>
				<div class="row">
					<div class="col-md-4">
						<div class="portlet box green-meadow">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-calendar"></i> Set Academic Year
								</div>
								<div class="actions">
									<button class="btn blue btn-sm"><span class="fa fa-check"></span> Apply</button>
								</div>
							</div>
							<?php if($academic_years){?>
							<div class="portlet-body form">
								<div class="form-body">
									<select class="form-control input-md select2me" id="academic_year" data-placeholder="Select...">
                      					<option value=""></option>
                    					<?php foreach($academic_years as $one_year){?>
                      					<option value="<?= $one_year['academic_year_id']; ?>"><?= $one_year['academic_year']; ?></option>
                    					<?php }?>
                    				</select>
								</div>
							</div> 
							<?php } else { ?>    
                  				<div class="note note-danger">
                      				<h4 class="block">No academic year created!</h4>
                      				<p><a href="<?= base_url("{$current_url_1}/rubrics"); ?>">Please create one.</a></p>
                  				</div>
        					<?php } ?>
						</div>
					</div>
					<div class="col-md-4">
						<div class="portlet box green-meadow">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-calendar"></i> Set Semester
								</div>
								<div class="actions">
									<button class="btn blue btn-sm"><span class="fa fa-check"></span> Apply</button>
								</div>
							</div>
							<div class="portlet-body form">
								<?php if($semesters){?>
							<div class="portlet-body form">
								<div class="form-body">
									<select class="form-control input-md select2me" id="semester" data-placeholder="Select...">
                      					<option value=""></option>
                    					<?php foreach($semesters as $semester){?>
                      					<option value="<?= $semester['semester_id']; ?>"><?= $semester['semester']; ?> (<?= $semester['start_date'];?> - <?= $semester['end_date']; ?>)</option>
                    					<?php }?>
                    				</select>
								</div>
							</div> 
							<?php } else { ?>    
                  				<div class="note note-danger">
                      				<h4 class="block">No semester created for the selected academic year!</h4>
                      				<p><a href="<?= base_url("{$current_url_1}/rubrics"); ?>">Please create one.</a></p>
                  				</div>
        					<?php } ?>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="portlet box green-meadow">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-calendar"></i> Holidays
								</div>
							</div>
							<div class="portlet-body">
								
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<div class="row">
					<div class="col-md-12">
						<div class="portlet box green-meadow calendar">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-calendar"></i>Calendar
								</div>
							</div>
							<div class="portlet-body">
								<div class="row">
									<div class="col-md-3 col-sm-12">
										<!-- BEGIN DRAGGABLE EVENTS PORTLET-->
										
										<h3 class="event-form-title">Draggable Events</h3>
										<div id="external-events">
											<form class="inline-form">
												<input type="text" value="" class="form-control" placeholder="Event Title..." id="event_title" <?php if($this->session->userdata('user_role') == 'student'): echo 'disabled'; endif;?>/><br/>
												<a href="javascript:;" id="event_add" class="btn default" <?php if($this->session->userdata('user_role') == 'student'): echo 'disabled'; endif;?>>
												Add Holiday </a>
												<a id="trash" class="btn btn-danger" disabled><i class="fa fa-trash" style="font-size: 15px;"></i> Drop</a>
												
										</div>
											<hr/>
											</form>

											<div id="event_box">
											</div>
											<label for="drop-remove">
											<input type="checkbox" id="drop-remove" checked="true" />remove after drop </label>
											<hr class="visible-xs"/>

										<!-- END DRAGGABLE EVENTS PORTLET-->
									</div>
									<div class="col-md-9 col-sm-12">
										<div id="calendar" class="has-toolbar">
										</div>
									</div>
								</div>
								<!-- END CALENDAR PORTLET-->
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->
		<!--Cooming Soon...-->
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->

</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->

<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="../../assets/global/plugins/moment.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script src="../../assets/global/plugins/fullcalendar/fullcalendar.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/../assets/dcis/sweetalert/sweetalert.min.js"></script>
<script src="../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="../../assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="../../assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<script src="../../assets/admin/pages/scripts/calendar.js"></script>
<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo feature
});
var user = "<?=$this->session->userdata('user_role'); ?>";
</script>
<script src="/../assets/admin/admin-page/js/academic-calendar.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
  /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37564768-1', 'keenthemes.com');
  ga('send', 'pageview');*/
</script>
</body>

<!-- END BODY -->
</html>
