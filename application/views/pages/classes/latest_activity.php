<!-- Post discussion block					 -->
<?php 

if($last_act[0]['perfect_score']!='0' && $last_act){
$pass = $pass[0]['pass'];
$fail = $fail[0]['fail'];
$total = $pass + $fail;
$pass_per = floor(($pass / $total)*100);
$fail_per = floor(($fail / $total)*100);

}
?>
<?php if($class['enable_latest_activity'] == '1') { ?>
	<!-- BEGIN Portlet PORTLET-->
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-bar-chart-o"></i>
				<span class="caption-subject bold uppercase"> Latest</span>
				<span class="caption-helper">Activity Results</span>
			</div>
		</div>
		<div class="portlet-body">
		<?php if($last_act[0]['perfect_score']!='0' && $last_act){?>
			<div class="scroller" style="height:172px" data-rail-visible="0" data-handle-color="#FFFFFF">
				<div class="row">
				<h5><center><?= $rub[0]['type']; ?> ( <?= $last_act[0]['activity_title']; ?> )</center></h5>
					<div class="col-md-6">
						<div class="easy-pie-chart">
							<div class="number visits" data-percent="<?= $pass_per ?>">
								<span>
								<?= $pass_per ?> </span>
								%
							</div>
							<a class="title" href="javascript:;">
							Passed <i class="fa fa-check-circle font-green-haze"></i>
							</a>
						</div>
					</div>
					<div class="margin-bottom-10 visible-sm">
					</div>
					<div class="col-md-6">
						<div class="easy-pie-chart">
							<div class="number bounce" data-percent="<?= $fail_per ?>">
								<span>
								<?= $fail_per ?> </span>
								%
							</div>
							<a class="title" href="javascript:;">
							Failed <i class="fa fa-times-circle font-red-sunglo"></i>
							</a>
						</div>
					</div>
				</div>


			</div><!-- END OF SCROLLER -->
		<?php } else { ?>
			<div class="note note-danger">
                <h4 class="block" style="text-align:center;">No Activities to display yet.</h4>
            </div>
		<?php } ?>
		</div>
	</div>
	<!-- END Portlet PORTLET-->
<?php } else { ?>
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-bar-chart-o"></i>
				<span class="caption-subject bold uppercase"> Latest</span>
				<span class="caption-helper">Activity Results</span>
			</div>
		</div>
		<div class="portlet-body">
			<div class="note note-danger">
	      <h4 class="block" style='text-align:center;'>Latest Acitivty Is Disabled</h4>
	  	</div>
	  </div>
  </div>
<?php } ?>
