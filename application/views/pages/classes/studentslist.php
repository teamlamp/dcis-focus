<!DOCTYPE html>
<html lang="en">
<head>
<?php foreach($one_class as $class) ?>
<title>DCIS | Students List</title>
<input type="hidden" id="class_id" value="<?= $class['class_id']; ?>">

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/dcis/sweetalert/sweetalert.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->
<style>
	.select2-container{
		z-index: 10050;
	}
</style>
<link rel="shortcut icon" href="favicon.ico"/>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- END STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Student List <small><?= $class['group_number']." ".$class['course_code']; ?></small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="icon-notebook"></i>
							<a href="<?= base_url('classes')?>">Classes</a>
							<i class="fa fa-angle-double-right"></i>
						</li>
						<li>
							<a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}")?>"><?= $class['group_number']." ".$class['course_code']; ?></a>
							<i class="fa fa-angle-double-right"></i>
						</li>
						<li>
							<a href="javascript:;">Students</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							 <ul class="dropdown-menu pull-right" role="menu">
				                <?php if($this->session->userdata('user_role') == "instructor" || $this->session->userdata('user_role') == "admin"){?>
				                <li>
									<a href="javascript:;>" data-toggle="modal" data-target="#importstudents"><i class="fa fa-plus"></i> Import Class List</a>
								</li>
								<li>
									<a href="#" data-toggle="modal" data-target="#add_class_student"><i class="fa fa-plus"></i> Add Students</a>
								</li>
				                <?php } else if ($this->session->userdata('user_role') == "student"){?>
				                <li>
				                  <a href="<?= base_url("{$current_url}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
				                </li>
				                <?php } ?>
				                <li class="divider"></li>
				                <?php if($this->session->userdata('user_role') != "student"){?>
				                <li>
				                  <a href="<?php echo base_url("{$current_url_1}/students"); ?>"><i class="fa fa-users"></i> Students List</a>
				                </li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/classrecord"); ?>"><i class="fa fa-file-text"></i> Class Record</a>
				                </li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/attendance"); ?>"><i class="fa fa-pencil"></i> Attendance</a>
				                </li>
				                <?php } else if ($this->session->userdata('user_role') == "student"){?>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
				                </li>
				                <?php } ?>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/calendar"); ?>"><i class="fa fa-calendar"></i> Class Calendar</a>
				                </li>
				                <?php if($this->session->userdata('user_role')!="student"){ ?>
				                <li class="divider"></li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/grades/record"); ?>"><i class="fa fa-save"></i> Record Grades</a>
				                </li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/grades/history"); ?>"><i class="fa fa-history"></i> Record History</a>
				                </li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/grades/rubrics"); ?>"><i class="fa fa-cube"></i> Class Rubrics</a>
				                </li>             
				                <li class="divider">
				                </li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/settings") ?>"><i class="fa fa-cog"></i> Class Settings</a>
				                </li>
				                <?php } ?>
				               </ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<?php if(!$students){?>
					<div class="note note-danger">
              <h4 class="block">No students in this class yet.</h4>
              <p>Please add your students by going to the "Actions" button and click "Import Class List" or "Add Students".</p>
          </div>
				<?php } else { ?>
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box grey-gallery">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-globe"></i>All Students
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover" id="classStudentslist">
								<thead>
									<tr>
										<th>ID </br> Number</th>
							 			<th>Last </br> Name</th>
							 			<th>First </br> Name</th>
							 			<th>Middle </br> Name</th>
							 			<th>Year </br> Level</th>
							 			<th>Total </br> Absences</th>
							 			<th>Attendance </br> Status</th>
							 			<th>Grade </br> Status</th>
							 			<th>Actions</th>
							 			<!-- <th></th> -->
									</tr>
								</thead>
								<tbody>
								<?php foreach($students as $student){ ?>
								 	<tr>
								 		<td><a href="javascript:;" class="gotogorrrow"><?= $student['school_id']; ?></a></td>
								 		<td><?= $student['lastname'];  ?></td>
								 		<td><?= $student['firstname']; ?></td>
								 		<td><?= $student['middlename']; ?></td>
								 		<td><?= $student['year_level']; ?></td>
								 		<td>      
										<?php



										$ret = countAbsences($student['school_id'], $class['class_id']); ?>
										<a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/StudentAttendanceRecord/{$student['class_member_id']}"); ?> "><?php echo $ret; ?></a>

										
								 		</td>
								 		<td><?php 
								 		updateAttendanceStatus($student['school_id'], $class['class_id'], $ret, $class['max_absences']);
										$num = checkAttendanceStatus($student['school_id'], $class['class_id']);
										
										if($num == 1){ ?>
										<span class="label label-sm label-success">Readmission</span>

										<?php }
										
										elseif($num == 2){ ?>
										<span class="label label-sm label-warning">Warning</span>
										<?php }
										
										elseif($num == 3){ ?>
										<span class="label label-sm label-danger">Dropped</span>
										<?php }
										
										else{}
										?>
								 		</td>
								 		<?php if($student['cm_grade_sf'] > ($student['class_grade_sf']/2)){?>
								 			<td><span class="label label-sm label-success">Passing</span></td>
								 		<?php } else { ?>
								 			<td><span class="label label-sm label-danger">Failing</span></td>
								 		<?php }?>
								 		<td><button class="btn btn-sm green-jungle"><i class="fa fa-warning"></i> Warn</button> 
								 		<form action="<?php echo base_url('classes/removeClassMember'); ?>" method="POST">
								 		<input type="hidden" name="class_id" value="<?php echo $class['class_id']; ?>">
								 		<input type="hidden" name="student_id" value="<?php echo $student['school_id']; ?>">
								 		<button class="btn btn-sm red-flamingo"><i class="fa fa-times"></i> Remove</button></td>
								 		</form>
								 		<!-- <td><button>Remove</button></td> -->
								 	</tr>
								<?php } ?>
								</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				<!-- END PAGE CONTENT-->
				</div>
				<?php } ?>
				<?php require_once 'application/views/modals/import_class_members.phtml';?>
				<?php require_once 'application/views/modals/add_class_student.phtml';?>
		</div>
			<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>
	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/fuelux/js/spinner.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->

	<?php require_once 'application/views/includes/theme_js.phtml';?>	
	<script>
      jQuery(document).ready(function() {    
				DcisTableAdvanced.init();
				AdminPage.init();
      });

      var classname = <?php echo json_encode($class['course_code']); ?>; 
      var groupnumber = <?php echo json_encode($class['group_number']); ?>;
      var class_id = $('#class_id').val();
      $(document).find('#id').val(class_id);
      
      $("#students").select2({  
      placeholder:"Select Students",      
      ajax: {
        url: BASE_URL+'users/findUser',
        dataType: 'json',
        tags: true,
        tokenSeparators: [",", " "],
        delay: 250,
        data: function (params) {
          return {
            q: params.term // search term
          };
        },
        processResults: function (data) {
          return {
            results: data
          };
        },
        cache: true
      },
      minimumInputLength: 2
      });
   </script>

   	<script src="/../assets/dcis/js/dcis-table-advance.js"></script>
	<script src="/../assets/admin/pages/scripts/admin.js"></script>	
	<script src="/../assets/dcis/sweetalert/sweetalert.min.js"></script>

<!-- END THEME PLUGINS -->
	
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

<!-- BEGIN HERE -->
<div style="height:30px;width:138px;padding:5px;position: absolute; margin: 0px; z-index: 9999; bottom: 40px; right: 20px;background:#333;opacity:0.9;display:none;" id="gotogorrid">
	<span class="font-grey-cararra">
		<a href="" class="font-grey-cararra" id="grades-link"><i class="fa fa-file-text"></i> Grades</a> | 
		<a href="" class="font-grey-cararra" id="profile-link"><i class="fa fa-user"></i> Profile </a>
	</span>
</div>
<!-- END HERE -->

</html>