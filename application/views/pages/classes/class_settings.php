<!DOCTYPE html>
<!-- BEGIN HEAD -->
<head>
<?php foreach($one_class as $class) ?>
<title>DCIS | Class Settings</title>
<input type="hidden" value="<?= $class['class_id']?>" id="class_id">
<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../assets/admin/pages/css/blog.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link href="../../../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<link href="../../../assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="../../../assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<link href="../../../assets/global/plugins/icheck/skins/all.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="../../../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">

<!-- BEGIN HEADER -->
<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix">
</div>
<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- BEGIN STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Class Settings <small><?= $class['group_number']." ".$class['course_code']; ?></small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
     				<li>
     				   <i class="icon-notebook"></i>
     				   <a href="<?= base_url('classes') ?>">Classes</a>
     				   <i class="fa fa-angle-double-right"></i>
      				</li>	
		            <li>
		              <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}")?>"><?= $class['group_number']." ".$class['course_code']; ?></a>
		              <i class="fa fa-angle-double-right"></i>
		            </li>				
					<li>
						<a href="index.html">Class Settings</a>
					</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<?php if($this->session->userdata('user_role') != "student"){?>
                <li>
                  <a href="<?php echo base_url("class/{$class['course_code']}/{$class['group_number']}/students"); ?>"><i class="fa fa-users"></i> Students List</a>
                </li>
                <li>
                  <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/classrecord"); ?>"><i class="fa fa-file-text"></i> Class Record</a>
                </li>
                <li>
                  <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/attendance"); ?>"><i class="fa fa-pencil"></i> Attendance</a>
                </li>
                <?php } else if ($this->session->userdata('user_role') == "student"){?>
                <li>
                  <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
                </li>
                <?php } ?>
                <li>
                  <a href="<?= base_url("{$current_url}/calendar"); ?>"><i class="fa fa-calendar"></i> Class Calendar</a>
                </li>
                <?php if($this->session->userdata('user_role')!="student"){ ?>
                <li class="divider"></li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/../{$class['course_code']}/{$class['group_number']}/grades/record"); ?>"><i class="fa fa-save"></i> Record Grades</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/../{$class['course_code']}/{$class['group_number']}/grades/history"); ?>"><i class="fa fa-history"></i> Record History</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/../{$class['course_code']}/{$class['group_number']}/grades/rubrics"); ?>"><i class="fa fa-cube"></i> Class Rubrics</a>
                </li>             
                <li class="divider">
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/../{$class['course_code']}/{$class['group_number']}/settings") ?>"><i class="fa fa-cog"></i> Class Settings</a>
                </li>
                <?php } ?>
							</ul>
						</div>
					</div>

				</div>
				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->
				<div class="profile-content">
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light">
								<div class="portlet-title tabbable-line tabbalable-custom">
									<!-- <div class="caption caption-md">
										<i class="fa fa-user hide"></i>
										<span class="caption-subject font-green-haze bold uppercase">Profile Account</span>
									</div> -->
									<ul class="nav nav-tabs nav-justified">
										<li class="active">
											<a href="#tab_1_1" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-lock"></i> Class Information</span></a>
										</li>
										<li>
											<a href="#tab_1_2" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-users"></i> Class Preference</span></a>
										</li>
										<li>
											<a href="#tab_1_3" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-bell"></i> Class Notification</span></a>
										</li>
									</ul>
								</div>
								<div class="portlet-body">
									<div class="tab-content">
										<!-- CLASS INFO TAB -->
										<?php require_once VIEWPATH.'/pages/classes/class_settings/class_information.php';?>
										<!-- END CLASS INFO TAB -->
										
										<!-- CLASS PREFERENCE TAB -->
										<?php require_once VIEWPATH.'/pages/classes/class_settings/class_preference.php';?>
										<!-- END CLASS PREFERENCE TAB -->

										<!-- CLASS NOTIFICATION TAB -->
										<?php require_once VIEWPATH.'/pages/classes/class_settings/class_notification.php';?>
										<!-- END CLASS NOTIFICATION TAB -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/../assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/icheck/icheck.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php require_once 'application/views/includes/theme_js.phtml';?> 
<script src="/../assets/admin/pages/scripts/form-icheck.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/fuelux/js/spinner.min.js"></script>
<script src="/../assets/admin/pages/scripts/components-pickers.js"></script>
<!-- BEGIN END LEVEL SCRIPTS -->
<script>
      jQuery(document).ready(function() {    
         Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
ComponentsPickers.init(); 
FormiCheck.init(); // init page demo
AdminPage.init();
      });
   </script>
<!-- END JAVASCRIPTS -->
<script src="/../assets/admin/pages/scripts/admin.js"></script> 
</body>
<!-- END BODY -->
</html>