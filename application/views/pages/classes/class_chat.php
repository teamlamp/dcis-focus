<!-- chat discussion block	-->
<?php if($class['enable_chatbox'] == '1'){ ?>
<div class="col-md-4" style="position:fixed;bottom:0;right:65px;width:345px;">
	<!-- BEGIN Portlet PORTLET-->
	<div class="portlet light" style="margin-bottom:0px !important;padding:10px">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-comments-o"></i>
				<span class="caption-subject bold uppercase">Chatbox</span>
				<span class="caption-helper"><?= $class['group_number']." ".$class['course_code']; ?></span>
			</div>

			<div class="tools">
				<a href="javascript:;" class="expand">
			</div>
		</div>
		
		<div class="portlet-body display-hide" id="chats">
			<div class="scroller" style="height: 353px;width:auto" data-always-visible="" data-rail-visible1="">
				<ul class="chats">
					<div class="chat-messages">
					</div>
				</ul>
			</div>
	<div class="chat-form">
				<form  class="">
					<input type="hidden" class="chat-name" value="<?= $this->session->userdata('school_id');?>">
					<input id="chat-content" class="form-control" type="text" autocomplete="off" placeholder="Type a message here..."/>
					 <a href="" id="submit_chat" ></a>
				<span class="chat-status"> <h5>&nbsp;<i id="stat-iconn" class="fa fa-circle-o-notch font-blue"></i> Status: <span id="statt">Idle</span></h5></span>
			</div>
			</form>								
		</div>
	</div>
	<!-- END Portlet PORTLET-->
</div>
<?php } else { ?>
<?php } ?>