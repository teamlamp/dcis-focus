<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Assessment</title>
<?php foreach($one_class as $class) ?>
<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">

<!-- BEGIN HEADER -->
<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix">
</div>
<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- BEGIN STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Student Assessment <small><?php echo $this->session->userdata('firstname')." ".$this->session->userdata('lastname');?> - <span id="student_id"><?= $this->session->userdata['school_id']; ?></span></small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
     				<li>
     				   <i class="icon-notebook"></i>
     				   <a href="<?= base_url('classes')?>">Classes</a>
     				   <i class="fa fa-angle-double-right"></i>
      				</li>	
		            <li>
		              <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}")?>"><?= $class['group_number']." ".$class['course_code']; ?></a>
		              <i class="fa fa-angle-double-right"></i>
		            </li>				
					<li>
						<a href="javascript:;">Assessment</a>
					</li>
					</ul>
			<!--		<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button disabled type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
							</ul>
						</div>
					</div> -->
				</div>
				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->

				<!-- BEGIN ROW -->
				<div class="row">
					<div class="col-md-7">
						<?php require_once VIEWPATH.'/pages/classes/assessment/performance_line.php';?>
					</div>

					<div class="col-md-5">
						<?php require_once VIEWPATH.'/pages/classes/assessment/gradepercentage_pie.php';?>
					</div>
				</div>
				<!-- END ROW -->

				<div class="profile-content">
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light">
								<div class="portlet-title tabbable-line tabbalable-custom">
									<!-- <div class="caption caption-md">
										<i class="fa fa-user hide"></i>
										<span class="caption-subject font-green-haze bold uppercase">Profile Account</span>
									</div> -->
									<ul class="nav nav-tabs nav-justified">
										<li class="active">
											<a href="#bargraph" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-bar-chart"></i> Calculated Grade</span></a>
										</li>
										<!-- <li>
											<a href="#datatable1" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-list-ul"></i> List Of Activities</span></a>
										</li> -->
										<li>
											<a href="#datatable2" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-tasks"></i> Grade Breakdown</span></a>
										</li>
									</ul>
								</div>
								<div class="portlet-body">
									<div class="tab-content">
										<!-- CLASS INFO TAB -->
										<?php require_once VIEWPATH.'/pages/classes/assessment/calculatedgrade_tab.php';?>
										<!-- END CLASS INFO TAB -->
										
										<!-- CLASS PREFERENCE TAB -->
										<?php // require_once VIEWPATH.'/pages/classes/assessment/activities_tab.php';?>
										<!-- END CLASS PREFERENCE TAB -->

										<!-- CLASS NOTIFICATION TAB -->
										<?php require_once VIEWPATH.'/pages/classes/assessment/gradesbreakdown_tab.php';?>
										<!-- END CLASS NOTIFICATION TAB -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/../assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?> 
<!-- END THEME PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
// For the charts
$(document).ready(function(){
	// For the line graph
	var performance_array = <?php echo json_encode($performance_array); ?>;
	$('#performance_line').fadeIn('slow');
	var line_chart = new AmCharts.AmSerialChart();
	line_chart.categoryField = "date";
	line_chart.tye = "serial";
	var line_graph = new AmCharts.AmGraph();
	line_graph.valueField = "percentage";
	line_graph.type = "line";
	//line_graph.lineColorField = "lineColor";
	line_graph.fillAlphas = 0.1;
	//line_graph.fillColorsField = "lineColor";
	var line_categoryAxis = line_chart.categoryAxis;
	line_categoryAxis.autoGrindCount = false;
	line_categoryAxis.gridCount = performance_array.length;
	line_categoryAxis.gridPosition = "start";
	line_categoryAxis.labelRotation = 45;
	line_chart.addGraph(line_graph);
  line_chart.dataProvider = performance_array;  
  line_chart.validateData();  
  line_chart.write('performance_line');

	// For Grade Percentage Chart
	$('#gradepercentage_chart').fadeIn('slow');
	var piedata = <?php echo json_encode($piedata) ?>;
  var piechart = AmCharts.makeChart("gradepercentage_chart", {
      "type": "pie",
      "theme": "light",

      "fontFamily": 'Open Sans',
      
      "color":    '#888',
      "valueField": "percentage",
      "titleField": "passfail",
      "exportConfig": {
          menuItems: [{
              icon: Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
              format: 'png'
          }]
      }
  }); 
  piechart.dataProvider = piedata;  
  piechart.validateData();  
  piechart.write('gradepercentage_chart');

});
</script>
<script>
jQuery(document).ready(function() {
	DcisTableAdvancedAssess.init();
   	// ChartsAmcharts.init();
});
</script>
   	<script src="/../assets/dcis/js/dcis-table-advance-assessment.js"></script>
<script src="/../assets/admin/pages/scripts/charts-amcharts.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>