<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Class </title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE STYLES -->
<link href="../../assets/admin/pages/css/blog.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
  <!-- BEGIN CONTAINER -->
  <div class="page-container">

    <!-- BEGIN SIDEBAR -->
    <?php require_once 'application/views/includes/sidebar.phtml';?>  
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">

        <!-- BEGIN STYLE CUSTOMIZER -->
        <?php require_once 'application/views/includes/style_customizer.phtml';?> 
        <!-- END STYLE CUSTOMIZER -->

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
        
        Class 
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="icon-notebook"></i>
              <a href="<?= base_url('classes') ?>">Classes</a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="javascript:;">Add Class</a>
            </li>
          </ul>
<!--          <div class="page-toolbar">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
              Actions <i class="fa fa-arrow-circle-down"></i>
              </button>
            </div>
          </div>-->
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit"></i>
                  <span class="caption-subject bold uppercase">Add Class</span>
                  <span class="caption-helper font-grey-cararra">create new class...</span>
                </div>
                <div class="tools">
                  <a href="javascript:;" class="collapse">
                  </a>
                  <a href="javascript:;" class="reload">
                  </a>
                </div>
              </div>
              <div class="portlet-body form">
              <?php echo validation_errors(); ?>
                <form role="" action="<?php echo base_url('classes/addNewClass'); ?>" method="POST" id="" class="form-horizontal">
                  <div class="form-body">
                    <div class="form-group form-md-line-input">
                      <label class="control-label col-md-2">Course</label>
                      <div class="col-md-10">
                        <select class="form-control select2me" name="course_id" data-placeholder="Select..." required>
                          <option value=""></option>
                          <?php
                          foreach($courses as $row){
                            echo "<option value='".$row->course_id."'>".$row->course_code."</option>";
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group form-md-line-input">
                      <label class="control-label col-md-2">Room</label>
                        <div class="col-md-10">
                          <select class="form-control select2me" name="room_id" data-placeholder="Select..." required>
                            <option value=""></option>
                          <?php
                          foreach($rooms as $row){
                            echo "<option value='".$row->room_id."'>".$row->building.$row->room_number."</option>";
                          }
                          ?>
                          </select>
                        </div>
                    </div>

<?php if($this->session->userdata('user_role') != 'instructor'): ?>

                    <div class="form-group form-md-line-input">
                    <label class="col-md-2 control-label">Instructor</label>
                      <div class="col-md-10">
                        <select class="form-control select2me" name="instructor_id" data-placeholder="Select..." required>
                          <option value=""></option>
                          <?php
                          foreach($instructors as $row)
                          {
                            if($row->lastname == NULL || $row->firstname == NULL)
                            {
                              echo "<option value='".$row->school_id."'>".$row->school_id."</option>"; 
                            }
                            else
                            {
                              echo "<option value='".$row->school_id."'>".$row->lastname.", ".$row->firstname."</option>";
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>

<?php endif; ?>

                    <div class="form-group form-md-line-input">
                    <label class="control-label col-md-2">Group Number</label>
                      <div class="col-md-10">
                        <div id="class_groupnumber">
                          <div class="input-group input-large">
                            <input type="number" name="group_number"class="spinner-input form-control" maxlength="3" readonly required>
                            <div class="spinner-buttons input-group-btn btn-group-vertical">
                              <button type="button" class="btn spinner-up btn-xs blue">
                                <i class="fa fa-angle-up"></i>
                              </button>
                              <button type="button" class="btn spinner-down btn-xs blue">
                                <i class="fa fa-angle-down"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group form-md-line-input">
                    <label class="control-label col-md-2">Max Absences</label>
                      <div class="col-md-10">
                        <div id="class_maxabsences">
                          <div class="input-group input-large">
                            <input type="text" name="max_absences" class="spinner-input form-control" maxlength="3" readonly required>
                            <div class="spinner-buttons input-group-btn btn-group-vertical">
                              <button type="button" class="btn spinner-up btn-xs blue">
                                <i class="fa fa-angle-up"></i>
                              </button>
                              <button type="button" class="btn spinner-down btn-xs blue">
                                <i class="fa fa-angle-down"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group form-md-line-input">
                      <label class="col-md-2 control-label">Schedule</label>
                      <div class="col-md-10">
                      <div class="input-group">
                        <div class="icheck-list">
                          <!-- MONDAY -->
                          <label>
                          <input id="checkbox1" type="checkbox" name="day[]" value="M" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="M"> Monday</label>
                            <div id="div1" class="input-group">
                              <input type="text" name="start_time[]" class="form-control timepicker timepicker-no-seconds" name="m-from">
                              <span class="input-group-addon">to </span>
                              <input type="text" name="end_time[]" class="form-control timepicker timepicker-no-seconds" name="m-to">
                            </div>
                          <!-- TUESDAY -->
                          <label class="class-days-label">
                            <input id="checkbox2" type="checkbox" name="day[]" value="T" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="T"> Tuesday</label>
                              <div id="div2" class="input-group">
                                <input type="text" name="start_time[]" class="form-control timepicker timepicker-no-seconds" name="t-from">
                                  <span class="input-group-addon">to </span>
                                <input type="text" name="end_time[]" class="form-control timepicker timepicker-no-seconds" name="t-to">
                              </div>
                          <!-- WEDNESDAY -->
                          <label class="class-days-label">
                            <input id="checkbox3" type="checkbox" name="day[]" value="W" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="W"> Wednesday</label>
                              <div id="div3" class="input-group">
                                <input type="text" name="start_time[]" class="form-control timepicker timepicker-no-seconds" name="w-from">
                                  <span class="input-group-addon">to </span>
                                <input type="text" name="end_time[]" class="form-control timepicker timepicker-no-seconds" name="w-to">
                              </div>
                          <!-- THURSDAY --> 
                          <label class="class-days-label">
                            <input id="checkbox4" type="checkbox" name="day[]" value="TH" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="TH"> Thursday</label>
                              <div id="div4" class="input-group">
                                <input type="text" name="start_time[]" class="form-control timepicker timepicker-no-seconds" name="th-from">
                                  <span class="input-group-addon">to </span>
                                <input type="text" name="end_time[]" class="form-control timepicker timepicker-no-seconds" name="th-to">
                              </div>
                          <!-- FRIDAY -->   
                          <label class="class-days-label">
                            <input id="checkbox5" type="checkbox" name="day" value="F" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="F"> Friday</label>
                              <div id="div5" class="input-group">
                                <input type="text" name="start_time[]" class="form-control timepicker timepicker-no-seconds" name="f-from">
                                  <span class="input-group-addon">to </span>
                                <input type="text" name="end_time[]" class="form-control timepicker timepicker-no-seconds" name="f-to">
                              </div>
                          <!-- SATURDAY -->
                          <label class="class-days-label">
                            <input id="checkbox6" type="checkbox" name="day[]" value="S" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="S"> Saturday</label>
                              <div id="div6" class="input-group">
                                <input type="text" name="start_time[]" class="form-control timepicker timepicker-no-seconds" name="from">
                                  <span class="input-group-addon">to </span>
                                <input type="text" name="end_time[]" class="form-control timepicker timepicker-no-seconds" name="to">
                              </div>
                        </div>
                      </div>
                      </div>
                    </div>
                  <div class="form-actions">
                    <div class="row">
                      <div class="col-md-offset-2 col-md-10">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn blue">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>               
              </div>
            </div>
          </div>
        </div>

        <!-- END PAGE CONTENT-->
      </div>
    </div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->

  <!-- BEGIN FOOTER -->
  <?php require_once 'application/views/includes/footer.phtml';?> 
  <!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
  <?php require_once 'application/views/includes/core_js.phtml';?>  
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/fuelux/js/spinner.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?> 
<!-- END THEME PLUGINS -->

<script>
jQuery(document).ready(function() { 
  ComponentsPickers.init(); 
  AdminPage.init();


});
</script>
<script src="/../assets/admin/pages/scripts/components-pickers.js"></script>
<script src="/../assets/admin/pages/scripts/admin.js"></script> 
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
