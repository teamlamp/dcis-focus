<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Classes</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
  <!-- BEGIN CONTAINER -->
  <div class="page-container">

    <!-- BEGIN SIDEBAR -->
    
    <?php require_once 'application/views/includes/sidebar.phtml';?>  
    <!-- END SIDEBAR -->

        <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">

        <!-- END STYLE CUSTOMIZER -->
        <?php require_once 'application/views/includes/style_customizer.phtml';?> 
        <!-- END STYLE CUSTOMIZER -->

        <h3 class="page-title">
        Classes <small>List</small>
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="icon-notebook"></i>
              <a href="javascript:;">Classes</a>
              <i class="fa fa-angle-double-right"></i>
            </li>
          </ul>
          <div class="page-toolbar">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true" >
              Actions <i class="fa fa-arrow-circle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
              </ul>
            </div>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
          <div class="col-md-12">

             <!-- BEGIN DASHBOARD STATS -->
        <div class="row">
        <!-- Start of list of classes -->
        <?php 
          if($classes){foreach($classes as $class){
        ?>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
          <a type="button" href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}")?>">
              <div class="dashboard-stat <?= $class['tile_color']; ?>">
                <div class="visual">
                  <i class="fa <?= $class['tile_icon']; ?>"></i>
                </div>
                <div class="details">
                  <div class="number" style="padding-top:15px!important">
                    <?= $class['group_number']; ?> <?= $class['course_code']; ?>
                  </div>
                  <div class="desc" style="width:250px!important" >
                    <?= $class['course_title']; ?>
                  </div>
                </div>
                </a>
                <a class="more" href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}")?>">
                <?= $class['building']."".$class['room_number']; ?> | <?php 
          
          $ret = getClassSchedule($class['class_id']); 

          foreach($ret->result() as $row){
            $start = strtotime($row->start_time);
            $end = strtotime($row->end_time);
            echo $row->day." ".date('h:iA', $start)."-".date('h:iA', $end)." | ";
          }

          ?> <i class="fa arrow-down-menu fa-chevron-circle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right ddmcl" style="margin-top:-30px!important; margin-right:15px!important;">
                <?php if($this->session->userdata('user_role') != "student"){?>
                <li>
                  <a href="<?php echo base_url("class/{$class['course_code']}/{$class['group_number']}/students"); ?>"><i class="fa fa-users"></i> Students List</a>
                </li>
                <li>
                  <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/classrecord"); ?>"><i class="fa fa-file-text"></i> Class Record</a>
                </li>
                <li>
                  <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/attendance"); ?>"><i class="fa fa-pencil"></i> Attendance</a>
                </li>
                <?php } else if ($this->session->userdata('user_role') == "student"){?>
                <li>
                  <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
                </li>
                <?php } ?>
                <?php if($this->session->userdata('user_role')!="student"){ ?>
                <li class="divider"></li>
                <li>
                  <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/grades/record"); ?>"><i class="fa fa-save"></i> Record Grades</a>
                </li>
                <li>
                  <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/grades/history"); ?>"><i class="fa fa-history"></i> Record History</a>
                </li>
                <li>
                  <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/grades/rubrics"); ?>"><i class="fa fa-cube"></i> Class Rubrics</a>
                </li>            
                <li class="divider"></li>
                <li>
                  <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}/settings") ?>"><i class="fa fa-cog"></i> Class Settings</a>
                </li>               
                <?php } ?>
              </ul>
              </div>
          </div>
        <?php 
        }}else{
        ?>
        <div class="row">
          <div class="col-md-12">
          <h2>NO Classes Found</h2>
          </div>
        </div>
        <?php
        }
        ?>
        <!-- End of list of classes -->
        </div>

          </div>
        </div>
        <!-- END PAGE CONTENT-->
      </div>
    </div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->

  <!-- BEGIN FOOTER -->
  <?php require_once 'application/views/includes/footer.phtml';?> 
  <!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
  <?php require_once 'application/views/includes/core_js.phtml';?>  
<!-- END CORE PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?> 
<!-- END THEME PLUGINS -->
  
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>