<!DOCTYPE html>
<html lang="en">
<head>
<?php foreach($one_class as $class) ?>
<title>DCIS | Class Record </title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
  <!-- BEGIN CONTAINER -->
  <div class="page-container">

    <!-- BEGIN SIDEBAR -->
    
    <?php require_once 'application/views/includes/sidebar.phtml';?>  
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">

        <!-- BEGIN STYLE CUSTOMIZER -->
        <?php require_once 'application/views/includes/style_customizer.phtml';?> 
        <!-- END STYLE CUSTOMIZER -->

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
        Class Record <small><?= $class['group_number']." ".$class['course_code']; ?></small>
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="icon-notebook"></i>
              <a href="<?= base_url('classes')?>">Classes</a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}")?>"><?= $class['group_number']." ".$class['course_code']; ?></a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="index.html">Class Record</a>
            </li>
          </ul>
          <div class="page-toolbar">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
              Actions <i class="fa fa-arrow-circle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <?php if($this->session->userdata('user_role') != "student"){?>
                <li>
                  <a href="<?php echo base_url("{$current_url_1}/students"); ?>"><i class="fa fa-users"></i> Students List</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/classrecord"); ?>"><i class="fa fa-file-text"></i> Class Record</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/attendance"); ?>"><i class="fa fa-pencil"></i> Attendance</a>
                </li>
                <?php } else if ($this->session->userdata('user_role') == "student"){?>
                <li>
                  <a href="<?= base_url("{$current_url_1}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
                </li>
                <?php } ?>
                <li>
                  <a href="<?= base_url("{$current_url_1}/calendar"); ?>"><i class="fa fa-calendar"></i> Class Calendar</a>
                </li>
                <?php if($this->session->userdata('user_role')!="student"){ ?>
                <li class="divider"></li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/grades/record"); ?>"><i class="fa fa-save"></i> Record Grades</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/grades/history"); ?>"><i class="fa fa-history"></i> Record History</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/grades/rubrics"); ?>"><i class="fa fa-cube"></i> Class Rubrics</a>
                </li>             
                <li class="divider">
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/settings") ?>"><i class="fa fa-cog"></i> Class Settings</a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        
        <!-- BEGIN PAGE CONTENT-->
        <?php
          // global variables
        $class_total = $rubtot[0]['class_grade_sf'];
        ?>
        <div class="row">
          <div class="col-md-12">
        <?php if($student_grades){?>
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-globe"></i>Class Record
                </div>
              </div>
              <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="classRecord">
                <?php $colmax = array();?>
                  <thead>
                    <tr>
                      <th colspan="2" style="border-bottom:1px solid #ddd"><center>Names</center></th>
                      <?php foreach($rubrics as $rubric){?>
                      <?php $colmax[] = $rubric['countActivity']; ?>
                      <th colspan="<?= $rubric['countActivity'] ?>" style="border-bottom:1px solid #ddd"><center><?= $rubric['type']." <br> ".$rubric['percent']?>%</center></th>
                      <?php } ?>  
                      <th style="border-bottom:1px solid #ddd;border-right:none">Raw Total<br><?= $rubtot[0]['class_grade_sf'] ?></th>
                      <th style="border-left:1px solid #ddd" rowspan="2">Equivalent</th>
                      <th style="border-left:1px solid #ddd" rowspan="2">Status</th>
                    </tr>
                    <tr>
                      <th>First <br>Name</th>
                      <th>Last <br>Name</th>
                      <?php foreach($activities as $activity){ ?>
                      <th><?= $activity['activity_title']."<br>".$activity['perfect_score']."" ?></th>
                      <?php } ?>
                      <th>Passing<br><?= $rubtot[0]['class_grade_sf']/2 ?></th>
                    </tr>
                  </thead>
                  <?php 
                  // Storing the names to an array
                  $alllnames = array();
                  $allfnames = array();
                  $allgrades_sf = array();
                  foreach($students as $student)
                  {
                    $alllnames[] = $student['lastname'];
                    $allfnames[] = $student['firstname'];
                    $allgrades_sf[] = $student['cm_grade_sf'];
                  }
                  $studnum = sizeof($alllnames);
                  // Storing the grades to an array
                  $allgrades = array();
                  $allgradeid = array();
                  foreach($student_grades as $grade){
                    $allgrades[] = $grade['score'];
                    $allgradeid[] = $grade['grade_id'];
                  }
                  $gradesnum = sizeof($allgrades);

                  $newgrades = array(array());
                  $newgradeid = array(array());

                  for($y=0;$y<$gradesnum;$y++){
                    $mod = $y % $studnum;
                    $newgrades[$mod][$y] = $allgrades[$y];
                    $newgradeid[$mod][$y] = $allgradeid[$y];
                  }

                  $flatgrades2 = array(array());
                  $flatgradeid2 = array(array());

                  for($x=0;$x<$studnum;$x++){
                    $flatgrades2[$x] = call_user_func('array_merge', $newgrades[$x]);
                    $flatgradeid2[$x] = call_user_func('array_merge', $newgradeid[$x]);
                  }
                  ?>
                  <br><br>
                  <tbody>
                    <?php for($row = 0;$row<$studnum;$row++){?>
                    <tr>
                      <td><?php echo $allfnames[$row]; ?></td>
                      <td><?php echo $alllnames[$row]; ?></td>
                      <?php for($i = 0; $i < $gradesnum/$studnum; $i++){?>
                       <td id="<?= $flatgradeid2[$row][$i] ?>" class="upgradecl"><?= $flatgrades2[$row][$i] ?></td>
                      <?php } ?>
                      <td><?= $allgrades_sf[$row] ?></td>
                      <?php $equi = round((5-($allgrades_sf[$row]*(4/$class_total))),2);?>
                      <td><?= $equi ?></td>
                      <td><?php $pass = $rubtot[0]['class_grade_sf']/2; if($allgrades_sf[$row]>=$pass) echo "Pass"; else echo "Fail";?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <span class="label label-danger">NOTE</span><em> Double click a grade to change it.</em>
              </div>
            </div>
        <?php }else {?>
            <div class="note note-danger">
                <h4 class="block">Class record is still empty. </h4>
                <p>Start recording scores (Actions > Record Grades).</p>
            </div>
        <?php }?>
          </div>
        </div>

</div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->
</div>
  <!-- BEGIN FOOTER -->
  <?php require_once 'application/views/includes/footer.phtml';?> 
  <!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
  <?php require_once 'application/views/includes/core_js.phtml';?>  
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/fuelux/js/spinner.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?>
  <script>
      jQuery(document).ready(function() {    
        DcisTableAdvanced.init();
        AdminPage.init();
      });
      var classname = <?php echo json_encode($class['course_code']); ?>; 
      var groupnumber = <?php echo json_encode($class['group_number']); ?>; 
   </script>
    <script src="/../assets/dcis/js/dcis-table-advance.js"></script>

  <script src="/../assets/admin/pages/scripts/admin.js"></script> 
  
<!-- END THEME PLUGINS -->
  
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>