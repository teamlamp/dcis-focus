<!DOCTYPE html>
<html lang="en">
<head>
<?php foreach($one_class as $class) ?>
<title>DCIS | Grade Rubrics</title>
<input type="hidden" id="class_id" value="<?= $class['class_id']; ?>">

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
  <!-- BEGIN CONTAINER -->
  <div class="page-container">

    <!-- BEGIN SIDEBAR -->
    
    <?php require_once 'application/views/includes/sidebar.phtml';?>  
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">

        <!-- END STYLE CUSTOMIZER -->
        <?php require_once 'application/views/includes/style_customizer.phtml';?> 
        <!-- END STYLE CUSTOMIZER -->

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
        Rubrics <small><?= $class['group_number']." ".$class['course_code']; ?></small>
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="icon-notebook"></i>
              <a href="<?= base_url('classes')?>">Classes</a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}")?>"><?= $class['group_number']." ".$class['course_code']; ?></a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="javascript:;">Rubrics</a>
            </li>
          </ul>
          <div class="page-toolbar">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
              Actions <i class="fa fa-arrow-circle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <?php if($this->session->userdata('user_role') != "student"){?>
                <li>
                  <a href="<?php echo base_url("{$current_url_2}/students"); ?>"><i class="fa fa-users"></i> Students List</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/classrecord"); ?>"><i class="fa fa-file-text"></i> Class Record</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/attendance"); ?>"><i class="fa fa-pencil"></i> Attendance</a>
                </li>
                <?php } else if ($this->session->userdata('user_role') == "student"){?>
                <li>
                  <a href="<?= base_url("{$current_url_2}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
                </li>
                <?php } ?>
                <li>
                  <a href="<?= base_url("{$current_url_2}/calendar"); ?>"><i class="fa fa-calendar"></i> Class Calendar</a>
                </li>
                <?php if($this->session->userdata('user_role')!="student"){ ?>
                <li class="divider"></li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/grades/record"); ?>"><i class="fa fa-save"></i> Record Grades</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/grades/history"); ?>"><i class="fa fa-history"></i> Record History</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/grades/rubrics"); ?>"><i class="fa fa-cube"></i> Class Rubrics</a>
                </li>             
                <li class="divider">
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/settings") ?>"><i class="fa fa-cog"></i> Class Settings</a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
              <div class="col-md-6">
                <div class="portlet light">
                    <div class="portlet-title">
                      <div class="caption font-blue">
                        <i class="fa fa-th-list font-blue"></i>
                        <span class="caption-subject bold uppercase"> Manage Rubrics</span>
                        <span class="caption-helper">create/modify your rubrics ...</span>
                      </div>
                   <!--   <div class="tools">
                        <a href="javascrcipt:;" class="collapse"></a>
                      </div> -->
                    </div>
                    <div class="portlet-body form">
                      <!-- BEGIN FORM-->
                      <form action="#" class="form-horizontal">
                      <div class="btntoright">
                          <a href="javascript:;" style="margin-right:10px" class="btn grey-gallery" id="add-category">
                          <i class="fa fa-plus"></i>Add Category</a></div>
                        <div class="form-body">
                        <div class="table-scrollable">
                          <table class="table table-bordered table-hover" id="categories-tables">
                          <thead>
                          <tr>
                            <th>Category</th>
                            <th>Percentage out of a 100%</th>
                            <th></th>
                          </tr>
                          </thead>
                            <tbody id="categories-tbody">
                              <?php if($rubrics){foreach($rubrics as $rubric){?>
                              <!-- if there is rubrics already then just display -->
                              <tr class="set-category set-row">
                                <td style="display:none;"><span class='formula-id'><?= $rubric['formula_id']; ?></span></td>
                                <td style="width:164px;"><span class="category-name"><?= $rubric['type']; ?></span></td>
                                <td style="width:257px;"><span class="category-percentage"><?= $rubric['percent'] ?></span></td>
                                <td style="width:30px;"><i class="fa fa-pencil edit-type"></i><i class="fa fa-check set-type" style="display:none;"></i><i class="fa fa-trash-o remove-type"></i></td>
                              </tr>
                              <?php }} else if(!($rubrics)) { ?>
                              <!-- if no rubrics then -->
                              <tr class="unset-category set-row">
                                <td style="width:164px;"><input type="text" class="form-control input-small category-name" placeholder="Type"></td>
                                <td style="width:257px;"><input type="number" class="form-control input-small category-percentage" placeholder="Enter percentage" max="100" min="1" maxlength="3"></td>
                                <td style="width:30px;"><i class="fa fa-pencil edit-type" style="display:none;"></i><i class="fa fa-check set-type"></i><i class="fa fa-trash-o remove-type"></i></td>
                              </tr>
                              <?php } ?>
                            </tbody>
                          </table>
                        </div>
                        </div>
                        <div class="form-actions fluid">
                          <div class="row">
                            <div class="col-md-offset-1 pull-left">
                              <h4>Total: <span style="text-decoration:underline;" id="rubric-total"></span> percent</h4>
                            </div>
                            <div class="col-md-offset-1 pull-right" style="padding-right:45px!important;">
                              <a href="javascript:;" class="btn grey-gallery" id="save-rubrics" disabled>
                                <i class="fa fa-check"></i> Save Rubrics </a>
                            </div>
                          </div>
                        </div>
                      </form>
                      <!-- END FORM-->
                      <span class="label label-danger">NOTE</span><em> Refresh page to cancel any unsaved changes.</em>
                    </div>
                  </div>
                  <!-- END OF PORTLET -->
              </div>
              <div class="col-md-6">
                <div class="portlet light">
                  <div class="portlet-title">
                    <div class="caption font-green-haze">
                      <i class="icon-bar-chart font-green-haze"></i>
                      <span class="caption-subject bold uppercase">Rubrics Chart</span>
                      <span class="caption-helper">rubric representation ...</span>
                    </div>
                  <!--  <div class="tools">
                      <a href="javascrcipt:;" class="collapse"></a>
                    </div> -->
                  </div>
                  <div class="portlet-body">
                    <div id="chart-contain">
                      <div id="rubrics_chart" class="chart"></div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div><!--End of row-->
      </div>
    </div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN FOOTER -->
  <?php require_once 'application/views/includes/footer.phtml';?> 
  <!-- END FOOTER -->
</div>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
  <?php require_once 'application/views/includes/core_js.phtml';?>  
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script src="/../assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?> 
<!-- END THEME PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
  
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>