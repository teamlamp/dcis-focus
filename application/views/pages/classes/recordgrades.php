<!DOCTYPE html>
<html lang="en">
<head>
<?php foreach($one_class as $class) ?>
<title>DCIS | Record Grades</title>
<input type="hidden" id="class_id" value="<?= $class['class_id']; ?>">

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
  <!-- BEGIN CONTAINER -->
  <div class="page-container">

    <!-- BEGIN SIDEBAR -->
    
    <?php require_once 'application/views/includes/sidebar.phtml';?>  
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">

        <!-- END STYLE CUSTOMIZER -->
        <?php require_once 'application/views/includes/style_customizer.phtml';?> 
        <!-- END STYLE CUSTOMIZER -->

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
        Record Grades <small><?= $class['group_number']." ".$class['course_code']; ?></small>
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="icon-notebook"></i>
              <a href="<?= base_url('classes')?>">Classes</a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}")?>"><?= $class['group_number']." ".$class['course_code']; ?></a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="javascript:;">Record Grades</a>
            </li>
          </ul>
          <div class="page-toolbar">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
              Actions <i class="fa fa-arrow-circle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <?php if($this->session->userdata('user_role') != "student"){?>
                <li>
                  <a href="<?php echo base_url("{$current_url_2}/students"); ?>"><i class="fa fa-users"></i> Students List</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/classrecord"); ?>"><i class="fa fa-file-text"></i> Class Record</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/attendance"); ?>"><i class="fa fa-pencil"></i> Attendance</a>
                </li>
                <?php } else if ($this->session->userdata('user_role') == "student"){?>
                <li>
                  <a href="<?= base_url("{$current_url_2}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
                </li>
                <?php } ?>
                <li>
                  <a href="<?= base_url("{$current_url_2}/calendar"); ?>"><i class="fa fa-calendar"></i> Class Calendar</a>
                </li>
                <?php if($this->session->userdata('user_role')!="student"){ ?>
                <li class="divider"></li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/grades/record"); ?>"><i class="fa fa-save"></i> Record Grades</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/grades/history"); ?>"><i class="fa fa-history"></i> Record History</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/grades/rubrics"); ?>"><i class="fa fa-cube"></i> Class Rubrics</a>
                </li>             
                <li class="divider">
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/settings") ?>"><i class="fa fa-cog"></i> Class Settings</a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        <!-- END PAGE HEADER-->        
        <!-- BEGIN PAGE CONTENT-->
        <!-- Begin 1st Row -->
        <!-- First Column -->
        <div class="row">
          <div class="col-md-4">
        <?php if($rubrics){?>
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-cubes"></i>Rubric Type                
                </div>
              </div>
              <div class="portlet-body form">
                <div class="form-body">
                    <select class="form-control input-inline input-medium" id="rubric-type">
                      <option value="0"> </option>
                    <?php foreach($rubrics as $rubric){?>
                      <option value="<?= $rubric['formula_id']; ?>"><?= ucfirst($rubric['type']) ?></option>
                    <?php }?>
                    </select>
                    <span class="help-inline" id="rubric-type-error" style="visibility:hidden;color:red;"> Please choose. </span>
                </div>
              </div>
            </div>
        <?php } else { ?>  <!-- if no rubrics created -->        
                  <div class="note note-danger">
                      <h4 class="block">No rubrics set.</h4>
                      <p><a href="<?= base_url("{$current_url_1}/rubrics"); ?>">Please create one.</a></p>
                  </div>
        <?php } ?>
          </div>
        <?php if($rubrics){?>
          <!-- Second Column -->
          <div class="col-md-4">
            <div class="portlet box red">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-font"></i> Activity Title
                </div>
              </div>
              <div class="portlet-body form">
                  <form class="form-body">
                    <div class="input-inline input-large  ">
                      <!-- <i class="fa fa-edit"></i> -->
                      <input type="text" class="form-control input-medium" id="activity-title-text" placeholder="Ex. Assignment 1, Midterm Exam">
                    
                    </div>
                    <span class="help-inline" id="activity-title-error" style="visibility:hidden;color:red;"> Cannot be blank. </span>
                  </form>
              </div>
            </div>
          </div>

          <!-- Third Column -->
          <div class="col-md-4">
            <div class="portlet box green">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-check-square-o"></i>Perfect Score
                </div>    
                <div class="actions">
                  <a id="set-perfect-score-btn" href="javascript:;" class="btn grey-gallery btn-sm">
                  <i class="fa fa-check" id="set-perfect-score-icon"></i> <span id="spsl">Set</span> </a>    
                </div>    
              </div>
              <div class="portlet-body form">
                  <form class="form-body">
                    <div class="input-inline input-large  ">
                      <!-- <i class="fa fa-edit" id="spsi"></i> -->
                      <input type="number" class="form-control input-medium" id="set-perfect-score-text" placeholder="Perfect Score" min="1">                    
                    </div>
                    <span class="help-inline" id="perfect-score-error" style="visibility:hidden;color:red;"> Need to set a value greater than 0. </span>
                  </form>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <!-- End 1st Row -->
        <!-- Begin 2nd Row -->
        <?php if($rubrics){?>
        <?php if($students){?>
        <div class="row">
          <div class="col-md-12">
            <div class="portlet box grey-gallery">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-globe"></i>All Students
                </div>
              </div>
              <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover" id="recordGrade">
                <thead>
                  <tr>
                    <th>ID </br> Number</th>
                    <th>Last </br> Name</th>
                    <td style="display:none;"></td>
                    <th>First </br> Name</th>
                    <th>Grade</th>
                    <th>Total Score</th>
                  </tr>
                </thead>
                <tbody>
                <?php if($students){foreach($students as $student){ ?>
                  <tr>
                    <td><?= $student['school_id']; ?></td>
                    <td><?= $student['lastname'];  ?></td>
                    <td style="display:none;"><span class="student_id"><?= $student['class_member_id']; ?></span></td>
                    <td><?= $student['firstname']; ?></td>
                    <td><input type="number" class="student-score form-control" min="0" max="50"></td>
                    <td class="table-perfect-score">-</td>
                  </tr>
                <?php }} ?>
                </tbody>
                </table>
              <center><button type="button" class='btn btn-success pull-center' id="record-grades">Record</button></center><br><br>
              </div>
            </div>
          </div>
        </div>
        <?php }else{?>
          <div class="note note-danger">
              <h4 class="block">No students in this class yet !</h4>
              <p>Please add your students so you can record activity scores.</p>
          </div>
        <?php } ?>
        <?php }?>
        <!-- End 2nd Row-->


    </div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->
</div>
  <!-- BEGIN FOOTER -->
  <?php require_once 'application/views/includes/footer.phtml';?> 
  <!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
  <?php require_once 'application/views/includes/core_js.phtml';?>  
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="/../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?> 
  <script>
      jQuery(document).ready(function() {    
        DcisTableAdvanced.init();
        ComponentsDropdowns.init();
      });

      var classname = <?php echo json_encode($class['course_code']); ?>; 
      var groupnumber = <?php echo json_encode($class['group_number']); ?>; 
   </script>
    <script src="/../assets/dcis/js/dcis-table-advance.js"></script>
  <script src="/../assets/admin/pages/scripts/components-dropdowns.js"></script>

<!-- END THEME PLUGINS -->
  
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>