<!-- BEGIN CLASS INFORMATION-->
<div class="tab-pane active" id="tab_1_1">
<hr style="margin-bottom:15px!important;margin-top:0px!important">
	<form  class="form-horizontal" role="form">
		<div class="form-body">
			
			<div class="form-group">
				<label class="col-md-3 control-label">Class Name</label>
				<div class="col-md-9">
					<input type="text" class="form-control input-large" placeholder="ICT 141">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Class Group No.</label>
				<div class="col-md-9">
					<input type="number" class="form-control input-large" placeholder="2" min="1">	
				</div>
			</div>


			<div class="form-group">
				<label class="col-md-3 control-label">Class Schedule</label>
				<div class="col-md-9">
					<div class="input-group">
						<div class="icheck-list">
						<!-- MONDAY -->
						<label>
						<input id="checkbox1" type="checkbox" name="" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="M"> Monday</label>
							<div id="div1" class="input-group">
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="m-from">
								<span class="input-group-addon">to </span>
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="m-to">
							</div>
						<!-- TUESDAY -->
						<label class="class-days-label">
						<input id="checkbox2" type="checkbox" name="" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="T"> Tuesday</label>
							<div id="div2" class="input-group">
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="t-from">
								<span class="input-group-addon">to </span>
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="t-to">
							</div>
						<!-- WEDNESDAY -->
						<label class="class-days-label">
						<input id="checkbox3" type="checkbox" name="" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="W"> Wednesday</label>
							<div id="div3" class="input-group">
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="w-from">
								<span class="input-group-addon">to </span>
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="w-to">
							</div>
						<!-- THURSDAY --> 
						<label class="class-days-label">
						<input id="checkbox4" type="checkbox" name="" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="TH"> Thursday</label>
							<div id="div4" class="input-group">
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="th-from">
								<span class="input-group-addon">to </span>
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="th-to">
							</div>
						<!-- FRIDAY -->   
						<label class="class-days-label">
						<input id="checkbox5" type="checkbox" name="" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="F"> Friday</label>
							<div id="div5" class="input-group">
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="f-from">
								<span class="input-group-addon">to </span>
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="f-to">
							</div>
						<!-- SATURDAY -->
						<label class="class-days-label">
						<input id="checkbox6" type="checkbox" name="" class="icheckbox_square-grey" data-checkbox="icheckbox_square-grey" value="S"> Saturday</label>
							<div id="div6" class="input-group">
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="from">
								<span class="input-group-addon">to </span>
								<input type="text" class="form-control timepicker timepicker-no-seconds" name="to">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Instructor</label>
					<div class="col-md-4">
						<select class="form-control input-large select2me" data-placeholder="Glen Pepito">
							<option value=""></option>
							<option value="">Angie Ceniza</option>
							<option value="">Tok Mendoza</option>
							<option value="">Joan tero</option>
							<option value="">Mary Jane Sabellano</option>
							<option value="">Archival Sevial</option>
							<option value="">Glen Pepito</option>
						</select>
					</div>															
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Students</label>
				<div class="col-md-9">
					<button type="button" class="btn green-haze"><i class="fa fa-plus"></i> Add</button>	
					<button type="button" class="btn default"><i class="fa fa-plus"></i> Import CSV</button>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Class Admin</label>
					<div class="col-md-9">
						<select class="form-control input-large select2me" data-placeholder="Angie Ceniza">
							<option value=""></option>
							<option value="">Angie Ceniza</option>
							<option value="">Tok Mendoza</option>
							<option value="">Joan tero</option>
							<option value="">Mary Jane Sabellano</option>
							<option value="">Archival Sevial</option>
							<option value="">Glen Pepito</option>
						</select>
					<button type="button" class="btn green-haze" style="margin-left:8px;"><i class="fa fa-plus"></i></button>	
					</div>
			</div>

			<hr style="margin:0px!important">
			<div class="form-actions btntoright">
				<button type="submit" class="btn green-haze">Save Changes</button>
				<button type="button" class="btn default">Cancel</button>
			</div>
		</div>
	</form>
</div>
<!-- END CLASS INFORMATION -->
