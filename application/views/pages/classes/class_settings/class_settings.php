						<div class="profile-content">
							<div class="row">
								<div class="col-md-12">
									<div class="portlet light">
										<div class="portlet-title tabbable-line">

											<ul class="nav nav-tabs" style="float: left;">
												<li class="active">
													<a href="#tab_1_1" data-toggle="tab">Class Information</a>
												</li>
												<li>
													<a href="#tab_1_2" data-toggle="tab">Class Preference</a>
												</li>
												<li>
													<a href="#tab_1_3" data-toggle="tab">Class Notification</a>
												</li>
											</ul>
										</div>
										<div class="portlet-body">
											<div class="tab-content">
												<!-- CLASS INFORMATION TAB -->
												<div class="tab-pane active" id="tab_1_1">
													<form  class="form-horizontal" role="form">
														<div class="form-body">
															
															<div class="form-group">
																<label class="col-md-3 control-label">Class name</label>
																<div class="col-md-9">
																	<input type="text" class="form-control" placeholder="ICT 141">
																</div>
															</div>

															<div class="form-group last password-strength">
																<label class="col-md-3 control-label">Class group</label>
																<div class="col-md-9">
																	<input type="number" class="form-control" placeholder="2">	
																</div>
															</div>


															<div class="form-group">
																<label class="col-md-3 control-label">Schedule</label>
																<div class="col-md-9">
																	<input type="text" class="form-control">
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label">Instructor</label>
																	<div class="col-md-4">
																		<select class="form-control input-large select2me" data-placeholder="Angie Ceniza">
																			<option value=""></option>
																			<option value="AC">Angie Ceniza</option>
																			<option value="TM">Tok Mendoza</option>
																		</select>
																	</div>															
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label">Students</label>
																<div class="col-md-9">
															<button type="button" class="btn green-haze"><i class="fa fa-plus"></i> Add</button>	
															<button type="button" class="btn default"><i class="fa fa-plus"></i> Import CSV</button>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-3 control-label">Class Admin</label>
																	<div class="col-md-9">
																		<select class="form-control input-medium select2me" data-placeholder="Angie Ceniza">
																			<option value=""></option>
																			<option value="AC">Angie Ceniza</option>
																			<option value="TM">Tok Mendoza</option>
																		</select>
															<button type="button" class="btn green-haze" style="margin-top: -55px; margin-left: 250px;"><i class="fa fa-plus"></i></button>	
																	</div>	

															</div>

														</div>
														<div class="form-actions right">
															<button type="submit" class="btn green-haze">Save Changes</button>
															<button type="button" class="btn default">Cancel</button>
														</div>
													</form>
												</div>
												<!-- END CLASS INFORMATION TAB -->
												<!-- CLASS PREFERENCE TAB -->
												<div class="tab-pane" id="tab_1_2">
													<form  class="form-horizontal" role="form">
														<div class="form-body">
															
															<div class="form-group">
																<label class="col-md-3 control-label">Class Tile Color</label>
																	<div class="col-md-9">
																		<select class="form-control input-large select2me" data-placeholder="Angie Ceniza">
																			<option value=""></option>
																			<option value="AC">Red</option>
																			<option value="TM">Blue</option>
																		</select>
																	</div>	
															</div>
															<div class="form-group">
																<label class="col-md-3 control-label">Class Tile Icon</label>
																	<div class="col-md-9">
																		<select class="form-control input-large select2me" data-placeholder="Angie Ceniza">
																			<option value=""></option>
																			<option value="AC">Database</option>
																			<option value="TM">Add</option>
																		</select>
																	</div>	

															</div>


															<div class="form-group">
																<label class="col-md-3 control-label">Dropdown Actions</label>
																	<div class="col-md-9">
																		<select class="form-control input-medium select2me" data-placeholder="Angie Ceniza">
																			<option value=""></option>
																			<option value="AC">Database</option>
																			<option value="TM">Add</option>
																		</select>
															<button type="button" class="btn green-haze" style="margin-top: -55px; margin-left: 250px;"><i class="fa fa-plus"></i></button>	
																	</div>	
															</div>

															<table class="table table-hover" >
																<tr>
																	<td style="border-top:none; width: 40%;">
																		<label class="col-md-8 control-label">Enable Latest Activity </label> 
																	</td>
																	<td style="border-top:none;">
																		<input type="checkbox" checked class="make-switch" data-size="small">
																	</td>
																</tr>
																<tr>
																	<td style="border-top:none;">
																		<label class="col-md-8 control-label">Enable Callouts</label>
																	</td>
																	<td style="border-top:none;">
																		<input type="checkbox" checked class="make-switch" data-size="small">
																	</td>
																</tr>
																<tr>
																	<td style="border-top:none;">
																		<label class="col-md-8 control-label">Students Edit Post</label>
																	</td>
																	<td style="border-top:none;">
																		<input type="checkbox" checked class="make-switch" data-size="small">
																	</td>
																</tr>
																<tr>
																	<td style="border-top:none;">
																		<label class="col-md-8 control-label">Students Delete Post</label>
																	</td>
																	<td style="border-top:none;">
																		<input type="checkbox" checked class="make-switch" data-size="small">
																	</td>
																</tr>
																<tr>
																	<td style="border-top:none;">
																		<label class="col-md-8 control-label">Enable Chatbox</label>
																	</td>
																	<td style="border-top:none;">
																		<input type="checkbox" checked class="make-switch" data-size="small">
																	</td>
																</tr>
																<tr>
																	<td style="border-top:none;">
																		<label class="col-md-8 control-label">Enable Post File Upload</label>
																	</td>
																	<td style="border-top:none;">
																		<input type="checkbox" checked class="make-switch" data-size="small">
																	</td>
																</tr>
															</table>

														</div>
														<div class="form-actions right">
															<button type="submit" class="btn green-haze">Save Changes</button>
															<button type="button" class="btn default">Cancel</button>
														</div>
													</form>
												</div>
												<!-- END CLASS PREFERENCE TAB -->
												<!-- CLASS NOTIFICATION TAB -->
												<div class="tab-pane" id="tab_1_3">
													<form  class="form-horizontal" role="form">
														<div class="form-body">
															<table class="table table-hover">
																<tr>
																	<td style="border-top:none; width:40%;">
																		<label class="col-md-8 control-label">Enable Internal Notification</label> 
																	</td>
																	<td style="border-top:none;">
																		<input type="checkbox" checked class="make-switch" data-size="small">
																	</td>
																</tr>
																<tr>
																	<td style="border-top:none;">
																		<label class="col-md-8 control-label">Enable External Notification</label>
																	</td>
																	<td style="border-top:none;">
																		<input type="checkbox" checked class="make-switch" data-size="small">
																	</td>
																</tr>
																<tr class="icheck-inline" >
																	<td style="border-top:none;">
																		<label class="col-md-8 control-label">Notify VIA Email</label>
																	</td>
																	<td style="border-top:none;">
																		<input type="checkbox" checked class="icheck">
																	</td>
																</tr>
																<tr class="icheck-inline" >
																	<td style="border-top:none;">
																		<label class="col-md-8 control-label">Notify VIA SMS</label>
																	</td>
																	<td style="border-top:none;">
																		<input type="checkbox" checked class="icheck">
																	</td>
																</tr>
															</table>

														<div class="form-actions right">
															<button type="submit" class="btn green-haze">Save Changes</button>
															<button type="button" class="btn default">Cancel</button>
														</div>
														</div>
													</form>
												</div>
												<!-- END CLASS NOTIFICATION TAB -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>