<!-- BEGIN CLASS PREFERENCE -->
<div class="tab-pane" id="tab_1_2">
<hr style="margin-bottom:15px!important;margin-top:0px!important">
	<form  class="form-horizontal" role="form">
		<div class="form-body">
		<!--	
			<div class="form-group">
				<label class="col-md-3 control-label">Class Tile Color</label>
						<button type="button" class="btn <?= $class['tile_color'] ?>" id="tile-color">Preview</button>
						<select class="form-control input-medium" required id="clsprftlcl">
							<option <?php if ($class['tile_color'] == "red"): ?> selected="selected"<?php endif; ?> value="red">Red</option>
							<option <?php if ($class['tile_color'] == "blue"): ?> selected="selected"<?php endif; ?> value="blue">Blue</option>
							<option <?php if ($class['tile_color'] == "yellow"): ?> selected="selected"<?php endif; ?> value="yellow">Yellow</option>
							<option <?php if ($class['tile_color'] == "green"): ?> selected="selected"<?php endif; ?> value="green">Green</option>
							<option <?php if ($class['tile_color'] == "purple"): ?> selected="selected"<?php endif; ?> value="purple">Purple</option>
							<option <?php if ($class['tile_color'] == "grey"): ?> selected="selected"<?php endif; ?> value="grey">Grey</option>
						</select>
			</div>
			-->
			<div class="form-group">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<label class="col-md-7 control-label">Class Tile Color &nbsp;&nbsp;
				<span class="btn-group btn-group">
						<button type="button" class="btn <?= $class['tile_color'] ?>" id="tile-color">Preview &nbsp;</button>
						<select class="form-control input-medium" required id="clsprftlcl">
							<option <?php if ($class['tile_color'] == "red"): ?> selected="selected"<?php endif; ?> value="red">Red</option>
							<option <?php if ($class['tile_color'] == "blue"): ?> selected="selected"<?php endif; ?> value="blue">Blue</option>
							<option <?php if ($class['tile_color'] == "yellow"): ?> selected="selected"<?php endif; ?> value="yellow">Yellow</option>
							<option <?php if ($class['tile_color'] == "green"): ?> selected="selected"<?php endif; ?> value="green">Green</option>
							<option <?php if ($class['tile_color'] == "purple"): ?> selected="selected"<?php endif; ?> value="purple">Purple</option>
							<option <?php if ($class['tile_color'] == "grey"): ?> selected="selected"<?php endif; ?> value="grey">Grey</option>
						</select>
                  </span></label>
            </div>
			<div class="form-group">
				<label class="col-md-3 control-label">Class Tile Icon</label>
					<div class="col-md-9">
						<select class="form-control input-medium" required style="font-family:'FontAwesome';" id="clsprftlcn">
							<option <?php if ($class['tile_icon'] == "fa-plus"): ?> selected="selected"<?php endif; ?> value="fa-plus">&#xf067; Add</option>
							<option <?php if ($class['tile_icon'] == "fa-archive"): ?> selected="selected"<?php endif; ?> value="fa-archive">&#xf187; Archive</option>
							<option <?php if ($class['tile_icon'] == "fa-briefcase"): ?> selected="selected"<?php endif; ?> value="fa-briefcase">&#xf0b1; Briefcase</option>
							<option <?php if ($class['tile_icon'] == "fa-camera"): ?> selected="selected"<?php endif; ?> value="fa-camera">&#xf030; Camera</option>
							<option <?php if ($class['tile_icon'] == "fa-cloud"): ?> selected="selected"<?php endif; ?> value="fa-cloud">&#xf0c2; Cloud</option>
							<option <?php if ($class['tile_icon'] == "fa-cog"): ?> selected="selected"<?php endif; ?> value="fa-cog">&#xf013; Cog</option>
							<option <?php if ($class['tile_icon'] == "fa-cube"): ?> selected="selected"<?php endif; ?> value="fa-cube">&#xf1b2; Cube </option>
							<option <?php if ($class['tile_icon'] == "fa-cubes"): ?> selected="selected"<?php endif; ?> value="fa-cubes">&#xf1b3; Cubes</option>
							<option <?php if ($class['tile_icon'] == "fa-dashboard"): ?> selected="selected"<?php endif; ?> value="fa-dashboard">&#xf0e4; Dashboard</option>
							<option <?php if ($class['tile_icon'] == "fa-database"): ?> selected="selected"<?php endif; ?> value="fa-database">&#xf1c0; Database</option>
							<option <?php if ($class['tile_icon'] == "fa-desktop"): ?> selected="selected"<?php endif; ?> value="fa-desktop">&#xf108; Desktop</option>							
							<option <?php if ($class['tile_icon'] == "fa-gamepad"): ?> selected="selected"<?php endif; ?> value="fa-gamepad">&#xf11b; Gamepad</option>
							<!-- <option <?php if ($class['tile_icon'] == "fa-hdd"): ?> selected="selected"<?php endif; ?> value="fa-hdd">&#xf0a0; Hard Drive</option> -->
							<option <?php if ($class['tile_icon'] == "fa-institution"): ?> selected="selected"<?php endif; ?> value="fa-institution">&#xf19c; Institution</option>
							<option <?php if ($class['tile_icon'] == "fa-image"): ?> selected="selected"<?php endif; ?> value="fa-image">&#xf03e; Image</option>
							<option <?php if ($class['tile_icon'] == "fa-microphone"): ?> selected="selected"<?php endif; ?> value="fa-microphone">&#xf130; Microhpone</option>
							<option <?php if ($class['tile_icon'] == "fa-mobile"): ?> selected="selected"<?php endif; ?> value="fa-mobile">&#xf10b; Mobile Phone</option>
							<option <?php if ($class['tile_icon'] == "fa-print"): ?> selected="selected"<?php endif; ?> value="fa-print">&#xf02f; Printer</option>
							<option <?php if ($class['tile_icon'] == "fa-star"): ?> selected="selected"<?php endif; ?> value="fa-star">&#xf005; Star</option>
							<option <?php if ($class['tile_icon'] == "fa-suitcase"): ?> selected="selected"<?php endif; ?> value="fa-suitcase">&#xf0f2; Suitcase</option>
							<option <?php if ($class['tile_icon'] == "fa-trophy"): ?> selected="selected"<?php endif; ?> value="fa-trophy">&#xf091; Trophy</option>
							<option <?php if ($class['tile_icon'] == "fa-user"): ?> selected="selected"<?php endif; ?> value="fa-user">&#xf007; User</option>
							<option <?php if ($class['tile_icon'] == "fa-users"): ?> selected="selected"<?php endif; ?> value="fa-users">&#xf0c0; Users</option>
							<option <?php if ($class['tile_icon'] == "fa-volume-up"): ?> selected="selected"<?php endif; ?> value="fa-volume-up">&#xf028; Volume</option>
							<option <?php if ($class['tile_icon'] == "fa-video-camera"): ?> selected="selected"<?php endif; ?> value="fa-video-camera">&#xf03d; Video Camera</option>
						</select>
					</div>	

			</div>

			<!-- START SWITCHES -->
			<div class="col-md-12" style="margin-bottom:15px;border-bottom:1px solid #eee">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-6 control-label">Enable Latest Activity</label>
							<div class="col-md-6">
							<?php if($class['enable_latest_activity'] == '1'){ ?>
								<input type="checkbox" checked class="make-switch" data-size="small" id="clsprfla">
							<?php } else { ?>
								<input type="checkbox" class="make-switch" data-size="small" id="clsprfla">
							<?php } ?>
							</div>
					</div>

					<div class="form-group">
						<label class="col-md-6 control-label">Enable Callouts</label>
							<div class="col-md-6">
							<?php if($class['enable_callouts'] == '1'){ ?>
								<input type="checkbox" checked class="make-switch" data-size="small" id="clsprfcts">
							<?php } else { ?>
								<input type="checkbox" class="make-switch" data-size="small" id="clsprfcts">
							<?php } ?>
							</div>
					</div>

					<div class="form-group">
						<label class="col-md-6 control-label">Enable Chatbox</label>
							<div class="col-md-6">
							<?php if($class['enable_chatbox'] == '1'){ ?>
								<input type="checkbox" checked class="make-switch" data-size="small" id="clsprfctbx">
							<?php } else { ?>
								<input type="checkbox" class="make-switch" data-size="small" id="clsprfctbx">
							<?php } ?>
							</div>
					</div>
				</div>

			</div>
			<!-- END SWITCHES -->

			<div class="form-actions btntoright">
			<hr style="margin:0px!important">
				<button type="submit" class="btn green-haze" id="svclsprfrn">Save Changes</button>
				<!-- <button type="button" class="btn default" id="cnclclprfrn">Cancel</button> -->
			</div>
		</div>
	</form>
</div>
<!-- END CLASS PREFERENCE -->