<!-- BEGIN CLASS NOTIFICATION -->
<div class="tab-pane" id="tab_1_3">
<hr style="margin-bottom:15px!important;margin-top:0px!important">
	<form  class="form-horizontal" role="form">
		<div class="form-body">
			<div class="col-md-12" style="margin-bottom:15px;border-bottom:1px solid #eee">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-6 control-label">Enable Internal Notification</label>
							<div class="col-md-6">
								<input type="checkbox" checked class="make-switch" data-size="small">
							</div>
					</div>
					<div class="form-group">
						<label class="col-md-6 control-label">Enable External Notification</label>
							<div class="col-md-6">
								<input type="checkbox" checked class="make-switch" data-size="small">
							</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-6 control-label">Notify VIA Email</label>
							<div class="col-md-6">
								<div class="icheck-inline">
									<input type="checkbox" class="icheck">
								</div>
							</div>
					</div>					
					<div class="form-group" id="notifviasms">
						<label class="col-md-6 control-label">Notify VIA SMS</label>
							<div class="col-md-6">
								<div class="icheck-inline">
									<input type="checkbox" class="icheck" disabled>
								</div>
							</div>
					</div>
				</div>
			<hr style="margin:0px!important">
			</div>
			<div class="form-actions btntoright">
				<button type="submit" class="btn green-haze">Save Changes</button>
			</div>
		</div>
	</form>
</div>
<!-- END CLASS NOTIFICATION -->