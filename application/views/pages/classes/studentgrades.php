<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Student Grades</title>
<?php foreach($one_class as $class) ?>
<?php foreach($student_grades as $student) ?>
<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- BEGIN STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Student Grade 
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="icon-notebook"></i>
							<a href="index.html">Classes</a>
							<i class="fa fa-angle-double-right"></i>
						</li>
						<li>
							<a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}")?>"><?= $class['group_number']." ".$class['course_code']; ?></a>
							<i class="fa fa-angle-double-right"></i>
						</li>
						<li>
							<a href="<?php echo base_url("{$current_url_2}/students"); ?>">Students</a>
							<i class="fa fa-angle-double-right"></i>
						</li>
						<li>
							<a href="javascript:;"><?= $student['school_id'] ?></a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="javascript:;>" data-toggle="modal" data-target="#addcourse"><i class="fa fa-plus"></i> Add New Course</a>
								</li>
								<li>
									<a href="#"><i class="fa fa-trash"></i> Delete Course</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box grey-gallery">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-graduation-cap"></i>Grades of <?= $student['firstname']." ".$student['lastname'] ?>
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
								<tr>
									<th>Activity Title</th>
					 				<th>Date</th>
					 				<th>Score</th>
					 				<th>Total</th>
					 				<th></th>
								</tr>
								</thead>
								<tbody>
								<?php foreach($student_grades as $grade) {?>
						 			<tr>
						 				<td><?= $grade['activity_title'] ?></td>
						 				<td><?= date('F d, Y',strtotime($grade['created_at'])) ?></td>
						 				<td><?= $grade['score'] ?></td>
						 				<td><?= $grade['total'] ?></td>
						 				<td><a href="<?= base_url("{$current_url_2}/grades/history/{$grade['history_id']}") ?>"><button class="btn btn-sm green-jungle"><i class="fa fa-edit"></i> Edit Grade</button></a></td>
						 			</tr>
						 		<?php } ?>
								</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
					<!-- BEGIN MODAL -->
					<?php require_once 'application/views/modals/add_course.phtml';?>	
					<!-- END MODAL -->
				<!-- END PAGE CONTENT-->
			</div>
		</div>
			<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>
	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/fuelux/js/spinner.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
	<?php require_once 'application/views/includes/theme_js.phtml';?>
	<script>
      jQuery(document).ready(function() {    
				TableAdvanced.init();
				AdminPage.init();
      });
   </script>
	<script src="/../assets/admin/pages/scripts/table-advanced.js"></script>
	<script src="/../assets/admin/pages/scripts/admin.js"></script>	
	
<!-- END THEME PLUGINS -->
	
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>