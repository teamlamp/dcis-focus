<!-- Post discussion block -->

	<!-- BEGIN Portlet PORTLET-->
	<div class="portlet light" style="height:auto">
		<div class="portlet-title">
			<div class="caption">
				<i class="icon-speech"></i>
				<span class="caption-subject bold uppercase"> POST</span>
				<span class="caption-helper">Discussions</span>
			</div>
			<div class="actions">
				<!-- <a class="btn  btn-default btn-icon-only" data-toggle="modal" href="#small" title="Attach files">
				<i class="fa fa-paperclip"></i></a>
				<a href="javascript:;" class="btn  btn-default btn-icon-only" title="Settings">
				<i class="fa fa-spin fa-gear"></i> </a> -->
				<!-- <a href="javascript:;" class="btn btn-circle btn-default btn-icon-only fullscreen"></a> -->
			</div>
		</div>
		<div class="portlet-body form">
			<form  class="form-horizontal" role="form" action="javascript:;">
				<div class="form-body">
					<div class="item">
						<div class="item-head">
							<div class="item-details">
								<img class="dist item-pic img-circle" style="width: 50px; height: 50px" src="<?= $this->session->userdata('photo')?>">
								<a href="<?= base_url("profile/{$this->session->userdata('school_id')}")?>" class="item-name primary-link"><?= $this->session->userdata('firstname').' '.$this->session->userdata('lastname');?></a>
							</div>
						</div>
						<div class="post">
							<input type="hidden" class="post-name" value="<?= $this->session->userdata('school_id');?>">
							<textarea id="post-here" class="wysihtml5 form-control" placeholder="Post a new discussion." rows="5"></textarea>
							<span class="post-status pull-left"> <h5>&nbsp;<i id="stat-icon" class="fa fa-circle-o-notch font-blue"></i> Status : <span id="stat">Idle</span></h5></span>
						</div>
					</div>
					<div class="btntoright">
						<button type="submit" id="submit_post" class="btn btn-sm green-haze">Post</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- END Portlet PORTLET-->

					<div class="modal fade bs-modal-sm" id="small" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-sm">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
												<h4 class="modal-title">Attachment</h4>
											</div>
											<div class="modal-body" style="display:inline">
												<button type="button" class="btn blue">Link</button>
												<button type="button" class="btn red">Photo/Video</button>
												<button type="button" class="btn green">Document</button>
											</div>
										</div>
										<!-- /.modal-content -->
									</div>
									<!-- /.modal-dialog -->
								</div>
								<!-- /.modal -->