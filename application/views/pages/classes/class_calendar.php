<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<?php foreach($one_class as $class) ?>
<title>DCIS | Class <?= $class['course_code'].' Group '.$class['group_number']; ?></title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="/../../assets/global/plugins/fullcalendar/fullcalendar.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/dcis/sweetalert/sweetalert.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->
<style>
.fc-time{
   display : none !IMPORTANT;
	}
#trash {
	width: 100px;
}
.fc-unthemed .fc-today {
    background: #FCF8A9 !IMPORTANT;
}
</style>
<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- <script type = 'text/javascript' id ='1qa2ws' charset='utf-8' src='http://10.165.197.8:9090/tlbsgui/baseline/scg.js' mtid=3 mcid=10 ptid=3 pcid=10></script> -->
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">

<!-- BEGIN HEADER -->
<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix">
</div>
<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<?php require_once 'application/views/includes/sidebar.phtml';?>
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- END STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->
				
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">        
		        Class <small><?= $class['course_code']. " Group ".$class['group_number']; ?> (7:30-9:00 MW)</small>
		        </h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
			            <li>
			              <i class="icon-notebook"></i>
			              <a href="<?= base_url('classes') ?>">Classes</a>
			              <i class="fa fa-angle-double-right"></i>
			            </li>
			            <li>
			              <a href="<?= base_url("class/{$class['course_code']}/{$class['group_number']}")?>"><?= $class['group_number']." ".$class['course_code']; ?></a>
			              <input id="class_id" type="hidden" value="<?= $class['class_id'];?>">
			              <input id="user_role" type="hidden" value="<?= $this->session->userdata('user_role');?>">
			              <input id="username" type="hidden" value="<?= $this->session->userdata('firstname').' '.$this->session->userdata('lastname');?>">
			              <i class="fa fa-angle-double-right"></i>
			            </li>
			            <li>
			              <a href="#">Calendar</a>
			            </li>
			          </ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
				                <?php if($this->session->userdata('user_role') == "instructor" || $this->session->userdata('user_role') == "admin"){?>
				                <?php } else if ($this->session->userdata('user_role') == "student"){?>
				                <li>
				                  <a href="<?= base_url("{$current_url}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
				                </li>
				                <li class="divider"></li>
				                <?php } ?>
				                
				                <?php if($this->session->userdata('user_role') != "student"){?>
				                <li>
				                  <a href="<?php echo base_url("{$current_url_1}/students"); ?>"><i class="fa fa-users"></i> Students List</a>
				                </li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/classrecord"); ?>"><i class="fa fa-file-text"></i> Class Record</a>
				                </li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/attendance"); ?>"><i class="fa fa-pencil"></i> Attendance</a>
				                </li>
				                <?php } else if ($this->session->userdata('user_role') == "student"){?>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
				                </li>
				                <?php } ?>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/calendar"); ?>"><i class="fa fa-calendar"></i> Class Calendar</a>
				                </li>
				                <?php if($this->session->userdata('user_role')!="student"){ ?>
				                <li class="divider"></li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/grades/record"); ?>"><i class="fa fa-save"></i> Record Grades</a>
				                </li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/grades/history"); ?>"><i class="fa fa-history"></i> Record History</a>
				                </li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/grades/rubrics"); ?>"><i class="fa fa-cube"></i> Class Rubrics</a>
				                </li>             
				                <li class="divider">
				                </li>
				                <li>
				                  <a href="<?= base_url("{$current_url_1}/settings") ?>"><i class="fa fa-cog"></i> Class Settings</a>
				                </li>
				                <?php } ?>
				               </ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<div class="portlet box green-meadow calendar">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-calendar"></i>Class Calendar
								</div>
							</div>
							<div class="portlet-body">
								<div class="row">
									<div class="col-md-3 col-sm-12">
										<!-- BEGIN DRAGGABLE EVENTS PORTLET-->
										
										<h3 class="event-form-title">Draggable Events</h3>
										<div id="external-events">
											<form class="inline-form">
												<input type="text" value="" class="form-control" placeholder="Event Title..." id="event_title" <?php if($this->session->userdata('user_role') == 'student'): echo 'disabled'; endif;?>/><br/>
												<a href="javascript:;" id="event_add" class="btn default" <?php if($this->session->userdata('user_role') == 'student'): echo 'disabled'; endif;?>>
												Add Event </a>
												<a id="trash" class="btn btn-danger" disabled><i class="fa fa-trash" style="font-size: 15px;"></i> Drop</a>
												
										</div>
											<hr/>
											</form>

											<div id="event_box">
											</div>
											<label for="drop-remove">
											<input type="checkbox" id="drop-remove" checked="true" />remove after drop </label>
											<hr class="visible-xs"/>

										<!-- END DRAGGABLE EVENTS PORTLET-->
									</div>
									<div class="col-md-9 col-sm-12">
										<div id="calendar" class="has-toolbar">
										</div>
									</div>
								</div>
								<!-- END CALENDAR PORTLET-->
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->
		<!--Cooming Soon...-->
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->

</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->

<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="/../../assets/global/plugins/moment.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script src="/../../assets/global/plugins/fullcalendar/fullcalendar.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/../assets/dcis/sweetalert/sweetalert.min.js"></script>
<script src="/../../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/../../assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="/../../assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<script src="/../../assets/admin/pages/scripts/calendar.js"></script>
<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo feature
});
var user = "<?=$this->session->userdata('user_role'); ?>";
var class_id = "<?=$class['class_id']?>"
</script>
<script src="/../assets/admin/admin-page/js/class_calendar.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
  /*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-37564768-1', 'keenthemes.com');
  ga('send', 'pageview');*/
</script>
</body>

<!-- END BODY -->
</html>
