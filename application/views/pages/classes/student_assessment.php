<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Assessment</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- BEGIN STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Student Assessment <small>Student name and/or ID number</small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="icon-notebook"></i>
							<a href="index.html">Classes</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">wtf</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Assessment</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-6">
						 Page content goes here
					</div>
					<div class="col-md-6">
						 Page content goes here
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="portlet light">
								<div class="portlet-title tabbable-line tabbalable-custom">
									<!-- <div class="caption caption-md">
										<i class="fa fa-user hide"></i>
										<span class="caption-subject font-green-haze bold uppercase">Profile Account</span>
									</div> -->
									<ul class="nav nav-tabs nav-justified">
										<li class="active">
											<a href="#bargraph" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-bar-chart"></i> Calculated Grade</span></a>
										</li>
										<li>
											<a href="#datatable1" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-list-ul"></i> List Of Activities</span></a>
										</li>
										<li>
											<a href="#datatable2" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-tasks"></i> Grade Breakdown</span></a>
										</li>
									</ul>
								</div>
								<div class="portlet-body">
									<div class="tab-content">
										<!-- CLASS INFO TAB -->
										<?php require_once VIEWPATH.'/pages/classes/assessment/barchart.php';?>
										<!-- END CLASS INFO TAB -->
										
										<!-- CLASS PREFERENCE TAB -->
										<?php require_once VIEWPATH.'/pages/classes/assessment/datatable.php';?>
										<!-- END CLASS PREFERENCE TAB -->

										<!-- CLASS NOTIFICATION TAB -->
										<?php require_once VIEWPATH.'/pages/classes/assessment/datatable-2.php';?>
										<!-- END CLASS NOTIFICATION TAB -->
									</div>
								</div>
							</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
			<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- BEGIN THEME PLUGINS -->
	<?php require_once 'application/views/includes/theme_js.phtml';?>	
<!-- END THEME PLUGINS -->
	
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>