<!DOCTYPE html>
<html lang="en">
<head>
<?php foreach($one_history as $history) ?>
<title>DCIS <?= date('F d, Y',strtotime($history['created_at'])) ?> : <?= ucfirst($history['type']) ?> (<?= ucfirst($history['activity_title']) ?>)</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
  <!-- BEGIN CONTAINER -->
  <div class="page-container">

    <!-- BEGIN SIDEBAR -->
    
    <?php require_once 'application/views/includes/sidebar.phtml';?>  
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">

        <!-- BEGIN STYLE CUSTOMIZER -->
        <?php require_once 'application/views/includes/style_customizer.phtml';?> 
        <!-- END STYLE CUSTOMIZER -->

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
        <?= date('F d, Y',strtotime($history['created_at']))?> <small><?= $history['group_number']." ".$history['course_code']; ?></small>
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
                    <i class="icon-notebook"></i>
                    <a href="<?= base_url('classes')?>">Classes</a>
                    <i class="fa fa-angle-double-right"></i>
                  </li>
                  <li>
                    <a href="<?= base_url("class/{$history['course_code']}/{$history['group_number']}")?>"><?= $history['group_number']." ".$history['course_code']; ?></a>
                    <i class="fa fa-angle-double-right"></i>
                  </li>
                  <li>
                    <a href="<?= base_url("{$current_url_2}/record"); ?>">Record Grades</a>
                    <i class="fa fa-angle-double-right"></i>
                  </li>
                  <li>
                    <a href="<?= base_url("{$current_url_2}/history"); ?>">History</a>
                    <i class="fa fa-angle-double-right"></i>
                  </li>
                  <li>
                    <a href="javascript:;"><?= date('F d, Y',strtotime($history['created_at'])) ?> : <?= ucfirst($history['type']) ?> (<?= ucfirst($history['activity_title']) ?>)</a>
                  </li>
          </ul>
          <div class="page-toolbar">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
              Actions <i class="fa fa-arrow-circle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <?php if($this->session->userdata('user_role') != "student"){?>
                <li>
                  <a href="<?php echo base_url("{$current_url_2}/../students"); ?>"><i class="fa fa-users"></i> Students List</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/../classrecord"); ?>"><i class="fa fa-file-text"></i> Class Record</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/../attendance"); ?>"><i class="fa fa-pencil"></i> Attendance</a>
                </li>
                <?php } else if ($this->session->userdata('user_role') == "student"){?>
                <li>
                  <a href="<?= base_url("{$current_url_2}/../assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
                </li>
                <?php } ?>
                <li>
                  <a href="<?= base_url("{$current_url_2}/calendar"); ?>"><i class="fa fa-calendar"></i> Class Calendar</a>
                </li>
                <?php if($this->session->userdata('user_role')!="student"){ ?>
                <li class="divider"></li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/record"); ?>"><i class="fa fa-save"></i> Record Grades</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/history"); ?>"><i class="fa fa-history"></i> Record History</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/rubrics"); ?>"><i class="fa fa-cube"></i> Class Rubrics</a>
                </li>             
                <li class="divider">
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_2}/settings") ?>"><i class="fa fa-cog"></i> Class Settings</a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
          <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue-chambray">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-calendar"></i><?= date('F d, Y',strtotime($history['created_at'])) ?> : <?= ucfirst($history['type']) ?> (<?= ucfirst($history['activity_title']) ?>)
                </div>
              </div>
              <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="oneGradeHistory">
                <thead>
                  <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th class="hidden">ID</th>
                    <th>Score</th>
                    <th>Total Score</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                foreach($one_history as $history){
                ?>                  
                  <tr>
                    <td><?= $history['firstname'] ?></td>
                    <td><?= $history['lastname'] ?></td>
                    <td class="hidden grade_id"><?= $history['grade_id'] ?></td>
                    <td><input type="number" class="edit-grade form-control input-small" value="<?= $history['score']; ?>" min="0"></td>
                    <td><span class="ttlscore"><?= $history['total'] ?></span></td>
                  </tr>                   
                <?php } ?>
                </tbody>
                </table>
                <center><button type="button" class='btn btn-success pull-center' id="update-grades">Update Grades</button></center> 
              </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
        <!-- END PAGE CONTENT-->
      </div>
      
    </div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->
</div>
  <!-- BEGIN FOOTER -->
  <?php require_once 'application/views/includes/footer.phtml';?> 
  <!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
  <?php require_once 'application/views/includes/core_js.phtml';?>  
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?> 
  <script>
      jQuery(document).ready(function() {    
        DcisTableAdvanced.init();
      });
      var ddate = <?= json_encode(date('F d, Y',strtotime($history['created_at'])))?>;
      var type = <?= json_encode(ucfirst($history['type'])) ?>;
      var title = <?= json_encode(ucfirst($history['activity_title'])) ?>;

      var classname = <?php echo json_encode($history['course_code']); ?>; 
      var groupnumber = <?php echo json_encode($history['group_number']); ?>; 

      var classname = ddate+" "+type+" "+title+" "+ "of Class " +classname;
      var groupnumber = <?php echo json_encode($history['group_number']); ?>; 
   </script>
    <script src="/../assets/dcis/js/dcis-table-advance.js"></script>
<!-- END THEME PLUGINS -->
  
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>