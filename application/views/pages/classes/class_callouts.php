<?php if($class['enable_callouts'] == '1'){ ?>
	<!-- BEGIN Portlet PORTLET-->
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-bullhorn"></i>
				<span class="caption-subject bold uppercase"> Callouts</span>
				<!-- <span class="caption-helper">Activity</span> -->
			</div>
			<div class="actions">
			<?php if($this->session->userdata('user_role') == "instructor" || $this->session->userdata('user_role') == "admin"){?>
			<a class="btn default font-grey-gallery" data-toggle="modal" href="#add_callout" title="Add new callout">
			<i class="fa fa-plus font-grey-cascade" id="add-callout"></i> </a>			
			<?php } ?>
				<!-- <a href="javascript:;" class="btn btn btn-default btn-icon-only fullscreen"></a> -->
			</div>
		</div>
		<div class="portlet-body">
			<div class="scroller" style="height: auto; max-height: 350px;" data-rail-visible="1" data-rail-color="yellow" data-handle-color="#a1b2bd">
				<div id="callouts-holder" data-toggle="context" data-target="#context-menu">
					<?php if(!($callouts)){echo "<div class='note note-danger' id='no-callouts-span'>
                <h4 class='block' style='text-align:center;'>No Callouts posted yet.</h4>
            </div>";} else {foreach($callouts as $callout) { ?>
					<?php if($callout['type'] == "info"){?>
					<div class="note note-info">
					<?php }else if($callout['type'] == "warning"){?>
					<div class="note note-warning">
					<?php }else if($callout['type'] == "danger"){?>
					<div class="note note-danger">
					<?php }?>
					<h5><strong style="text-transform: uppercase;"><?= $callout['title']; ?></strong></h5>
					<p id="<?= $callout['callout_id'] ?>">
						<?= $callout['content']; ?>
					</p>
					<sub><?php if($callout['created_at'] != $callout['modified_at']) 
						echo date('F d, Y - g:m A', strtotime($callout['modified_at']))." Edited"; 
							else
						echo date('F d, Y - g:m A', strtotime($callout['modified_at'])); 
					?></sub>
					</div>
					<?php }} ?>
				</div>
			</div>
		</div>
	</div>
	<!-- END Portlet PORTLET-->


		<!-- /.modal -->
			<div class="modal fade bs-modal-lg" id="add_callout" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
				<form action='' method="POST" id="form-add-callout">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Add a Callout &nbsp;&nbsp;
							<span class="btn-group btn-group">
								<button type="button" class="btn blue" id="callout-type-label">Callout Type</button>
								<button type="button" class="btn btn-right blue dropdown-toggle" data-toggle="dropdown"><i class="fa fa-angle-down"></i></button>
								<div class="dropdown-menu hold-on-click dropdown-radiobuttons" id="callout-type" role="menu" required>
									<label ><input class="callout-type" type="radio" name="callout-btn" id="callout-btn-1" checked="checked"><span class="label label-info">Info</span></label>
									<label ><input class="callout-type" type="radio" name="callout-btn" id="callout-btn-2"><span class="label label-warning">Warning</span></label>
									<label ><input class="callout-type" type="radio" name="callout-btn" id="callout-btn-3"><span class="label label-danger">Danger</span></label>
								</div>
							</span></h4>
						</div>
						<div class="modal-body">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon" id="callout-heads-up">
								<i class="fa fa-bullhorn"></i> Info
								</span>
								<input type="text" id="callout-title" class="form-control" name="title" placeholder="Callout Title" required>
							</div>
						</div>
							<textarea class="form-control todo-taskbody-taskdesc input" name="content" rows="8" placeholder="Make an announcement." id="callout-content" required style="margin-top:15px;resize:none;"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn blue" id="save-callout">Save Callout</button>
						</div>
					</div>										
				</form>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
<?php } else { ?>
<div class="portlet light">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-bullhorn"></i>
				<span class="caption-subject bold uppercase"> Callouts</span>
				<!-- <span class="caption-helper">Activity</span> -->
			</div>
		</div>
		<div class="portlet-body">
			<div class='note note-danger'>
            <h4 class='block' style='text-align:center;'>Callouts Is Disabled</h4>
        </div>				
		</div>
<?php } ?>

<a class="btn default font-grey-gallery" data-toggle="modal" href="#edit_callout" title="Add new callout" style="display:none;">
<i class="fa fa-dashboard font-grey-cascade" id="edit-callout"></i> </a>
 
<div class="modal fade bs-modal-lg" id="edit_callout" tabindex="-1" role="dialog" aria-hidden="true">
<input type="hidden" id="edttclltid">
	<div class="modal-dialog modal-lg">
	<form action='' method="POST" id="form-edit-callout">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit Callout &nbsp;&nbsp;
				<span class="btn-group btn-group">
					<button type="button" class="btn blue" id="edit-callout-type-label">Callout Type</button>
					<button type="button" class="btn btn-right blue dropdown-toggle" data-toggle="dropdown"><i class="fa fa-angle-down"></i></button>
					<div class="dropdown-menu hold-on-click dropdown-radiobuttons" id="edit-callout-type" role="menu" required>
						<label ><input class="edit-callout-type-o" type="radio" name="callout-btn" id="callout-btn-1" value="info"><span class="label label-info">Info</span></label>
						<label ><input class="edit-callout-type-o" type="radio" name="callout-btn" id="callout-btn-2" value="warning"><span class="label label-warning">Warning</span></label>
						<label ><input class="edit-callout-type-o" type="radio" name="callout-btn" id="callout-btn-3" value="danger"><span class="label label-danger">Danger</span></label>
					</div>
				</span></h4>
			</div>
			<div class="modal-body">
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon" id="edit-callout-heads-up">
					<i class="fa fa-bullhorn"></i> Info
					</span>
					<input type="text" id="edit-callout-title" class="form-control" name="title" placeholder="Callout Title" required>
				</div>
			</div>
				<textarea class="form-control todo-taskbody-taskdesc input" name="content" rows="8" placeholder="Make an announcement." id="edt-callout-content" required style="margin-top:15px;resize:none;"></textarea>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn blue" id="edit-save-callout">Save Edit</button>
			</div>
		</div>										
	</form>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>