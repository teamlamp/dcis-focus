<?php 
if($one_student_grades){
	$performance_array = array(array());
	$ctr = 0;
	$previousValue = null;
	$prevpercentage = 0;
	foreach($one_student_grades as $stud_grade){
		$score = $stud_grade['score'];
		$percentage = floor(($stud_grade['score']/$stud_grade['total'])*100);
		if($previousValue){
			$prevpercentage = $previousValue;
		}
		$previousValue = floor(($stud_grade['score']/$stud_grade['total'])*100);
		if($percentage == $prevpercentage){
			$performance_array[$ctr]['lineColor'] = NULL;
			$string = "Your performance has been stable.";
		}
		if($percentage < $prevpercentage){
			$performance_array[$ctr]['lineColor'] = "#00FF00";
			$diff = abs($percentage - $prevpercentage);
			$string = "Your performance has decreased by ".$diff."% from the previous activtiy.";
		}
		if($percentage > $prevpercentage){
			$performance_array[$ctr]['lineColor'] = "#FF0000";
			$diff = $percentage - $prevpercentage;		
			$string = "Your performance has increased by ".$diff."% from the previous activtiy.";
		}
		$performance_array[$ctr]['date'] = date('M d, Y',strtotime($stud_grade['created_at']))."<br>".$stud_grade['activity_title'];
		$performance_array[$ctr]['percentage'] = $percentage;
		$ctr++;
	}
}
?>
<!-- BEGIN CHART PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-bar-chart font-green-haze"></i>
			<span class="caption-subject bold uppercase font-green-haze"> Activity Performance</span>
			<!-- <span class="caption-helper">with changing color</span> -->
		</div>
	</div>
	<?php if($one_student_grades){?>
	<center><span><?= $string ?></span></center>
	<?php } ?>
	<div class="portlet-body">
	<?php if($one_student_grades){ ?>
		<div id="performance_line" class="chart"></div>
	<?php } else { ?>
		<div class="note note-danger">
      <h4 class="block">No grades yet.</h4>
	  </div>
	<?php }?>
	</div>
</div>
<!-- END CHART PORTLET-->