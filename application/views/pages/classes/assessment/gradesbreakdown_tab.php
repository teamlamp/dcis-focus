<!-- BEGIN PAGE CONTENT-->
<div class="tab-pane" id="datatable2">
	<div class="row">
		<div class="col-md-12">
		<?php if($one_student_grades){?>
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet box blue-chambray">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-tasks"></i>Grade Breakdown
					</div>
					<div class="tools">
					</div>
				</div>
				<div class="portlet-body">
					<table class="table table-striped table-bordered table-hover" id="latestactivitylist">
						<thead>
							<tr>
								<th>Activity Type</th>
								<th>Date</th>
								<th>Activity Title</th>
								<th>Score</th>
								<th>Perfect Score</th>
								<th>Score %</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($one_student_grades as $grade){?>
							<tr>
								<td><?= $grade['type'] ?></td>
								<td><?= date('F d, Y - g:m A',strtotime($grade['created_at'])) ?></td>
								<td><?= $grade['activity_title'] ?></td>
								<td><?= $grade['score'] ?></td>
								<td><?= $grade['perfect_score'] ?></td>
								<?php $x = floor(($grade['score']/$grade['perfect_score']) * 100); ?>
								<td><?= $x." %" ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
			<?php } else { ?>
				<div class="note note-danger">
		      <h4 class="block">No grades yet.</h4>
			  </div>
			<?php } ?>
		</div>
	</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
