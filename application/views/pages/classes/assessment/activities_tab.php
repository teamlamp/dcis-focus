<!-- BEGIN PAGE CONTENT-->
<div class="tab-pane" id="datatable1">
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet box grey-gallery">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-list-ul"></i>List of Activities
					</div>
					<div class="tools">
					</div>
				</div>
				<div class="portlet-body">
					<table class="table table-striped table-bordered table-hover" id="">
						<thead>
							<tr>
								<th>Activity Type</th>
								<th>Date</th>
								<th>Activity Title</th>
								<th>Perfect Score</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($list_of_activities as $activity){?>
							<tr>
								<td><?= $activity['type'] ?></td>
								<td><?= date('F d, Y - g:m A',strtotime($activity['created_at'])) ?></td>
								<td><?= $activity['activity_title'] ?></td>
								<td><?= $activity['perfect_score'] ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
