<?php
if($failpass[0]['cm_grade'] != NULL && $failpass[0]['cm_grade'] != '0'){
$piedata = array(array());

$pass = floor(($failpass[0]['cm_grade']/$failpass[0]['c_grade']) * 100);
$fail = 100 - $pass;

$piedata[0]['passfail'] = "Achieved";
$piedata[0]['percentage'] = $pass;
$piedata[1]['passfail'] = "Failed";
$piedata[1]['percentage'] = $fail;
}
?>
<!-- BEGIN CHART PORTLET-->
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-bar-chart font-green-haze"></i>
			<span class="caption-subject bold uppercase font-green-haze"> Grade percentage</span>
			<!-- <span class="caption-helper">bar and line chart mix</span> -->
		</div>
	</div>
	<div class="portlet-body">
	<?php if($failpass[0]['cm_grade'] != NULL && $failpass[0]['cm_grade'] != '0'){?>
		<div id="gradepercentage_chart" class="chart"></div>
	<?php } else {?>
		<div class="note note-danger">
	    <h4 class="block">No grades yet.</h4>
	 </div>
	<?php } ?>
	</div>
</div>
<!-- END CHART PORTLET-->
