<?php 
if($student_data[0]['class_grade_sf'] != NULL && $student_data[0]['class_grade_sf'] != '0'){
foreach($student_data as $stud_data) 
$pass = $stud_data['class_grade_sf']/2;
$grade = $stud_data['cm_grade_sf'];
}
?>
<div class="tab-pane active" id="bargraph">
	<!-- BEGIN ROW -->
		<div class="row">
		<?php if($student_data[0]['class_grade_sf'] != NULL && $student_data[0]['class_grade_sf'] != '0'){ ?>
			<div class="col-md-6">
			<?php if($grade < $pass){?>
				<div class="note note-danger">
					<h4 class="block"><center>Your <strong>Raw Grade</strong> for now is :</center></h4>
					<p><center><h1><?= $grade ?> / <?= $failpass[0]['c_grade'] ?></h1></center></p>
					<p><center>Put more effort.</center></p>
				</div>
			<?php } else { ?>
				<div class="note note-success">
					<h4 class="block"><center>Your <strong>Raw Grade</strong> for now is :</center></h4>
					<p><center><h1><?= $grade ?> / <?= $failpass[0]['c_grade'] ?></h1></center></p>
					<p><center>Keep up the good work.</center></p>
				</div>
			<?php } ?>
				<!-- END CHART PORTLET-->
			</div>
			<div class="col-md-6">
			<?php if($grade < $pass){?>
				<div class="note note-danger">
					<h4 class="block"><center>Your <strong>Equivalent Grade</strong> for now is :</center></h4>
					<?php $equi = round((5-($grade*(4/$failpass[0]['c_grade']))),2); ?>
					<p><center><h1><?= $equi ?></h1></center></p>
					<p><center>Put more effort.</center></p>
				</div>
			<?php } else { ?>
				<div class="note note-success">
					<h4 class="block"><center>Your <strong>Equivalent Grade</strong> for now is :</center></h4>
					<?php $equi = round((5-($grade*(4/$failpass[0]['c_grade']))),2); ?>
					<p><center><h1><?= $equi ?></h1></center></p>
					<p><center>Keep up the good work.</center></p>
				</div>
			<?php } ?>
				<!-- END CHART PORTLET-->
			</div>
		<?php } else { ?>
			<div class="col-md-12">
				<div class="note note-danger">
		      <h4 class="block">No grades yet.</h4>
			  </div>
			</div>
		<?php } ?>
		</div>
	<!-- END ROW -->
</div>