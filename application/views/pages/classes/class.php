<!DOCTYPE html>
<html lang="en">
<head>
<?php foreach($one_class as $class) ?>
<title>DCIS | Class <?= $class['course_code'].' Group '.$class['group_number']; ?></title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE STYLES -->
<link href="../../assets/admin/pages/css/blog.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>

<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->


<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
  <!-- BEGIN CONTAINER -->
  <div class="page-container">

    <!-- BEGIN SIDEBAR -->
    <?php require_once 'application/views/includes/sidebar.phtml';?>  
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">

        <!-- BEGIN STYLE CUSTOMIZER -->
        <?php require_once 'application/views/includes/style_customizer.phtml';?> 
        <!-- END STYLE CUSTOMIZER -->

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">        
        Class <small><?= $class['course_code']. " Group ".$class['group_number']; ?> (7:30-9:00 MW)</small>
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="icon-notebook"></i>
              <a href="<?= base_url('classes') ?>">Classes</a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="javascript:;"><?= $class['group_number']." ".$class['course_code']; ?></a>
              <input id="class_id" type="hidden" value="<?= $class['class_id'];?>">
              <input id="user_role" type="hidden" value="<?= $this->session->userdata('user_role');?>">
              <input id="username" type="hidden" value="<?= $this->session->userdata('firstname').' '.$this->session->userdata('lastname');?>">
            </li>
          </ul>
          <div class="page-toolbar">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
              Actions <i class="fa fa-arrow-circle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <?php if($this->session->userdata('user_role') != "student"){?>
                <li>
                  <a href="<?php echo base_url("{$current_url}/students"); ?>"><i class="fa fa-users"></i> Students List</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url}/classrecord"); ?>"><i class="fa fa-file-text"></i> Class Record</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url}/attendance"); ?>"><i class="fa fa-pencil"></i> Attendance</a>
                </li>
                <?php } else if ($this->session->userdata('user_role') == "student"){?>
                <li>
                  <a href="<?= base_url("{$current_url}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url}/StudentAttendance"); ?> "><i class="fa fa-file-text"></i> Attendance</a>
                </li>
                <?php } ?>
                <li>
                  <a href="<?= base_url("{$current_url}/calendar"); ?>"><i class="fa fa-calendar"></i> Class Calendar</a>
                </li>
                <?php if($this->session->userdata('user_role')!="student"){ ?>
                <li class="divider"></li>
                <li>
                  <a href="<?= base_url("{$current_url}/grades/record"); ?>"><i class="fa fa-save"></i> Record Grades</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url}/grades/history"); ?>"><i class="fa fa-history"></i> Record History</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url}/grades/rubrics"); ?>"><i class="fa fa-cube"></i> Class Rubrics</a>
                </li>             
                <li class="divider">
                </li>
                <li>
                  <a href="<?= base_url("{$current_url}/settings") ?>"><i class="fa fa-cog"></i> Class Settings</a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
          <div class="col-md-8">
            <!-- <div class="profile-sidebar"> -->
            <?php require_once VIEWPATH.'/pages/classes/post_discussion.php';?>
            <?php require_once VIEWPATH.'/pages/classes/post_feeds.php';?>
              <!-- </div> -->
          </div>
          <div class="col-md-4">
            <!-- <div class="blog-sidebar"> -->
            <?php require_once VIEWPATH.'/pages/classes/latest_activity.php';?>
            <?php require_once VIEWPATH.'/pages/classes/class_callouts.php';?>
            <!-- </div> -->
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <!-- <div class="col-md-8"></div> -->
            <?php require_once VIEWPATH.'/pages/classes/class_chat.php';?>
          </div>
        </div>

        <!-- END PAGE CONTENT-->
      </div>
    </div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->

  <!-- BEGIN FOOTER -->
  <?php require_once 'application/views/includes/footer.phtml';?> 
  <!-- END FOOTER -->
</div>

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script type="text/javascript">
var session_school_id  = <?php echo json_encode($this->session->userdata('school_id')); ?>; 
var session_userphoto  = <?php echo json_encode($this->session->userdata('photo')); ?>; 

 var playSound = function (){ 
       var sound =  document.createElement('div');
       sound.setAttribute('style','display:none');
        
        sound.innerHTML='<audio id="sound-notif" autoplay="autoplay"><source src="/../assets/dcis/sound/poit.mp3" type="audio/mpeg" /><source src="/../assets/dcis/sound/poit.oog"  type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="/../assets/dcis/sound/poit.mp3"  /></audio>';
        document.getElementById('toast-container').appendChild(sound);
        
        setTimeout(function(){
			if ($('#sound-notif').length > 0) {
			$('#sound-notif').remove();
			}
		}, 3000)
    };
</script>


<!-- BEGIN CORE PLUGINS -->
  <?php require_once 'application/views/includes/core_js.phtml';?> 
<script type="text/javascript" src="/../assets/dcis/node/js/ui-post.js"></script>
<script type="text/javascript" src="/../assets/dcis/node/js/ui-comment.js"></script>
<script type="text/javascript" src="/../assets/dcis/node/js/ui-chat.js"></script>
  
<script type="text/javascript" src="/../assets/dcis/node/js/post.js"></script>
<script type="text/javascript" src="/../assets/dcis/node/js/comment.js"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="../../assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="../../assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>

<script type="text/javascript" src="../../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script src="../../assets/admin/pages/scripts/components-editors.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?> 
<!-- END THEME PLUGINS -->

<script>
jQuery(document).ready(function() {
   Index.init();   
   Index.initDashboardDaterange();
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   // Index.initChat();
   Index.initMiniCharts();
   ComponentsEditors.init();
   
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
