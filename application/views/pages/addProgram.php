<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | User Management</title>
<!-- <input type="hidden" id="class_id" value="<?= $class['class_id']; ?>"> -->

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<link href="../../assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>

<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<link rel="stylesheet" type="text/css" href="/../assets/admin/pages/css/custom.css"/>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- END STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Programs
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="icon-notebook"></i>
							<a href="<?= base_url('programs')?>">Programs</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="<?= base_url('pages/addProgram')?>"><i class="fa fa-plus"></i> Add Program</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT -->
				<div class="row">
          <div class="col-md-12">
            <div class="portlet box grey-gallery">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit"></i>
                  <span class="caption-subject bold uppercase">Add Program</span>
                </div>
              </div>
              <div class="portlet-body form">
                <form role="" action="<?php echo base_url('programs/addNewProgram'); ?>" id="consultation" method="POST" class="form-horizontal">
                  <?php echo validation_errors(); ?>
                  <div class="form-body">
                    <div class="form-group form-md-line-input">
                      <label class="col-md-2 control-label" for="form_control_1">Program Code</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control" id="form_control_1" name="program_code" placeholder="e.g BSICT, BSIT">
                        <div class="form-control-focus">
                        </div>
                      </div>
                    </div>
                    <div class="form-group form-md-line-input">
                      <label class="col-md-2 control-label" for="form_control_1">Program Description</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control" id="form_control_1" name="program_desc" placeholder="e.g Bachelor of Science in Information and Communication Technology">
                        <div class="form-control-focus">
                        </div>
                      </div>
                    </div>
                   </div>
                  <div class="form-actions">
                    <div class="row">
                      <div class="col-md-offset-2 col-md-10">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn blue">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>               
              </div>
            </div>
          </div>
        </div>
				<!-- BEGIN PAGE CONTENT-->
		<?php require_once 'application/views/modals/import_users.phtml';?>
		</div>

			<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>
	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
	<?php require_once 'application/views/includes/theme_js.phtml';?>	
	<script>
      jQuery(document).ready(function() {    

		DcisTableCoursesAndUsers.init();
		
      });
   </script>
	<script src="/../assets/dcis/js/dcis-table-courses-users.js"></script>

<!-- END THEME PLUGINS -->
	
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>