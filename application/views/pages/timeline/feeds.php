	<!-- TIMELINE ITEM -->
	<?php
	if (isset($_GET['date'])) {
		$thedate = date('F d, Y @ H:m:i a',strtotime($_GET['date']));
	}else{
		$thedate = date('F d, Y @ H:m:i a');
	}
	?>
	<?php if ($feeds){ 
	foreach ($feeds as $feed)
	{
			 ?>
					<div class="timeline-item">
						<div class="timeline-badge">
							<img style="width:80px; height:80px;" class="timeline-badge-userpic img-circle" src="<?= $feed['photo']?>">
						</div>
						<div class="timeline-body">
							<div class="timeline-body-arrow">
							</div>
							<div class="timeline-body-head">
								<div class="timeline-body-head-caption">
									<a href="<?= base_url("profile/{$feed['school_id']}")?>" class="timeline-body-title font-blue-madison"><?= $feed['username'] ?></a>
									<span class="timeline-body-content font-grey-cascade"> posted in class <?= $feed['course_code'] ?> group <?= $feed['group_number']?></span>
									<small class="timeline-body-time font-grey-cascade"><?= date('F d, Y @ H:m:i a',strtotime($feed['modified_at'])) ?></small>
								</div>
							</div>
				 			<a class="" href="<?= base_url("class/{$feed['course_code']}/{$feed['group_number']}")?>">
								<div class="timeline-body-content">
								<input type="hidden" class="hide" name="discussion_id" value="<?= $feed['discussion_id']?>"/>
									<span class="font-grey-cascade"><?= $feed['discussion_content']?></span>
								</div>
							</a>
						</div>
					</div>

		<?php } }else{ ?>
					<div class="timeline-item">
						<div class="timeline-badge">
							<img style="width:80px; height:80px;" class="timeline-badge-userpic img-circle" src="/../assets/global/img/logo/dcislogo.png">
						</div>
						<div class="timeline-body">
							<div class="timeline-body-arrow">
							</div>
							<div class="timeline-body-head">
								<div class="timeline-body-head-caption">
									<a href="javascript:;" class="timeline-body-title font-blue-madison">Computer and Information Sciences</a>
									<span class="timeline-body-content font-grey-cascade"> system message</span>
									<span class="timeline-body-time font-grey-cascade"><?= $thedate ?></span>
								</div>
							</div>
							<div class="timeline-body-content">
								<span class="font-grey-cascade">There are no feeds for this day. Start posting discussions in your classes.</span>
							</div>
						</div>
					</div>
		<?php }
		?>

					<!-- END TIMELINE ITEM -->

