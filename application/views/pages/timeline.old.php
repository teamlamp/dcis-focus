<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Timeline</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../assets/admin/pages/css/timeline.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo ">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- END STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->


				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Timeline <small></small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Timeline</a>
							<i class="fa fa-angle-right"></i>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->
				<div class="timeline">
				
					<!-- TIMELINE ITEM -->
					<div class="timeline-item">
						<div class="timeline-badge">
							<div class="timeline-icon">
								<i class="icon-docs font-red-intense"></i>
							</div>
						</div>
						<div class="timeline-body">
							<div class="timeline-body-arrow">
							</div>
							<div class="timeline-body-head">
								<div class="timeline-body-head-caption">
									<span class="timeline-body-alerttitle font-green-haze">Server Report</span>
									<span class="timeline-body-time font-grey-cascade">Yesterday at 11:00 PM</span>
								</div>
								<div class="timeline-body-head-actions">
									<div class="btn-group dropup">
										<button class="btn btn-circle green-haze btn-sm dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
										Actions <i class="fa fa-angle-down"></i>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li>
												<a href="javascript:;">Action </a>
											</li>
											<li>
												<a href="javascript:;">Another action </a>
											</li>
											<li>
												<a href="javascript:;">Something else here </a>
											</li>
											<li class="divider">
											</li>
											<li>
												<a href="javascript:;">Separated link </a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="timeline-body-content">
								<span class="font-grey-cascade">
								Lorem ipsum dolore sit amet <a href="javascript:;">Ispect</a>
								</span>
							</div>
						</div>
					</div>
					<!-- END TIMELINE ITEM -->
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
	<?php require_once 'application/views/includes/theme_js.phtml';?>	
<!-- END THEME PLUGINS -->

<script src="../../assets/admin/pages/scripts/timeline.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
Timeline.init(); // init timeline page
});
</script>
	
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>