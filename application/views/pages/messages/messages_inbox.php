<div class="tab-pane active" id="inbox">		
	<!-- BEGIN INBOX CONTENT -->
	<div class="portlet light msgbox">
		<div class="portlet-title">
			<div class="caption font-blue">
				<i class="fa fa-inbox font-blue"></i>
				<span class="caption-subject bold uppercase"> Inbox</span>
			</div>
			<div class="actions">
				<div class="btn-group">
					<a class="btn blue btn-sm msgboxadm" href="javascript:;" data-toggle="dropdown"  disabled="disabled">
					<i class="fa fa-cogs"></i> Actions <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-right">
						<li>
							<a href="javascript:;" class="delselmsgs">
							<i class="fa fa-trash-o"></i> Delete Marked Messages </a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="portlet-body">
		<?php if($inbox_messages){?>
			<table class="table table-striped table-bordered table-hover" id="inboxm2">
				<thead>
					<tr>
						<th class="table-checkbox">
							<input type="checkbox" class="group-checkable" data-set="#inboxm2 .checkboxes"/>
						</th>
						<th>
							<i class="fa fa-user"></i> Sender
						</th>
						<th>
							<i class="fa fa-font"></i> Message Title
						</th>
						<th>
							<i class="fa fa-calendar"></i> Date Received
						</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($inbox_messages as $inbox){?>
					<tr class="odd gradeX" id="<?= $inbox['msg_map_id'] ?>" style="<?php if($inbox['msgmap_isRead']==0){ echo "color: black; font-weight:bold;";}else{ echo "color: none;";}?>">
						<td> <input type="checkbox" value="<?= $inbox['msg_map_id'] ?>" class="checkboxes"/>	</td>
						<td class="msgclosed"><?= $inbox['firstname']." ".$inbox['lastname'] ?></td>
						<td class="msgclosed"><?= $inbox['msg_subject'] ?></td>
						<td class="msgclosed"><?= date('F d, Y g:m A',strtotime($inbox['msg_created_at'])) ?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		<?php } else {?>
			<div class="note note-danger">
          <h4 class="block">No messages in your inbox.</h4>
          <p>Received messages will be viewed here.</p>
      </div>
		<?php } ?>
		</div>
	</div>
	<!-- END EXAMPLE TABLE PORTLET-->
</div>