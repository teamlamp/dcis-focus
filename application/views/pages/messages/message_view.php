<!-- BEGIN PAGE CONTENT-->
<div id="opnmsg" class="portlet light">
<input type="hidden" value="<?= $one_message['msgmap_id'] ?>" id="msgid">
<div class="portlet-title">
		<div class="caption font-green-meadow">
			<i class="fa fa-briefcase font-green-meadow"></i>
			<span class="caption-subject bold uppercase"> message</span>
			<!-- <span class="caption-helper">send private messages ...</span> -->
		</div>
		<div id="close" class="tools">
		<button class="btn green-meadow btn-sm" id="xmsgview"><i class="fa fa-times"></i></button>
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12 blog-page">
				<div class="row">
					<div class="col-md-12 article-block">
						<h3 style="margin-top:0;" id="rplysubject"><?= $one_message['msg_subject'] ?></h3>
						<div class="blog-tag-data">
							<!-- <img src="../../assets/admin/pages/media/gallery/item_img.jpg" class="img-responsive" alt=""> -->
							<div class="row">
								<div class="col-md-6">
									<ul class="list-inline blog-tags">
										<li>
											<i class="fa fa-user"></i><input type="hidden" value="<?= $one_message['msg_author_id'] ?>" id="rplyrecipient">
											From : <a href="<?= base_url("profile/{$one_message['msg_author_id']}") ?>">
											<?= $sender[0]['firstname']." ".$sender[0]['lastname'] ?> </a>
										</li>
										<li>
											<i class="fa fa-user"></i>
											To : <a href="<?= base_url("profile/{$one_message['msgmap_recipient_id']}") ?>">
											<?= $recipient[0]['firstname']." ".$recipient[0]['lastname'] ?></a>
										</li>
									</ul>
								</div>
								<div class="col-md-6 blog-tag-data-inner">
									<ul class="list-inline">
										<li>
											<i class="fa fa-calendar"></i>
											<a href="javascript:;">
											<?= date('F d, Y g:m A',strtotime($one_message['msg_created_at'])) ?> </a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!--end news-tag-data-->
						<div>
							<p>
								<?= $one_message['msg_body'] ?>
							</p>
						</div>
						
						<div class="post-comment">
							<form role="form" action="#">

								<div class="form-group">
									<textarea class="wysihtml5 form-control" rows="3" placeholder="Start composing your message here" id="replycnt"></textarea>
								</div>
								<div class="form-group pull-right">
									<a href="javascript:;" class="btn grey" id="trashcmsg"> Trash</a>
									<a href="javascript:;" class="btn grey" id="permatrash" style="display: none;"> Delete </a>
									<a href="javascript:;" class="btn green-meadow" id="sndRply"> Send Reply</a>
								</div>
							</form>
						</div>
					</div>
					<!--end col-md-9-->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->