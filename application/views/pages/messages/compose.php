<div class="tab-pane" id="compose">
<!-- BEGIN PAGE CONTENT-->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXTRAS PORTLET-->
			<div class="portlet light" style="padding: 12px 10px 15px 10px !important;">
				<div class="portlet-title">
					<div class="caption font-green-meadow">
						<i class="fa fa-edit font-green-meadow"></i>
						<span class="caption-subject bold uppercase"> Compose a message</span>
					</div>
				</div>
				<div class="portlet-body form">
					<form class="form-horizontal form-bordered" method="POST" action="<?= base_url('message/sendmessage') ?>" id="sndmsg">
						<div class="form-body">
							<div class="form-group">

								<div class="form-group">
									<label class="control-label col-md-2">To :</label>
									<div class="col-md-10">
										<!-- <select class="form-control input-large select2me" data-placeholder="Select...">
										</select> -->
										<select multiple id="tagRecipients" class="form-control select2 input-inline input-xlarge" name="messageRecipients[]" required>
											<!-- prepend draft recipients -->
											<!-- <option value="3620194" selected="selected">select2/select2</option> -->
										</select>

										<span class="help-block">
										Type in the recipient of this message.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label">Subject :</label>
									<div class="col-md-10">
										<input type="text" class="form-control input-inline input-xlarge" placeholder="Enter message subject/title" name="messageSubject" id="msgsubj" required>
										<!-- <span class="help-block">Maximum of 20 charaters only.</span> wala daw limit ang subject-->
									</div>
								</div>

								<div class="form-group">
									<textarea class="wysihtml5 form-control" rows="6" placeholder="Start composing your message here ..." name="messageContent" id="msgcont" required></textarea>
								</div>

									<div class="inbox-compose-btn">
										<button class="btn green-meadow"><i class="fa fa-check"></i>Send</button>
										<!-- <button class="btn">Discard</button> -->
										<button class="btn" id="drftcompose">Draft</button>
										<button class="btn" id="msgdiscard">Discard</button>
										<!-- <button id="testdel">DEL</button> -->
									</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>