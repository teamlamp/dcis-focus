<div class="" id="senddraft">
<!-- BEGIN PAGE CONTENT-->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXTRAS PORTLET-->
			<div class="portlet light" style="padding: 12px 10px 15px 10px !important;">
				<div class="portlet-title">
					<div class="caption font-green-meadow">
						<i class="fa fa-edit font-green-meadow"></i>
						<span class="caption-subject bold uppercase"> Send Draft </span>
					</div>
				</div>
				<div class="portlet-body form">
					<form class="form-horizontal form-bordered" method="POST" action="<?= base_url('message/sendDraft') ?>" id="snddrft">
					<input type="hidden" name="msg_group" value="<?= $msg_group['msg_group'] ?>">
						<div class="form-body">
							<div class="form-group">

								<div class="form-group">
									<label class="control-label col-md-2">To :</label>
									<div class="col-md-10">
										<!-- <select class="form-control input-large select2me" data-placeholder="Select...">
										</select> -->
										<select multiple id="draftRecipients" class="form-control select2 input-inline input-xlarge" name="draftRecipients[]" required>
											<!-- prepend draft recipients -->
											<?php foreach($draft_recipients as $recipient){?>
												<option value="<?= $recipient['school_id'] ?>" selected="selected"><?= $recipient['firstname']." ".$recipient['lastname'] ?></option>
											<?php } ?>
										</select>

										<span class="help-block">
										Type in the recipient of this message.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label">Subject :</label>
									<div class="col-md-10">
										<input type="text" value="<?= $subject['msg_subject'] ?>" class="form-control input-inline input-xlarge" placeholder="Enter message subject/title" name="draftSubject" id="draftmsgsubj" required>
										<!-- <span class="help-block">Maximum of 20 charaters only.</span> wala daw limit ang subject-->
									</div>
								</div>

								<div class="form-group">
									<textarea class="wysihtml5 form-control" rows="6" placeholder="Start composing your message here ..." name="draftContent" id="draftmsgcont" required><?= $content['msg_body'] ?></textarea>
								</div>

									<div class="inbox-compose-btn">
										<button class="btn green-meadow"><i class="fa fa-check"></i>Send</button>
										<!-- <button class="btn">Discard</button> -->
										<button class="btn" id="dftcancel">Cancel</button>
									</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#draftRecipients").select2({        
    ajax: {
      url: BASE_URL+'users/findUser',
      dataType: 'json',
      tags: true,
      tokenSeparators: [",", " "],
      delay: 200,
      data: function (params) {
        return {
          q: params.term // search term
        };
      },
      processResults: function (data) {
        return {
          results: data
        };
      },
      cache: true
    },
    minimumInputLength: 3
	});
});	
</script>