<?php


?>
<div class="tab-pane" id="trash">
	<!-- <div class="inbox-header">
		<h1 class="pull-left">Trash</h1>
		<form class="form-inline pull-right" action="index.html">
			<div class="input-group input-medium">
				<input type="text" class="form-control" placeholder="Password">
				<span class="input-group-btn">
				<button type="submit" class="btn green"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
	</div> -->

<!-- BEGIN INBOX CONTENT -->
	<div class="portlet light msgbox">

		<div class="portlet-title">
			<div class="caption font-grey-gallery">
				<i class="fa fa-trash font-grey-gallery"></i>
				<span class="caption-subject bold uppercase"> Trash</span>
			</div>
			<div class="actions">
				<div class="btn-group">
					<a class="btn grey-gallery btn-sm msgboxadm" href="javascript:;" data-toggle="dropdown" disabled="disabled">
					<i class="fa fa-cogs"></i> Actions <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-right">
						<li>
							<a href="javascript:;" id="permadelselmsgs">
							<i class="fa fa-trash-o"></i>Permanent Delete<br>Marked Messages</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="portlet-body">
		<?php if($trash_messages){?>
			<table class="table table-striped table-bordered table-hover" id="trashm">
				<thead>
					<tr>
						<th class="table-checkbox">
							<input type="checkbox" class="group-checkable" data-set="#trashm .checkboxes" disabled/>
						</th>
						<th>
							<i class="fa fa-user"></i> Sender
						</th>
						<th>
							<i class="fa fa-font"></i> Message Title
						</th>
						<th>
							<i class="fa fa-calendar"></i> Date Sent
						</th>
					</tr>
				</thead>
				<tbody id="tblbdytrsh">
				<?php foreach($trash_messages as $trash){?>
					<tr class="odd gradeX" id="<?= $trash['msg_id'] ?>">
						<td> <input type="checkbox" value="<?= $trash['msg_id'] ?>" class="checkboxes" disabled/></td>
						<td class="msgclosed"><?= $trash['firstname']." ".$trash['lastname'] ?></td>
						<td class="msgclosed"><?= $trash['msg_subject'] ?></td>
						<td class="msgclosed"><?= date('F d, Y g:m A',strtotime($trash['msg_created_at'])) ?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
			<span class="label label-danger">NOTE</span><em> Group deletion is not allowed here to prevent accidents.</em>
		<?php } else {?>
			<div class="note note-danger" id="ntrshntrr">
          <h4 class="block">No messages in your trash.</h4>
          <p>Deleted messages will be viewed here. Messages in Trash ca be completely deleted by opening the selected message and clicking the "delete" button.</p>
      </div>
		<?php } ?>
		</div>
	</div>
	<!-- END EXAMPLE TABLE PORTLET-->
</div>