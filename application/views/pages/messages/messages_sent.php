
<div class="tab-pane" id="sent">

		<!-- BEGIN DRAFT CONTENT -->
	<div class="portlet light msgbox">

		<div class="portlet-title">
			<div class="caption font-blue-hoki">
				<i class="fa fa-send font-blue-hoki"></i>
				<span class="caption-subject bold uppercase"> Outbox</span>
			</div>
			<div class="actions">
				<div class="btn-group">
					<a class="btn blue-hoki btn-sm msgboxadm" href="javascript:;" data-toggle="dropdown" disabled="disabled"><!-- class="msgboxadm" -->
					<i class="fa fa-cogs"></i> Actions <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-right">
						<li>
							<a href="javascript:;" class="delselmsgs">
							<i class="fa fa-trash-o"></i> Delete Marked Messages </a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="portlet-body">
		<?php if($sentbox_messages){?>
			<table class="table table-striped table-bordered table-hover" id="sentboxm">
				<thead>
					<tr>
						<th class="table-checkbox">
							<input type="checkbox" class="group-checkable" data-set="#sentboxm .checkboxes"/>
						</th>
						<th>
							<i class="fa fa-user"></i> Receiver
						</th>
						<th>
							<i class="fa fa-font"></i> Message Title
						</th>
						<th>
							<i class="fa fa-calendar"></i> Date Sent
						</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($sentbox_messages as $sent){?>
					<tr class="odd gradeX" id="<?= $sent['msg_map_id'] ?>">
						<td> <input type="checkbox" value="<?= $sent['msg_map_id'] ?>" class="checkboxes"/>	</td>
						<td class="msgclosed"><?= $sent['firstname']." ".$sent['lastname'] ?></td>
						<td class="msgclosed"><?= $sent['msg_subject'] ?></td>
						<td class="msgclosed"><?= date('F d, Y g:m A',strtotime($sent['msg_created_at'])) ?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		<?php } else {?>
			<div class="note note-danger">
          <h4 class="block">No messages in your sentbox.</h4>
          <p>Sent messages can be viewed here. Start composing and send it to someone.</p>
      </div>
		<?php } ?>
		</div>
	</div>
	<!-- END EXAMPLE TABLE PORTLET-->
</div>