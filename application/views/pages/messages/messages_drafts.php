<div class="tab-pane" id="drafts">
	<!-- <div class="inbox-header">
		<h1 class="pull-left">Drafts</h1>
		<form class="form-inline pull-right" action="index.html">
			<div class="input-group input-medium">
				<input type="text" class="form-control" placeholder="Password">
				<span class="input-group-btn">
				<button type="submit" class="btn green"><i class="fa fa-search"></i></button>
				</span>
			</div>
		</form>
	</div> -->
<!-- BEGIN INBOX CONTENT -->
	<div class="portlet light msgbox">
		<div class="portlet-title">
			<div class="caption font-grey-cascade">
				<i class="fa fa-file-text font-grey-cascade"></i>
				<span class="caption-subject bold uppercase"> Drafts</span>
			</div>
			<div class="actions">
				<div class="btn-group">
					<a class="btn grey-cascade btn-sm msgboxadm" href="javascript:;" data-toggle="dropdown" disabled="disabled">
					<i class="fa fa-cogs"></i> Actions <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-right">
						<li>
							<a href="javascript:;" class="delselmsgs">
							<i class="fa fa-trash-o"></i> Delete Marked Messages </a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="portlet-body">
		<?php if($drafts_messages){?>
			<table class="table table-striped table-bordered table-hover" id="draftsm">
				<thead>
					<tr>
						<th class="table-checkbox">
							<input type="checkbox" class="group-checkable" data-set="#draftsm .checkboxes"/>
						</th>
						<th>
							<i class="fa fa-user"></i> Receiver
						</th>
						<th>
							<i class="fa fa-font"></i> Message Title
						</th>
						<th>
							<i class="fa fa-calendar"></i> Date Sent
						</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($drafts_messages as $draft){?>
					<tr class="odd gradeX" id="<?= $draft['msg_map_id'] ?>">
						<td> <input type="checkbox" value="<?= $draft['msg_map_id'] ?>" class="checkboxes"/>	</td>
						<td class="rsnddrft"><?= $draft['firstname']." ".$draft['lastname'] ?></td>
						<td class="rsnddrft"><?= $draft['msg_subject'] ?></td>
						<td class="rsnddrft"><?= date('F d, Y g:m A',strtotime($draft['msg_created_at'])) ?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		<?php } else {?>
			<div class="note note-danger">
          <h4 class="block">No messages in your drafts.</h4>
          <p>Unfinished / Drafted messages will be viewed here.</p>
      </div>
		<?php } ?>
		</div>
	</div>
	<!-- END EXAMPLE TABLE PORTLET-->
</div>