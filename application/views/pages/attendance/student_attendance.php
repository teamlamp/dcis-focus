<!DOCTYPE html>
<html lang="en">
<head>
<?php foreach($one_class as $class) ?>

<title>DCIS | Class <?= $class['course_code'].' Group '.$class['group_number']; ?></title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE STYLES -->
<link href="/../assets/admin/admin-page/gridster/dist/jquery.gridster.css" rel="stylesheet" type="text/css"/>
<link href="/../assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
<style>
  .grid-body {
    padding-bottom: 0px !important;
  }
</style>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
  <!-- BEGIN CONTAINER -->
  <div class="page-container">

    <!-- BEGIN SIDEBAR -->
    <?php require_once 'application/views/includes/sidebar.phtml';?>  
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">

        <!-- BEGIN STYLE CUSTOMIZER -->
        <?php require_once 'application/views/includes/style_customizer.phtml';?> 
        <!-- END STYLE CUSTOMIZER -->

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">        
        Class <small><?= $class['course_code']. " Group ".$class['group_number']; ?> (7:30-9:00 MW)</small>
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="icon-notebook"></i>
              <a href="<?= base_url('classes') ?>">Classes</a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="javascript:;"><?= $class['group_number']." ".$class['course_code']; ?></a>
              <input id="class_id" type="hidden" value="<?= $class['class_id'];?>">
              <input id="user_role" type="hidden" value="<?= $this->session->userdata('user_role');?>">
              <input id="username" type="hidden" value="<?= $this->session->userdata('firstname').' '.$this->session->userdata('lastname');?>">
            </li>
          </ul>
          <div class="page-toolbar">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
              Actions <i class="fa fa-arrow-circle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <?php if($this->session->userdata('user_role') != "student"){?>
                <li>
                  <a href="<?php echo base_url("{$current_url_1}/students"); ?>"><i class="fa fa-users"></i> Students List</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/classrecord"); ?>"><i class="fa fa-file-text"></i> Class Record</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/attendance"); ?>"><i class="fa fa-pencil"></i> Attendance</a>
                </li>
                <?php } else if ($this->session->userdata('user_role') == "student"){?>
                <li>
                  <a href="<?= base_url("{$current_url_1}/assessment"); ?> "><i class="fa fa-file-text"></i> Assessment</a>
                </li>
                <?php } ?>
                <?php if($this->session->userdata('user_role')!="student"){ ?>
                <li class="divider"></li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/grades/record"); ?>"><i class="fa fa-save"></i> Record Grades</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/grades/history"); ?>"><i class="fa fa-history"></i> Record History</a>
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/grades/rubrics"); ?>"><i class="fa fa-cube"></i> Class Rubrics</a>
                </li>             
                <li class="divider">
                </li>
                <li>
                  <a href="<?= base_url("{$current_url_1}/settings") ?>"><i class="fa fa-cog"></i> Class Settings</a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->

        <div class="row">
         <?php 
            $c = 0;
         if(!$absences)
          { ?>
            <div class="note note-success">
              <h4 class="block">No absences recorded!</h4>
          </div>
         <?php }
          else{

         foreach($absences as $a)
         {
        date_default_timezone_set("Asia/Manila");
        $datestring = "%Y-%m-%d";
        $ndate = date('F d, Y',strtotime($a->date_today));


          echo "Marked as ".$a->type." on ".$ndate;
          echo "<br>";
          $c++;
         }

         echo "Total # of absence(s) is ".$c;
          }

         ?>

<br>
<br>

       <?php
       $d = 0; 
         if(!$lates)
          { ?>
            <div class="note note-success">
              <h4 class="block">No lates recorded!</h4>
          </div>
          <?php }
          else{
            
         foreach($lates as $l)
         {
    date_default_timezone_set("Asia/Manila");
$datestring = "%Y-%m-%d";
    $ndate = date('F d, Y',strtotime($l->date_today));


          echo "Marked as ".$l->type." on ".$ndate;
          echo "<br>";
          $d++;
         }

         echo "Total # of late(s) is ".$d;
          }

if($c == NULL && $d == NULL){
$e = 0;
}

elseif($c != NULL && $d == NULL){
  $e = $c;
}

elseif($c == NULL && $d != NULL){
  $f = $d / 3;
  $e = number_format($f);
}

else{
$e = $c + ($d / 3);
}


echo "<br><br>";
if($e >= $class['max_absences'])
{
  echo "Status: <b>Dropped</b>";
}

if($e >= 3)
{
  echo "Status: <b>Readmission</b>";
}

         ?>


  
        </div>

        <!-- END PAGE CONTENT-->
      </div>
    </div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->

  <!-- BEGIN FOOTER -->
  <?php require_once 'application/views/includes/footer.phtml';?> 
  <!-- END FOOTER -->
</div>

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->


<!-- BEGIN CORE PLUGINS -->
  <?php require_once 'application/views/includes/core_js.phtml';?> 

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/admin/admin-page/gridster/dist/jquery.gridster.js"></script>
<script type="text/javascript" src="/../assets/admin/pages/scripts/admin.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?> 
<!-- END THEME PLUGINS -->

<script>
jQuery(document).ready(function() {
   AdminPage.init();
});
</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
