<div class="portlet light blue" style="padding-bottom:5px!important;display: none;">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-copy font-grey-cararra"></i>
        <span class="caption-subject bold uppercase font-grey-cararra"> Tasks</span>
        <span class="caption-helper font-grey-cararra">Take some personal notes</span>
    </div>
    <div class="actions">
      <a class="btn grey-gallery font-grey-cararra" data-toggle="modal" href="#edit_task">
      <i class="fa fa-plus font-grey-cararra" id="edit-task"></i> Edit Task </a>
    </div>
  </div>
</div> 

<!-- /.modal -->
<div class="modal fade bs-modal-lg" id="edit_task" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
  <form action='' method="POST" id="form-edit-task">
        <input id="school_id" type="hidden" value="<?= $this->session->userdata('school_id');?>">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit Task &nbsp;&nbsp;
        <span class="btn-group btn-group">
        <button type="button" class="btn blue" id="edit-task-color">Task Color</button>
        <select class="form-control input-medium" id="edit-task-type">
                    <option value="blue">Blue</option>
                    <option value="green">Green</option>
                    <option value="red">Red</option>
                    <option value="yellow">Yellow</option>
                    <option value="purple">Purple</option>
                    <option value="grey">Grey</option>
                  </select>
                  </span></h4>
      </div>
      <div class="modal-body">
      <div class="form-group">
        <div class="input-group">
          <span class="input-group-addon" id="task-heads-up">
          <i class="fa fa-tasks"></i>
          </span>
          <input type="text" id="edit-task-title" maxlength="15" class="form-control" name="title" placeholder="Task Title" required>
        </div>
          <!-- <div class="form-group form-md-line-input has-success form-md-floating-label">
            <div class="input-icon" id="task-heads-up">
              <input type="text" id="task-title" class="form-control" name="title">
              <label for="form_control_1">Task Title</label>
              <span class="help-block">Note: Task title can reach a maximum of 15 charaters only!</span>
              <i class="fa fa-tasks"></i>
            </div>
          </div> -->
      </div>
        <textarea class="form-control todo-taskbody-taskdesc input" name="content" rows="3" placeholder="Type your task/notes content ..." id="edit-task-content" required style="margin-top:15px;resize:none;"></textarea>
      </div>
      <input type="hidden" id="edit-task-id">
      <div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn blue" id="edit-save-task">Save Edited Task</button>
      </div>
    </div>                    
  </form>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>