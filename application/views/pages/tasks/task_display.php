<div id="tskhldrid">
	<?php if($my_tasks){foreach($my_tasks as $task){?>
	<div class="col-md-4 tskhldr" ><!-- style="border: 1px solid #0F5003;" -->
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet box <?= $task['color'] ?> tskdsplyclr">
			<div class="portlet-title">
				<div class="caption tskdsplyttl">
					<?= $task['title'] ?>
				</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-default btn-sm tskedt" title="Edit">
					<i class="fa fa-pencil"></i> </a>
					<a href="javascript:;" class="btn btn-default btn-sm tskdone" style="margin-left:0px!important" title="Completed">
					<i class="fa fa-check"></i> </a>									
					<input type="hidden" class="tskid" value="<?= $task['task_id'] ?>">
				</div>
			</div>
			<div class="portlet-body">
				<div class="scroller tskdsplycnt" style="height:200px">
					<?= $task['content'] ?>
				</div>
				<h6 class="tskdsplytime"><?= date('F d, Y - g:m A', strtotime($task['modified_at'])) ?></h6>
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
	<?php }} else{?>
	<div class="col-md-12" id="no-task-span">
		<div class="note note-danger">
		    <h4 class="block">No tasks to display.</h4>
		    <p>Please add a task.</p>
		</div>
	</div>
	<?php } ?>
</div>

<div id="deltskhldrid" style="display: none;">
	<?php if($deleted_tasks){foreach($deleted_tasks as $del_task){?>
	<div class="col-md-4 deltskhldr"><!-- style="border: 1px solid #0F5003;" -->
		<!-- BEGIN Portlet PORTLET-->
		<div class="portlet box <?= $del_task['color'] ?> tskdsplyclr">
			<div class="portlet-title">
				<div class="caption tskdsplyttl">
					<?= $del_task['title'] ?>
				</div>
				<div class="actions">
					<a href="javascript:;" class="btn btn-default btn-sm tskdel" style="margin-left:0px!important" title="Delete">
					<i class="fa fa-trash"></i> </a>									
					<input type="hidden" class="tskid" value="<?= $del_task['task_id'] ?>">
				</div>
			</div>
			<div class="portlet-body">
				<div class="scroller tskdsplycnt" style="height:200px">
					<?= $del_task['content'] ?>
				</div>
				<h6 class="tskdsplytime"><?= date('F d, Y - g:m A', strtotime($del_task['modified_at'])) ?></h6>
			</div>
		</div>
		<!-- END Portlet PORTLET-->
	</div>
	<?php }} else{?>
	<div class="col-md-12" id="no-task-span">
		<div class="note note-danger">
		    <h4 class="block">No completed tasks to display.</h4>
		</div>
	</div>
	<?php } ?>
</div>