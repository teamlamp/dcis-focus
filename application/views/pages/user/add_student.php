<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Add student </title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE STYLES -->
<link href="../../assets/admin/pages/css/blog.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="../../assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/admin/admin-page/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
  <!-- BEGIN CONTAINER -->
  <div class="page-container">

    <!-- BEGIN SIDEBAR -->
    <?php require_once 'application/views/includes/sidebar.phtml';?>  
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">

        <!-- BEGIN STYLE CUSTOMIZER -->
        <?php require_once 'application/views/includes/style_customizer.phtml';?> 
        <!-- END STYLE CUSTOMIZER -->

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
        
        Users
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="icon-users"></i>
              <a href="<?= base_url('users') ?>">Users</a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="javascript:;">Add Student</a>
            </li>
          </ul>
          <div class="page-toolbar">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
              Actions <i class="fa fa-arrow-circle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <li>
                  <a href="javascript:;>" data-toggle="modal" data-target="#importusers"><i class="fa fa-plus"></i> Import Users</a>
                </li>
                <li>
                  <a href="<?= base_url('users/addStudent') ?>"><i class="fa fa-plus"></i> Add Student</a>
                </li>
                <li>
                  <a href="<?= base_url('users/addInstructor') ?>"><i class="fa fa-plus"></i> Add Instructor</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
          <div class="col-md-12">
            <div class="portlet box grey-gallery">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit"></i>
                  <span class="caption-subject bold uppercase">Add student</span>
                </div>
              </div>
              <div class="portlet-body form">
              <?php echo validation_errors(); ?>
                <form role="" action="<?php echo base_url('users/addNewStudent'); ?>" id="add_student" method="POST" class="form-horizontal form-bordered form-row-stripped">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-3 control-label">School ID</label>
                      <div class="col-md-5">
                        <input type="text" class="form-control" name="school_id" placeholder="School ID">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Program
                      </label>
                      <div class="col-md-5">
                        <select  class="form-control select2me" name="program_id" data-placeholder="Select...">
                          <option value=""></option>
                          <?php foreach ($programs as $program):?>
                            <option value="<?=$program->program_id;?>"><?=$program->program_code;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3">Date of Birth</label>
                      <div class="col-md-5">
                        <div class="input-group date date-picker" data-date-format="mm/dd/yyyy">
                          <input type="text" class="form-control" readonly name="consultation_date">
                          <span class="input-group-btn">
                          <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                      </div>
                    </div>


                  </div>
                  <!-- end form body -->
                  <div class="form-actions">
                    <div class="row">
                      <div class="col-md-offset-2 col-md-10">
                        <button type="button" class="btn default">Cancel</button>
                        <button type="submit" class="btn blue">Submit</button>
                      </div>
                    </div>
                  </div>
                  <!-- end form action -->
                </form>               
              </div>
            </div>
          </div>
        </div>

        <!-- END PAGE CONTENT-->
      </div>
    </div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->

  <!-- BEGIN FOOTER -->
  <?php require_once 'application/views/includes/footer.phtml';?> 
  <!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
  <?php require_once 'application/views/includes/core_js.phtml';?>  
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="/../assets/admin/admin-page/select2/select2.js"></script>

<script type="text/javascript" src="/../assets/global/plugins/jquery-validation/js/jquery.validate.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?> 
<!-- END THEME PLUGINS -->

<script>
jQuery(document).ready(function() { 
});
</script>

<script src="/../assets/admin/admin-page/js/custom.js"></script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
