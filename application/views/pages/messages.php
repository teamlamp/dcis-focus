<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Messages</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/admin/pages/css/inbox.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<link href="/../assets/admin/pages/css/blog.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/jquery-multi-select/css/multi-select.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">
<!-- END BODY -->

<!-- BEGIN HEADER -->
<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->



		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- END STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Messages <small></small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-archive"></i>
							<a href="<?= base_url('messages') ?>">Messages</a>
							<i class="fa fa-angle-double-right"></i>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->
					<div class="portlet-body">
						<div class="row inbox">
							<div class="col-md-2">
							<!-- <ul class="inbox-nav margin-bottom-10">
								<li class="compose-btn">
									<a href="<?= base_url('message/compose')?>" class="btn green">
									<i class="fa fa-pencil"></i> Compose </a>
								</li>
							</ul> -->
							<ul class="ver-inline-menu tabbable margin-bottom-10">
									<li class="tabopttab">
										<a data-toggle="tab" href="#compose" class="tabopt">
										<i class="fa fa-edit"></i> Compose </a>
									</li>
									<li class="active tabopttab">
										<a data-toggle="tab" href="#inbox" class="tabopt">
										<i class="fa fa-inbox"></i> Inbox </a>
										<span class="after">
										</span>
									</li>
									<li class="tabopttab">
										<a data-toggle="tab" href="#sent" class="tabopt">
										<i class="fa fa-send"></i> Sent </a>
									</li>
									<li class="tabopttab" id="tabdrafts">
										<a data-toggle="tab" href="#drafts" class="tabopt">
										<i class="fa fa-file-text"></i> Drafts </a>
									</li>
									<li class="tabopttab">
										<a data-toggle="tab" href="#trash" class="tabopt">
										<i class="fa fa-trash"></i> Trash </a>
									</li>
									
								</ul>
							</div>
							
							<div class="col-md-10">
								<div class="tab-content">
									<!-- BEGIN INBOX -->
									<?php require_once VIEWPATH.'/pages/messages/messages_inbox.php';?>
									<!-- END OF INBOX -->

									<!-- BEGIN COMPOSE -->
									<?php require_once VIEWPATH.'/pages/messages/compose.php';?>
									<!-- END OF COMPOSE -->

									<!-- BEGIN SENT -->
									<?php require_once VIEWPATH.'/pages/messages/messages_sent.php';?>
									<!-- END OF SENT -->

									<!-- BEGIN DRAFTS -->
									<?php require_once VIEWPATH.'/pages/messages/messages_drafts.php';?>
									<!-- END OF DRAFTS -->

									<!-- BEGIN TRASH -->
									<?php require_once VIEWPATH.'/pages/messages/messages_trash.php';?>
									<!-- END OF TRASH -->

									<!-- BEGIN MESSAGE VIEW -->
									<?php //require_once VIEWPATH.'/pages/messages/message_view.php';?>
									<!-- END OF MESSAGE VIEW -->
								</div>
							</div>
							</div>
						</div>
				</div>
			</div>
		</div>
		<!-- END CONTENT -->
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="/../assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>

<script type="text/javascript" src="/../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php require_once 'application/views/includes/theme_js.phtml';?>
<script src="/../assets/admin/pages/scripts/components-dropdowns.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#tagRecipients").select2({        
    ajax: {
      url: BASE_URL+'users/findUser',
      dataType: 'json',
      tags: true,
      tokenSeparators: [",", " "],
      delay: 200,
      data: function (params) {
        return {
          q: params.term // search term
        };
      },
      processResults: function (data) {
        return {
          results: data
        };
      },
      cache: true
    },
    minimumInputLength: 3
	});
});
</script>
<script src="/../assets/admin/pages/scripts/components-editors.js"></script>
<script>
jQuery(document).ready(function() {
	TableManaged.init();
   ComponentsEditors.init();
	
});
</script>
<script src="/../assets/admin/pages/scripts/table-managed.js"></script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
