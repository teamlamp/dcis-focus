<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | User Management</title>
<!-- <input type="hidden" id="class_id" value="<?= $class['class_id']; ?>"> -->

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<link href="../../assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>

<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<link rel="stylesheet" type="text/css" href="/../assets/admin/pages/css/custom.css"/>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- END STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Programs
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="icon-notebook"></i>
							<a href="<?= base_url('programs')?>">Programs</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="<?= base_url('programs/addProgram')?>"><i class="fa fa-plus"></i> Add Program</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="profile-content">
				<!--beginning -->
				<?php foreach($programs as $row) { if($row->program_code != "NA"){ ?>
					<div class="col-md-4">
							<!-- PORTLET MAIN -->
							<div class="portlet light profile-sidebar-portlet">
								<!-- SIDEBAR USERPIC -->
								<div class="programs-code" style="margin: 0 auto;">
								<div class="valign"><?php echo $row->program_code; ?></div>
								</div>
	
								<!-- END SIDEBAR USERPIC -->
								<!-- SIDEBAR USER TITLE -->
								<div class="profile-usertitle">
									<div class="profile-usertitle-job">
										 Bachelor of Science in
									</div>
									<div class="profile-usertitle-name uppercase" style="font-size:15px !important;">
										 Information and Communication Technology
									</div>
								</div>
								<!-- END SIDEBAR USER TITLE -->
								<!-- SIDEBAR BUTTONS -->
								<div class="profile-userbuttons">
									<button type="button" class="btn btn-circle green-haze btn-sm"><span class="fa fa-edit"></span> Edit</button>
									<form action="<?php echo base_url('programs/deleteProgram'); ?>" method="POST">
									<input type="hidden" name="program_id" value="<?php echo $row->program_id; ?>">
									<button type="submit" class="btn btn-circle btn-danger btn-sm"><span class="fa fa-trash-o"></span> Delete</button>
									</form>
								</div>
								<!-- END SIDEBAR BUTTONS -->
								<div class="row list-separated profile-stat">
									<div class="col-md-5 col-sm-6 col-xs-6 col-md-offset-1">
										<div class="uppercase profile-stat-title">
											 1300
										</div>
										<div class="uppercase profile-stat-text">
											 Students
										</div>
									</div>
									<div class="col-md-5 col-sm-6 col-xs-6">
										<div class="uppercase profile-stat-title">
											 15
										</div>
										<div class="uppercase profile-stat-text">
											 Instructors
										</div>
									</div>
								</div>
							</div>
							<!-- END PORTLET MAIN -->
						</div>
						<?php } } ?>
						<!--end -->
				</div>
				<!-- BEGIN PAGE CONTENT-->
		<?php require_once 'application/views/modals/import_users.phtml';?>
		</div>

			<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>
	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
	<?php require_once 'application/views/includes/theme_js.phtml';?>	
	<script>
      jQuery(document).ready(function() {    

		DcisTableCoursesAndUsers.init();
		
      });
   </script>
	<script src="/../assets/dcis/js/dcis-table-courses-users.js"></script>

<!-- END THEME PLUGINS -->
	
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>