<!DOCTYPE html>
<!-- BEGIN HEAD -->
<head>
<title>DCIS | Settings</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="/../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<link href="/../assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="/../assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
<link href="/../assets/global/plugins/icheck/skins/all.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">

<!-- BEGIN HEADER -->
<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix">
</div>
<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- BEGIN STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Settings
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-cogs"></i>
							<a href="index.html">Settings</a>
							<i class="fa fa-angle-double-right"></i>
						</li>
					</ul>
				</div>
				<!-- END PAGE HEADER-->

				<!-- BEGIN PAGE CONTENT-->

				<div class="profile-content">
					<div class="row">
						<div class="col-md-12">
							<div class="portlet light">
								<div class="portlet-title tabbable-line tabbalable-custom">
									<!-- <div class="caption caption-md">
										<i class="fa fa-user hide"></i>
										<span class="caption-subject font-green-haze bold uppercase">Profile Account</span>
									</div> -->
									<ul class="nav nav-tabs nav-justified">
										<li class="active">
											<a href="#tab_1_1" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-user"></i> Personal Info</span></a>
										</li>
										<li>
											<a href="#tab_1_2" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-eye"></i> Privacy Settings</span></a>
										</li>
										<li>
											<a href="#tab_1_3" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-image"></i> Change Profile Photo</span></a>
										</li>
										<li>
											<a href="#tab_1_4" data-toggle="tab"><span class="font-grey-gallery"><i class="fa fa-lock"></i> Change Password</span></a>
										</li>
									</ul>
								</div>
								<div class="portlet-body">
									<div class="tab-content">
										<!-- PERSONAL INFO TAB -->
										<?php require_once VIEWPATH.'/pages/settings/setting_personalinfo.php';?>
										<!-- END PERSONAL INFO TAB -->
										
										<!-- PRIVACY SETTINGS TAB -->
										<?php require_once VIEWPATH.'/pages/settings/setting_privacy.php';?>
										<!-- END PRIVACY SETTINGS TAB -->

										<!-- CHANGE AVATAR TAB -->
										<?php require_once VIEWPATH.'/pages/settings/setting_changeavatar.php';?>
										<!-- END CHANGE AVATAR TAB -->

										<!-- CHANGE PASSWORD TAB -->
										<?php require_once VIEWPATH.'/pages/settings/setting_changepassword.php';?>
										<!-- END CHANGE PASSWORD TAB -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/../assets/global/plugins/icheck/icheck.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/../assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/../assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="/../assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<script src="/../assets/admin/pages/scripts/form-icheck.js"></script>
<script src="/../assets/admin/pages/scripts/components-pickers.js"></script>
<script src="/../assets/admin/pages/scripts/components-form-tools.js"></script>
<script src="/../assets/admin/pages/scripts/admin.js"></script>
<!-- BEGIN END LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {    
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features
	FormiCheck.init(); // init page demo
	ComponentsPickers.init();
	ComponentsFormTools.init();
});
</script>


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>