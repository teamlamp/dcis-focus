<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Consultation</title>
<!-- <input type="hidden" id="class_id" value="<?= $class['class_id']; ?>"> -->

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/dcis/sweetalert/sweetalert.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- END STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Consultations
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="icon-notebook"></i>
							<a href="<?= base_url('consultation')?>">Consultation</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
							<?php if($this->session->userdata('user_role') == "instructor" || $this->session->userdata('user_role') == "admin"){?>
								<li>
									<a href="<?= base_url('consultation/addNewConsultation')?>"><i class="fa fa-plus"></i> Add New Consultation</a>
								</li>
										<?php } ?>
								<li>
								<?php 

//$role = $this->session->userdata('user_id');
								if($this->session->userdata('user_role') == "instructor"){?>
								<a href="<?php echo base_url("exports/exportAllConsultationRecordsByInstructorAsPDF"); ?>">Generate Report</a>
								<?php }
								elseif($this->session->userdata('user_role') == "student"){?>
									<a href="<?php echo base_url("exports/exportAllConsultationRecordsByStudentAsPDF"); ?>">Generate Report</a>
							<?php }

							else{ ?>
									<a href="<?= base_url('exports/exportAllConsultationRecordsAsPDF')?>">Generate Report</a>
								<?php } ?>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>

							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<?php if(!$consultations){?>
				<div class="note note-danger">
              		<h4 class="block">No consultation records.</p>
          		</div>
				<?php } else { ?>
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box grey-gallery">
							<div class="portlet-title ">
								<div class="caption">
									<i class="fa fa-globe"></i>All Consultation 
								</div>
							</div>
							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover" id="usersList">
								<thead>
									<tr>
										<th>Consultation #</th>
										<th>Topic</th>
							 			<th>Date</th>
							 			<th>Class</th>
							 			
							 			<!-- <th></th> -->
									</tr>
								</thead>
								<tbody>
								<?php 
          							if($consultations){foreach($consultations as $consultation){ ?>
          							<tr data-toggle="modal" data-id = "<?= $consultation['consultation_id'];?>" data-content="<?= $consultation['consultation_content'];?>" data-target="#viewconsultation">
								 		<td class="consultation_id"><?= $consultation['consultation_id'];?></a></td>
								 		<td class="topic"><?= $consultation['consultation_topic']; ?></td>
								 		<td class="consultation_date"><?= $consultation['consultation_date']?></td>
								 		<td class="course_code">G<?= $consultation['group_number']?> <?= $consultation['course_code']?></td>
								 		
								 		<!-- <td><button>Remove</button></td> -->
								 	</tr>
								<?php } ?>
								</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				<!-- END PAGE CONTENT-->
				</div>
				<?php } }?>
		</div>

			<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>
	<!-- MODAL -->
	<?php require_once 'application/views/modals/consultation_students.phtml' ?>
	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="/../assets/admin/pages/scripts/components-editors.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
	<?php require_once 'application/views/includes/theme_js.phtml';?>	
	<script>
      jQuery(document).ready(function() {    
		DcisTableCoursesAndUsers.init();
		ComponentsEditors.init();
		
      });
   </script>
   <script>
   	
   </script>
   <script src="/../assets/admin/admin-page/js/custom.js"></script>
   <script src="/../assets/dcis/js/dcis-table-courses-users.js"></script>
   <script src="/../assets/dcis/sweetalert/sweetalert.min.js"></script>
<!-- END THEME PLUGINS -->
	
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>
