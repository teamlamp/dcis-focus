<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Consultation </title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE STYLES -->
<link href="../../assets/admin/pages/css/blog.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/admin/admin-page/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
  <!-- BEGIN CONTAINER -->
  <div class="page-container">

    <!-- BEGIN SIDEBAR -->
    <?php require_once 'application/views/includes/sidebar.phtml';?>  
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <div class="page-content">

        <!-- BEGIN STYLE CUSTOMIZER -->
        <?php require_once 'application/views/includes/style_customizer.phtml';?> 
        <!-- END STYLE CUSTOMIZER -->

        <!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
        
        Consultation 
        </h3>
        <div class="page-bar">
          <ul class="page-breadcrumb">
            <li>
              <i class="icon-notebook"></i>
              <a href="<?= base_url('consultation') ?>">Consultations</a>
              <i class="fa fa-angle-double-right"></i>
            </li>
            <li>
              <a href="javascript:;">New Consultation</a>
            </li>
          </ul>
          <div class="page-toolbar">
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
              Actions <i class="fa fa-arrow-circle-down"></i>
              </button>
              <ul class="dropdown-menu pull-right" role="menu">
                <?php if($this->session->userdata('user_role') == "instructor" || $this->session->userdata('user_role') == "admin"){?>
                <li>
                  <a href="<?= base_url('consultation/addNewConsultation')?>"><i class="fa fa-plus"></i> Add New Consultation</a>
                </li>
                <li>
                  <a href="#">Generate Report</a>
                </li>
                <li class="divider">
                </li>
                <li>
                  <a href="#">Separated link</a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
          <div class="col-md-12">
            <div class="portlet box grey-gallery">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-edit"></i>
                  <span class="caption-subject bold uppercase">New Consultation</span>
                </div>
              </div>
              <div class="portlet-body form">
                <form role="" action="<?php echo base_url('consultation/addConsultation'); ?>" id="consultation" method="post" class="form-horizontal">
                  <div class="form-body">
                  <div class="alert alert-danger display-hide">
                      <button class="close" data-close="alert"></button>
                      You have some form errors. Please check below.
                    </div>
                  <?php echo validation_errors(); ?>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Consultation Title</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control" name="consultation_topic" placeholder="Enter consultation title" required>
                        <div class="form-control-focus">
                        </div>
                      </div>
                    </div>
                    <?php if($this->session->userdata('user_role') != 'instructor'): ?>
                    <div class="form-group">
                    <label class="col-md-2 control-label">Instructor</label>
                      <div class="col-md-10">
                        <select id="instructor" class="form-control select2me required" name="instructor_id" data-placeholder="Select..." aria-required="true" aria-invalid="false">
                          <option value=""></option>
                          <?php

                            foreach($instructors as $row){
                              echo "<option value='".$row->instructor_id."'>".$row->firstname." ".$row->lastname."</option>";
                            }

                          ?>

                        </select>
                      </div>
                    </div>
                    <?php endif; ?>
                    <?php if($this->session->userdata('user_role') == 'instructor'): ?>
                      <input type="hidden" value="<?=$instructor_id[0]?>" name="instructor_id">
                    <?php endif; ?>
                    <div class="form-group">
                    <label class="col-md-2 control-label">Class</label>
                      <div class="col-md-10">
                        <select id="classes" class="form-control select2me" name="classes" data-placeholder="Select...">
                          <option value=""></option>
                          <?php foreach ($classes as $class):?>
                          <option value="<?=$class['class_id'];?>">Group <?=$class['group_number'];?> <?=$class['course_code'];?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Participating Students
                      </label>
                      <div class="col-md-10">
                        <select id="consultees" class="form-control students" name="consultees[]" data-placeholder="Select..." multiple="multiple">
                          <option value=""></option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2">Datepicker</label>
                      <div class="col-md-5">
                        <div class="input-group date date-picker" data-date-format="mm/dd/yyyy">
                          <input type="text" class="form-control" readonly name="consultation_date">
                          <span class="input-group-btn">
                          <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Consultation</label>
                      <div class="col-md-10">
                        <textarea class="wysihtml5 form-control" name="consultation_content" rows="15"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions">
                    <div class="row">
                      <div class="col-md-offset-2 col-md-10">
                        <input type="button" class="btn default" value="reset">
                        <button type="submit" class="btn green">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>               
              </div>
            </div>
          </div>
        </div>

        <!-- END PAGE CONTENT-->
      </div>
    </div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->

  <!-- BEGIN FOOTER -->
  <?php require_once 'application/views/includes/footer.phtml';?> 
  <!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
  <?php require_once 'application/views/includes/core_js.phtml';?>  
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="/../assets/admin/admin-page/select2/select2.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/jquery-validation/js/jquery.validate.js"></script>
<script src="/../assets/admin/pages/scripts/components-editors.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
  <?php require_once 'application/views/includes/theme_js.phtml';?> 
<!-- END THEME PLUGINS -->

<script>
jQuery(document).ready(function() { 
  ComponentsPickers.init();
  ComponentsEditors.init();
  /*AdminPage.init();*/
});
</script>
<script>
$(document).ready(function () {

    $("#consultees").select2();

     $('.classes').prop('placeholder','Select an instructor first...')
    $('.students').prop("disabled", true);

});
  
   
</script>
<script src="/../assets/admin/admin-page/js/custom.js"></script>
<script src="/../assets/admin/pages/scripts/components-pickers.js"></script>
<script src="/../assets/dcis/js/dcis-table-courses-users.js"></script>
<!-- <script src="/../assets/admin/pages/scripts/admin.js"></script>  -->
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
