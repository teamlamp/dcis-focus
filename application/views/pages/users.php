<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | User Management</title>
<!-- <input type="hidden" id="class_id" value="<?= $class['class_id']; ?>"> -->

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<link href="../../assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>

<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<link rel="stylesheet" type="text/css" href="/../assets/admin/pages/css/custom.css"/>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- END STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				User Management
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="icon-users"></i>
							<a href="<?= base_url('users')?>">Users</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="javascript:;>" data-toggle="modal" data-target="#importusers"><i class="fa fa-plus"></i> Import Users</a>
								</li>
								<li>
									<a href="<?= base_url('users/addStudent') ?>"><i class="fa fa-plus"></i> Add Student</a>
								</li>
								 <li>
                  					<a href="<?= base_url('users/addInstructor') ?>"><i class="fa fa-plus"></i> Add Instructor</a>
               				 	</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="profile-content">
				<div class="row">
					<div class="col-md-12">
						<div class="portlet light">
							<div class="portlet-title tabbable-line tabbalable-custom">
							 	<ul class="nav nav-tabs nav-justified">
							 		<li class="active">
										<a href="#students" data-toggle="tab">
										Students </a>
									</li>
									<li>
										<a href="#instructors" data-toggle="tab">
										Instructors </a>
									</li>
							 	</ul>
							</div>
							<div class="portlet-body">
							 <div class="tab-content">
							 	<div class="tab-pane active" id="students">
							 		<?php if(!$students){?>
									<div class="note note-danger">
              							<h4 class="block">No students in the database.</p>
          							</div>
									<?php } else { ?>
									<div class="portlet box grey-gallery" style="box-shadow: none !important;">
										
										<div class="portlet-body">
											<table class="table table-striped table-bordered table-hover" id="usersList">
											<thead>
												<tr>
													<th>Student ID</th>
							 						<th>Firstname</th>
							 						<th>Middlename</th>
							 						<th>Lastname</th>
							 						<th>Program</th>
							 						<th>Year Level</th>
							 						<th>Actions</th>
							 						<!-- <th></th> -->
												</tr>
											</thead>
											<tbody>
											<?php 
          										if($students){foreach($students as $students){ ?>
								 					<td class="student-id"><?= $students['school_id'];?></a></td>
								 					<td><?= $students['firstname']?></td>
								 					<td><?= $students['middlename']; ?></td>
								 					<td><?= $students['lastname']; ?></td>
								 					<td><?= $students['program_code']; ?></td>
								 					<td><?= $students['year_level']; ?></td>
								 					<td><a href="<?= base_url('profile').'/'.$students['school_id']?>" class="btn btn-sm green-jungle" id="view" ><i class="fa fa-eye" ></i> View</a> 
								 					<button class="btn btn-sm red-flamingo delete-student"><i class="fa fa-times"></i> Remove</button></td>
								 					<!-- <td><button>Remove</button></td> -->
								 				</tr>
											<?php } } ?>

											</tbody>
											</table>
										</div>
									</div>
							 		<?php } ?>
							 	</div>
							 	<div class="tab-pane" id="instructors">
							 		<?php if(!$instructors){?>
									<div class="note note-danger">
              							<h4 class="block">No instructors in the database.</p>
          							</div>
									<?php } else { ?>
									<div class="portlet box grey-gallery" style="box-shadow: none !important;">
										
										<div class="portlet-body">
											<table class="table table-striped table-bordered table-hover" id="instructorsList">
											<thead>
												<tr>
													<th>School ID</th>
							 						<th>Firstname</th>
							 						<th>Middlename</th>
							 						<th>Lastname</th>
							 						<th>E-mail</th>
							 						<th>Phone Number</th>
							 						<th>Degree</th>
							 						<th>Status</th>
							 						<th>Actions</th>
							 						<!-- <th></th> -->
												</tr>
											</thead>
											<tbody>
											<?php 
          										if($instructors){foreach($instructors as $instructor){ ?>
								 					<td><?= $instructor['school_id'];?></a></td>
								 					<td><?= $instructor['firstname']?></td>
								 					<td><?= $instructor['middlename']; ?></td>
								 					<td><?= $instructor['lastname']; ?></td>
								 					<td><?= $instructor['email']; ?></td>
								 					<td><?= $instructor['phone']; ?></td>
								 					<td><?= $instructor['degree']; ?></td>
								 					<td><?= $instructor['status']; ?></td>
								 					<td><a href="" class="btn btn-sm green-jungle" id="view" ><i class="fa fa-eye" ></i> View</a> 
								 					<button class="btn btn-sm red-flamingo"><i class="fa fa-times"></i> Remove</button></td>
								 					<!-- <td><button>Remove</button></td> -->
								 				</tr>
											<?php } } ?>

											</tbody>
											</table>
										</div>
									</div>
							 		<?php } ?>
							 	</div>
							 </div>
						</div>
					</div>
				</div>
				</div>
				<!-- BEGIN PAGE CONTENT-->
		<?php require_once 'application/views/modals/import_users.phtml';?>
		</div>

			<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>
	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
	<?php require_once 'application/views/includes/theme_js.phtml';?>	
	<script>
      jQuery(document).ready(function() {    

		DcisTableCoursesAndUsers.init();
		
      });
     $('.delete-student').on('click', function() {
     	var id = $(this).closest('tr').find('.student-id').text(); 
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this record!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: 'btn-danger',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false
        },
     function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: BASE_URL+'users/deactivateStudent',
                type: 'post',
                data: {id:id},
                success: function () {
                    swal("Deleted!", "This record has been successfully deleted!", "success");
                    window.location.reload();
                },
                error: function () {
                    alert('ajax failure');
                }
            });
            
            } else {
                swal("Cancelled", "No queries executed", "error");
            }
        });
    });
   </script>
	<script src="/../assets/dcis/js/dcis-table-courses-users.js"></script>

<!-- END THEME PLUGINS -->
	
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>