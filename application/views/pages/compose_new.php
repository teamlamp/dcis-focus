<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<title>DCIS | Compose</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="/../assets/admin/pages/css/inbox.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/jquery-multi-select/css/multi-select.css"/>

<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-summernote/summernote.css">
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">

<!-- BEGIN HEADER -->
<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix">
</div>
<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		<?php require_once 'application/views/includes/sidebar.phtml';?>
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

				<!-- BEGIN STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Compose <small>private messaging</small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-archive"></i>
							<a href="<?= base_url('messages') ?>">Messages</a>
							<i class="fa fa-angle-double-right"></i>
						</li>
						<li>
							<a href="#">Compose</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXTRAS PORTLET-->
						<div class="portlet light">
							<div class="portlet-title">
								<div class="caption font-green-meadow">
									<i class="fa fa-edit font-green-meadow"></i>Compose Message
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse">
									</a>
									<a href="javascript:;" class="reload">
									</a>
								</div>
							</div>
							<div class="portlet-body form">
								<form class="form-horizontal form-bordered" method="POST" action="<?= base_url('message/sendmessage') ?>">
									<div class="form-body">
										<div class="form-group">

											<div class="form-group">
												<label class="control-label col-md-2">To :</label>
												<div class="col-md-10">
													<!-- <select class="form-control input-large select2me" data-placeholder="Select...">
													</select> -->
													<select multiple id="tagRecipients" class="form-control select2 input-inline input-xlarge" name="messageRecipients[]"></select>

													<span class="help-block">Type in the recipient of this message.</span>
												</div>
											</div>

											<div class="form-group">
												<label class="col-md-2 control-label">Subject :</label>
												<div class="col-md-10">
													<input type="text" class="form-control input-inline input-xlarge" placeholder="Enter message subject/title" maxlength="20" name="messageSubject">
													<span class="help-block">
													Maximum of 20 charaters only!</span>
												</div>
											</div>

											<div class="form-group">
												<textarea class="wysihtml5 form-control" rows="6" placeholder="Start composing your message here ..." name="messageContent"></textarea>
											</div>

												<div class="inbox-compose-btn">
													<button class="btn green-meadow"><i class="fa fa-check"></i>Send</button>
													<button class="btn">Discard</button>
													<button class="btn">Draft</button>
												</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->

</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/select2/select2.min.js"></script>

<script type="text/javascript" src="/../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="/../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script src="/../assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="/../assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php require_once 'application/views/includes/theme_js.phtml';?>



<script src="/../assets/admin/pages/scripts/components-dropdowns.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#tagRecipients").select2({        
    ajax: {
      url: BASE_URL+'users/sendtouser',
      dataType: 'json',
      tags: true,
      tokenSeparators: [",", " "],
      delay: 200,
      data: function (params) {
        return {
          q: params.term // search term
        };
      },
      processResults: function (data) {
        return {
          results: data
        };
      },
      cache: true
    },
    minimumInputLength: 1
	});


});
</script>
<script src="/../assets/admin/pages/scripts/components-editors.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
	ComponentsDropdowns.init();
   	ComponentsEditors.init();
});   
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

<!-- <div class="inbox-form-group input-cc display-hide">
	<a href="javascript:;" class="close">
	</a>
	<label class="control-label">Cc:</label>
	<div class="controls controls-cc">
		<input type="text" name="cc" class="form-control">
	</div>
</div>
<div class="inbox-form-group input-bcc display-hide">
	<a href="javascript:;" class="close">
	</a>
	<label class="control-label">Bcc:</label>
	<div class="controls controls-bcc">
		<input type="text" name="bcc" class="form-control">
	</div>
</div> -->