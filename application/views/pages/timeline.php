<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS</title>

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="../../assets/global/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" type="text/css"/>
<link href="../../assets/admin/pages/css/timeline.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->
<?php require_once 'application/views/includes/theme_style.html';?>
<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">


<?php require_once 'application/views/includes/header.phtml';?>
<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">

		<!-- BEGIN SIDEBAR -->
		
		<?php require_once 'application/views/includes/sidebar.phtml';?>	
		<!-- END SIDEBAR -->

		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- END STYLE CUSTOMIZER -->
				<?php require_once 'application/views/includes/style_customizer.phtml';?>	
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				Timeline <small></small>
				</h3>
				<div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Timeline</a>
							<i class="fa fa-angle-double-right"></i>
						</li>
					</ul>
			<!--		<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-arrow-circle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</div>
					</div> -->
				</div>
				<!-- END PAGE HEADER-->

				<!-- BEGIN DATE PAGINATOR -->
					<div class="row">
						
					<div class="col-md-12">
					<div id="timeline_datepignator" class="datepaginator"></div>
					</div>
				
	
					</div>
				<!-- END PAGE PAGINATOR -->

				<!-- BEGIN PAGE CONTENT-->
				<div class="timeline">
					<!-- TIMELINE ITEM -->
					<?php require_once VIEWPATH.'pages/timeline/feeds.php';?>
					<!-- END OF TIMELINE ITEM -->

				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<?php require_once 'application/views/includes/footer.phtml';?>	
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS -->
	<?php require_once 'application/views/includes/core_js.phtml';?>	
<!-- END CORE PLUGINS -->

<!-- BEGIN THEME PLUGINS -->
	<?php require_once 'application/views/includes/theme_js.phtml';?>	
<!-- END THEME PLUGINS -->

<!-- BEGIN DATE PAGINATOR PLUGIN -->
<script src="../../assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="../../assets/global/plugins/bootstrap-datepaginator/bootstrap-datepaginator.min.js" type="text/javascript"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<!-- END DATE PAGINATOR PLUGIN -->
<script type="text/javascript">
var timelinedate  = <?php echo json_encode($this->input->get('date')); ?>;
</script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="../../assets/admin/pages/scripts/ui-datepaginator.js"></script>
<script src="../../assets/admin/pages/scripts/timeline.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {       
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
UIDatepaginator.init();
// Timeline.init(); // init timeline page
});
</script>
	
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>