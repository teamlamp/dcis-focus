<!DOCTYPE html>
<html lang="en">
<head>
<title>DCIS | Consultation</title>
<!-- <input type="hidden" id="class_id" value="<?= $class['class_id']; ?>"> -->

<!-- BEGIN META CONTENT -->
<?php require_once 'application/views/includes/meta.html';?>
<!-- END META CONTENT -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require_once 'application/views/includes/mandatory_style.html';?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/dcis/sweetalert/sweetalert.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
<link rel="stylesheet" type="text/css" href="/../assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN THEME STYLES -->

<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-md page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">

<!-- END HEADER -->

<div class="clearfix"></div> <!-- LEAVE THIS ALONE -->

<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">


		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">

				<!-- END STYLE CUSTOMIZER -->
				<!-- END STYLE CUSTOMIZER -->

				<!-- BEGIN PAGE HEADER-->
				<?php date_default_timezone_set("Asia/Manila");
$datestring = "%Y-%m-%d";
$cdate = mdate($datestring);
    $ndate = date('F d, Y',strtotime($cdate)); ?>
				<h3 class="page-title">
				Consultation Records as of <?php echo $ndate; ?>
				</h3>

				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<?php if(!$consultations){?>
				<div class="note note-danger">
              		<h4 class="block">No consultation records.</p>
          		</div>
				<?php } else { ?>
				<div class="row">
					<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box grey-gallery">

							<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover" id="usersList">
								<thead>
									<tr>
										<th align="center">Consultation #</th>
										<th align="center" colspan="3">Topic</th>
							 			<th align="center" colspan="2">Date</th>
							 			<th align="center" colspan="2">Class</th>
							 			
							 			<!-- <th></th> -->
									</tr>
								</thead>
								<tbody>
								<?php 
          							
          							foreach($consultations as $consultation){ ?>
          							<tr>
          								<td colspan="7"></td>
          							</tr>
          							<tr>
          							<?php $date = date('F d, Y',strtotime($consultation->consultation_date)); ?>


								 		<td align="center"><?php echo $consultation->consultation_id;?></a></td>
								 		<td colspan="3" align="center"><?php echo $consultation->consultation_topic; ?></td>
								 		<td align="left" colspan="2"><?php echo $date;?></td>
								 		<td align="right" colspan="2">G<?php echo $consultation->group_number." ".$consultation->course_code; ?></td>
								 		<!-- <td><button>Remove</button></td> -->
								 		
								 	</tr>
								 	<tr><td colspan="7"><?php echo $consultation->consultation_content; ?></td>
								 	</tr>
								 	
								 	<tr>
								 	<td align="right"><b>Consultees:</b></td>
								 	<td colspan="7"><?php
								 	foreach($consultees as $c){
								 		if($c->consultation_id == $consultation->consultation_id){
								 			echo $c->firstname." ".$c->lastname.", ";
								 		}
								 	}
								 	?></td>
								 	</tr>
								<?php } ?>
								</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				<!-- END PAGE CONTENT-->
				</div>
				<?php } ?>
		</div>

			<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
</div>
	<!-- MODAL -->
	<!-- END FOOTER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->

	
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>
