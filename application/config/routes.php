<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
|$route['journals'] = 'blogs';
|A URL containing the word “journals” in the first segment will be remapped to the “blogs” class.
|
|$route['blog/joe'] = 'blogs/users/34';
|A URL containing the segments blog/joe will be remapped to the “blogs” class and the “users” method. The ID will be set to “34”.
|
|$route['product/(:any)'] = 'catalog/product_lookup';
|A URL with “product” as the first segment, and anything in the second will be remapped to the “catalog” class and the “product_lookup” method.
|
|$route['product/(:num)'] = 'catalog/product_lookup_by_id/$1';
|A URL with “product” as the first segment, and a number in the second will be remapped to the “catalog” class and the “product_lookup_by_id” method passing in the match as a variable to the method.
|
|Make routes case $route['(?i)sample'] 
*/
$route['default_controller'] = 'home'; // Base URL goes here
$route['/'] = 'home';  

$route['(?i)login'] = 'login';  // Base URL/login
$route['404_override'] = 'errorcon/error404';
$route['translate_uri_dashes'] = FALSE;
$route['classes'] = 'classes';
$route['class/(:any)/(:num)'] = "classes/getclass/$1/$2";
$route['class/(:any)/(:num)/students'] = "classes/getStudentsList/$1/$2";
$route['class/(:any)/(:num)/settings'] = "classes/class_settings/$1/$2";
$route['class/(:any)/(:num)/assessment'] = "classes/getassessment/$1/$2";
$route['class/(:any)/(:num)/grades/(:num)'] = "grades/getStudentGrades/$1/$2/$3";
$route['class/(:any)/(:num)/grades/record'] = "grades/recordgrades/$1/$2";
$route['class/(:any)/(:num)/grades/rubrics'] = "rubrics/graderubrics/$1/$2";
$route['class/(:any)/(:num)/grades/history'] = "grades/gradehistory/$1/$2";
$route['class/(:any)/(:num)/grades/history/(:num)'] = "grades/onehistory/$1/$2/$3";
$route['class/(:any)/(:num)/classrecord'] = "classes/classrecord/$1/$2";
$route['class/(:any)/(:num)/classrecord'] = "classes/classrecord/$1/$2";
$route['class/(:any)/(:num)/attendance'] = "attendance/index/$1/$2";
$route['class/(:any)/(:num)/calendar'] = "classes/classCalendar/$1/$2";
$route['class/(:any)/(:num)/StudentAttendance'] = "classes/viewStudentAttendanceRecords/$1/$2";
$route['class/(:any)/(:num)/StudentAttendanceRecord/(:num)'] = "classes/viewStudentAttendance/$1/$2/$3";
$route['class/checkAttendance'] = "classes/checkAttendance";
$route['class/getHandledClasses'] = "classes/getHandledClasses";
$route['class/getHandledStudents'] = "classes/getHandledStudents";
$route['class/updateSeatPlan'] = "classes/updateSeatPlan";
$route['profile/(:any)'] = "profile/getprofile/$1";
$route['test'] = "classes/test";
$route['message/(:num)'] = "message/viewmessage/$1";

// error routing