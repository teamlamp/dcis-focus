<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timeline extends Auth_Controller {

  function __construct() {
    parent::__construct();
      $this->data['user_role'] = $this->session->userdata('user_role');
      $this->data['school_id'] = $this->session->userdata('school_id');
      
      $this->load->model('Timeline_model');
      // $this->load->model('Comment_model');


  }

	public function index()
	{	
		if (($this->input->get('date'))) {
			$this->data['date'] = $this->input->get('date');
		}else{
			$this->data['date'] = date('Y-m-d');
		}
      	$this->data['feeds'] = $this->Timeline_model->getPostFeed($this->data['school_id'],$this->data['date']);
        // $this->data['comments'] = $this->Comment_model->getPostComment($this->data['school_id'],$this->data['date']);

		$this->load->view('pages/timeline',$this->data);
	}

  public function countNotifications()
  {
    $this->load->model('Alerts_model');
    $data['count'] = $this->Alerts_model->countNotifications();
    $this->load->view('pages/timeline/count_alerts', $data);
  }

	public function logout(){
		$this->session->unset_userdata('school_id');
    $this->session->unset_userdata('user_role');
    $this->session->unset_userdata('user_id');
    $this->session->unset_userdata('logged_in');
		$this->session->sess_destroy();
		$base_url = base_url(); redirect($base_url);
	}
}
