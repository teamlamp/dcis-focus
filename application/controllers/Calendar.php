<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	class Calendar extends Auth_Controller {
	
	function __construct() {
    	parent::__construct();
      	$this->load->model('Calendar_model');
      	$this->load->model('Semesters_model');
      	$this->load->model('Academic_years_model');
  	}

	public function index()
	{
		$this->data['academic_years'] = $this->Calendar_model->getAllYears();
		$this->data['semesters'] = $this->Calendar_model->getSemester();
		$this->load->view('pages/calendar', $this->data);
	}

	public function addYear () {
		$this->load->view('pages/addYear');
	}

	public function addAcademicYear()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required|callback__regex_check|callback__date_compare');
		$this->form_validation->set_rules('end_date', 'End Date', 'required|callback__regex_check');

			if ($this->form_validation->run())
			{

				$p = new Academic_years_model();
				date_default_timezone_set("Asia/Manila");
				//Extract years from date input
				$year_input1 = $this->input->post('start_date');
				$year_input2 = $this->input->post('end_date');
				
				$getyear1 = strtotime($year_input1);
				$getyear2 = strtotime($year_input2);
				$year1 = date("Y", $getyear1);
				$year2 = date("Y", $getyear2);

				$start_date= date('Y-m-d',strtotime($this->input->post('start_date')));
				$end_date= date('Y-m-d',strtotime($this->input->post('end_date')));
			
				$ay_name = $year1."-".$year2;
				if($check = $p->checkIfAcademicYearExists($ay_name) == false)
				{
				//send values to model
					$p->academic_year = $year1."-".$year2;
					$p->start_date = $start_date;
					$p->end_date = $end_date;

					$result=$p->addAcademicYear();
					
					if (!$result)
					{
						echo mysqli_error($result);
					}
					
					else
					{
						redirect('calendar', 'refresh');
								
					}
				}

				else
				{
					echo "<script type='text/javascript'>alert('Academic Year Already Exists');</script>";
			        echo "<script type='text/javascript'>history.go(-1);</script>";
				}

			}

			else
			{
				$this->load->view('pages/addYear');
			}
	}

	function _regex_check($str)
	{
		
		if (preg_match("/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/",$str))
    		{
        		return true;
   			 }
	    else
	    {
	    	$this->form_validation->set_message('_regex_check', 'The date format you entered is invalid !');
	        return false;
	    }
	}

	function _date_compare(){

		$start = strtotime($_POST['start_date']);
		$end = strtotime($_POST['end_date']);

		if ($start < $end){

			if($end >= $start + 26300000)
			{
				return true;
			}

			else
			{
				$this->form_validation->set_message('_date_compare', 'The end date field should be at least 10 months more than the start date field.');
				return false;
			}	
		}

		else {
			$this->form_validation->set_message('_date_compare', 'The start date should be before the end date.');
			return false;
		}

	}

	public function addSemester() {
		$this->data['academic_years'] = $this->Calendar_model->getAllYears();
		$this->load->view('pages/addSemester', $this->data);
	}

	public function addNewSemester()
	{

		$this->load->library('form_validation');
		$this->form_validation->set_rules('semester', 'Semester', 'required|callback__checkIfSemesterExists');
		$this->form_validation->set_rules('academic_year_id', 'Academic Year', 'required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'required|callback__regex_check|callback__checkSemesterStartEndMonthInterval');
		$this->form_validation->set_rules('end_date', 'End Date', 'required|callback__regex_check');

		if ($this->form_validation->run())
		{

			$this->load->model('Semesters_model');
			$p = new Semesters_model();
			$p->academic_year_id = $this->input->post('academic_year_id');
			$p->semester = $this->input->post('semester');
			$p->start_date = date('Y-m-d', strtotime($this->input->post('start_date')));
			$p->end_date = date('Y-m-d', strtotime($this->input->post('end_date')));
			$result=$p->addSemester();
			
			if (!$result)
			{
				echo mysqli_error($result);
			}
			
			else
			{
				echo "<script type='text/javascript'>alert('Successfully added new semester!')</script>";
				redirect('calendar', 'refresh');			
			}
		}

		else
		{
			$this->load->view('pages/addSemester');
		}
	}

	function _checkSemesterStartEndMonthInterval(){
		$start = strtotime($_POST['start_date']);
		$end = strtotime($_POST['end_date']);

		if ($start < $end)
		{
			if($this->input->post('semester') == "Summer")
			{
				if($end >= $start + 2592000) // + 1 month
				{
					return true;
				}

				else
				{
					$this->form_validation->set_message('_checkSemesterStartEndMonthInterval', 'The end date field should be at least 1 month more than the start date field.');
					return false;
				}	
			}

			else
			{
				if($end >= $start + 10368000) // + 4 months
				{
					return true;
				}

				else
				{
					$this->form_validation->set_message('_checkSemesterStartEndMonthInterval', 'The end date field should be at least 4 months more than the start date field.');
					return false;
				}						
			}
		}

		else
		{
			$this->form_validation->set_message('_checkSemesterStartEndMonthInterval', 'The start date should be before the end date.');
			return false;
		}
	}

	function _checkIfSemesterExists()
	{
		if($check = $this->Semesters_model->checkIfSemesterAlreadyExists($this->input->post('semester'), $this->input->post('academic_year_id')) == false)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('_checkIfSemesterExists', 'The semester you are trying to add already exists.');
			return false;
		}
	}

	public function getHoliday () {
		$this->data['holiday'] = $this->Calendar_model->getHoliday();
		echo json_encode($this->data['holiday']);

	}
	public function addHoliday() {
		if($this->input->post('type') == 'new'){
			$data = array(
				'title' => $this->input->post('title'),
				'start' => $this->input->post('startdate'),
				'end' => $this->input->post('startdate'),
				'allDay' => true
				);
		$this->Calendar_model->addHoliday($data);
		}
	}

	public function updateHoliday() {
		if($this->input->post('type') == 'changetitle') {
			$id = $this->input->post('eventid');
			$data = array (
				'title' => $this->input->post('title')
				);
			$update = $this->Calendar_model->updateHoliday($id,$data);
			if($update)
     			echo json_encode(array('status'=>'success'));
   			else
     			echo json_encode(array('status'=>'failed'));
		} 
		if ($this->input->post('type') == 'resize') {

			$id = $this->input->post('eventid');
			$data = array (
				'end' => $this->input->post('end')
				);
			$update = $this->Calendar_model->updateHoliday($id,$data);
			if($update)
     			echo json_encode(array('status'=>'success'));
   			else
     			echo json_encode(array('status'=>'failed'));
		}
		if ($this->input->post('type') == 'resetdate') {

			$id = $this->input->post('eventid');
			$data = array (
				'start' => $this->input->post('start'),
				'end' => $this->input->post('end')
				);
			$update = $this->Calendar_model->updateHoliday($id,$data);
			if($update)
     			echo json_encode(array('status'=>'success'));
   			else
     			echo json_encode(array('status'=>'failed'));
		}

	}

	public function deleteHoliday() {
		if($this->input->post('type') == 'delete') {
			$id = $this->input->post('id');
			$delete = $this->Calendar_model->deleteHoliday($id);
			if($delete)
     			echo json_encode(array('status'=>'success'));
   			else
     			echo json_encode(array('status'=>'failed'));
		}
	}

	public function getClassCalendar ($class_id) {
		$this->data['events'] = $this->Calendar_model->getClass($class_id);
		foreach ($this->data['events'] as $event) {
			$fetched[] = array(
							'id' => $event['calendar_id'],
							'title' => $event['title'],
							'start' => $event['start'],
							'end' => $event['end'],
							'allDay' => $event['allDay']
				); 
		}
		echo json_encode($fetched);

	}

	public function addEvent() {
		if($this->input->post('type') == 'new'){
			$data = array(
				'class_id' => $this->input->post('class_id'),
				'title' => $this->input->post('title'),
				'start' => $this->input->post('startdate'),
				'end' => $this->input->post('startdate'),
				'allDay' => true
				);
		$this->Calendar_model->addEvent($data);
		}
	}
	public function updateEvent() {
		$class_id = $this->input->post('class_id');
		if($this->input->post('type') == 'changetitle') {
			$id = $this->input->post('eventid');
			$data = array (
				'title' => $this->input->post('title')
				);
			$update = $this->Calendar_model->updateEvent($id,$class_id,$data);
			if($update)
     			echo json_encode(array('status'=>'success'));
   			else
     			echo json_encode(array('status'=>'failed'));
		} 
		if ($this->input->post('type') == 'resize') {

			$id = $this->input->post('eventid');
			$data = array (
				'end' => $this->input->post('end')
				);
			$update = $this->Calendar_model->updateEvent($id,$class_id,$data);
			if($update)
     			echo json_encode(array('status'=>'success'));
   			else
     			echo json_encode(array('status'=>'failed'));
		}
		if ($this->input->post('type') == 'resetdate') {

			$id = $this->input->post('eventid');
			$data = array (
				'start' => $this->input->post('start'),
				'end' => $this->input->post('end')
				);
			$update = $this->Calendar_model->updateEvent($id,$class_id,$data);
			if($update)
     			echo json_encode(array('status'=>'success'));
   			else
     			echo json_encode(array('status'=>'failed'));
		}
	}

	public function deleteEvent() {
		if($this->input->post('type') == 'delete') {
			$id = $this->input->post('eventid');
			$delete = $this->Calendar_model->deleteEvent($id);
			if($delete)
     			echo json_encode(array('status'=>'success'));
   			else
     			echo json_encode(array('status'=>'failed'));
		}
	}
	public function viewUpdateAcademicYearForm(){}
	public function updateAcademicYear(){}
	public function viewUpdateSemesterForm(){}
	public function updateSemester(){}


}
