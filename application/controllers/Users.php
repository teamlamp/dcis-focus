<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Auth_Controller {

	public function __construct(){
		parent::__construct();
    $this->data['school_id'] = $this->session->userdata('school_id');

    $this->load->model('Usermanagement_model');
    $this->load->model('User_model');
    $this->load->model('Programs_model');
	}
 	public function index() {

    $this->data['students'] = $this->Usermanagement_model->getAllStudents();
    $this->data['instructors'] = $this->Usermanagement_model->getAllInstructors();
    $this->load->view('pages/users', $this->data);
    }

  public function addStudent() {
    $this->data['programs'] = $this->Programs_model->getAllPrograms();
    $this->load->view('pages/user/add_student',$this->data);   
  }

  public function addInstructor() {
    $this->load->view('pages/user/add_instructor');   
  }

  public function addNewStudent(){
    $this->load->library('form_validation');
    $this->form_validation->set_rules('school_id', 'School ID', 'required|callback_checkIfUserAlreadyExists');
    $this->form_validation->set_rules('program_id', 'Program', 'required');
    $this->form_validation->set_rules('consultation_date', 'Date of Birth', 'required');
    
    $ndate = date('Ymd',strtotime($this->input->post('consultation_date')));
    
//echo json_encode($ndate);
    if ($this->form_validation->run())
    {
      $data = array(
        'school_id' => $this->input->post('school_id'),
        'password' => sha1($ndate),
        'user_role' => 'student');

        $data2 = array(
        'school_id' => $this->input->post('school_id'),
        'program_id' => $this->input->post('program_id'));
      
      if($this->Usermanagement_model->addNewStudent($data, $data2))
      {
        echo "<script type='text/javascript'>alert('Student successfully added');</script>";
        redirect('Users', 'refresh');
      }

      else
      {
        echo "<script type='text/javascript'>alert('Error!');</script>";
        redirect('Users/addStudent', 'refresh');

      }

    }

    else
    {
      $this->data['programs'] = $this->Programs_model->getAllPrograms();
      $this->load->view('pages/user/add_student', $this->data);
    }

  }

    public function addNewInstructor(){
    $this->load->library('form_validation');
    $this->form_validation->set_rules('school_id', 'School ID', 'required|callback_checkIfUserAlreadyExists');
    $this->form_validation->set_rules('consultation_date', 'Date of Birth', 'required');
    
    $ndate = date('Ymd',strtotime($this->input->post('consultation_date')));
    
//echo json_encode($ndate);
    if ($this->form_validation->run())
    {
      $data = array(
        'school_id' => $this->input->post('school_id'),
        'password' => sha1($ndate),
        'user_role' => 'instructor');

        $data2 = array(
        'school_id' => $this->input->post('school_id'));
        
      if($this->Usermanagement_model->addNewInstructor($data, $data2))
      {
        echo "<script type='text/javascript'>alert('Instructor successfully added');</script>";
        redirect('Users', 'refresh');
      }

      else
      {
        echo "<script type='text/javascript'>alert('Error!');</script>";
        redirect('Users/addInstructor', 'refresh');

      }    


    }

    else
    {
      
      $this->load->view('pages/user/add_instructor');
    }

  }

  public function checkIfUserAlreadyExists()
  {
    if($return = $this->Usermanagement_model->checkIfUserAlreadyExists($this->input->post('school_id')) == false)
    {
      return true;
    }

    else
    {
      $this->form_validation->set_message('checkIfUserAlreadyExists', 'The user you are trying to add already exists.');
      return false;
    }
  }

  public function importUsers(){

    //$this->load->model('Classes_model'); naka tawag na daan ang mga needed models sa constructor
    if ($data['result']=$this->Usermanagement_model->importUsers())
    {
      //redirect('classes', 'refresh');
      echo "<script type='text/javascript'>history.go(-1);</script>";
    }

    else{

      echo mysqli_error($data['result']);
    }
  }

  public function findUser(){
    $findthis = strip_tags(trim($_GET['q'])); 
    $this->data['recipients'] = $this->User_model->findUser($findthis,$this->data['school_id']);

    if($this->data['recipients']){
      foreach ($this->data['recipients'] as $key => $value) {
        $name = $value['firstname']." ".$value['lastname'];
        $data[] = array('id' => $value['school_id'], 'text' => $name);        
      } 
    } else {
       $data[] = array('id' => '0', 'text' => 'No user found.');
    }

    echo json_encode($data);
  }
  public function deactivateStudent() {
    $data['active'] = 0;
    $id = $this->input->post('id');
    $this->Usermanagement_model->deactivateStudent($id,$data);

  }
}

