<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Auth_Controller {

	public function __construct()
	{
		parent::__construct();
    	$this->load->model('Usermanagement_model');
    	$this->load->model('upload_model');
	}

	public function index()
	{
		$this->load->view('pages/settings_new');
	}

	public function editStudentProfile()
	{
		$data = array(
			'firstname' => $this->input->post('firstname'),
			'middlename' => $this->input->post('middlename'),
			'lastname' => $this->input->post('lastname'),
			'program_id' => $this->input->post('program_id'),
			'dob' => date('Y-m-d', strtotime($this->input->post('dob'))),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone')
			);
		
		$result = $this->Usermanagement_model->editStudentProfile($data);
		
		if (!$result)
   	 	{
      		echo mysqli_error($result);
    	}
    
	    else
	    {
	      echo "<script type='text/javascript'>alert('Successfully updated your profile!')</script>";
	      redirect('', 'refresh');
	    }
	}

	public function editInstructorProfile()
	{
			$data = array(
			'firstname' => $this->input->post('firstname'),
			'middlename' => $this->input->post('middlename'),
			'lastname' => $this->input->post('lastname'),
			'dob' => date('Y-m-d', strtotime($this->input->post('dob'))),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone')
			);
		$this->Usermanagement_model->editInstructorProfile($data);
	}

	public function editAdminProfile()
	{
		$this->Usermanagement_model->editAdminProfile();
	}

	public function editStaffProfile()
	{
		$this->Usermanagement_model->editStaffProfile();
	}


	public function logout(){
		$this->session->unset_userdata('school_id');
		$this->session->sess_destroy();
		$base_url = base_url(); redirect($base_url);
	}


function uploadAvatar()
	{
		if($this->input->post('upload'))
		{
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']    = '1024';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
			$this->load->library('upload', $config);
			
			if ( ! $this->settings->uploadAvatar())
			{
				$error = array('error' => $this->upload->display_errors());
				$this->load->view('', $error);
			}
			else
			{
				$data=$this->upload->data();
				$this->thumb($data);
				$file=array(
				'img_name'=>$data['raw_name'],
				'thumb_name'=>$data['raw_name'].'_thumb',
				'ext'=>$data['file_ext'],
				'upload_date'=>time()
				);
				$this->upload_model->add_image($file);
				$data = array('upload_data' => $this->upload->data());
				$this->load->view('upload_success', $data);
			}
		}

		else
		{
			redirect(site_url('upload'));
		}
	}

	function thumb($data)
	{
		$config['image_library'] = 'gd2';
		$config['source_image'] =$data['full_path'];
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 275;
		$config['height'] = 250;
		$this->load->library('image_lib', $config);
		$this->image_lib->resize();
	}


function changePassword()
{
	$this->load->library('form_validation');
	$this->form_validation->set_rules('old_password', 'Current Password', 'required|callback_confirmOldPassword');
	$this->form_validation->set_rules('new_password', 'Password', 'required');
	$this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|matches[new_password]');

	if ($this->form_validation->run())
    {
        $this->Usermanagement_model->changePassword();
        $res = array (
        		'data' => [],
        		'status' => 200,
        		'message' => "Successfully changed password."
        );
        echo json_encode($res);

    }
       
    else
    {
        $mess = validation_errors();
        $res = array (
        		'data' => [],
        		'status' => 500,
        		'message' => $mess
        );
        echo json_encode($res);        	
    }

}


function confirmOldPassword()
{
	if ($check = $this->Usermanagement_model->confirmPassword($this->input->post('old_password')) == true)
	{
		return true;
	}

	else
	{
		$this->form_validation->set_message('confirmOldPassword', 'The old password you provided is incorrect.');
		return false;
	}
}

}
