<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends Auth_Controller {

  function __construct() {
      parent::__construct();
      $this->data['user_role'] = $this->session->userdata('user_role');
      $this->data['user_id'] = $this->session->userdata('user_id');
      $this->data['school_id'] = $this->session->userdata('school_id');
        
      $this->load->model('Message_model');
      $this->load->model('User_model');
    }

  public function index() {
    $this->data['inbox_messages'] = $this->Message_model->getInboxMessages($this->data['school_id']);
    $this->data['sentbox_messages'] = $this->Message_model->getSentboxMessages($this->data['school_id']);
    $this->data['drafts_messages'] = $this->Message_model->getDraftMessages($this->data['school_id']);
    $this->data['trash_messages'] = $this->Message_model->getTrashMessages($this->data['school_id']);
    $this->load->view('pages/messages',$this->data);
  }

  public function viewMessage(){
    $id = $this->input->post('msgmap_id');
    $this->data['one_message'] = $this->Message_model->getOneMessage($this->data['school_id'],$id);
    //print_r($this->data['one_message']); exit();
    if($this->data['one_message']){
      $this->data['sender'] = $this->User_model->getUserInfoSenderEx($this->data['one_message']['msg_author_id'],$id);
      $this->data['recipient'] = $this->User_model->getUserInfoRecipientEx($this->data['one_message']['msgmap_recipient_id'],$id);
      //print_r($this->data['recipient']);
      $this->load->view('pages/messages/message_view',$this->data);
      $this->Message_model->messageIsRead($this->data['school_id'],$id);
    } else {
      //redirect ('404');
    }
  }  

  public function sendMessage(){
    $recipients = $this->input->post('messageRecipients');
    $subject = $this->input->post('messageSubject');
    $content = nl2br($this->input->post('messageContent'));
    foreach($recipients as $r){
      $msgmap_id = $this->Message_model->setRecipient($r);
      $this->Message_model->setMessageActual($msgmap_id,$this->data['school_id'],$subject,$content);
    } 
    redirect('message');
  } 

  public function sendReply(){
    $recipient = $this->input->post('replyRecip');
    $subject = $this->input->post('replySubject');
    $content = nl2br($this->input->post('replyContent'));
    $msgmap_id = $this->Message_model->setRecipient($recipient);
    $this->Message_model->setMessageActual($msgmap_id,$this->data['school_id'],$subject,$content);
    redirect('message');
  } 

  public function draftMessage(){
    $recipients = $this->input->post('recipients');
    $subject = $this->input->post('subject');
    $content = nl2br($this->input->post('content'));
    $ctr=0;
    foreach($recipients as $r){
      $x = $ctr++;
      $msgmap_id = $this->Message_model->setRecipientDraft($r);
      $msg_group = $msgmap_id - $x;
      echo $msg_group."  ---  ".$x;
      $this->Message_model->setMessageActualDraft($msgmap_id,$this->data['school_id'],$subject,$content,$msg_group);
    }
  }

  public function reSendDraft(){
    $msg_id = $this->input->post('msgmap_id');
    $this->data['msg_group'] = $this->Message_model->getMessageGroup($this->data['school_id'],$msg_id); 
    $this->data['draft_recipients'] = $this->Message_model->getDraftRecipients($this->data['msg_group']['msg_group']);
    $this->data['subject'] = $this->Message_model->getSubjectByGroup($this->data['msg_group']['msg_group']);
    $this->data['content'] = $this->Message_model->getContentByGroup($this->data['msg_group']['msg_group']);
    $this->load->view('pages/messages/send_draft',$this->data);
  }

  public function sendDraft(){
    $msg_group = $this->input->post('msg_group');
    $recipients = $this->input->post('draftRecipients'); // from the form
    $subject = $this->input->post('draftSubject');
    $content = nl2br($this->input->post('draftContent'));
    $draft_recipients = $this->Message_model->getDraftRecipients($msg_group); // the original recipients
    $orig_recipients = array();
    foreach($draft_recipients as $or){
      $orig_recipients[] = $or['school_id'];
    }
    $new_recipients = array();
    $diffnew = array_diff($recipients,$orig_recipients);
    foreach($diffnew as $dfnw){
      $new_recipients[] = $dfnw;
    }
    foreach($new_recipients as $nr){
      $msgmap_id = $this->Message_model->setRecipient($nr);
      $this->Message_model->setMessageActual($msgmap_id,$this->data['school_id'],$subject,$content);
    } 
    $new_set_recipients = array();
    $nsra = array_intersect($orig_recipients,$recipients);
    foreach($nsra as $nsrr){
      $new_set_recipients[] = $nsrr;
    }
    foreach($new_set_recipients as $r){
      $this->Message_model->setInboxtoDraft_r($r,$msg_group);
      $this->Message_model->setSenttoDraft_s($this->data['school_id'],$subject,$content,$r);
    }     
    redirect('message');
  }

  public function deleteMessage(){
    $todelete = $this->input->post('to_trash');
    foreach($todelete as $d){
      //$delete_array[] = array('pm_id' => $d,
      //                        'isTrash' => 1);
      $this->data['one_message'] = $this->Message_model->getOneMessage($this->data['school_id'],$d);
      if($this->data['one_message']['msg_author_id'] == $this->data['school_id']){
        $col_name = "msg_type_s"; $tbl_name = "message_actual"; $m_type = "msg_id";
        $this->Message_model->deleteMessage($d,$col_name,$tbl_name,$m_type); // trash message_actual copy
      } else if($this->data['one_message']['msgmap_recipient_id'] == $this->data['school_id']){
        $col_name = "msgmap_type_r"; $tbl_name = "message_map"; $m_type = "msgmap_id";
        $this->Message_model->deleteMessage($d,$col_name,$tbl_name,$m_type); // trash message_map copy
      }
    }    
  }

  public function deleteForeverSingle(){
    $msg_id = $this->input->post('msg_id');
    $this->data['one_message'] = $this->Message_model->getOneMessage($this->data['school_id'],$msg_id);
    if($this->data['one_message']['msg_author_id'] == $this->data['school_id']){
      $col_name = "msg_active"; $tbl_name = "message_actual"; $m_type = "msg_id";
      $this->Message_model->deleteMessageForever($msg_id,$col_name,$tbl_name,$m_type); // trash message_actual copy
    } else if($this->data['one_message']['msgmap_recipient_id'] == $this->data['school_id']){
      $col_name = "msgmap_active"; $tbl_name = "message_map"; $m_type = "msgmap_id";
      $this->Message_model->deleteMessageForever($msg_id,$col_name,$tbl_name,$m_type); // trash message_map copy
    }
  }

  public function countUnread(){
    $unreadcount = $this->Message_model->countUnread($this->data['school_id']);
    if($unreadcount['unreadcount'] > 0){
      echo '<i class="icon-envelope-open"></i>
            <span class="badge badge-default msgcounthdr">'.$unreadcount['unreadcount'].'</span>';
    }else{
      echo '<i class="icon-envelope-open"></i>';
    }
    //echo $unreadcount['unreadcount'];
  }

}

