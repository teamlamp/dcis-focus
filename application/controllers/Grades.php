<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grades extends Auth_Controller {
  
  function __construct() {
    parent::__construct();
      $this->data['user_role'] = $this->session->userdata('user_role');
      $this->data['user_id'] = $this->session->userdata('user_id');
      $this->data['school_id'] = $this->session->userdata('school_id');;
      // 
      $this->load->model('Classes_model');
      $this->load->model('Students_model');
      $this->load->model('Grades_model');
      $this->load->model('Rubrics_model');
  }

// Grades Page
//

  public function recordGrades($course_code, $group_number){
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $x = $this->data['one_class'][0]['class_id'];
    $this->data['students'] = $this->Students_model->getStudentsList($x);
    if($this->data['one_class'] && $this->data['user_role'] != "student"){
      $this->data['rubrics'] = $this->Rubrics_model->getRubrics($x);
      $this->load->view('pages/classes/recordgrades',$this->data);
    }else{
      redirect('404');
    }
  }

  public function saveGrades(){
    $data = $this->input->post('grades');
    $this->Grades_model->savegrades($data);    
    $this->updateRubricsNGradesTotal();
  }

  public function updateSingleGrade(){
    $data = array(
      'grade_id' => $this->input->post('grade_id'),
      'score' => $this->input->post('score')
      );
    $this->Grades_model->updateSingleGrade($data);    
    $this->updateRubricsNGradesTotal();
  }

  public function updateAllGrades(){
    $data = $this->input->post('new_grades');
    $this->Grades_model->updateAllGrades($data);
    $this->updateRubricsNGradesTotal();
  }

  public function updateRubricsNGradesTotal(){
    $rubtot = $this->Rubrics_model->addRubricsTotal();
    $class_id = $rubtot[0]['class_id']; 
    $this->Rubrics_model->storeRubTot($rubtot[0]['class_id'], $rubtot[0]['rubtot']);
    $gradestot = $this->Grades_model->addStudentGradesTot($class_id);
    $this->Grades_model->updateStudentGradesTot($gradestot);
  }

  public function getStudentGrades($course_code,$group_number,$school_id){
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $class_id = $this->data['one_class'][0]['class_id'];
    $this->data['student_grades'] = $this->Grades_model->getStudentGrades($school_id, $class_id);
    if($this->data['one_class'] && $this->data['student_grades'] && $this->data['user_role'] != "student"){
      $this->load->view('pages/classes/studentgrades',$this->data);
    }else
      redirect('404');
  }

  public function recordHistory(){
    $this->data['one_class'] = $this->Classes_model->getOneClass($this->input->post('course'), $this->input->post('group'));
    $x = $this->data['one_class'][0]['class_id'];
    $data = array(
      'formula_id' => $this->input->post('formula_id'),
      'activity_title' => $this->input->post('activity_title'),
      'perfect_score' => $this->input->post('perfect_score'),
      'class_id' => $x
    );
    $this->Grades_model->recordHistory($data);
  }

  public function gradeHistory($course_code, $group_number){
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $x = $this->data['one_class'][0]['class_id'];
    $this->data['history'] = $this->Grades_model->getHistory($x);
    if($this->data['one_class'] && $this->data['user_role'] != "student"){
      $this->load->view('pages/classes/grade_history',$this->data);
    }else{
      redirect('404');
    }
  }

  public function lastHistory(){
    $x = $this->Grades_model->getLastHistory();
    echo $x->grade_history_id;
  }

  public function oneHistory($course_code,$group_number,$history_id){
    $this->data['one_history'] = $this->Grades_model->oneHistory($course_code,$group_number,$history_id);
    if($this->data['one_history'] && $this->data['user_role'] != "student")
      $this->load->view('pages/classes/grade_onehistory',$this->data);
    else
      redirect('404');
  }

}
