<?php
class Upload extends CI_Controller {

	function do_upload()
	{
	$id = $this->session->userdata('school_id');
			$this->load->model('Upload_model');
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']    = '1024';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());
				echo "<script type='text/javascript'>alert('Failed in uploading image.');</script>";
				redirect('timeline', 'refresh');
			}
			else
			{
				$data=$this->upload->data();
				$this->thumb($data);
				$file=array(
				'user_id' => $id,
				'img_name'=>$data['raw_name'],
				'thumb_name'=>$data['raw_name'].'_thumb',
				'ext'=>$data['file_ext'],
				'upload_date'=>time()
				);
				$this->Upload_model->add_image($file);
				$data = array('upload_data' => $this->upload->data());
				redirect('timeline', 'refresh');
			}
	}

	function thumb($data)
	{
		$config['image_library'] = 'gd2';
		$config['source_image'] =$data['full_path'];
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 275;
		$config['height'] = 250;
		$this->load->library('image_lib', $config);
		$this->image_lib->resize();
	}
}