<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
   	
   	// Setting constructor to check if user is already logged in
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in'))
        { 
            redirect('timeline'); // If user is already logged in he/she's redirected to his/her dashboard
        }
    }

   	// Setting login view and validation for loggin user 
	public function index(){
	$error = NULL;

		if (isset($_POST['submit'])): // If form is submitted then take the credentials
		
		$school_id = $_POST['school_id']; 
		$password = $_POST['password']; 

		$this->load->model('User_model'); // Load user model to instantiate model class 
		
		$valid_user = $this->User_model->checkUserExist($school_id);	// Check if school_id is valid and exist in the database
		if (!$valid_user) {
		$error['message'][] = (object) array('header' => 'Incorrect School ID', 'message' => 'The school id you entered does not belong to any account. Please try again.' ); // Error output if given school_id is invalid
		}

		$success = $this->User_model->login($school_id,$password); // User can now log in if the school_id given is valid
		if ($success) {
			foreach ($success as $value) {
			$user_id = $value->user_id;
      $user_role = $value->user_role;
			}
		$this->setUserSession($user_id,$school_id,$user_role); // If user's school_id and password matches database a session is created
			redirect('/timeline'); // User is then redirected to his/her dashboard/home
		}elseif($valid_user && !$success){ // If login fails but school_id is valid
		$error['message'][] = (object) array('header' => 'Please re-enter your password', 'message' => 'The password you entered is incorrect. Please try again.' ); // Error output if password does not match school_id
		}
		
		endif;
		$this->load->view('login',$error); // Load view file and prompts error message if there is
	}

	// Set session for user
	private function setUserSession($user_id,$school_id,$user_role){
    $this->load->model('academic_years_model');
    $this->load->model('semesters_model');
    $academic_year_id = $this->academic_years_model->getCurrentAcademicYear();
    $semester_id = $this->semesters_model->getCurrentSemester();

    $this->setUserName($school_id,$user_role);
		$newdata = array(	// Storing new session instance every success login
			    'user_id' => $user_id,
          'user_role' => $user_role,
        	'school_id'  => $school_id,
        	'academic_year_id' => $academic_year_id,
        	'semester_id' => $semester_id,
        	'logged_in' => TRUE
       	);
		// Setting session for validated user
		$this->session->set_userdata($newdata);
	}

  private function setUserName($school_id,$user_role){
    $this->load->model('User_model');
    $info = $this->User_model->getUserInfo($school_id,$user_role);
    $userinfo = array(
        'photo' => $info['photo'],
        'firstname' => $info['firstname'],
        'lastname' => $info['lastname'],
        'image' => $info['image'],
        'ext' => $info['ext']
      );
    $this->session->set_userdata($userinfo);
  }
}
