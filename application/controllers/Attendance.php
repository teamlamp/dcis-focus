<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends Auth_Controller {
  
  function __construct() {
    parent::__construct();
      $this->data['user_role'] = $this->session->userdata('user_role');
      $this->data['user_id'] = $this->session->userdata('user_id');
      $this->data['school_id'] = $this->session->userdata('school_id');
      //

      $this->load->model('Instructors_model');
      $this->load->model('Courses_model');
      $this->load->model('Rooms_model');
      $this->load->model('Classes_model');
      $this->load->model('Students_model');
      $this->load->model('Callouts_model');
      $this->load->model('Grades_model');
      $this->load->model('Rubrics_model');
  }

  public function index($course_code, $group_number){
    if($this->data['user_role'] != "student"){
      $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
      $class_id = $this->data['one_class'][0]['class_id'];
      $this->data['members'] = $this->Classes_model->getAllClassMembers($class_id);
      $this->data['status'] = $this->Classes_model->getAttendanceStatus($class_id);
      $this->load->view('pages/attendance/attendance', $this->data);

      //echo json_encode($this->data['status']);
    } else {
      redirect('404');
    }
    
  }

    public function viewStudentAttendanceRecords(){    
      $c = "ICT126";
      $g = 1;
    $this->data['one_class'] = $this->Classes_model->getOneClass($c, $g);
    $class_id = $this->data['one_class'][0]['class_id'];
    $this->data['member'] = $this->Classes_model->getClassMember($class_id);
    $member_id = $this->data['member'][0]['class_member_id'];
    $this->data['absences'] = $this->Classes_model->getStudentAbsences($member_id);
    $this->data['lates'] = $this->Classes_model->getStudentLates($member_id);
    //echo json_encode($data);
  if($this->data['one_class']){
      $this->load->view('pages/attendance/student_attendance',$this->data);
    }else{
      redirect('404');
    }



//echo json_encode($this->data['absences']);

  }
//$this->load->view('pages/attendance/student_attendance');




}
