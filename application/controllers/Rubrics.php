<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rubrics extends Auth_Controller {
  
  function __construct() {
    parent::__construct();
      $this->data['user_role'] = $this->session->userdata('user_role');
      $this->data['user_id'] = $this->session->userdata('user_id');
      $this->data['school_id'] = $this->session->userdata('school_id');;
      // 
      $this->load->model('Classes_model');
      $this->load->model('Rubrics_model');
      $this->load->model('Grades_model');
  }

// Rubrics Page
//

  public function gradeRubrics($course_code, $group_number){
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $x = $this->data['one_class'][0]['class_id'];
    if($this->data['one_class']  && $this->data['user_role'] != "student"){
      $this->data['rubrics'] = $this->Rubrics_model->getRubrics($x);
      $this->load->view('pages/classes/graderubrics',$this->data);
    }else{
      redirect('404');
    }     
  }

  public function saveRubrics(){
    $data = $this->input->post('batch');
    $this->Rubrics_model->saveRubrics($data);

    $rows = $this->Rubrics_model->getRubrics($data[0]['class_id']);

    foreach($rows as $row){
    echo "<tr class='set-category set-row'>
            <td style='display:none;'><span class='formula-id'>".$row['formula_id']."</span></td>
            <td style='width:164px;'><span class='category-name'>".$row['type']."</span></td>
            <td style='width:257px;'><span class='category-percentage'>".$row['percent']."</span></td>
            <td style='width:30px;'><i class='fa fa-pencil edit-type'></i><i class='fa fa-check set-type' style='display:none;'></i><i class='fa fa-trash-o remove-type'></i></td>
          </tr>";
    }
    $this->totalRubs();
  }

  public function deleteRubrics(){
    $data = $this->input->post('rev_row');
    $this->Rubrics_model->deleteRubrics($data);
    $this->Grades_model->delGrades($data);
    $this->Grades_model->delRH($data);
    $this->totalRubs();
  }

  public function updateRubrics(){
    $data = $this->input->post('edit_rows');
    $this->Rubrics_model->updateRubrics($data);
    $this->totalRubs();
  }

  public function updateSingleRH(){
    $data = array(
      'grade_history_id' => $this->input->post('grade_history_id'),
      'activity_title' => $this->input->post('activity_title')
      );
    $this->Rubrics_model->updateSingleRH($data);  
  }

  public function updateSingleTS(){
    $data = array(
      'grade_history_id' => $this->input->post('grade_history_id'),
      'perfect_score' => $this->input->post('perfect_score')
      );
    $x = $this->input->post('actual_ts');
    $y = $this->input->post('perfect_score');
    $z = $this->input->post('grade_history_id');
    if($y < $x){
      $this->Rubrics_model->updateAllTS($y,$z);
    }
    $this->Rubrics_model->updateTSOnly($y,$z);
    $this->Rubrics_model->updateSingleTS($data);
    $this->totalRubs();
  }

  public function checkIfRubricsHasGrades(){
    $formula_id = $this->input->post('formula_id');
    $test = $this->Rubrics_model->checkIfRubricHasGrades($formula_id);
    $x;
    if($test) $x = 1; else $x = 0; 
    echo $x;
  }

  public function totalRUbs(){
    $rubtot = $this->Rubrics_model->addRubricsTotal();
    $class_id = $rubtot[0]['class_id']; 
    $this->Rubrics_model->storeRubTot($rubtot[0]['class_id'], $rubtot[0]['rubtot']);
    $gradestot = $this->Grades_model->addStudentGradesTot($class_id);
    $this->Grades_model->updateStudentGradesTot($gradestot);
  }

}
