<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultation extends Auth_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Consultation_model');
    $this->load->model('Classes_model');
	}
 	public function index(){

    if($this->session->userdata('user_role') == 'student'){
      $this->data['consultations'] = $this->Consultation_model->getAllConsultationByStudent();
    }
          elseif($this->session->userdata('user_role') == 'instructor'){
            $result = $this->Consultation_model->getInstructorID($this->session->userdata('school_id'));
            $this->data['consultations'] = $this->Consultation_model->getAllConsultationByInstructor($result);
          }
else{
  $this->data['consultations'] = $this->Consultation_model->getAllConsultation();
}

 		
    	$this->load->view('pages/consultation/consultations', $this->data);
  	}

  	public function addNewConsultation() {
      $this->load->model('Instructors_model');
      $i = new Instructors_model();
      $data['instructors'] = $i->getAllInstructors();
      if ($this->session->userdata['user_role'] != 'admin') {
        if($this->session->userdata['user_role'] == 'instructor') {
          $data['instructor_id'] = $this->Consultation_model->getInstructorID($this->session->userdata('school_id'));
        }
        $data['classes'] = $this->Consultation_model->getConsultationClasses($this->session->userdata('school_id'));
      }

  		$this->load->view('pages/consultation/addnewconsultation', $data);
  	}

  function dateValidation()
  {
    date_default_timezone_set("Asia/Manila");
    $date = $this->input->post('consultation_date');
    $date_today = mdate("%Y-%m-%d");

    if(strtotime($date) <= strtotime($date_today))
    {
      return true;
    }

    else
    {
      $this->form_validation->set_message('dateValidation', 'Unable to add consultation records for future dates.');
      return false;
    }
  }

  function _regex_check($str)
  {
    
    if (preg_match("/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/",$str))
        {
            return true;
         }
      else
      {
        $this->form_validation->set_message('_regex_check', 'The date format you entered is invalid !');
          return false;
      }
  }

    public function addConsultation()
    {
        $p = new Consultation_model();
        $p->consultation_date= date('Y-m-d',strtotime($this->input->post('consultation_date')));
        
        /*if($this->session->userdata('user_role') == "instructor")
        {
          $p->instructor_id = $this->session->userdata('school_id');
        }
        else
        {
          $p->instructor_id = $this->input->post('instructor_id');
        }*/
        $p->instructor_id = $this->input->post('instructor_id');
        $p->class_id = $this->input->post('classes');
        $p->consultation_topic = $this->input->post('consultation_topic');
        $p->consultation_content = $this->input->post('consultation_content');
        $consultation_id = $p->addNewConsultation();

        $result = $p->addConsultees($consultation_id);

        if($result) {
          echo json_encode(array('response'=>'success'));// return success
        } else {
          echo json_encode(array('response'=>'failed'));// return fail
        }

    }

  public function getConsultees(){
    $id = strip_tags(trim($_POST['id'])); 
    $this->data['result'] = $this->Consultation_model->getConsultees($id);
    if ($this->data['result']) {
      foreach ($this->data['result'] as $key => $value) {
        $name = $value['firstname']." ".$value['lastname'];
        $data[] = array(
                'id' => $value['school_id'],
                'name' => $name
                );
        }
    echo json_encode($data);
    }
  
  }

  public function getClassMembers() {
    $id = strip_tags(trim($_GET['id']));
    $this->data['result'] = $this->Classes_model->getClassMembersName($id);
    if ($this->data['result']) {
      foreach ($this->data['result'] as $key => $value) {
        $name = $value['firstname']." ".$value['lastname'];
        $data[] = array(
                'id' => $value['school_id'],
                'name' => $name
                );
        }
    echo json_encode($data);
    }
  }

  public function getHandledCLasses() {
    $instructor_id = strip_tags(trim($_GET['id']));
    $school_id = $this->Consultation_model->getInstructorSchoolID($instructor_id);
    $this->data['result'] = $this->Consultation_model->getConsultationClasses($school_id);
    if ($this->data['result']) {
      foreach ($this->data['result'] as $key => $value) {
        $text = "Group ".$value['group_number']." ".$value['course_code'];
        $data[] = array(
                'id' => $value['class_id'],
                'text' => $text
                );
        }
    echo json_encode($data);
    }
  }

  public function deleteConsultation() {
    $id = strip_tags(trim($_GET['id']));
    $this->Consultation_model->deleteConsultees($id);
    $this->Consultation_model->deleteConsultation($id);
  }

}
