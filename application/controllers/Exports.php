<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exports extends Auth_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['user_role'] = $this->session->userdata('user_role');
      $this->data['user_id'] = $this->session->userdata('user_id');
      $this->data['school_id'] = $this->session->userdata('school_id');
      //
      $this->load->library('form_validation');
      $this->load->model('Instructors_model');
      $this->load->model('Courses_model');
      $this->load->model('Rooms_model');
      $this->load->model('Classes_model');
      $this->load->model('Students_model');
      $this->load->model('Callouts_model');
      $this->load->model('Grades_model');
      $this->load->model('Rubrics_model');
	}

  public function exportAllConsultationRecordsAsPDF()
  {

date_default_timezone_set("Asia/Manila");
$datestring = "%Y-%m-%d";
$cdate = mdate($datestring);
    $ndate = date('F d, Y',strtotime($cdate));

    $this->load->model('Exports_model');
    $data = array();
    $data['consultations'] = $this->Exports_model->getAllConsultation();
    $data['consultees'] = $this->Exports_model->getAllConsultees();

    $this->load->view('exports/consultation_records', $data);
    // Get output html
    $html = $this->output->get_output();
    
    // Load library
    $this->load->library('dompdf_gen');
    $filename = "Consultation Records as of ".$ndate.".pdf";
    // Convert to PDF
    $this->dompdf->load_html($html);
    $this->dompdf->render();
    $this->dompdf->stream($filename);
  //echo json_encode($data['consultees']);
  }


public function exportAllConsultationRecordsByInstructorAsPDF()
  {
    date_default_timezone_set("Asia/Manila");
$datestring = "%Y-%m-%d";
$cdate = mdate($datestring);
    $ndate = date('F d, Y',strtotime($cdate));

    $this->load->model('Exports_model');
    $data = array();
    $data['consultations'] = $this->Exports_model->getAllConsultationByInstructor();
    $data['consultees'] = $this->Exports_model->getAllConsultees();

    $this->load->view('exports/consultation_records', $data);
    // Get output html
    $html = $this->output->get_output();
    
    // Load library
    $this->load->library('dompdf_gen');
    $filename = "Consultation Records by Instructor as of ".$ndate.".pdf";
    // Convert to PDF
    $this->dompdf->load_html($html);
    $this->dompdf->render();
    $this->dompdf->stream($filename); 
  //echo json_encode($id);
  }

public function exportAllConsultationRecordsByStudentAsPDF()
  {
    date_default_timezone_set("Asia/Manila");
    $datestring = "%Y-%m-%d";
    $cdate = mdate($datestring);
    $ndate = date('F d, Y',strtotime($cdate));

    $this->load->model('Exports_model');
    $data = array();
    $data['consultations'] = $this->Exports_model->getAllConsultationByStudent();
    $data['consultees'] = $this->Exports_model->getAllConsultees();

    $this->load->view('exports/consultation_records', $data);
    // Get output html
    $html = $this->output->get_output();
    
    // Load library
    $this->load->library('dompdf_gen');
    $filename = "Consultation Records by Student as of ".$ndate.".pdf";
    // Convert to PDF
    $this->dompdf->load_html($html);
    $this->dompdf->render();
    $this->dompdf->stream($filename);
  //echo json_encode($id);
  }








}
