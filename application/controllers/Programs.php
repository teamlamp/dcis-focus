<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	class Programs extends Auth_Controller {
	
	function __construct() {
    	parent::__construct();
    	$this->load->library('form_validation');
      	$this->load->model('Programs_model');
  	}	

	public function index() {
		//$this->data['academic_years'] = $this->Calendar_model->getAllYears();
		$data['programs'] = $this->Programs_model->getAllPrograms();
		//$data['students'] = $this->Programs_model->countStudents();
		$this->load->view('pages/programs', $data);
	}

	public function addProgram () {

		$this->load->view('pages/addProgram');
	}

	public function addNewProgram()
	{
		$this->load->library('form_validation');
	    $this->form_validation->set_rules('program_code', 'Program Code', 'required|callback_checkIfProgramAlreadyExists');
	    //$this->form_validation->set_rules('course_title', 'Course Title', 'required');

	    if ($this->form_validation->run())
	    {
			$data = array(
			'deparment_id' => 1,
			'program_code' => $this->input->post('program_code'));
	   		
	   		$result = $this->Programs_model->addNewProgram($data);
	   	 	
	   	 	if (!$result)
	   	 	{
	      		echo mysqli_error($result);
	    	}
	    
		    else
		    {
		      echo "<script type='text/javascript'>alert('Successfully added new program!')</script>";
		      redirect('programs', 'refresh');
		    }
		}

        else
        {
        	$this->load->view('pages/addProgram');
      	}
   }

    public function checkIfProgramAlreadyExists()
	{    
		if ($check = $this->Programs_model->checkIfProgramAlreadyExists($this->input->post('program_code')) == false)
	  	{
	    	return true;
	   	}
	   
	    else
	    {
	    	$this->form_validation->set_message('checkIfProgramAlreadyExists', 'The program you are trying to add already exists!');
	      	return false;
	    }
	}

   	public function deleteProgram(){

		$this->load->model('Programs_model');
		$p = new Programs_model();
		$p->program_id = $this->input->post('program_id');
		$result = $p->deleteProgram();

			if(!$result){
				echo mysqli_error($result);
			}

			else{
				echo "<script type='text/javascript'>alert('Successfully deleted program!')</script>";
				redirect('programs', 'refresh');
			}
	
	
	}

	public function getAllPrograms()
	{
		echo json_encode($this->Programs_model->getAllPrograms());
	}

}
