<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
   	
   	// Setting constructor to check if user is already logged in
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in'))
        { 
            redirect('timeline'); // If user is already logged in he/she's redirected to his/her dashboard
        }
    }

	public function index(){

		$this->load->view('pages/home/index.html'); 
	}

}
