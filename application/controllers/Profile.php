<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Auth_Controller {

  function __construct() {
    parent::__construct();
      $this->data['user_role'] = $this->session->userdata('user_role');
      $this->data['user_id'] = $this->session->userdata('user_id');
      $this->data['school_id'] = $this->session->userdata('school_id');;
      // 
      $this->load->model('User_model');
      $this->load->model('Classes_model');
  }

  // public function index()
  // {
  //   $this->load->view('pages/profile_new');
  // }

  public function logout(){
    $this->session->unset_userdata('school_id');
    $this->session->sess_destroy();
    $base_url = base_url(); redirect($base_url);
  }

  public function getProfile($school_id){
    $this->data['user_data'] = $this->User_model->getUserRole($school_id);
    if($this->data['user_data'][0]['user_role'] == "admin"){
      $this->data['profile_data'] = $this->User_model->getAdminProfile($school_id);
      $this->load->view('pages/profile_admin',$this->data);

    }else if ($this->data['user_data'][0]['user_role'] == "staff"){
      $this->data['profile_data'] = $this->User_model->getStaffProfile($school_id);
      $this->load->view('pages/profile_staff',$this->data);

    }else if ($this->data['user_data'][0]['user_role'] == "instructor"){
      $this->data['profile_data'] = $this->User_model->getInstructorProfile($school_id);
      $this->data['handled_classes'] = $this->Classes_model->getHandledClasses($school_id);
      $this->load->view('pages/profile_ins',$this->data);

    }else if ($this->data['user_data'][0]['user_role'] == "student"){
      $this->data['profile_data'] = $this->User_model->getStudentProfile($school_id);
      $this->data['enrolled_classes'] = $this->Classes_model->getEnrolledClasses($school_id);
      $this->load->view('pages/profile_stud',$this->data);
    } else {
      redirect('404');
    }
  }
}
