<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends Auth_Controller {

  public function __construct(){
    parent::__construct();
    $this->data['school_id'] = $this->session->userdata('school_id');
    $this->load->model('Tasks_model');
  }

	public function index(){
    $this->data['my_tasks'] = $this->Tasks_model->getMyTassks($this->data['school_id']);
    $this->data['deleted_tasks'] = $this->Tasks_model->getDelTasks($this->data['school_id']);
		$this->load->view('pages/tasks/tasks', $this->data);
	}

// Task Section
//

  public function saveTask(){

    $data = array(
      'school_id' => $this->input->post('school_id'),
      'color' => $this->input->post('color'),
      'title' => $this->input->post('title'),
      'content' => $this->input->post('content')
    );
    $t=time();
    $id = $this->Tasks_model->saveTask($data);
    echo "<div class='col-md-4 tskhldr'>
          <!-- BEGIN Portlet PORTLET-->
          <div class='portlet box ".$data['color']." tskdsplyclr'>
            <div class='portlet-title'>
              <div class='caption tskdsplyttl'>
                ".$data['title']."
              </div>
              <div class='actions'>
                <a href='javascript:;' class='btn btn-default btn-sm tskedt' title='Edit'>
                <i class='fa fa-pencil'></i> </a>
                <a href='javascript:;' class='btn btn-default btn-sm tskdone' style='margin-left:0px!important' title='Completed'>
                <i class='fa fa-check'></i> </a>  
                <input type='hidden' class='tskid' value='".$id."'>                
              </div>
            </div>
            <div class='portlet-body'>
            <div class='scroller tskdsplycnt' style='height:200px'>
                ".$data['content']."
              </div>
              <h6 class='tskdsplytime'>".date('F d, Y - g:m A', $t)."</h6>
            </div>
          </div>
          <!-- END Portlet PORTLET-->
        </div>";

  }

  public function countTasks(){
    $taskcount = $this->Tasks_model->countTasks($this->data['school_id']);
    echo $taskcount['taskcount'];
  }

  public function delTask(){
    $task_id = $this->input->post('task_id');
    $this->Tasks_model->delTask($this->data['school_id'],$task_id);
  }

  public function delTaskPerma(){
    $task_id = $this->input->post('task_id');
    $this->Tasks_model->delTaskPerma($this->data['school_id'],$task_id);
  }

  public function editTask(){
    $task_id = $this->input->post('task_id');
    $data = $this->Tasks_model->getTask($this->data['school_id'],$task_id);
    echo json_encode($data);
  }

  public function saveedittask(){
    $task_id = $this->input->post('task_id');
    $color = $this->input->post('color');
    $title = $this->input->post('title');
    $content = $this->input->post('content');
    $this->Tasks_model->editTask($task_id,$color,$title,$content);
  }
}
