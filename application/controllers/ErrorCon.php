<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ErrorCon extends Auth_Controller {
  
  function __construct() {
    parent::__construct();
  }

  private function index($id){
    if($id == 404){
      $this->load->view('welcome_message');          
    }     
  }

  public function error404(){
    $this->load->view('errors/cli/error_404');  
  }
}
