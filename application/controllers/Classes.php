<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends Auth_Controller {
  
  function __construct() {
    parent::__construct();
      $this->data['user_role'] = $this->session->userdata('user_role');
      $this->data['user_id'] = $this->session->userdata('user_id');
      $this->data['school_id'] = $this->session->userdata('school_id');
      //
      $this->load->library('form_validation');
      $this->load->model('Instructors_model');
      $this->load->model('Courses_model');
      $this->load->model('Rooms_model');
      $this->load->model('Classes_model');
      $this->load->model('Students_model');
      $this->load->model('Callouts_model');
      $this->load->model('Grades_model');
      $this->load->model('Rubrics_model');
  }

// Class Section
//

  public function index(){
    // admin & staff side of class page
    if($this->data['user_role'] == "admin" || $this->data['user_role'] == "staff")
    {
      $this->data['classes'] = $this->Classes_model->getAllClasses();
      $this->load->view('pages/classes/classlist',$this->data);
    }
    // instructor side of class page
    elseif($this->data['user_role'] == "instructor")
    {
      $this->data['classes'] = $this->Classes_model->getHandledClasses($this->data['school_id']);
      $this->load->view('pages/classes/classlist',$this->data);
    }
    // student side of the class page
    elseif($this->data['user_role'] == "student")
    {
      $this->data['classes'] = $this->Classes_model->getEnrolledClasses($this->data['school_id']);
      $this->load->view('pages/classes/classlist',$this->data);
    }
    else
    {
      redirect('404');
    }
  }

  // Get One Class
  public function getClass($course_code, $group_number){    
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $this->data['callouts'] = $this->Callouts_model->getCallOuts($course_code, $group_number);
    $class_id = $this->data['one_class'][0]['class_id'];
    if($this->data['one_class']){
      // For last activity >>
      $this->data['last_act'] = $this->Grades_model->getLastActivity($class_id);
      if($this->data['last_act']){
        $id = $this->data['last_act'][0]['last_act_id'];
        $this->data['rub'] = $this->Rubrics_model->getRubDetails($this->data['last_act'][0]['formula_id']);
        $this->data['pass'] = $this->Grades_model->countPass($id);
        $this->data['fail'] = $this->Grades_model->countFail($id);
      }
      // <<
      $this->load->view('pages/classes/class',$this->data);
    }else{
      redirect('404');
    }
  }

// Admin - Students List Section
  public function getStudentsList($course_code, $group_number){
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $x = $this->data['one_class'][0]['class_id'];
    $this->data['students'] = $this->Students_model->getStudentsList($x);
    if($this->data['one_class'] && $this->data['user_role'] != "student"){ 
      $this->load->view('pages/classes/studentslist',$this->data);
    }else{
      redirect('404');
    }
  }

// Student - Assessment Section
  public function getAssessment($course_code, $group_number){ 
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $class_id = $this->data['one_class'][0]['class_id'];
    $school_id = $this->session->userdata('school_id');  
    if($this->data['one_class']){
      $this->data['student_data'] = $this->Students_model->getStudentData($school_id, $class_id);
      $this->data['one_student_grades'] = $this->Grades_model->getStudentGrades($school_id, $class_id); 
      $this->data['failpass'] = $this->Grades_model->passfailChance($school_id, $class_id); 
      $this->data['list_of_activities'] = $this->Grades_model->getHistory($class_id);
      $this->load->view('pages/classes/assessment',$this->data);
    }else{
      redirect('404');
    }     
  }

// Callouts Section
  public function saveCallOut(){
  	$this->data['one_class'] = $this->Classes_model->getOneClass($this->input->post('course'), $this->input->post('group'));
    $x = $this->data['one_class'][0]['class_id'];
    $data = array(
      'title' => $this->input->post('title'),
      'type' => $this->input->post('type'),
      'content' => $this->input->post('content'),
      'class_id' => $x
    );
    $id = $this->Callouts_model->saveCallOut($data);
    $t=time();
    if($data['type'] == "Info") $type = "info";
    else if($data['type'] == "Warning") $type = "warning";
    else if($data['type'] == "Danger") $type = "danger";
    echo "
      <div class='note note-".$type."'>
      <strong style='text-transform: uppercase;'>".$data['title']."</strong>
      <p id='".$id."'>
        ".$data['content']."
      </p>
      <sub>".date('F d, Y - g:m A', $t)."</sub>
      </div>";
  }

  public function updatecallout(){
    $a = $this->input->post('id');
    $b = $this->input->post('type');
    $c = $this->input->post('subject');
    $d = $this->input->post('content');
    $this->Callouts_model->updatecallout($a,$b,$c,$d);
  }

//Class settings
  public function class_settings($course_code,$group_number){
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    if($this->data['one_class']  && $this->data['user_role'] != "student"){
      $this->load->view('pages/classes/class_settings',$this->data);
    } else {
      redirect ('404');
    }
  }

  public function saveClassPref(){
    $class_id = $this->input->post('class_id');
    $tile_color = $this->input->post('tile_color');
    $tile_icon = $this->input->post('tile_icon');
    $enable_latest_activity = $this->input->post('enable_latest_activity');
    $enable_callouts = $this->input->post('enable_callouts');
    $enable_chatbox = $this->input->post('enable_chatbox');
    $this->Classes_model->saveClassPref($class_id,$tile_color,$tile_icon,$enable_latest_activity,$enable_callouts,$enable_chatbox);
  }

  public function add_class(){
  
    $data['instructors'] = $this->Instructors_model->getAllInstructors();
    $data['courses'] = $this->Courses_model->getAllCourses();
    $data['rooms'] = $this->Rooms_model->getAllRooms();
    $this->load->view('pages/classes/add_class', $data);
  }

public function checkIfClassExists(){
  $group_number = $this->input->post('group_number');
  $course_id = $this->input->post('course_id');

  if ($check = $this->Classes_model->checkIfClassExists($course_id, $group_number) == false)
  {
        return true;

    }

    else 
    {
      $this->form_validation->set_message('checkIfClassExists', 'The class you are trying to add already exists.');
      return false;
    }

}

  public function addNewClass()
  {
$this->form_validation->set_rules('course_id', 'Course', 'required');
$this->form_validation->set_rules('room_id', 'Room', 'required');
$this->form_validation->set_rules('group_number', 'Group #', 'required|callback_checkIfClassExists');
$this->form_validation->set_rules('max_absences', 'Maximum Number of Absences', 'required');
    if($this->session->userdata('user_role') != "instructor")
    {
      $this->form_validation->set_rules('instructor_id', 'Instructor', 'required');
    }
          if ($this->form_validation->run())
          {
                //adding of class schedule
                $ctr = 0;
                for($i=0; $i < count($_POST['day']); $i++)
                {
                  $start_time = $_POST['start_time'][$i]; 
                  $end_time = $_POST['end_time'][$i];

                  if(strtotime($end_time) < strtotime($start_time) + 1800)
                  {
                    $ctr++;
                  }
                } // end of for loop
                if ($ctr == 0)
                {

                  if($this->session->userdata('user_role') != "instructor")
                  {
                    $data = array(
                    'course_id' => $this->input->post('course_id'),
                    'instructor_id' => $this->input->post('instructor_id'),
                    'room_id' => $this->input->post('room_id'),
                    'group_number' => $this->input->post('group_number'),
                    'max_absences' => $this->input->post('max_absences'),
                    'semester_id' => $this->session->userdata('semester_id')
                    );
                  }

                  else
                  {
                    $data = array(
                    'course_id' => $this->input->post('course_id'),
                    'instructor_id' => $this->session->userdata('school_id'),
                    'room_id' => $this->input->post('room_id'),
                    'group_number' => $this->input->post('group_number'),
                    'max_absences' => $this->input->post('max_absences'),
                    'semester_id' => $this->session->userdata('semester_id')
                    );
                  }

                  if($result = $this->Classes_model->addNewClass($data))
                  {
                      redirect('classes', 'refresh');
                  }
                  else
                  {
                    echo mysqli_error($result);
                  }
              } // end of if statement to check if time format is valid
              else
              {
                  echo "<script type='text/javascript'>alert('Invalid Schedule Inputs');</script>";
                  echo "<script type='text/javascript'>history.go(-1);</script>";
              }
          } // end of validation success
          
          else // validation errors
          {
            $this->load->view('pages/classes/add_class');
          }
  }

//Class Record
  public function classrecord($course_code, $group_number){
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $x = $this->data['one_class'][0]['class_id'];
    if($this->data['one_class']  && $this->data['user_role'] != "student"){
      $this->data['rubrics'] = $this->Rubrics_model->classRecordrubrics($x);
      $this->data['activities'] = $this->Rubrics_model->getActivities($x);
      $this->data['students'] = $this->Students_model->getStudentsList($x);
      $this->data['student_grades'] = $this->Grades_model->allGrades($x);
      $this->data['rubtot'] = $this->Rubrics_model->getRubTot($x);
      $this->load->view('pages/classes/classrecord',$this->data);
    }else{
      redirect('404');
    }
    //print_r($this->data['student_grades']);
  }

  public function importClassList()
  {
    $class_id = $this->input->post('class_id');
    //$this->load->model('Classes_model'); naka tawag na daan ang mga needed models sa constructor
    if ($data['result']=$this->Classes_model->importClassList($class_id))
    {
      //redirect('classes', 'refresh');
      echo "<script type='text/javascript'>history.go(-1);</script>";
      $this->setZeroToMissedActs($class_id);
    }
    else
    {
      echo mysqli_error($data['result']);
    }
  }
  
public function removeClassMember()
{
  $student_id = $this->input->post('student_id');
  $class_id = $this->input->post('class_id');
  if ($data['result'] = $this->Classes_model->removeClassMember($student_id, $class_id))
  {
    echo "<script type='text/javascript'>history.go(-1);</script>";
  }

  else
    {
      echo mysqli_error($data['result']);
    }
}

  // If naa nay mga na record na grades unya naay newly added na student 
  // Set to zero ang iyang mga na miss na activities or else maguba ang class record page
  public function setZeroToMissedActs($class_id){
    $class_members = $this->Students_model->getStudentsList($class_id); // get all students of a class
    $flat_class_members = array_column($class_members, 'class_member_id'); // store all class_member_id
    $cm_with_grades = $this->Students_model->cmWithGrades($class_id); // get students with record grades
    $flat_cm_with_grades = array_column($cm_with_grades, 'class_member_id'); // store all class_member_id with grades 
    $students_with_no_grades = array_diff($flat_class_members,$flat_cm_with_grades); // get the class_member_id without grades
    $new_stud_ng = array();
    foreach($students_with_no_grades as $swng){
      $new_stud_ng[] = $swng;
    }
    $nogradesnum = sizeof($students_with_no_grades);
    $recorded_acts = $this->Rubrics_model->getActivities($class_id); // get list of recorded activities
    $flat_record_acts_id = array_column($recorded_acts, 'grade_history_id');
    $flat_record_acts_formula_id = array_column($recorded_acts, 'formula_id');
    $flat_record_acts_total = array_column($recorded_acts, 'perfect_score');
    $recordedactsnum = sizeof($flat_record_acts_formula_id);
    // set missed activities to zero
    for($ctr=0;$ctr<$nogradesnum;$ctr++){
      for($i=0;$i<$recordedactsnum;$i++){
        $this->Grades_model->setZeroToMissedActs($new_stud_ng[$ctr],$flat_record_acts_id[$i],$flat_record_acts_formula_id[$i],$flat_record_acts_total[$i]);
      }
    }
  }

public function updateSeatPlan()
{

  /*
  [{"studentID":"12105455","classID":"28","col":1,"row":1},
    {"studentID":"12102719","classID":"28","col":2,"row":1},
    {"studentID":"12104744","classID":"28","col":3,"row":2},
    {"studentID":"12103895","classID":"28","col":3,"row":1}]
  */
   if(isset($_GET['data'])){
      
      $result = $this->Classes_model->updateSeatPlan($_GET['data']);
      echo json_encode($result);
   }else{
      echo "not found";
   };
}

public function addClassMemberManually()
{
 if($data['result'] = $this->Classes_model->addClassMemberManually($_POST['students'], $_POST['class_id'])){
    echo '<script type="text/javascript">sweetAlert("Success!", "Students successfully added in the class!", "success");</script>';
     echo "<script type='text/javascript'>history.go(-1);</script>";
    }
    else
    {
      echo mysqli_error($data['result']);
    }
//$students = $_POST['students'];
    //echo json_encode($students[0]);
    //echo json_encode($_POST['class_id']);
}

public function checkAttendance(){

  if( isset($_POST['class_member_id']) && isset($_POST['type']) ){
    $result = $this->Classes_model->checkAttendance($_POST['class_member_id'], $_POST['type']);
    echo json_encode($result);
 }else{
    echo "not found";
 };
}

public function getHandledClasses(){
  echo json_encode($this->Classes_model->getHandledClasses($_GET["id"]));
}

public function getHandledStudents(){
  echo json_encode($this->Classes_model->getClass($_GET["id"]));
}

    public function viewStudentAttendanceRecords($course_code, $group_number){    
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $class_id = $this->data['one_class'][0]['class_id'];
    $this->data['member'] = $this->Classes_model->getClassMember($class_id);
    $member_id = $this->data['member'][0]['class_member_id'];
    $this->data['absences'] = $this->Classes_model->getStudentAbsences($member_id);
    $this->data['lates'] = $this->Classes_model->getStudentLates($member_id);
    //echo json_encode($data);
  if($this->data['one_class']){
      $this->load->view('pages/attendance/student_attendance',$this->data);
    }else{
      redirect('404');
    }

  }



      public function viewStudentAttendance($course_code, $group_number, $class_member_id){    
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $class_id = $this->data['one_class'][0]['class_id'];
    //$this->data['member'] = $this->Classes_model->getClassMember($class_id);
    //$member_id = $this->data['member'][0]['class_member_id'];
    $this->data['absences'] = $this->Classes_model->getStudentAbsences($class_member_id);
    $this->data['lates'] = $this->Classes_model->getStudentLates($class_member_id);
    //echo json_encode($data);
  if($this->data['one_class']){
      $this->load->view('pages/attendance/student_attendance',$this->data);
    }else{
      redirect('404');
    }

  }

  public function classCalendar($course_code, $group_number) {
    $this->data['one_class'] = $this->Classes_model->getOneClass($course_code, $group_number);
    $this->load->view('pages/classes/class_calendar',$this->data);
  }

  public function oldClass() {
  // admin & staff side of class page
    if($this->data['user_role'] == "admin" || $this->data['user_role'] == "staff") {
      $this->data['classes'] = $this->Classes_model->getAllClasses();
      $this->load->view('pages/classes/previous_classlist',$this->data);
    }
    // instructor side of class page
    elseif($this->data['user_role'] == "instructor") {
      $this->data['classes'] = $this->Classes_model->getHandledClasses($this->data['school_id']);
      $this->load->view('pages/classes/previous_classlist',$this->data);
    }
    // student side of the class page
    elseif($this->data['user_role'] == "student") {
      $this->data['classes'] = $this->Classes_model->getEnrolledClasses($this->data['school_id']);
      $this->load->view('pages/classes/previous_classlist',$this->data);
    }
    else {
      redirect('404');
    }
  }

}

      