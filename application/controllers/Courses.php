<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends Auth_Controller {

  function __construct() {
    parent::__construct();
    $this->data['user_role'] = $this->session->userdata('user_role');
    $this->load->model('Courses_model');
    $this->load->library('form_validation');
  }
  public function index(){
    if($this->data['user_role'] != "student"){
      $this->data['courses'] = $this->Courses_model->getAll();
      $this->load->view('pages/courses',$this->data);
    } else {
      redirect('404');
    }
  }

  public function add()
  {
    //$this->load->model('Programs_model');
    //$p = new Programs_model();
    //$data = array();
    //$data['programs'] = $p->getAll();
    $this->load->view('pages/addnewcourse');
  }


  public function addCourse()
  {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('course_code', 'Course Code', 'required|callback_checkIfCourseAlreadyExists');
    $this->form_validation->set_rules('course_title', 'Course Title', 'required');
    $this->form_validation->set_rules('program_id', 'Program', 'required');

      if ($this->form_validation->run())
      {

        $this->load->model('Courses_model');
        $p = new Courses_model();
        $p->program_id = $this->input->post('program_id');
        $p->course_code = strtoupper($this->input->post('course_code'));
        $p->course_title = $this->input->post('course_title');
        $p->req_year = $this->input->post('req_year');
        $p->req_semester = $this->input->post('req_semester');
        
        $result=$p->addCourse();
        
        if (!$result){
          echo mysqli_error($result);
        }
        
        else
        {
          echo "<script type='text/javascript'>alert('Successfully added new course!')</script>";
          redirect('courses', 'refresh');
        }
      } // end of adding course

      else
      {
        $this->load->view('pages/addnewcourse');
      }
  }

  public function checkIfCourseAlreadyExists()
  {    
    if ($check = $this->Courses_model->checkIfCourseAlreadyExists($this->input->post('course_code')) == false)
    {
      return true;
    }
    
    else
    {
      $this->form_validation->set_message('checkIfCourseAlreadyExists', 'The course you are trying to add already exists!');
      return false;
    }
  }




  public function viewUpdateSemesterForm(){}
  public function updateSemester(){}
  public function deleteSemester(){}
  

}
