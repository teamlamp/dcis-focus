$(document).ready(function(){
console.log("cstm-used");
// Global Functions
    var user_role = $('#srrl').val();
    var content = "";
    function convert2hex(x){
        var rgbString = x; // get this in whatever way.

        var parts = rgbString.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        // parts now should be ["rgb(0, 70, 255", "0", "70", "255"]

        delete (parts[0]);
        for (var i = 1; i <= 3; ++i) {
            parts[i] = parseInt(parts[i]).toString(16);
            if (parts[i].length == 1) parts[i] = '0' + parts[i];
        } 
        var hexString ='#'+parts.join('').toUpperCase(); // "#0070FF"

        return hexString;
    }

    function getcourse(){
        var patharray = window.location.pathname.split( '/' );
        var course = patharray[2];
        return course;
    }

    function getgroup(){
        var patharray = window.location.pathname.split( '/' );
        var group = patharray[3];
        return group;
    }

    function curDate(){
        var m_names = new Array("January", "February", "March", 
        "April", "May", "June", "July", "August", "September", 
        "October", "November", "December");

        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        var complete_cur_date =  m_names[curr_month]+" "+curr_date+", "+curr_year;
        return complete_cur_date;
    }

    function curTime(){
        var a_p = "";
        var d = new Date();
        var curr_hour = d.getHours();
        if (curr_hour < 12) {
           a_p = "AM";
        } else {
           a_p = "PM";
        } 
        if (curr_hour == 0) {
           curr_hour = 12;
        }
        if (curr_hour > 12) {
           curr_hour = curr_hour - 12;
        }

        var curr_min = d.getMinutes();

        curr_min = curr_min + "";

        if (curr_min.length == 1) {
           curr_min = "0" + curr_min;
        }

        var complete_cur_time = curr_hour + ":" + curr_min + " " + a_p;
        return complete_cur_time;
    }

// Set Active To Sidebar
    var url = window.location.href;
    var url_split_1 = url.lastIndexOf('/') +1;
    var url_1 = url.substring(0,url_split_1);

    var last_url = url.substr(url.lastIndexOf('/') + 1)

    var patharray = window.location.pathname.split( '/' );
    var domain = document.domain
    var reallink = 'http://'+domain+'/'+patharray[1]; 
    var class1 = 'http://'+domain+'/classes';
    var class2 = 'http://'+domain+'/class';

    $('#sidebar').find('li a').each(function() {
        //console.log($(this).attr('href'));
        if(this.href === reallink){
            $(this).closest('li').addClass('active');
        }else if(reallink === class2){
            $('#sidebar').find('a').each(function(){
                if(this.href === class1){
                    $(this).closest('li').addClass('active');
                }
             });
        }
    });

    if(last_url == "login"){
        $(document).keypress(function(e){
            if (e.which == 13){
                $("#user-login").click();
            }
        });       
    }


// Set Idle=>Typing in post_discussion.php
    $('#post-here').keydown(function() {
        $('#stat-icon').addClass('fa-spin');
        $('#stat').text('Typing');
    });

    $('#post-here').on('input',function(){
        var x = $('#post-here').val().length;
        if(x == 0){
            $('#stat-icon').removeClass('fa-spin');
            $('#stat').text('Idle');
        }
    });

// Set Idle=>Typing in class_chat.php
    $('#chat-content').keydown(function() {
        $('#stat-iconn').addClass('fa-spin');
        $('#statt').text('Typing');
    });

    $('#chat-content').on('input',function(){
        var z = $('#chat-content').val().length;
        if(z == 0){
            $('#stat-iconn').removeClass('fa-spin');
            $('#statt').text('Idle');
        }
    });

// Limit input type='number' to 0-9 only
    $(document).on('keydown',':input[type="number"]',function(e){
        //e.preventDefault();
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 13 && (e.which < 96 || e.which > 105) && e.which != 9) {
            return false;
    }
    });
    
// Global Alert when there are unsaved changes
//
    // Another way to bind the event
    // $(window).bind('beforeunload', function() {
    //     if(unsaved){
    //         return "You have unsaved changes on this page.";
    //     }
    // });

    // Monitor dynamic inputs
    // $(document).on('change', ':input', function(){ //triggers change in all input fields including text type
    //     unsaved = true;
    // });

// Open dropdown in Classlist.php
//
    $('.arrow-down-menu').on('click', function(e){       
        var curclass = this.className;  
        e.preventDefault();      
        if($(this).hasClass('fa-chevron-circle-down'))
        {            
            $('.ddmcl').slideUp();
            $('i.arrow-down-menu').removeClass('fa-chevron-circle-up');
            $('i.arrow-down-menu').addClass('fa-chevron-circle-down');
            $(this).removeClass('fa-chevron-circle-down');
            $(this).addClass('fa-chevron-circle-up');
            $(this).closest('.col-lg-4').find('ul').slideDown();
        }
        else if($(this).hasClass('fa-chevron-circle-up'))
        {
            $(this).removeClass('fa-chevron-circle-up');
            $(this).addClass('fa-chevron-circle-down');
            $(this).closest('.col-lg-4').find('ul').slideUp();
        }
    });

// Hide class list dropdown when user clicks anywhere on the document
//
    $(document).on('click',function(event){
        if($('.ddmcl').is(':visible')) {
            var target = $(event.target);
            if(!target.is('i')){
                $('.ddmcl').slideUp();
                $('i.arrow-down-menu').removeClass('fa-chevron-circle-up');
                $('i.arrow-down-menu').addClass('fa-chevron-circle-down');
            }
        }else if(!$('.ddmcl').is(':visible')){
            // do nothing
        }
    });

// Callouts
    // Callout type change color
    $(".callout-type").click(function () {
        $x = $(this).closest('label').find('.label').html();
        switch($x){
            case("Info"):
                $('#callout-type-label').removeClass('yellow-gold red');
                $('#callout-heads-up').html("<i class='fa fa-bullhorn'></i> Info");
                $('#callout-type-label').addClass('blue');
                break;
            case("Warning"):
                $('#callout-type-label').removeClass('blue red');
                $('#callout-heads-up').html("<i class='fa fa-bullhorn'></i> Warning");
                $('#callout-type-label').addClass('yellow-gold');  
                break;
            case("Danger"):
                $('#callout-type-label').removeClass('yellow-gold blue');
                $('#callout-heads-up').html("<i class='fa fa-bullhorn'></i> Danger");
                $('#callout-type-label').addClass('red');
                break;
        }
    });

    // Ajax saving callout
    $("#form-add-callout").submit(function(event){
        event.preventDefault();
        var title = $('#callout-title').val();
        var type = $('.callout-type:checked').closest('label').find('.label').html();
        var content = $('#callout-content').val();
        var course = getcourse();
        var group = getgroup(); 
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+'classes/savecallout',
                        dataType: 'html',
                        data: {title:title,type:type,content:content,group:group,course:course},
                        async: false,
                        success: function(data){                
                            $('#callouts-holder').fadeIn('slow').prepend(data);
                            $('#no-callouts-span').hide();
                            $('#add-callout').trigger('click');
                            $('#add_callout').fadeOut();

                            //reset model fields
                            $('#callout-title').val('');
                            $('#uniform-callout-btn-1').find('span').addClass('checked');
                            $('#callout-btn-1').prop('checked', true);
                            $('#uniform-callout-btn-2').find('span').removeClass('checked');
                            $('#uniform-callout-btn-3').find('span').removeClass('checked');
                            $('#callout-heads-up').html("<i class='fa fa-bullhorn'></i> Reminder");
                            $('#callout-type-label').removeClass('yellow-gold red');
                            $('#callout-type-label').addClass('blue');
                            $('#callout-content').val('');
                            // swal("Added!", "Callout is added.", "success");
                             swal({   title: "Success",   text: "Callout added successfully.", type:"success", timer: 1000,   showConfirmButton: false }); 

                            },
                        error: function(data){
                            // swal("Error","Something went wrong.","error");
                            swal({   title: "Opss...",   text: "Something went wrong.", type:"error", timer: 1000,   showConfirmButton: false }); 

                        } 
                    });             
        return false;
    });

    // update callout
if(user_role != "student"){
    var cltid;
    var thecallt;
    $(document).on('dblclick','.note:not(#no-callouts-span)',function(){
        $('#edit-callout').trigger('click');
        var thecallt = $(this);
        var cltid = $(thecallt).find('p').attr('id');
        var type = $(thecallt).attr('class');
        var type_split = type.split("-");
        var subject = $(thecallt).find('strong').text().trim();
        var content = $(thecallt).find('p').text().trim();
        type_str = type_split[1].toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        $('#edttclltid').val(cltid);
        var color = colorthing(type_str);
        $('#edit-callout-type-label').removeClass();
        $('#edit-callout-type-label').addClass('btn '+color);
        $('#edit-callout-heads-up').html('<i class="fa fa-bullhorn"></i> '+type_str);
        $('#edit-callout-title').val(subject);
        $('#edt-callout-content').val(content);
        $('span').closest('.radio').find('span').removeClass('checked');
        $('span.label:contains("'+type_str+'")').closest('label').find('span').addClass('checked');
    });

    $('#form-edit-callout').submit(function(event){
        event.preventDefault();
        var x = $('#edttclltid').val();
        var newtype = $('#edit-callout-type').find('.checked').find('input').val();
        var newsubject = $('#edit-callout-title').val();
        var newcontent = $('#edt-callout-content').val();    

        $.ajax({
            type: "POST",
            url: BASE_URL+"classes/updatecallout",
            data: {id:x,type:newtype,subject:newsubject,content:newcontent},
            success: function(data){
                $('#'+x+'').closest('.note').removeClass('note-info note-warning note-danger');
                $('#'+x+'').closest('.note').addClass('note-'+newtype+'');
                $('#'+x+'').closest('.note').find('strong').text(newsubject);
                var d = curDate();
                var t = curTime(); 
                $('#'+x+'').next('sub').text(d+" - "+t+" Edited");
                $('#'+x+'').text(newcontent);
                $('#edit_callout').fadeOut();
                $('#edit-callout').trigger('click');
                // swal("Updated","Callout is updated.","success");
                swal({   title: "Updated",   text: "Callout has been updated.", type:"success", timer: 1000,   showConfirmButton: false }); 

            }
        });
    });
}
    function colorthing(type){
        switch(type){
            case("Info"):
                return "blue";
                break;
            case("Warning"):
                return "yellow-gold"
                break;
            case("Danger"):
                return "red";
                break;
        }
    }

    // for edit color type
    $(".edit-callout-type-o").click(function () {
        $x = $(this).closest('label').find('.label').html();
        switch($x){
            case("Info"):
                $('#edit-callout-type-label').removeClass('yellow-gold red');
                $('#edit-callout-heads-up').html("<i class='fa fa-bullhorn'></i> Info");
                $('#edit-callout-type-label').addClass('blue');
                break;
            case("Warning"):
                $('#edit-callout-type-label').removeClass('blue red');
                $('#edit-callout-heads-up').html("<i class='fa fa-bullhorn'></i> Warning");
                $('#edit-callout-type-label').addClass('yellow-gold');  
                break;
            case("Danger"):
                $('#edit-callout-type-label').removeClass('yellow-gold blue');
                $('#edit-callout-heads-up').html("<i class='fa fa-bullhorn'></i> Danger");
                $('#edit-callout-type-label').addClass('red');
                break;
        }
    });


// Tasks
    // change color
    $('#task-type').on('change',function(){
        var color = $('#task-type').find(":selected").val();
        $('#task-color').removeClass();
        $('#task-color').addClass('btn '+color+'');
    });

    // ajax saving tasks
    $("#form-add-task").submit(function(event){
        event.preventDefault();
        var title = $('#task-title').val();
        var color = $('#task-type').val();
        var content = $('#task-content').val();
        var school_id = $('#school_id').val();
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+'tasks/saveTask',
                        dataType: 'html',
                        data: {title:title, school_id:school_id, color:color,content:content},
                        async: false,
                        success: function(data){                
                            $('#tasksgohere').prepend(data);
                            $('#no-task-span').hide();
                            $('#add-task').trigger('click');
                            $('#add_task').fadeOut();
                            $(".taskcounthdr").text(parseInt($(".taskcounthdr").text()) + 1);

                            //reset model fields
                            $('#task-title').val('');
                            $('#task-heads-up').html("<i class='fa fa-tasks' /i>");
                            $('#task-content').val('');
                            $('#task-color').removeClass().addClass('btn blue');
                            $('#task-type').val('blue');
                            // swal("Added!", "Task is added.", "success");
                            swal({   title: "Success",   text: "Task added successfully", type:"success", timer: 1000,   showConfirmButton: false }); 
 
                            },
                        error: function(data){
                            // swal("Error","Something went wrong.","error");
                            swal({   title: "Opss...",   text: "Something went wrong.", type:"error", timer: 1000,   showConfirmButton: false }); 

                        }
                    }); 
        
        return false;
    });

    // complete task
    var sndtdltsks;
    $(document).on('click','.tskdone',function(){
        var x = $(this);
        var task_id = $(this).siblings('.tskid').val();
        swal({   title: "Done with this task?", 
        text: 'Task will be marked as done.',   
            type: "info",   
            showCancelButton: true,  
            confirmButtonText: "Done",   
            cancelButtonText: "Cancel",   
            closeOnConfirm: false,   
            closeOnCancel: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+'tasks/delTask',
                        data: {task_id:task_id},
                        success: function(data){
                            $(".taskcounthdr").text(parseInt($(".taskcounthdr").text()) - 1);
                            $(x).closest('.tskhldr').find('.actions').hide();
                            sndtdltsks = $(x).closest('.tskhldr').html();
                            $(x).closest('.tskhldr').fadeOut('slow').remove();
                            $('#deltskhldrid').prepend("<div class='col-md-4 deltskhldr'>"+sndtdltsks+"</div>");
                            swal("Success", 'You can view finished tasks under actions\n "View completed tasks"', "success"); 
                        }, error: function(data){
                            swal("Error", "Something went wrong.", "error");   
                        }
                    });                      
                } 
        });
    });

    // edit task
    var thisedittsk;
    $(document).on('click','.tskedt',function(){
        thisedittsk = $(this);
        var task_id = $(this).siblings('.tskid').val();        
        $.ajax({
            type: "POST",
            url: BASE_URL+'tasks/editTask',
            dataType: "json",
            data: {task_id:task_id},
            success: function(data){
                $('#edit-task-title').val(data[0].title);
                $('#edit-task-content').val(data[0].content);
                $('#edit-task-id').val(data[0].task_id);
                $('#edit-task-color').removeClass('blue');
                $('#edit-task-color').addClass(data[0].color);
                $('#edit-task-type').val(data[0].color);
                $('#edit-task').trigger('click');
            }
        });
    });

    // delete task
    $(document).on('click','.tskdel',function(){
        var x = $(this);
        var task_id = $(this).siblings('.tskid').val();
        swal({   title: "Permanently delete task?",    
            type: "warning",   
            showCancelButton: true,  
            confirmButtonText: "Delete Task",   
            cancelButtonText: "Cancel",   
            closeOnConfirm: false,   
            closeOnCancel: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+'tasks/delTaskPerma',
                        data: {task_id:task_id},
                        success: function(data){
                            $(x).closest('.deltskhldr').remove();
                            swal("Delete", "Your task has been deleted.", "success"); 
                        }, error: function(data){
                            swal("Error", "Something went wrong.", "error");   
                        }
                    });                      
                } 
        });
    });

    // save edited task
    $(document).on('click','#edit-save-task',function(event){
        event.preventDefault();
        var task_id = $('#edit-task-id').val();
        var color = $('#edit-task-type').val();
        var title = $('#edit-task-title').val();
        var content = $('#edit-task-content').val();
        var d = curDate();
        var t = curTime();    
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+'tasks/saveedittask',
                        data: {task_id:task_id,color:color,title:title,content:content},
                        success: function(data){
                            $('#edit_task').hide();
                            $('#edit-task').trigger('click');
                            $(thisedittsk).closest('.tskhldr').find('.tskdsplyttl').text(title);
                            $(thisedittsk).closest('.tskhldr').find('.tskdsplycnt').text(content);
                            $(thisedittsk).closest('.tskhldr').find('.tskdsplytime').text(d+" - "+t);
                            $(thisedittsk).closest('.tskhldr').find('.tskdsplyclr').attr('class','portlet box '+color+' tskdsplyclr');
                            //$(thisedittsk).closest('.tskhldr').find('.tskdsplyclr').removeClass();
                            swal("Edited", "Your task has been edited.", "success");   
                        }, error: function(data){
                            swal("Error","Something went wrong.","error");
                        }
                    });
    });

    // view deleted tasks
    $(document).on('click','#vwtskopt',function(){
        var x = $(this);
        var text = $(x).text();
        if(text == "View Completed Tasks"){
            $(x).html('<i class="fa fa-eye"></i>View Ongoing Tasks');
            $('#deltskhldrid').fadeIn('slow').show();
            $('#tskhldrid').fadeOut('slow').hide();
        } else if(text == "View Ongoing Tasks"){
            $(x).html('<i class="fa fa-eye"></i>View Completed Tasks');
            $('#deltskhldrid').fadeOut('slow').hide();
            $('#tskhldrid').fadeIn('slow').show();
        }
    });

// Record Grades Page
    function isperfectscoreset(){
        var value = $('#set-perfect-score-text').val();
        var text = $('#spsl').text();
        if(text == "Edit" && value != ""){
            return true;
        }else{
            $('#set-perfect-score-text').css('border-color','red');
            $('#perfect-score-error').css('visibility','visible');
            return false;
        }
    }

    function isactivitytitle(){
        var text = $('#activity-title-text').val();
        if(text != ""){
            return true;
        }else{
            $('#activity-title-text').css('border-color','red');
            $('#activity-title-error').css('visibility','visible');
            return false;
        }
    }

    function isrubricset(){
        var rubric = $('#rubric-type option:selected').text();
        if(rubric == " "){
            $('#rubric-type').css('border-color','red');
            $('#rubric-type-error').css('visibility','visible');
            return false;
        }else{
            return true;
        }
    }

    function isstudentscoresset(){
        var z = true;
        $('.student-score').each(function(index){
            if($('.student-score').eq(index).val() == ""){
                $('.student-score').eq(index).closest('tr').css('background-color','#FF5A50');
                z =  false;
            }
        });
        return z;
    }

    $('#set-perfect-score-btn').on('click',function(){
        var value = $('#set-perfect-score-text').val();
        var text = $('#spsl').text();
        if(text == "Set"){
            if(value != 0){
                $('.table-perfect-score').html(value);
                // $('#spsi').hide();
                $('#set-perfect-score-text').replaceWith($("<input type='text' id='set-perfect-score-text' class='form-control input-medium' readonly/>").val(value));
                $('#set-perfect-score-icon').removeClass('fa-check');
                $('#spsl').text('Edit');
                $('#set-perfect-score-icon').addClass('fa-pencil');
                return true;
            }else{
                $('#set-perfect-score-text').css('border-color','red');
                $('#perfect-score-error').css('visibility','visible');
                return false;
            }
        }else if(text == "Edit"){
            var value2 = $('#set-perfect-score-text').text();
            $('#spsl').text('Set');
            // $('#spsi').show();
            $('#set-perfect-score-text').replaceWith($('<input type="number" class="form-control input-medium" id="set-perfect-score-text" placeholder="Perfect Score" min="1" />').val(value));
        }
    });

    $('.student-score').focus(function(){
        $(this).closest('tr').css('background-color','');
    });

    $(document).on('click','#set-perfect-score-text',function(){
        $('#set-perfect-score-text').css('border-color','');
        $('#perfect-score-error').css('visibility','hidden');        
    });

    $(document).on('click','#activity-title-text',function(){
        $('#activity-title-text').css('border-color','');
        $('#activity-title-error').css('visibility','hidden');
    });

    $('#rubric-type').on('click',function(){
        $('#rubric-type').css('border-color','');
        $('#rubric-type-error').css('visibility', 'hidden');
    });

    $('.student-score').on('keyup',function(){
        var total = parseInt($('#set-perfect-score-text').val());
        var now = parseInt($(this).val());
        //console.log(now+"  "+total);
        if(now > total){
            $(this).val(total);
        }
        if(!total){
            $(this).val('');
        }
    });

    $('#record-grades').on('click', function(){        
        var x = isperfectscoreset();
        var y = isrubricset();
        var z = isstudentscoresset();  
        var a = isactivitytitle();              
        var rubrics_formula_id = $('#rubric-type').val();
        var total = $('#set-perfect-score-text').val();
        var course = getcourse();
        var group = getgroup();
        var grades = [];
        var activity = $('#activity-title-text').val();
        if(x == true && y == true && z == true && a == true){
            swal({   title: "Record grade?",     
                type: "warning",   
                showCancelButton: true,  
                confirmButtonText: "Record Grade",   
                cancelButtonText: "Cancel",   
                closeOnConfirm: false,   
                closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) {    
                        // Save history
                        $.ajax({
                            type: 'POST',
                            url: BASE_URL+'grades/recordhistory',
                            data: {formula_id:rubrics_formula_id,activity_title:activity,perfect_score:total,course:course,group:group},
                            success: function(data){
                                //console.log('History added!')
                                lastID();
                            }
                        });
                        // get id of last history
                        function lastID(){
                            $.ajax({    
                                type: "GET",
                                url: BASE_URL+'grades/lasthistory',             
                                dataType: "html",       
                                success: function(response){   
                                    //console.log("Last ID got");         
                                    useID(response);
                                }
                            });
                        }
                        // Get id of last history and insert with function below
                        function useID(y){
                            var lastid = y;
                            $('.student-score').each(function(index){
                                var class_member_id = $('.student_id').eq(index).text();
                                var score = $('.student-score').eq(index).val();    
                                grades.push({
                                    rubrics_formula_id: rubrics_formula_id,
                                    history_id: lastid,
                                    class_member_id: class_member_id,
                                    score: score,
                                    total: total
                                });
                            });  

                            $.ajax({
                            type: "POST",
                            url: BASE_URL+'grades/savegrades',
                            data: {grades:grades},
                            success: function(data){
                                    $('#set-perfect-score-text').replaceWith($('<input type="number" class="form-control input-medium" id="set-perfect-score-text" placeholder="Perfect Score" min="1" />'));
                                    $('.student-score').val('');
                                    $('#rubric-type').val(0);
                                    $('#set-perfect-score-text').val('');
                                    $('#activity-title-text').val('');
                                    $('.table-perfect-score').html('-');
                                    $('#spsl').text("Set");
                                    swal("Recorded!", "Grades were recorded.", "success");  
                                }
                            });  
                        }  
                         
                    } else {     
                        swal("Cancelled", "Grades were not recorded.", "error");   
                    } 
            });        
        }        
    });

// Update Grades page
//
    var to_change_grades = [];
    $('#update-grades').on('click',function(){
        $('.edit-grade').each(function(index){
            var grade_id = $('.edit-grade').eq(index).closest('tr').find('.grade_id').html();
            var new_grade = $('.edit-grade').eq(index).val();
            to_change_grades.push({
                grade_id: grade_id,
                score: new_grade
            });
        })
        for(var i = 0; i< to_change_grades.length; i++){
            //console.log(to_change_grades[i]['score']+ " "+to_change_grades[i]['grade_id']);
        }
        $.ajax({
            type: "POST",
            url: BASE_URL+'grades/updateAllGrades',
            data: {new_grades:to_change_grades},
            success: function(data){
                //content = "Grades updated.";
                //showNotification(content);
                swal('Success!','Grades have been updated','success');
                location.reload();
            }
        });
    });

    $(document).on('keyup','.edit-grade',function(){
        var total = +$('.ttlscore').first().text();
        var cur = +$(this).val();
        if(cur > total){
            $(this).val(total);
        }
    });

// Show password area
//
    $('#password-eye').on('hover',function(){
        var x = $(this).css('color');
        var hexString = convert2hex(x);
        if(hexString == "#000000"){
            $(this).attr('title','Hide password');
        }else if(hexString == "#808080"){
            $(this).attr('title','Show password');
        }
    });

    $('#password-eye').on('click', function(){
        var x = $(this).css('color');
        var hexString = convert2hex(x);
        if(hexString == "#000000"){
            $(this).css('color','grey');
            $('#password').attr('type', 'password');
        }else if(hexString == "#808080"){
            $(this).css('color','black');
            $('#password').attr('type', 'text');
        }
    });

// Grade Rubrics Page
//
if(last_url == "rubrics"){
    function refreshchart(){
        $('#chart-contain').fadeIn();
        var chart = AmCharts.makeChart("rubrics_chart", {
            "type": "pie",
            "theme": "light",

            "fontFamily": 'Open Sans',
                    
            "color":    '#888',
            "valueField": "percentage",
            "titleField": "schoolactivity",
            "exportConfig": {
                menuItems: [{
                icon: Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/export.png",
                format: 'png'
                }]
            }
        });  
        var piedata = [];
        $('.category-name').each(function(index){    
            var category = $('.category-name').eq(index).text();
            var percentage = $('.category-percentage').eq(index).text();
            piedata.push({
                schoolactivity: category,
                percentage: percentage
            });
        });
        chart.dataProvider = piedata;  
        chart.validateData();
        
        chart.write('rubrics-chart');
    };

    refreshchart();        
    $(document).on('hover','.set-type, .remove-type, .edit-type',function(){
       $(this).css('cursor','pointer'); 
    });

    // Editing
    var edit_rows = [];
    $(document).on('click','.set-type, .remove-type, .edit-type',function(){    
        $('#save-rubrics').removeAttr('disabled');     
        if($(this).hasClass('set-type')){
            var rsum = add_rubric_percentages();
            if(rsum > 100){
                swal("Oops...", "Total percentage must be less than 100", "error");
            }else{
                var category_name = $(this).closest('tr').find('.category-name').val();
                var category_percentage = $(this).closest('tr').find('.category-percentage').val();
                // FOR UPDATING
                if($(this).closest('tr').find('.formula-id').length){
                    var id = $(this).closest('tr').find('.formula-id').text();
                    var type = $(this).closest('tr').find('.category-name').val();
                    var percent = $(this).closest('tr').find('.category-percentage').val();
                    edit_rows.push({
                        formula_id: id,
                        type: type,
                        percent: percent
                    });                
                }  
                // IF FIRST TIME ADDING
                if(category_name == "" || category_percentage == ""){
                    $(this).closest('tr').css('background-color', '#FF5A50');
                }else{                
                    $(this).closest('tr').removeClass('unset-category');
                    $(this).closest('tr').addClass('set-category');
                    $(this).closest('tr').css('background-color','');
                    $(this).closest('tr').find('.category-name').replaceWith($("<span class='category-name' />").text(category_name));
                    $(this).closest('tr').find('.category-percentage').replaceWith($("<span class='category-percentage' />").text(category_percentage));
                    $(this).closest('tr').find('.edit-type').show();
                    $(this).hide();
                }
            }
        }//else if($(this).hasClass('remove-type')){
            // $(this).closest('tr').remove();
        //}
        else if($(this).hasClass('edit-type')){
            var category_name = $(this).closest('tr').find('.category-name').text();
            var category_percentage = $(this).closest('tr').find('.category-percentage').text();
            $(this).closest('tr').removeClass('set-category');
            $(this).closest('tr').addClass('unset-category');
            $(this).closest('tr').find('.category-name').replaceWith($("<input type='text' class='form-control input-small category-name' />").val(category_name));
            $(this).closest('tr').find('.category-percentage').replaceWith($("<input type='number' class='form-control input-small category-percentage' />").val(category_percentage));
            $(this).closest('tr').find('.set-type').show();
            $(this).hide();   
        }
    });

    // Adding new entry
    $('#add-category').on('click',function(e){
        e.preventDefault();
        $('#categories-tbody').append('<tr class="unset-category set-row">'+
                                        '<td style="width:164px;"><input type="text" class="form-control input-small category-name" placeholder="Type"></td>'+
                                        '<td style="width:257px;"><input type="number" class="form-control input-small category-percentage" placeholder="Enter percentage" max="100" min="1"></td>'+
                                        '<td style="width:30px;"><i class="fa fa-pencil edit-type" style="display:none;"></i><i class="fa fa-check set-type"></i><i class="fa fa-trash-o remove-type"></i></td>'+
                                        '</tr>');
        $('#save-rubrics').removeAttr('disabled');
    });

    $(document).on('keyup','.category-percentage',function(){
        var rsum = checkleft();
        var left = 100 - rsum;
        var now = +($(this).val());
        if(now > left){
            $(this).val(left);
        }
        if(!left){
            $(this).val('');
        }
    });

    function checkleft(){
        var sum = 0;
        $('.category-percentage').each(function() {
            sum += Number($(this).text());          
        });
        return sum;
    }

    // Saving the rubrics
    $('#save-rubrics').on('click',function(e){
        e.preventDefault();
        if($('.unset-category').length>0){
            $('.unset-category').css('background-color','#FF5A50');
        }else{
            swal({   
                title: "Are you sure?", 
                text: 'Class rubrics will be updated',   
                type: "warning",   
                showCancelButton: true,    
                confirmButtonText: "Update",   
                cancelButtonText: "Cancel",   
                closeOnConfirm: false,   
                closeOnCancel: false }, 
                function(isConfirm){   
                    if (isConfirm) {   
                        var class_id = $('#class_id').val();
                        var arr = [];
                        $('.set-row:not(:has(td>.formula-id))').each(function(index){
                            var type = $('.set-row:not(:has(td>.formula-id))').eq(index).find('.category-name').text();
                            var percent = $('.set-row:not(:has(td>.formula-id))').eq(index).find('.category-percentage').text();                  
                                arr.push({
                                    class_id: class_id,
                                    type: type,
                                    percent: percent
                                });
                        });

                        if(remove_rows.length > 0){
                            $.ajax({
                                type: "POST",
                                url: BASE_URL+'rubrics/deleterubrics',
                                data: {rev_row:remove_rows},
                                success: function(data){
                                    //console.log('Rows removed');
                                    //console.log(data);
                                }
                            });
                        }

                        if(edit_rows.length > 0){
                            $.ajax({
                                type: "POST",
                                url: BASE_URL+'rubrics/updaterubrics',
                                data: {edit_rows:edit_rows},
                                success: function(data){
                                    //console.log('Rows updated');
                                }
                            });
                        }

                        if(arr.length > 0){
                            $.ajax({
                                type: "POST",
                                url: BASE_URL+'rubrics/saverubrics',
                                data: {batch:arr},
                                success: function(data){
                                    $('.set-category').remove();
                                    $('#categories-tbody').append(data);
                                    //console.log('Rows added');
                                }
                            });
                        }
                        $('#chart-contain').css('display','none');
                        refreshchart();
                        // content = "Rubrics updated.";
                        // showNotification(content);
                        $('#save-rubrics').attr('disabled','disabled');  
                        // swal("Updated", "Grade rubrics has been updated.", "success"); 
                swal({   title: "Updated",   text: "Grade rubrics has been updated.\n Page will reload...", type:"success", timer: 1000,   showConfirmButton: false },
                    function (){
                        location.reload();
                    }
                    ); 
                            // setTimeout(function (){
                            //     location.reload();
                            // }, 5000);
                        add_rubric_percentages();
                    } else {     
                        // swal("Cancelled", "Grade rubrics has not been updated.", "error"); 
                swal({   title: "Cancelled",   text: "Grade rubrics has not been updated.\n Page will reload...", type:"error", timer: 1000,   showConfirmButton: false },
                    function (){
                                location.reload();
                    }
                    ); 

                         } 
            });
            
                
        }

    });  

    // Deleting Rubrics
    var remove_rows = [];
    var hasGrades = 0;
    $(document).on('click','.remove-type',function(){
        var thsrm = $(this);
        if($(this).closest('tr').find('.formula-id').length){   
            var formula_id = $(this).closest('tr').find('.formula-id').text();
            $.ajax({
                type: "POST",
                url: BASE_URL+'rubrics/checkIfRubricsHasGrades',
                data: {formula_id:formula_id},
                dataType: "html",
                success: function(data){
                    hasGrades = data;
                        //console.log(hasGrades);
                    if(hasGrades == 1){
                        swal({   title: "Are you sure?",   
                            text: 'This rubric contains recorded grades you will also delete the grades associated with it.',   
                            type: "warning",   
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",   
                            confirmButtonText: "Yes delete it",   
                            cancelButtonText: "Cancel",   
                            closeOnConfirm: false,   
                            closeOnCancel: true 
                            }, function(isConfirm){   
                                if (isConfirm) {     
                                    $(thsrm).closest('tr').remove();
                                    swal({   title: "Removed",   text: 'Please click "Save Rubrics" to update class rubric.', type:"success", timer: 3000,   showConfirmButton: false }); 
                                    // swal("Deleted", "Rubrics and grades will be deleted on Saving Rubrics.", "success");  
                                    remove_rows.push(formula_id); 
                                } 
                        }); 
                    } else {
                        swal({   title: "Are you sure?",   
                            text: 'This rubric will be removed from your class.',   
                            type: "warning",   
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",   
                            confirmButtonText: "Yes delete it",   
                            cancelButtonText: "Cancel",   
                            closeOnConfirm: false,   
                            closeOnCancel: true 
                            }, function(isConfirm){   
                                if (isConfirm) {     
                                     remove_rows.push(formula_id);
                                    $(thsrm).closest('tr').remove();
                                    swal({   title: "Removed",   text: 'Please click "Save Rubrics" to update class rubric.', type:"success", timer: 3000,   showConfirmButton: false }); 

                                } 
                        });

                    }
                }
            });  
        }else{
            $(thsrm).closest('tr').remove();
        }
        $('#save-rubrics').removeAttr('disabled');
        add_rubric_percentages();
    });

    // Showing total percentage
    $(document).keyup('.category-percentage',function(e){
        add_rubric_percentages();
    });

    function setcolor(sum){
        if(sum == 100 || sum < 100){
            $('#rubric-total').css('color','green');
        }
        else if(sum > 100){
            $('#rubric-total').css('color','red');
        }
    }
    function add_rubric_percentages(){
        var sum = 0;
        $('.category-percentage').each(function() {
            sum += Number($(this).val());
            sum += Number($(this).text());          
        });
        $('#rubric-total').text(sum);
        setcolor(sum);
        return sum;
    }
    add_rubric_percentages();


    $(document).on('click','.category-name, .category-percentage',function(){
        $(this).closest('tr').css('background-color','');
    }); 

    $(document).on('keypress','.category-percentage, .category-name',function(e){
        //console.log("enter");
        if (e.which == 13){
            $(this).closest('.unset-category').find('.set-type').click();
        }
    });  
}// END OF RUBRICS PAGE

// Students List
    $(document).on('click',function(event){
        var target = $(event.target);
        if(!target.is('a')){
            $('#gotogorrid').hide();
        }
    });

    $(".gotogorrrow").click(function(event) {
        var school_id = $(this).text();
        $('#gotogorrid').hide();
        $('#gotogorrid').css('left',event.pageX);      // <<< use pageX and pageY
        $('#gotogorrid').css('top',event.pageY+10);
        $('#profile-link').attr('href',BASE_URL+'profile/'+school_id);
        $('#grades-link').attr('href',url_1+'grades/'+school_id);
        $("#gotogorrid").show();
    });    

// Class Record
if(last_url == "classrecord"){    
    var actual_grade;
    var actual_grade_id;
    $(document).on('dblclick','.upgradecl',function(){
        hideallSwals();
        if($('#upgrade').is(':visible')){
            $('#upgrade').css('background-color','#FF5A50');
            swal({   
            title: "You have an unsaved grade.",   
            type: "warning",     
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Go back.",   
            closeOnConfirm: true});
        }else{
            actual_grade = $(this).text();
            actual_grade_id = $(this).attr('id');
            //actual_grade = grade;
            //actual_grade_id = grade_id;
            $(this).replaceWith($("<input type='number' style='border:2px solid #20a46d' class='form-control input-small' id='upgrade'/>").val(actual_grade));
        }
    });

    $(document).on('keyup','#upgrade',function(event){ 
        hideallSwals();           
        var cell = $(this);
        if (event.keyCode == 27){            
            $(cell).replaceWith($('<td id="'+actual_grade_id+'" class="upgradecl"/>').text(actual_grade));
            swal("Cancelled", "Grade has not been changed.", "error");
        }else if(event.keyCode == 13){  
            swal({   
            title: "Are you sure?",
            text: 'You are about to update a students\' grade.\n Click "Update" to continue.',  
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Update",   
            cancelButtonText: "Cancel",   
            closeOnConfirm: false,   
            closeOnCancel: false }, 
            function(isConfirm){   
                if (isConfirm) { 
                    var ngrade = $(cell).val();                    
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+'grades/updateSingleGrade',
                        data: {grade_id:actual_grade_id,score:ngrade},
                        success: function(data){
                            var new_cell = $('<td id="'+actual_grade_id+'" style="background-color:#26C281;)" class="upgradecl"/>').text(ngrade);
                            $(cell).replaceWith(new_cell);                            
                            $(new_cell).stop().animate({ backgroundColor: jQuery.Color("#26C281") }, {
                                duration: 3000,
                                complete: function() { $(new_cell).removeAttr('style') }
                            });
                            // swal("Updated!", "Grade has been updated.", "success");
                        swal({   title: "Updated",   text: 'Students\' grade has been updated.', type:"success", timer: 1000,   showConfirmButton: false },
                            function (){
                                location.reload();
                            }
                            ); 

                        }
                    });
                    //hideallSwals();
                } else {     
                    $(cell).replaceWith($('<td id="'+actual_grade_id+'" class="upgradecl"/>').text(actual_grade));
                    // swal("Cancelled", "Grade has not been changed.", "error"); 
                        swal({   title: "Cancelled",   text: 'Students\' grade has been changed.', type:"error", timer: 1000,   showConfirmButton: false }); 
                    //hideallSwals();
                }   
            });
        }
    });    

    $(document).on('click','#upgrade',function() {
        hideallSwals();
        //$(this).select();
        $(this).css('background-color','');
    }); 
}

// In Record History Page
    var actual_title;
    var actual_act_id;
    $(document).on('dblclick','.rhsat',function(){
        hideallSwals();
        if($('#rhuat').is(':visible')){
            $('#rhuat').css('background-color','#FF5A50');
            swal({ 
            title: "Oops...",  
            text: "You have an unsaved activity title, please save your changes first.",   
            type: "warning",     
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Go back.",   
            closeOnConfirm: true});
        }else{
            actual_title = $(this).text();
            actual_act_id = $(this).attr('id');
            $(this).replaceWith($("<input type='text' style='border:2px solid #20a46d' class='form-control input-small' id='rhuat'/>").val(actual_title));
        }
    });

    $(document).on('keyup','#rhuat',function(event){ 
        hideallSwals();           
        var cell = $(this);
        if (event.keyCode == 27){            
            $(cell).replaceWith($('<td id="'+actual_act_id+'" class="rhsat"/>').text(actual_title));
            swal("Cancelled", "Grade has not been changed.", "error");
        }else if(event.keyCode == 13){ 
                    var new_title = $(cell).val();                    
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+'rubrics/updateSingleRH',
                        data: {activity_title:new_title,grade_history_id:actual_act_id},
                        success: function(data){
                            var new_cell = $('<td id="'+actual_act_id+'" style="background-color:#26C281;)" class="rhsat"/>').text(new_title);
                            $(cell).replaceWith(new_cell);                            
                            $(new_cell).stop().animate({ backgroundColor: jQuery.Color("#26C281") }, {
                                duration: 3000,
                                complete: function() { $(new_cell).removeAttr('style') }
                            });
                            swal("Updated", "Activity title has been renamed.", "success"); 
                        }
                    });
        }
    });    

    $(document).on('click','#rhuat',function() {
        hideallSwals();
        //$(this).select();
        $(this).css('background-color','');
    }); 

    var actual_total;
    var actual_ts_id;
    $(document).on('dblclick','.rhts',function(){
        hideallSwals();
        if($('#rhtsc').is(':visible')){
            $('#rhtsc').css('background-color','#FF5A50');
            swal({ 
            title: "Oops...",  
            text: "You have an unsaved total score, please save your changes first.",   
            type: "warning",     
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Go back.",   
            closeOnConfirm: true});
        }else{
            actual_total = $(this).text();
            var before_ts_id = $(this).attr('id').split('-');
            actual_ts_id = before_ts_id[1];
            $(this).replaceWith($("<input type='text' style='border:2px solid #20a46d' class='form-control input-small' id='rhtsc'/>").val(actual_total));
        }

    });
    $(document).on('keyup','#rhtsc',function(event){ 
        hideallSwals();           
        var cell = $(this);
        if (event.keyCode == 27){            
            $(cell).replaceWith($('<td id="ts-'+actual_ts_id+'" class="rhts"/>').text(actual_total));
            swal("Cancelled", "Grade has not been changed.", "error");
        }else if(event.keyCode == 13){ 
                    var new_ts = $(cell).val();                    
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+'rubrics/updateSingleTS',
                        data: {perfect_score:new_ts,grade_history_id:actual_ts_id,actual_ts:actual_total},
                        success: function(data){
                            var new_cell = $('<td id="ts-'+actual_ts_id+'" style="background-color:#26C281;)" class="rhts"/>').text(new_ts);
                            $(cell).replaceWith(new_cell);                            
                            $(new_cell).stop().animate({ backgroundColor: jQuery.Color("#26C281") }, {
                                duration: 3000,
                                complete: function() { $(new_cell).removeAttr('style') }
                            });
                            swal("Updated", "Activity total score has been changed.", "success"); 
                        }
                    });
        }
    });

    $(document).on('click','#rhtsc',function() {
        hideallSwals();
        //$(this).select();
        $(this).css('background-color','');
    }); 

// Messages Page
    // sending a message NOT WORKING if with SWAL
    $('#sndmsg').submit(function(event){
        event.preventDefault();
        var recipient = $('#tagRecipients').val();
        var subject = $('#msgsubj').val();
        var content = $('iframe').contents().find('.wysihtml5-editor').html();
        //console.log(content);
        $.ajax({
            type: "POST",
            url: BASE_URL+"message/sendmessage",
            data: {messageRecipients:recipient,messageSubject:subject,messageContent:content},
            success: function(data){
                // swal("Sent","Your message has been sent.","success");
        swal({   title: "Sent",   text: "Your message has been sent.", type:"success", timer: 1000,   showConfirmButton: false },
            function(){
                setTimeout(function(){
                location.reload();
                }, 1000);
            }); 

            }
        });
    });

    //discarding a newly composed
    $(document).on('click','#msgdiscard',function(event){
        event.preventDefault();
        swal({   title: "Are you sure?",
            text: "You're about to discard your composed message",   
            type: "warning",   
            confirmButtonColor: "#DD6B55",   
            showCancelButton: true,
            confirmButtonText: "Discard",   
            cancelButtonText: "Cancel",   
            closeOnConfirm: false,   
            closeOnCancel: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $("#tagRecipients").select2("val", "");
                    $('#msgsubj').val('');
                    $('#msgcont').data("wysihtml5").editor.clear();
                    // swal("Drafted!", "Your message has been discarded.", "success");
                    swal({   title: "Discarded",   text: "Your message has been discarded.", type:"success", timer: 1000,   showConfirmButton: false }); 
                } 
        });
    });

    // opena message
    var dlfrthis;
    var dlopnmsg;
    $(document).on('click','.msgclosed',function(){
        var id = $(this).closest('.gradeX').attr('id');
        //alert(id);
        dlfrthis = $(this).closest('.gradeX');
        dlopnmsg = $(dlfrthis).clone();
        $('.msgbox').fadeOut('slow').hide();
        $(this).closest('tr').removeAttr("style");
        $.ajax({
            type: "POST",
            url: BASE_URL+'message/viewmessage',
            data: {msgmap_id:id},
            success: function(data){
                var x = parseInt($('.msgcounthdr').text());
                if(x > 0){
                    $(".msgcounthdr").text(parseInt($(".msgcounthdr").text()) -1);
                } else {
                    $(".msgcounthdr").text('0');
                }
                $('.tab-content').append(data);
                var active_tab = $('li.tabopttab.active').find('a').text().replace(/ /g,'');;
                var active_tab_trim = $.trim(active_tab);
                    if(active_tab_trim == "Trash"){
                        $('#trashcmsg, #sndRply, #replycnt').hide();
                        $('#permatrash').show();
                    }else if(active_tab_trim == "Sent"){
                        $('#sndRply, #replycnt').hide();
                    }
            }, error: function(){
                window.location.replace('404');
            }
        });        
    });

    // reply to message
    $(document).on('click','#sndRply',function(event){
        event.preventDefault();
        var subject = $('#rplysubject').text();
        var content = $('#replycnt').val();
        var recipient = $('#rplyrecipient').val();

        swal({   title: "Send reply?",
            text: "Your composed message will be sent as a reply.",    
            type: "warning",   
            showCancelButton: true,  
            confirmButtonText: "Send",   
            cancelButtonText: "Cancel",   
            closeOnConfirm: false,   
            closeOnCancel: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+"message/sendreply",
                        data: {replyRecip:recipient,replySubject:subject,replyContent:content},
                        success: function(data){
                            // $('#replycnt').val('');
                            $('#replycnt').data("wysihtml5").editor.clear();
                            // swal("Sent!","Your reply has been sent.","success");
swal({   title: "Sent",   text: "Your reply has been sent successfully", type:"success", timer: 1000,   showConfirmButton: false }); 

                        }
                    });
                } 
        });        
    });

    // checking a checkbox. Unlink opening message
    $(document).on('click','.checkboxes',function(){
        var here = $(this);
        //$(here).trigger('click');
        enableActionDropdownInMessage(here);
        if($('.checkboxes').closest('.checked').length == 0){ // see if there are any checked boxes is 0 then disable action
            removeCheckedInMessages();
            disableActionDropdownsInMessages();
        } 
    });

    // for group checkable
    $(document).on('click','.group-checkable',function(){
        $(this).closest('.msgbox').find('.msgboxadm').removeAttr('disabled'); 
    });

    // work around for group checkable. if no more checked boxes then disable dropdown
    $(document).on('mouseleave','.group-checkable',function(){
        if($('.checkboxes').closest('.checked').length == 0){ // see if there are any checked boxes is 0 then disable action
            disableActionDropdownsInMessages();
        }
    });

    // Closing a message
    $(document).on('click','#xmsgview',function() {
        $('#opnmsg').fadeOut('slow').remove();
        $('.msgbox').fadeIn("slow");
    });

    // close a message when clicking on new tab
    $(document).on('click','.tabopt',function(){
        $('#xmsgview').trigger('click');
        $('.tabopttab').removeClass('active');
        $(this).closest('li').addClass('active');
        $('.msgbox').fadeIn('slow');     
        $('#senddraft').remove();
        removeCheckedInMessages(); // also disables the action dropdown          
    });

    function removeCheckedInMessages(){
        $('.checkboxes').closest('span').removeClass('checked');  
        $('.group-checkable').closest('span').removeClass('checked');   
        disableActionDropdownsInMessages();     
    }

    function disableActionDropdownsInMessages(){
        $('.msgboxadm').attr('disabled','disabled'); 
    }

    function enableActionDropdownInMessage(here){
        $(here).closest('.msgbox').find('.msgboxadm').removeAttr('disabled');  
    }

    // for deleting messages (to trash only) using checkbox
    var frmmsgtodel;
    $(document).on('click','.delselmsgs',function(){
        var to_trash = [];        
        swal({   title: "Are you sure?",
            text: "You can stil view deleted messages in your trash.",    
            type: "warning",   
            confirmButtonColor: "#DD6B55",   
            showCancelButton: true,  
            confirmButtonText: "Delete",   
            cancelButtonText: "Cancel",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $('.checked:has(> .checkboxes)').each(function(index){
                        var msg2del_id = $('.checked:has(> .checkboxes)').eq(index).closest('tr').attr('id');
                        $('.checked:has(> .checkboxes)').eq(index).closest('tr').removeAttr('style');
                        $('.checked:has(> .checkboxes)').eq(index).closest('tr').find('.checkboxes').attr('disabled','disabled'); 
                        frmmsgtodel = $('.checked:has(> .checkboxes)').eq(index).closest('tr').clone();
                        $('#tblbdytrsh').prepend(frmmsgtodel);
                        to_trash.push(msg2del_id);
                        $('.checked:has(> .checkboxes)').eq(index).closest('tr').fadeOut('slow').hide();
                    });
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+"message/deletemessage",
                        data: {to_trash:to_trash},
                        success: function(data){
                            removeCheckedInMessages();
                            // swal("Deleted!", "Message(s) has been deleted.", "success");
                        swal({   title: "Deleted",   text: "Your message has been deleted.\n Page will reload...", type:"success", timer: 1000,   showConfirmButton: false },
                            function(){
                                setTimeout(function(){
                                    location.reload();
                                }, 1000);
                            }); 

                        }
                    });
                } else {     
                    removeCheckedInMessages();
                    // swal("Cancelled!", "Your message has not been deleted.", "error");
                swal({   title: "Cancelled",   text: "Your message has is safe.", type:"error", timer: 1000,   showConfirmButton: false }); 

                } 
        });
    });

    // for deleting currently opened message
    $(document).on('click','#trashcmsg',function(event){
        event.preventDefault();
        var revthis = $('#msgid').val();
        var msgid = [];
        msgid.push($('#msgid').val());
        //var cur_tab = $('')
        swal({   title: "Are you sure?",    
            text: "You can still view this message in trash.",
            type: "warning",   
            showCancelButton: true,  
            confirmButtonText: "Delete",   
            confirmButtonColor: "#DD6B55",   
            cancelButtonText: "Cancel",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+"message/deletemessage",
                        data: {to_trash:msgid},
                        success: function(data){
                            $('#opnmsg').fadeOut('slow').remove();
                            $('.msgbox').fadeIn('slow').show();
                            var newdlopnmsg = $(dlopnmsg).removeAttr('style');
                            $('#tblbdytrsh').prepend(newdlopnmsg);
                            $('#'+revthis).fadeOut('slow').remove();                            
                            // swal("Deleted!", "Your message has been deleted.", "success");
                            swal({   title: "Deleted",   text: "Your message has been deleted.", type:"success", timer: 1000,   showConfirmButton: false }); 
   
                        }
                    });
                } else {     
                    // swal("Cancelled!", "Your message has not been deleted.", "error");
                    swal({   title: "Cancelled",   text: "Your message has is safe.", type:"error", timer: 1000,   showConfirmButton: false }); 
   
                } 
        });
    });

    // for delete forever a trash message (opened message)
    $(document).on('click','#permatrash',function(){
        var msg_id = $('#msgid').val();
        swal({   title: "Are you sure?",   
            text: "You can not recover this message when deleted from trash.",   
            type: "warning",   
            confirmButtonColor: "#DD6B55",   
            showCancelButton: true,  
            confirmButtonText: "Delete",   
            cancelButtonText: "Cancel",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: BASE_URL+"message/deleteforeversingle",
                        data: {msg_id:msg_id},
                        success: function(data){
                            $('#opnmsg').remove();
                            $('.msgbox').show();
                            $(dlfrthis).remove(); 
                            // swal("Deleted!", "Your message has been deleted forever", "success");
                            swal({   title: "Deleted",   text: "Your message will not be recovered.", type:"success", timer: 1000,   showConfirmButton: false },
                                function(){
                                    setTimeout(function(){
                                        location.reload();
                                    }, 1000);
                                }); 

                        }
                    });                   
                } else {     
                    // swal("Cancelled", "May forever.", "error");
                swal({   title: "Cancelled",   text: "Your message is safe for now.", type:"error", timer: 1000,   showConfirmButton: false }); 

                } 
        });
    });

    // for drafting a composed message
    $(document).on('click','#drftcompose',function(event){
        event.preventDefault();
        var draft_recipients = $("#tagRecipients").val();
        var subject = $('#msgsubj').val();
        var content = $('#msgcont').val();
        if(draft_recipients == null){
            swal("At least put a recipient.");
        } else {
            swal({   title: "Are you sure?",
                text: "Your composed message will be saved as draft.",    
                type: "warning",   
                confirmButtonColor: "#DD6B55",   
                showCancelButton: true,  
                confirmButtonText: "Draft",   
                cancelButtonText: "Cancel",   
                closeOnConfirm: false,   
                closeOnCancel: true 
                }, function(isConfirm){   
                    if (isConfirm) {     
                        $.ajax({
                            type: "POST",
                            url: BASE_URL+"message/draftmessage",
                            data: {recipients:draft_recipients,subject:subject,content:content},
                            success: function(data){
                                // swal("Drafted!", "Your message has been drafted.", "success");
                            swal({   title: "Drafted",   text: "Your message has been drafted.\n Page will reload...", type:"success", timer: 1000,   showConfirmButton: false,},
                                function(){                                   
                                // location.reload();
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);  
                                }
                                ); 
                            }
                        });
                    } 
            });
        }
    });

    // opening a draft message to resend
    $(document).on('click','.rsnddrft',function(){
        var id = $(this).closest('.gradeX').attr('id');
        $('.msgbox').fadeOut('slow').hide();
        $(this).closest('tr').removeAttr("style");
        $.ajax({
            type: "POST",
            url: BASE_URL+'message/resenddraft',
            data: {msgmap_id:id},
            success: function(data){
                $('.tab-content').append(data);
                $('#draftmsgcont').trigger('click');
            }, error: function(){
                //window.location.replace('404');
            }
        });  
    });
    /*edited by paul jess - (document).submit kay mo affect sa document wide submit*/
    $(document).on('submit','#snddrft',function(){
        swal({   title: "Sent",   text: "Your draft has been sent.", type:"success", timer: 1000,   showConfirmButton: false },
            function(){
                setTimeout(function(){
                    location.reload();
                }, 1000);
            });
    });

    // cancel draft
    $(document).on('click','#dftcancel',function(event){
        event.preventDefault();
        $('#senddraft').remove();
        $('.msgbox').show();
    });


// WYSIWYG drafts
$(document).ready(function () {
   $('#draftmsgcont').live('click', function () {
     $('#draftmsgcont').wysihtml5();
     var wysihtml5Editor = $("#draftmsgcont").data("wysihtml5").editor;
     // console.log('wysihtml5Editor: '+wysihtml5Editor);
     // The following is important since wysihtml5 is initialized asynchronously
     wysihtml5Editor.observe("load", function() {
       wysihtml5Editor.composer.commands.exec("bold");
     });
   });
});
// WYSIWYG reply
$(document).ready(function () {
   $('#replycnt').live('click', function () {
     $('#replycnt').wysihtml5();
     var wysihtml5Editor = $("#replycnt").data("wysihtml5").editor;
     // console.log('wysihtml5Editor: '+wysihtml5Editor);
     // The following is important since wysihtml5 is initialized asynchronously
     wysihtml5Editor.observe("load", function() {
       wysihtml5Editor.composer.commands.exec("bold");
     });
   });
});

// notif via sms message error 
$(document).ready(function () {
   $('#notifviasms').live('click', function (event) {
    event.preventDefault();
    swal("Sorry", "This functionality is not yet available with the current version.", "error");
   });
});

// Timeline date paginator
    $( window ).load(function() {
        var c_date = $('.dp-today').attr('data-moment');
        var s_date = $('.dp-item').attr('data-moment');
        var paginationlength = $('.dp-item');
        $( ".dp-item" ).each(function( index ) {
         if ($( this ).attr('data-moment') > c_date) {
            $( this ).remove();
         }
        });
    });
    
    $(document).on('click','.dp-item',function(){
        var c_date = $('.dp-today').attr('data-moment');
        var s_date = $('.dp-item').attr('data-moment');
        var paginationlength = $('.dp-item');
        $( ".dp-item" ).each(function( index ) {
         if ($( this ).attr('data-moment') > c_date) {
            $( this ).remove();
         }
        });
    });
    
// For headers
    // count number of tasks      
    $.ajax({
        type: "POST",
        url: BASE_URL+"tasks/countTasks",
        dataType: "html", 
        success: function(data){
            $('.taskcounthdr').text(data);
        }
    });

    // count number of unread messages
    countunread();
    var t = 1;
    function countunread(){
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: BASE_URL+"message/countUnread",
                dataType: "html", 
                success: function(data){
                    countunread();
                    t = 30000;
                    $('#msgctr').html(data);
                }
            });
        }, t);
    }

    // count number of unread messages
    //     setInterval(function(){        
    // $.ajax({
    //     type: "POST",
    //     url: BASE_URL+"message/countUnread",
    //     dataType: "html", 
    //     success: function(data){
    //         $('.msgcounthdr').text(data);
    //     }
    // });
    // }, 30000);

// Class Preferences
    // saving
    $(document).on('click','#svclsprfrn',function(event){
        event.preventDefault();
        var class_id = $('#class_id').val();
        var color = $('#clsprftlcl').find(":selected").val();
        var icon = $('#clsprftlcn').find(":selected").val();
        var ltst_activity = $('#clsprfla').bootstrapSwitch('state');
        var cllts = $('#clsprfcts').bootstrapSwitch('state');
        var chtbx = $('#clsprfctbx').bootstrapSwitch('state');
        var la_val = (ltst_activity  == true) ? '1' : '0';
        var cl_val = (cllts  == true) ? '1' : '0';
        var ch_val = (chtbx  == true) ? '1' : '0';
        //console.log(class_id+"|"+color+"|"+icon+"|"+la_val+"|"+cl_val+"|"+ch_val);
        $.ajax({
            type: "POST",
            url: BASE_URL+"classes/saveclasspref",
            data: {class_id:class_id,tile_color:color,tile_icon:icon,enable_latest_activity:la_val,enable_callouts:cl_val,enable_chatbox:ch_val},
            success: function(data){
                swal("Saved", "Class preferences has been updated.", "success");
            }
        }); 
    });

    // changing color
    $(document).on('change','#clsprftlcl',function(){
        var x = $(this).find(':selected').val();
        $('#tile-color').removeClass();
        $('#tile-color').addClass('btn '+x);
    });

// Peronsal Notification only (TO BE REMOVED AND REPLACED BY SWAL)
    function showNotification(content){
        var x = $('<div class="toast toast-success">'+
            '<div class="toast-title"><span id="notif-title">Success!</span></div>'+
            '<div class="toast-message"><span id="notif-content">'+content+'</span></div>'+
        '</div>'
            ).hide().prependTo("#toast-container").fadeIn("slow");
        setTimeout(function() { $(x).fadeOut('slow'); }, 5000);
    } 

    function hideallSwals(){
        $('.sweet-overlay').remove();
        $('.sweet-alert').remove();
    }

// SAMPLE SWAL PLEASE DO NOT DELETE
    function sampleSwal(){
        swal({   title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,  
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {     
                    swal("Deleted!", "Your imaginary file has been deleted.", "success");   
                } else {     
                    swal("Cancelled", "Your imaginary file is safe :)", "error");   
                } 
        });
    }

});
