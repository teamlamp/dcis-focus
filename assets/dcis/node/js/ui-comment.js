(function (){
	var getNode = function (s){
		return document.querySelector(s);
	},
	class_id = getNode('#class_id'),
	postName = getNode('.post-name'),
	username = getNode('#username'),

	defaultStatus = status.textContent,
	setStatus = function(s){
		status.textContent = s;

		if (s !== defaultStatus) {
			var delay = setTimeout(function(){
				setStatus(defaultStatus);
				stat.style.color = "#ccc";
				clearInterval(delay);
			}, 2000);
		}
	},

	formattedTime = function(date){
			// Create a new JavaScript Date object based on the timestamp
		date = new Date(date),
		// Hours part from the timestamp
		months = ['January','February','March','April','May','June','July','August','September','October','November','December'],
		year = date.getFullYear(),
		month = months[date.getMonth()],
		day = date.getDate(),
		hours = date.getHours(),
		minutes = "0" + date.getMinutes(),
		seconds = "0" + date.getSeconds();
	var ampm = 'am';
		if (hours > 12) {
		hours -= 12;
		ampm = 'pm';
		} else if (hours === 0) {
		hours = 12;
		}
		// Will display time in MM. DD, YY 10:30:23 format
		var formattedTime = month + ' ' + day + ', ' + year + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) +ampm;
		return formattedTime;
},

commentCounter = function (discussion_id){
	socket.emit('setCommentCount',{
	discussion_id: discussion_id
	});

		socket.on('getCommentCount',function(data){
		if (data.length) {
		for (var x = 0; x < data.length; x++) {
        var oldcounter = document.getElementById('commentCount_'+data[x].discussion_id);

			if (oldcounter != null) {
			var counter = document.createElement('span'),
				counterParent = oldcounter.parentNode;

			counter.setAttribute('id', 'commentCount_'+data[x].discussion_id);
			if(data[x].count == 1){
			counter.textContent = ' '+data[x].count+' comment';
			}else{
			counter.textContent = ' '+data[x].count+' comments';
			}
            counterParent.replaceChild(counter, oldcounter);
		 }
		}
		}
	});

};
	var lastdid = 0, sessionlastid = 0;

	try{
        var socket = io.connect('http://127.0.0.1:8080');
	}catch(e){
		//Set status to warn user
	}
	if (socket !==  undefined) {

		//listen for comment notification
		socket.on('notify_comment',function(data){
		if (data.length) {
		for (var x = 0; x < data.length; x++) {
		if(data[x].class_id == class_id.value || session_school_id != data[x].school_id){
        playSound();
		toastr.options = {
            "closeButton": true,
            "newestOnTop": false,
            "positionClass": "toast-bottom-right",
        };
        // toastr.options.onclick = window.location.href='http://www.google.com/';
        toastr.info('Commented "'+data[x].content+'" on a post in this class', data[x].username);
         }
	     }
	 }
	});
		//listen for comment_output
		socket.on('output_comment',function(data){

			if (data.length) {
				for (var x = data.length-1; x >= 0; x--) {

					var	panel_body_media = document.createElement('div'),
						media_list = document.createElement('ul'),
						media = document.createElement('li'),
						image = document.createElement('a'),
						media_body = document.createElement('div'),
						comment_show = document.createElement('div'),
						comment_head = document.createElement('div'),
						comment_text = document.createElement('div'),
						comment_school_id = document.createElement('input'),
						comment_feed_id = document.createElement('input'),
						edited =  document.createElement('sup');


					media_list.setAttribute('class', 'media-list');
					media.setAttribute('class', 'media');
					media_body.setAttribute('class', 'media-body todo-comment');
					comment_show.setAttribute('id','comment_show_'+data[x].discussion_id);
					comment_show.setAttribute('class','panel-collapse collapse');
					comment_show.setAttribute('aria-expanded','false');
					comment_show.setAttribute('style','height:0px');
					comment_head.setAttribute('class', 'todo-comment-head');
					comment_school_id.setAttribute('type', 'hidden');
					comment_school_id.setAttribute('class', 'csid');
					comment_school_id.setAttribute('value', data[x].school_id);
					comment_feed_id.setAttribute('type', 'hidden');
					comment_feed_id.setAttribute('class', 'cfid');
					comment_feed_id.setAttribute('value', data[x].comment_id);
					// comment_text.setAttribute('class', 'todo-text-color');

					edited.setAttribute('id', 'edited_'+data[x].comment_id);

					media.insertAdjacentHTML('beforeend','<a class="pull-left" style="padding-right: 0px;" href="javascript:;"><img class="dist item-pic circle" src='+data[x].photo+' style="width: 30px !important; height: 30px !important"></a>');			
					comment_head.insertAdjacentHTML('beforeend','<div class=" col-md-6 blog-tag-data-inner"><a class="item-name primary-link" href="/profile/'+data[x].school_id+'">'+data[x].username+'</a></div>');
					comment_text.insertAdjacentHTML('beforeend','<span id="comment_'+data[x].comment_id+'" class="comment" style="padding-top:25px;padding-left:15px">'+data[x].content+'<span>');
					
					if (session_school_id == data[x].school_id) {
					media_body.insertAdjacentHTML('beforeend','<div class="col-md-6 item-details" style="color:#78cff8;"><ul class="list-inline"><li id="commentDate_'+data[x].comment_id+'"><i class="fa fa-calendar"></i><a href="javascript:;"> '+formattedTime(data[x].modified_at)+' </a></li><li><i class="fa fa-pencil commentEdit" title="Edit"></i><i class="fa fa-trash commentDelete" title="Delete"></i></li></ul></div>');

					}else{
					media_body.insertAdjacentHTML('beforeend','<div class="col-md-6 item-details" style="color:#78cff8;"><ul class="list-inline"><li id="commentDate_'+data[x].comment_id+'"><i class="fa fa-calendar"></i><a href="javascript:;"> '+formattedTime(data[x].modified_at)+' </a></li></ul></div>');

					}
					
					if (data[x].edited == 1) {
						edited.textContent = ' Edited';
					}

					//Append
					if (document.getElementById('post-comment'+data[x].discussion_id) != null) {
					document.getElementById('post-comment'+data[x].discussion_id).appendChild(panel_body_media);
					document.getElementById('post-comment'+data[x].discussion_id).insertBefore(panel_body_media, document.getElementById('post-comment'+data[x].discussion_id).childNodes[x-data.length]);
					}

					commentCounter(data[x].discussion_id);

					// comment_show.appendChild(panel_body_media);
					panel_body_media.appendChild(media_list);
					media_list.appendChild(media);

					media.appendChild(media_body);
					media_body.appendChild(comment_head);
					media_body.appendChild(comment_text);
					media_body.appendChild(comment_school_id);
					media_body.appendChild(comment_feed_id);
					media_body.insertBefore(comment_head, media_body.firstChild);
					comment_text.appendChild(edited);

				if (!data[x].comment_id) {
						lastdid = +lastdid + 1;
						sessionlastid = lastdid;
						console.log('comment if on loop discussion_id value'+sessionlastid);

					}else if (data[0].comment_id){
						lastdid = data[0].comment_id;
						sessionlastid = lastdid;
						console.log('comment else if on loop discussion_id value'+sessionlastid);

					}
				
				}
			}
		});

		//listen for status
		socket.on('comment_status', function(data){
			setStatus((typeof data === 'object') ? data.message : data);
			if (data.clear === true) {
				$('textarea').filter('[id*=comment_textarea]').val('');
			}
		});


		$(document).on('click','.submit_comment',function(event){
		    var content = $(this).closest('.panel-default').find('#comment_textarea').val(),
		        school_id = postName.value,
		        comment_id = +sessionlastid + 1,
		        photo = session_userphoto,
		        discussion_id = $(this).closest('.item').find('.pid').val();
		        class_id = document.querySelector('#class_id').value;
		        username = document.querySelector('#username').value;
				
				console.log('comment clicked discussion_id value '+discussion_id);
		        
		       if (event.which === 1) {
		        socket.emit('input_comment',{
		        	comment_id: comment_id,
		            class_id: class_id,
		            school_id: school_id,
		            username: username,
		            discussion_id: discussion_id,
		            content: content,
		            photo: photo,
		            modified_at: Math.floor(Date.now())
		        });
			event.preventDefault();
		    	}
		});
	}
})();