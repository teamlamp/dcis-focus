var mysql = require("mysql");
client = require('socket.io').listen(8080).sockets;

// First you need to create a connection to the db
con = mysql.createConnection({
  host: "127.0.0.1",
  user: "josephlaotzu",
  password: "konib",
  database: 'dcis_db'
});


con.connect(function(err){
  if(err){
    throw err;
    console.log('Error connecting to Db');
    return;
  }
  console.log('Connection established');

  client.on('connection', function(socket){
    
    // ---------------------------------- start of post server -------------------------------------

      var sendStatus = function(s){
       socket.emit('status', s)   
      };

    // Emit all messages
     //Wait for post_input
    socket.on('setClassInfo', function(data){
    var class_id = data.class_id;
      var sql    = 'SELECT * FROM `discussion` LEFT JOIN user ON discussion.school_id = user.school_id WHERE class_id = '+class_id+' AND discussion.active = "1" ORDER BY discussion_id DESC'; 
      con.query(sql, function(err, results) {
        if (err) throw err;
        socket.emit('post_output', results);
      });

    });

    //Wait for post_input
    socket.on('post_input', function(data){
    var id,
        username = data.username,
        class_id = data.class_id,
        school_id = data.school_id,
        message = data.discussion_content,
        created_at = data.created_at,
        whitespacePattern = /^\s*$/;

       if (whitespacePattern.test(message)) {
          sendStatus('Message is required.');
       }else{        
          var post  = {discussion_id: id, class_id: class_id, school_id: school_id, username: username, discussion_content: message};

          con.query("INSERT INTO discussion SET ? ", post, function(err, result) {
            if (err) throw err;
            //post_output Latest message to all clients
            client.emit('post_output', [data]);
            client.emit('post_notify', [data]);
           sendStatus({ message: "New discussion posted.", clear: true });
          });
       }

    });


    // ------------------------------- end of post server -----------------------------------

    // ------------------------------ start of chat server------------------------------------


      // var sendChat_status = function(s){
      //  socket.emit('chatstatus', s)  
      // };

    // Emit all messages
    socket.on('setClassInfo', function(data){
    var class_id = data.class_id;
    var sql1    = 'SELECT * FROM `class_chatbox` LEFT JOIN user ON class_chatbox.school_id = user.school_id WHERE class_id = '+class_id+' AND class_chatbox.active = "1" ORDER BY convo_id DESC LIMIT 150'; 
    con.query(sql1, function(err, results) {
      if (err) throw err;
      // console.log(results);
      socket.emit('output_chat', results);
    });
    });

    //Wait for input_chat
    socket.on('input_chat', function(data){
    var id,
        class_id = data.class_id,
        school_id = data.school_id,
        username = data.username,
        message = data.content,
        whitespacePattern = /^\s*$/;

       if (whitespacePattern.test(message)) {
          socket.emit('chatstatus',{message: "Message is required.", clear: true});
       }else{        
          var post  = {convo_id: id, class_id: class_id, school_id: school_id, username: username ,content: message};
          con.query('INSERT INTO `class_chatbox` SET ?', post, function(err, result) {
            if (err) throw err;

            //outout_chat Latest message to all clients
            client.emit('output_chat', [data]);
            socket.emit('chatstatus',{message: "Message sent!", clear: true});
           // sendChat_status({message: "Message sent!", clear: true});

          });
       }

    });

// --------------------------------------- end of chat server -----------------------------------

// --------------------------------------- Start of post edit server -----------------------------------

		socket.on('setEditedPost', function(data){
			var discussion_id = data.discussion_id,
				discussion_content = data.discussion_content;
        var post = {discussion_content: discussion_content, edited : 1};
		    var sql1    = 'UPDATE discussion SET ? WHERE discussion_id ='+discussion_id+''; 
		    con.query(sql1, post,function(err, results) {
		      if (err) throw err;
		      client.emit('getEditedPost', [data]);
		    });
		});

		socket.on('setDeletedPost', function(data){
			var discussion_id = data.discussion_id,
          school_id = data.school_id,
          username = data.username,
          sure = data.sure;
      if (sure == true) {
		    var sql1    = 'UPDATE discussion, comment SET discussion.active = "0", comment.active = CASE WHEN comment.discussion_id = "'+discussion_id+'" THEN "0" WHEN comment.discussion_id != "'+discussion_id+'" THEN "1" END WHERE discussion.discussion_id = "'+discussion_id+'"'; 
		    con.query(sql1, function(err, results) {
		      if (err) throw err;
		      client.emit('postDeleted', {
		      	success: true,
            school_id: school_id,
            username: username,
            discussion_id: discussion_id
		      });
		    });
      }
		});
// -------------------------------------- end of post edit server -----------------------------------


    // ------------------------------ start of comment server------------------------------------

    socket.on('setCommentCount',function(data){
    var discussion_id = data.discussion_id;
      var sql    = 'SELECT discussion_id, COUNT(discussion_id) AS count FROM `comment` WHERE discussion_id = '+discussion_id+' AND active ="1"'; 
      con.query(sql, function(err, count) {
        if (err) throw err;
        client.emit('getCommentCount', count);
      });

    });
      var sendChat_status = function(s){
       socket.emit('comment_status', s)   
      };

    // Emit all messages
    socket.on('setClassInfo', function(data){
    var class_id = data.class_id;
      var sql    = 'SELECT * FROM `comment`  LEFT JOIN user ON comment.school_id = user.school_id WHERE class_id = '+class_id+' AND comment.active = "1" ORDER BY comment_id DESC'; 
      con.query(sql, function(err, comments) {
        if (err) throw err;
        socket.emit('output_comment', comments);
      });

    });

    socket.on('input_comment', function(data){
    var id,
        class_id = data.class_id,
        school_id = data.school_id,
        username = data.username,
        discussion_id = data.discussion_id,
        content = data.content,
        whitespacePattern = /^\s*$/;

       if (whitespacePattern.test(content)) {
          sendChat_status('Message is required.');
       }else{        
          var post  = {comment_id: id, discussion_id: discussion_id ,class_id: class_id, school_id: school_id, username: username ,content: content};
          con.query('INSERT INTO `comment` SET ?', post, function(err, result) {
            if (err) throw err;
            //outout_chat Latest message to all clients
            client.emit('output_comment', [data]);
            client.emit('notify_comment', [data]);
           sendChat_status({message: "Message sent!", clear: true});
          });
       }

    });

// --------------------------------------- end of comment server -----------------------------------

// --------------------------------------- Start of comment edit server -----------------------------------

    socket.on('setEditedcomment', function(data){
      var comment_id = data.comment_id,
        content = data.content;
        var edited =  {content: content, edited: 1};
        var sql1    = 'UPDATE comment SET ? WHERE comment_id ='+comment_id+''; 
        con.query(sql1, edited,function(err, results) {
          if (err) throw err;
          client.emit('getEditedcomment', [data]);
        });
    });

    socket.on('setDeletedcomment', function(data){
      var comment_id = data.comment_id,
          school_id = data.school_id,
          username = data.username,
          sure = data.sure;
      if (sure == true) {
        var sql1    = 'UPDATE comment SET active = "0" WHERE comment_id = '+comment_id+''; 
        con.query(sql1, function(err, results) {
          if (err) throw err;
          client.emit('commentDeleted', {
            success: true,
            school_id: school_id,
            username: username,
            comment_id: comment_id
          });
        });
      }
    });
// -------------------------------------- end of comment edit server -----------------------------------


// END OF EVERYTHING
  });

});

