// ----------------- Start of edit comment ---------------------
  var  formattedTime = function(date){
            // Create a new JavaScript Date object based on the timestamp
        date = new Date(date),
        // Hours part from the timestamp
        months = ['January','February','March','April','May','June','July','August','September','October','November','December'],
        year = date.getFullYear(),
        month = months[date.getMonth()],
        day = date.getDate(),
        hours = date.getHours(),
        minutes = "0" + date.getMinutes(),
        seconds = "0" + date.getSeconds();
    var ampm = 'am';

        if (hours > 12) {
        hours -= 12;
        ampm = 'pm';
        } else if (hours === 0) {
        hours = 12;
        }
        // Will display time in MM. DD, YY 10:30:23 format
        var formattedTime = month + ' ' + day + ', ' + year + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) +ampm;
        return formattedTime;
};

 var  escapeHTML = function( string )
{
    var pre = document.createElement('pre');
    var text = document.createTextNode( string );
    pre.appendChild(text);
    return pre.innerHTML;
}

$(document).on('click','.commentEdit',function(){
    var text = $(this).closest('.todo-comment').find('.comment').text(),
        comment_id = $(this).closest('.todo-comment').find('.comment').attr('id'),
        comment = $(this).closest('.todo-comment').find('.comment'),
        school_id = $(this).closest('.todo-comment').find('.csid').val(),
        editButton = $(this).closest('.todo-comment').find('.commentEdit');

        if (session_school_id == school_id) {            
        comment.replaceWith("<textarea id='"+comment_id+"' class='editedCommentText form-control margin-top-5 margin-bottom-5'>"+text+"</textarea>");
        editButton.replaceWith("<i class='fa fa-check saveCommentEdit' title='Save'></i><i class='fa fa-times cancelCommentEdit' title='Cancel'></i>");
        }
});

$(document).on('click','.cancelCommentEdit',function(){
    var text = $(this).closest('.todo-comment').find('.editedCommentText').text(),
        comment = $(this).closest('.todo-comment').find('.editedCommentText'),
        editingstat = $(this).closest('.todo-comment').find('.editing'),
        saveButton = $(this).closest('.todo-comment').find('.saveCommentEdit'),
        cancelButton = $(this).closest('.todo-comment').find('.cancelCommentEdit'),
        forfeed_id = $(this).closest('.todo-comment').find('.cfid').val(),
        feed_id = 'comment_'+forfeed_id;

        comment.replaceWith('<span id='+feed_id+' class="comment" style="padding-top:25px;padding-left:15px">'+text+'<span></span></span>');
        saveButton.replaceWith("<i class='fa fa-pencil commentEdit' title='Edit'></i>");
        cancelButton.remove();
        editingstat.remove();


});



(function(){

var commentCounter = function (discussion_id){
    socket.emit('setCommentCount',{
    discussion_id: discussion_id
    });

        socket.on('getCommentCount',function(data){
        if (data.length) {
        for (var x = 0; x < data.length; x++) {
        var oldcounter = document.getElementById('commentCount_'+data[x].discussion_id);

            if (oldcounter != null) {
            var counter = document.createElement('span'),
                counterParent = oldcounter.parentNode;

            counter.setAttribute('id', 'commentCount_'+data[x].discussion_id);
            if(data[x].count == 1){
            counter.textContent = ' '+data[x].count+' comment';
            }else{
            counter.textContent = ' '+data[x].count+' comments';
            }
            counterParent.replaceChild(counter, oldcounter);
         }
        }
        }
    });

};
    try{
        var socket = io.connect('http://127.0.0.1:8080');
    }catch(e){
        //Set status to warn user
    }
    var username = document.querySelector('#username').value;
    var newtext, comment, school_id, discussion_id, saveButton ,cancelButton, newDate;
    var lastdid = 0, sessionlastid = 0;

$(document).on('click','.saveCommentEdit',function(){
        newtext = $(this).closest('.todo-comment').find('.editedCommentText').val();
        comment_id = $(this).closest('.todo-comment').find('.cfid').val(),
        school_id = $(this).closest('.todo-comment').find('.csid').val();
        discussion_id = $(this).closest('.item').find('.pid').val();
        saveButton = $(this).closest('.todo-comment').find('.saveCommentEdit');
        cancelButton = $(this).closest('.todo-comment').find('.cancelCommentEdit');
        newDate = $(this).closest('.todo-comment').find('.commentDate');    

        socket.emit('setEditedcomment',{
            comment_id: comment_id,
            class_id: class_id.value,
            school_id: school_id,
            username: username,
            discussion_id: discussion_id,
            content: newtext,
            modified_at: formattedTime(Math.floor(Date.now())),

        });
});

socket.on('getEditedcomment', function(data){
    if (data.length) {
        for (var x = 0; x < data.length; x++) {

            comment = document.getElementById('comment_'+data[x].comment_id);
            date = document.getElementById('commentDate_'+data[x].comment_id+'');
            edited =  document.getElementById('edited_'+data[x].comment_id);   

   if (data[x].class_id == class_id.value) {
            if (comment.tagName == "SPAN") {
                var newcomment = document.createElement('span'),
                    commentParent = comment.parentNode,
                    newdate = document.createElement('li'),
                    dateParent = date.parentNode,
                    newedited = document.createElement('sup'),
                    editedParent = edited.parentNode;

                newcomment.setAttribute('class','comment');
                newcomment.setAttribute('id', 'comment_'+data[x].comment_id);
                newcomment.setAttribute('style','padding-top:25px;padding-left:15px');
                newcomment.textContent = data[x].content;

                newedited.textContent = ' Edited';

                newdate.setAttribute('id','commentDate_'+data[x].comment_id+'');
                newdate.insertAdjacentHTML('beforeend','<i class="fa fa-calendar"></i><a href="javascript:;"> '+data[x].modified_at+' </a>');
                
                newedited.setAttribute('id','edited_'+data[x].comment_id);

                commentParent.replaceChild(newcomment, comment);
                dateParent.replaceChild(newdate, date);
                editedParent.replaceChild(newedited, edited);

                
                playSound();
                toastr.options = {
                    "closeButton": true,
                    "newestOnTop": false,
                    "positionClass": "toast-bottom-right",
                };
                toastr.info('Edited a comment in this class', data[x].username);

                
            }else{

            // swal('Aweeeee','comment has been edited.','success');
            swal({   title: "Success",   text: "Your comment has been edited", type:"success", timer: 1000,   showConfirmButton: false }); 


                var newcomment = document.createElement('span'),
                    commentParent = comment.parentNode,
                    newdate = document.createElement('li'),
                    dateParent = date.parentNode,
                    newedited = document.createElement('sup'),
                    editedParent = edited.parentNode;

                newcomment.setAttribute('class','comment');
                newcomment.setAttribute('style','padding-top:25px;padding-left:15px');
                newcomment.setAttribute('id', 'comment_'+data[x].comment_id);
                newcomment.textContent = data[x].content;

                newedited.textContent = ' Edited';

                newdate.setAttribute('id','commentDate_'+data[x].comment_id+'');
                newdate.insertAdjacentHTML('beforeend','<i class="fa fa-calendar"></i><a href="javascript:;"> '+data[x].modified_at+' </a>');
                
                newedited.setAttribute('id','edited_'+data[x].comment_id);

                commentParent.replaceChild(newcomment, comment);
                dateParent.replaceChild(newdate, date);
                editedParent.replaceChild(newedited, edited);

            saveButton.replaceWith("<i class='fa fa-pencil commentEdit' title='Edit'></i>");
            cancelButton.remove();
            }
        }

    }
}
});

$(document).on('click','.commentDelete',function(){
        comment_id = $(this).closest('.todo-comment').find('.cfid').val();
        school_id = $(this).closest('.todo-comment').find('.csid').val();
        discussion_id = $(this).closest('.item').find('.pid').val();

        var deletecomment = function (sure){
            socket.emit('setDeletedcomment',{
            comment_id: comment_id, username: username, school_id: school_id, sure: sure
            });
        };
        if (session_school_id == school_id) {

        swal({  title: "Are you sure?",   
                text: "You will not be able to recover this comment!",   
                type: "warning",   showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Delete Comment",   
                closeOnConfirm: false,
                closeOnCancel: false
            }, 
                function(isConfirm){  
                if (isConfirm == true) {  
                        deletecomment(true);
                        commentCounter(discussion_id);
                        // swal("Deleted!", "Your comment has been deleted.", "success"); 
            swal({   title: "Success",   text: "Your comment has been deleted.", type:"success", timer: 1000,   showConfirmButton: false }); 

                    } else if (isConfirm == false){
                        deletecomment(false);
                        // swal("Cancelled!", "Your comment is safe.", "error");
            swal({   title: "Cancelled",   text: "Your comment is safe", type:"error", timer: 1000,   showConfirmButton: false }); 

                    }
                });

        }

});

socket.on('commentDeleted', function(data){
    if (data.success == true) {
        // console.log(data.discussion_id);
            comment = document.getElementById('comment_'+data.comment_id);
            console.log(comment);
            commentParent = comment.parentNode;
            commentParentx2 = commentParent.parentNode;
            commentParentx3 = commentParentx2.parentNode;
            commentParentx4 = commentParentx3.parentNode;
            commentParentx5 = commentParentx4.parentNode;

            commentParentx5.remove();
        if (session_school_id != data.school_id) {
            playSound();
                toastr.options = {
                    "closeButton": true,
                    "newestOnTop": false,
                    "positionClass": "toast-bottom-right",
                };
                toastr.info('Removed a comment in this class', data.username);
        }
    }      
});


})();


// ----------------End of edit comment -------------------------