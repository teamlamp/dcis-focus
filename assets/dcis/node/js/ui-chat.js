(function (){
	var getNode = function (s){
		return document.querySelector(s);
	},

	//Get required nodes
	Chat_status = getNode('.chat-status span'),
	chat_contents = getNode('.chat-messages'),
	textarea = getNode('#chat-content'),
	chatName = getNode('.chat-name'),
	submit = getNode('#submit_chat'),
	stat = getNode('#stat');

	if (Chat_status == null) { console.log("Chatbox is disabled");}else{

	var defaultChat_status = Chat_status.textContent,

	setChat_status = function(s){
		Chat_status.textContent = s;

		if (s !== defaultChat_status) {
			var delay = setTimeout(function(){
				setChat_status(defaultChat_status);
				stat.style.color = "#ccc";
				clearInterval(delay);
			}, 2000);
		}
	};

		formattTime = function(date){
			// Create a new JavaScript Date object based on the timestamp
		date = new Date(date),
		// Hours part from the timestamp
		months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
		year = date.getFullYear(),
		month = months[date.getMonth()],
		day = date.getDate(),
		hours = date.getHours(),
		minutes = "0" + date.getMinutes(),
		seconds = "0" + date.getSeconds();
	var ampm = 'am';
		if (hours > 12) {
		hours -= 12;
		ampm = 'pm';
		} else if (hours === 0) {
		hours = 12;
		}

		// Will display time in MM. DD, YY 10:30:23 format
		var formattedTime = month + ' ' + day + ', ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) +ampm;
		return formattedTime;
};

	try{
        var socket = io.connect('http://127.0.0.1:8080');
	}catch(e){
		//Set Chat_status to warn user
	}
	if (socket !==  undefined) {

		//listen for output_chat
		socket.on('output_chat',function(data){
			if (data.length) {
				for (var x = data.length-1; x >= 0; x--) {
				if(data[x].class_id == class_id.value){

					var dir = document.createElement('li'),
						img =  document.createElement('img'),
						msg = document.createElement('div'),
						arrow = document.createElement('span'),
						name = document.createElement('a'),
						datetime = document.createElement('span'),
						body = document.createElement('span'),
						sup = document.createElement('sup'),
						scroll = document.createElement('div');

					if (chatName.value == data[x].school_id) {
					
					dir.setAttribute('class','out');		
					name.textContent = "You";
					msg.appendChild(datetime);
				    datetime.appendChild(sup);
					msg.appendChild(name);				
					sup.textContent = formattTime(data[x].modified_at)+ "  ";	
					}else{
					
					dir.setAttribute('class','in');
					name.textContent = data[x].username;
					msg.appendChild(name);
				    msg.appendChild(datetime);
				    datetime.appendChild(sup);
					sup.textContent = "  " +formattTime(data[x].modified_at);
					}

					arrow.setAttribute('class', 'arrow');
					msg.setAttribute('class', 'message');
					img.setAttribute('class', 'avatar');
					img.setAttribute('alt', 'photo');
					img.setAttribute('src', data[x].photo);
					name.setAttribute('class', 'name');
					datetime.setAttribute('class', 'datetime');
					body.setAttribute('class', 'body');
					body.setAttribute('style', 'word-wrap: break-word;');
					scroll.setAttribute("class", "scrollIntoView");


					body.textContent = data[x].content;


					
					chat_contents.appendChild(dir);
					chat_contents.insertBefore(dir, chat_contents.childNodes[x-data.length]);					
					
					dir.appendChild(img);
					dir.insertBefore(img, dir.firstChild);

					dir.appendChild(msg);
					msg.appendChild(arrow);
					msg.appendChild(body);
					msg.appendChild(scroll);
					msg.insertBefore(scroll, msg.lastChild).scrollIntoView();


					}

				}
			}
		});

		//listen for Chat_status
		socket.on('chatstatus', function(data){
			setChat_status((typeof data === 'object') ? data.message : data);
			if (data.clear === true) {
				textarea.value = '';
				stat.style.color = "#0099FF";
			}else{
				stat.style.color = "red";
			}
		});

		// listed for keydown event
		textarea.addEventListener('keydown', function(event){
			var self = this,
				school_id = chatName.value,
				timeStamp = Math.floor(Date.now());

				// event.shiftKey.preventDefault();

			if (event.which === 13 && event.shiftKey === false) {
				socket.emit('input_chat',{
					class_id: class_id.value,
					school_id: school_id,
					username: username.value,
					content: self.value,
					photo: session_userphoto,
					modified_at: timeStamp

				});
				event.preventDefault();
			}
		});

	}
}
})();