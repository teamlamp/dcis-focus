(function (){
	var getNode = function (s){
		return document.querySelector(s);
	},

	//Get required nodes
	status = getNode('.post-status span'),
	discussion_contents = getNode('.feed-contents'),
	textarea = getNode('.post textarea'),
	postName = getNode('.post-name'),
	submit = getNode('#submit_post'),
	stat = getNode('#stat'),
	class_id = getNode('#class_id'),
	username = getNode('#username'),

	defaultStatus = status.textContent,
	setStatus = function(s){
		status.textContent = s;

		if (s !== defaultStatus) {
			var delay = setTimeout(function(){
				setStatus(defaultStatus);
				stat.style.color = "#ccc";
				clearInterval(delay);
			}, 2000);
		}
	},

	formattedTime = function(date){
			// Create a new JavaScript Date object based on the timestamp
		date = new Date(date),
		// Hours part from the timestamp
		months = ['January','February','March','April','May','June','July','August','September','October','November','December'],
		year = date.getFullYear(),
		month = months[date.getMonth()],
		day = date.getDate(),
		hours = date.getHours(),
		minutes = "0" + date.getMinutes(),
		seconds = "0" + date.getSeconds();
	var ampm = 'am';
		if (hours > 12) {
		hours -= 12;
		ampm = 'pm';
		} else if (hours === 0) {
		hours = 12;
		}
		// Will display time in MM. DD, YY 10:30:23 format
		var formattedTime = month + ' ' + day + ', ' + year + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) +ampm;
		return formattedTime;
},

commentCounter = function (discussion_id){
	socket.emit('setCommentCount',{
	discussion_id: discussion_id
	});

		socket.on('getCommentCount',function(data){
		if (data.length) {
		for (var x = 0; x < data.length; x++) {
        var oldcounter = document.getElementById('commentCount_'+data[x].discussion_id);
			if (oldcounter != null) {
			var counter = document.createElement('span'),
				counterParent = oldcounter.parentNode;

			counter.setAttribute('id', 'commentCount_'+data[x].discussion_id);
			
			if(data[x].count == 1){
			counter.textContent = ' '+data[x].count+' comment';
			}else{
			counter.textContent = ' '+data[x].count+' comments';
			}

            counterParent.replaceChild(counter, oldcounter);
		 }
		}
		}
	});

};

	var lastdid = 0, sessionlastid = 0;
	try{
        var socket = io.connect('http://127.0.0.1:8080');
	}catch(e){
		//Set status to warn user
	}
	if (socket !==  undefined) {
	     

		// Lister for post notif 
		socket.on('post_notify',function(data){
		if (data.length) {
		for (var x = 0; x < data.length; x++) {
		if(data[x].class_id == class_id.value && postName.value != data[x].school_id){
        playSound();
		toastr.options = {
            "closeButton": true,
            "newestOnTop": false,
            "positionClass": "toast-bottom-right",
        };
        // toastr.options.onclick = window.location.href='http://www.google.com/';
        toastr.info('Added a new post to your class', data[x].username);
         }
	     }
	 }
	});


		//listen for post_output
		socket.on('post_output',function(data){
			if (data.length) {
				for (var x = 0; x < data.length; x++) {
				if(data[x].class_id == class_id.value && data[x].active == 1){
					var discussion_content	= document.createElement('div'),
						portlet_body = document.createElement('div'),
						item = document.createElement('div'),
						item_body = document.createElement('div'),
						tag_data = document.createElement('div'),
						row = document.createElement('div'),
						feed = document.createElement('span'),

						col_item_details = document.createElement('div'),
						feed_pic = document.createElement('img'),
						feed_name = document.createElement('a'),


						col_inner = document.createElement('div'),
						list_inline = document.createElement('ul'),
						comment = document.createElement('div'),
						discussion_id = document.createElement('input'),
						school_id = document.createElement('input'),
						edited = document.createElement('sup');
	
					// set element attribute
					discussion_id.setAttribute('type', 'hidden');
					discussion_id.setAttribute('class', 'pid');
					discussion_id.setAttribute('value', data[x].discussion_id);

					edited.setAttribute('id', 'edited_'+data[x].discussion_id);


					school_id.setAttribute('type', 'hidden');
					school_id.setAttribute('class', 'sid');
					school_id.setAttribute('value', data[x].school_id);

					comment.setAttribute('id', 'comment_'+data[x].discussion_id);
					discussion_content.setAttribute('class','portlet light');
					portlet_body.setAttribute('class', 'portlet-body');
					item.setAttribute('class', 'item');
					item_body.setAttribute('class', 'item-body');
					tag_data.setAttribute('class', 'blog-tag-data');
					row.setAttribute('class', 'row');
					feed.setAttribute('class', 'feed');
					feed.setAttribute('id', 'feed_'+data[x].discussion_id+'');

					col_item_details.setAttribute('class', 'col-md-6 item-details');
					col_inner.setAttribute('class', 'col-md-6 blog-tag-data-inner');
					list_inline.setAttribute('class', 'list-inline');
					
					if (session_school_id == data[x].school_id) {
					list_inline.insertAdjacentHTML('beforeend', '<li id="postDate_'+data[x].discussion_id+'"><i class="fa fa-calendar"></i><a href="javascript:;"> '+formattedTime(data[x].modified_at)+' </a></li><li><i class="fa fa-pencil postEdit" title="Edit"></i><i class="fa fa-trash postDelete" title="Delete"></i></li>');
					}else{
					list_inline.insertAdjacentHTML('beforeend', '<li id="postDate_'+data[x].discussion_id+'"><i class="fa fa-calendar"></i><a href="javascript:;"> '+formattedTime(data[x].modified_at)+' </a></li>');
					}

					if (data[x].edited == 1) {
						edited.textContent = ' Edited';
					}

					feed_pic.setAttribute('class', 'dist item-pic img-circle');
					feed_pic.setAttribute('style', 'width:50px; height:50px;');
					feed_pic.setAttribute('src', data[x].photo);

					feed_name.setAttribute('class', 'item-name primary-link');
					feed_name.setAttribute('href', '/profile/'+data[x].school_id+'');
					
					feed_name.textContent = data[x].username;

					feed.innerHTML = data[x].discussion_content;

					//Append discussion_contents

					discussion_contents.appendChild(discussion_content);
					discussion_contents.insertBefore(discussion_content, discussion_contents.childNodes[data.length-1]);

					discussion_content.appendChild(portlet_body);
					discussion_content.insertBefore(portlet_body, discussion_content.lastChild);

					portlet_body.appendChild(item);
					portlet_body.insertBefore(item, portlet_body.lastChild);

					item.appendChild(item_body);
					item.insertBefore(item_body, item.lastChild);

					item_body.appendChild(tag_data);
					item_body.insertBefore(tag_data, item_body.lastChild);
					item_body.appendChild(comment);

					tag_data.appendChild(row);
					tag_data.insertBefore(row, tag_data.lastChild);

					tag_data.appendChild(feed);
					tag_data.insertBefore(feed, tag_data.lastChild);

					row.appendChild(col_item_details);
					row.insertBefore(col_item_details, row.lastChild);

					col_item_details.appendChild(feed_name);
					col_item_details.insertBefore(feed_name, col_item_details.lastChild);
					col_item_details.appendChild(feed_pic);
					col_item_details.insertBefore(feed_pic, col_item_details.firstChild);

					row.appendChild(col_inner);
					row.insertBefore(col_inner, row.childNodes[1]);
					col_inner.appendChild(list_inline);

					tag_data.appendChild(edited);

					col_inner.insertBefore(list_inline, col_inner.lastChild);
					row.appendChild(discussion_id);
					row.appendChild(school_id);

// ---------------------------------- comment part ------------------------- //

					var panel_default = document.createElement('div'),
						panel_heading = document.createElement('div'),
						comment_show_toggle = document.createElement('div'),
						comment_show = document.createElement('div'),
						hr = document.createElement('hr'),
						comment_input = document.createElement('div'),
						comment_form = document.createElement('form'),
						form_group = document.createElement('div'),
						comment_submit = document.createElement('div'),
						comment_button = document.createElement('button');

					panel_default.setAttribute('class','panel-default');
					panel_heading.setAttribute('class', 'panel-heading');
					comment_show.setAttribute('id','comment_show_'+data[x].discussion_id);
					comment_show.setAttribute('class','panel-collapse collapse');
					comment_show.setAttribute('aria-expanded','false');
					comment_show.setAttribute('style','height:0px');
					comment_input.setAttribute('id', 'post-comment'+data[x].discussion_id);
					comment_form.setAttribute('form', 'form');
					comment_form.setAttribute('action', 'javascript:;');
					form_group.setAttribute('class','form-group');
					comment_submit.setAttribute('class','btntoright');
					comment_button.setAttribute('class', 'margin-top-20 btn btn-sm green-haze submit_comment');
					comment_button.setAttribute('id', 'submit_comment');
					comment_button.setAttribute('type', 'submit');

					commentCounter(data[x].discussion_id)
					
					comment_show_toggle.insertAdjacentHTML('beforeend', '<a class="comment_show-toggle collapsed" data-toggle="collapse" data-parent="#comment_show_'+data[x].discussion_id+'" href="#comment_show_'+data[x].discussion_id+'" aria-expanded="false" style="text-decoration:none"><i class="fa fa-comments"></i><span id="commentCount_'+data[x].discussion_id+'"> Be first to comment...</span></a>');

					form_group.insertAdjacentHTML('beforeend','<textarea class="col-md-10 form-control" id="comment_textarea" placeholder="Leave a comment ..." rows="2"></textarea>');
					comment_button.insertAdjacentHTML('beforeend','Comment');

					//Append 
					item_body.appendChild(panel_default);
					item_body.insertBefore(panel_default, item_body.lastChild);
					panel_default.appendChild(panel_heading);
					panel_heading.appendChild(comment_show_toggle);
					panel_default.appendChild(comment_show);
					panel_default.insertBefore(comment_show, panel_default.lastChild);

					comment_show.appendChild(comment_input);

					var inputparent = comment_input.parentNode;
					inputparent.appendChild(comment_form);

					inputparent.insertBefore(comment_form, comment_input.lastChild);
					comment_form.appendChild(form_group);
					comment_form.insertBefore(form_group, comment_form.lastChild);
					comment_form.appendChild(comment_submit);
					comment_form.insertBefore(comment_submit, comment_form.lastChild);
					comment_submit.appendChild(comment_button);
					comment_submit.insertBefore(comment_button, comment_submit.lastChild);

				// get all id
				if (!data[x].discussion_id) {
						lastdid = +lastdid + 1;
						sessionlastid = lastdid;
						console.log('if on loop discussion_id value'+sessionlastid);

					}else if (data[0].discussion_id){
						lastdid = data[0].discussion_id;
						sessionlastid = lastdid;
						console.log('else if on loop discussion_id value'+sessionlastid);
					}
				}

			}
			}else{
					var discussion_content	= document.createElement('div'),
						portlet_body = document.createElement('div'),
						item = document.createElement('div'),
						item_body = document.createElement('div'),
						tag_data = document.createElement('div'),
						row = document.createElement('div'),
						feed = document.createElement('div'),

						col_item_details = document.createElement('div'),
						feed_pic = document.createElement('img'),
						feed_name = document.createElement('a'),
						hr = document.createElement('hr'),


						col_inner = document.createElement('div'),
						list_inline = document.createElement('ul'),
						comment = document.createElement('div');

					// set element attribute

					comment.setAttribute('id', 'comment');
					discussion_content.setAttribute('class','portlet light');
					portlet_body.setAttribute('class', 'portlet-body');
					item.setAttribute('class', 'item');
					item_body.setAttribute('class', 'item-body');
					tag_data.setAttribute('class', 'blog-tag-data');
					row.setAttribute('class', 'row');
					feed.setAttribute('class', 'feed');

					col_item_details.setAttribute('class', 'col-md-6 item-details');
					col_inner.setAttribute('class', 'col-md-6 blog-tag-data-inner');
					list_inline.setAttribute('class', 'list-inline');
					list_inline.insertAdjacentHTML('beforeend', '<li><i class="fa fa-calendar"></i><a href="javascript:;"> '+formattedTime('1995 05 15 12:59:59')+' </a></li><li><i class="fa fa-angle-down"></i></li>');

					feed_pic.setAttribute('class', 'dist item-pic circle');
					feed_pic.setAttribute('src', '../../assets/admin/layout3/img/avatar4.jpg');

					feed_name.setAttribute('class', 'item-name primary-link');
					feed_name.setAttribute('href', '/profile/admin');
					
					feed_name.textContent = 'Computer and Information Sciences';

					feed.textContent = 'Welcome to your class page! This is a System generated post.';

					//Append discussion_contents

					discussion_contents.appendChild(discussion_content);
					discussion_contents.insertBefore(discussion_content, discussion_contents.childNodes[data.length-1]);

					discussion_content.appendChild(portlet_body);
					discussion_content.insertBefore(portlet_body, discussion_content.lastChild);

					portlet_body.appendChild(item);
					portlet_body.insertBefore(item, portlet_body.lastChild);

					item.appendChild(item_body);
					item.insertBefore(item_body, item.lastChild);

					item_body.appendChild(tag_data);
					item_body.insertBefore(tag_data, item_body.lastChild);
					item_body.appendChild(hr);
					item_body.appendChild(comment);

					tag_data.appendChild(row);
					tag_data.insertBefore(row, tag_data.lastChild);

					tag_data.appendChild(feed);
					tag_data.insertBefore(feed, tag_data.lastChild);

					row.appendChild(col_item_details);
					row.insertBefore(col_item_details, row.lastChild);

					col_item_details.appendChild(feed_name);
					col_item_details.insertBefore(feed_name, col_item_details.lastChild);
					col_item_details.appendChild(feed_pic);
					col_item_details.insertBefore(feed_pic, col_item_details.firstChild);

					row.appendChild(col_inner);
					row.insertBefore(col_inner, row.childNodes[1]);
					col_inner.appendChild(list_inline);
					col_inner.insertBefore(list_inline, col_inner.lastChild);
			}
		});

		//listen for status
		socket.on('status', function(data){
			setStatus((typeof data === 'object') ? data.message : data);
			if (data.clear === true) {
				textarea.value = '';
				$('#post-here').data("wysihtml5").editor.clear();
				console.log(data.message);
				document.getElementById("stat").style.color = "#0099FF";
			}else{
				document.getElementById("stat").style.color = "#990000";
			}
		});


		socket.emit('setClassInfo',{
			class_id: class_id.value,
			username: username.value
		});



		// listed for keydown event
		submit.addEventListener('click', function(event){
				var self = textarea,
				discussion_id = +sessionlastid + 1,
				school_id = postName.value,
				timeStamp = Math.floor(Date.now());
				console.log('clicked discussion_id value '+discussion_id);

			if (event.which === 1) {
				socket.emit('post_input',{
					discussion_id: discussion_id,
					class_id: class_id.value,
					username: username.value,
					school_id: school_id,
					discussion_content: self.value,
					modified_at: timeStamp,
					photo: session_userphoto,
					active: 1
				});
				event.preventDefault();
			}
			});
	}
})();