// ----------------- Start of edit post ---------------------
  var  formattedTime = function(date){
            // Create a new JavaScript Date object based on the timestamp
        date = new Date(date),
        // Hours part from the timestamp
        months = ['January','February','March','April','May','June','July','August','September','October','November','December'],
        year = date.getFullYear(),
        month = months[date.getMonth()],
        day = date.getDate(),
        hours = date.getHours(),
        minutes = "0" + date.getMinutes(),
        seconds = "0" + date.getSeconds();
    var ampm = 'am';

        if (hours > 12) {
        hours -= 12;
        ampm = 'pm';
        } else if (hours === 0) {
        hours = 12;
        }
        // Will display time in MM. DD, YY 10:30:23 format
        var formattedTime = month + ' ' + day + ', ' + year + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2) +ampm;
        return formattedTime;
};

 var  escapeHTML = function( string )
{
    var pre = document.createElement('pre');
    var text = document.createTextNode( string );
    pre.appendChild(text);
    return pre.innerHTML;
}

var feed_id = null;



$(document).on('click','.postEdit',function(){
    var text = $(this).closest('.item').find('.feed').html(),
        feed_id = $(this).closest('.item').find('.feed').attr('id'),
        feed = $(this).closest('.item').find('.feed'),
        school_id = $(this).closest('.item').find('.sid').val(),
        editButton = $(this).closest('.item').find('.postEdit');
        console.log(text);
        if (session_school_id == school_id) {            
        feed.replaceWith("<textarea id='"+feed_id+"' class='editedText form-control wysihtml5' style='min-height:300px'>"+text+"</textarea>");
        editButton.replaceWith("<i class='fa fa-check savePostEdit' title='Save'></i><i class='fa fa-times cancelPostEdit' title='Cancel'></i>");
        }
});

$(document).on('click','.cancelPostEdit',function(){
    var text = $(this).closest('.item-body').find('.editedText').val(),
        feed = $(this).closest('.item-body').find('.editedText'),
        forfeed_id = $(this).closest('.item').find('.pid').val(),
        feed_id = 'feed_'+forfeed_id;
        saveButton = $(this).closest('.item').find('.savePostEdit'),
        cancelButton = $(this).closest('.item').find('.cancelPostEdit');
        
        $('.editedText').data('wysihtml5').editor.composer.disable();
        $('.editedText').data('wysihtml5').editor.composer.hide();
        $('.editedText').data('wysihtml5').editor.toolbar.hide();
        feed.replaceWith("<span class='feed' id="+feed_id+">"+text+"</span>");
        // feed.innerHTML = text;

        saveButton.replaceWith("<i class='fa fa-pencil postEdit' title='Edit'></i>");
        cancelButton.remove();

});

$(document).ready(function () {
   $('.postEdit').live('click', function () {
     $('.editedText').wysihtml5();
     var wysihtml5Editor = $(".editedText").data("wysihtml5").editor;
     console.log('wysihtml5Editor: '+wysihtml5Editor);
     // The following is important since wysihtml5 is initialized asynchronously
     wysihtml5Editor.observe("load", function() {
       wysihtml5Editor.composer.commands.exec("bold");
     });
   });
});

(function(){
    var getNode = function (s){
        return document.querySelector(s);
    };

    try{
        var socket = io.connect('http://127.0.0.1:8080');
    }catch(e){
        //Set status to warn user
    }
    var username = document.querySelector('#username').value;
    var newtext, feed, school_id, discussion_id, saveButton ,cancelButton, newDate;
    var lastdid = 0, sessionlastid = 0;

$(document).on('click','.savePostEdit',function(){
        feed_id = $(this).closest('.item-body').find('.editedText').attr('id');
        newtext = getNode('#'+feed_id);
        // date_id = $(this).closest('.item-body').find('.editedText').attr('id');
        school_id = $(this).closest('.item').find('.sid').val();
        discussion_id = $(this).closest('.item').find('.pid').val();
        saveButton = $(this).closest('.item').find('.savePostEdit');
        cancelButton = $(this).closest('.item').find('.cancelPostEdit');
        newDate = $(this).closest('.item').find('.postDate');  

        $('.editedText').data('wysihtml5').editor.composer.disable();
        $('.editedText').data('wysihtml5').editor.composer.hide();
        $('.editedText').data('wysihtml5').editor.toolbar.hide();

        socket.emit('setEditedPost',{
            feed_id: feed_id,
            class_id: class_id.value,
            school_id: school_id,
            username: username,
            discussion_id: discussion_id,
            discussion_content: newtext.value,
            modified_at: formattedTime(Math.floor(Date.now())),

        });
});

socket.on('getEditedPost', function(data){
    if (data.length) {
        for (var x = 0; x < data.length; x++) {

            feed = document.getElementById(data[x].feed_id);
            date = document.getElementById('postDate_'+data[x].discussion_id+'');
            edited =  document.getElementById('edited_'+data[x].discussion_id);    
   if (data[x].class_id == class_id.value) {
            if (feed.tagName == "SPAN") {
                var newfeed = document.createElement('span'),
                    feedParent = feed.parentNode,
                    newdate = document.createElement('li'),
                    dateParent = date.parentNode,
                    newedited = document.createElement('sup'),
                    editedParent = edited.parentNode;

                newfeed.setAttribute('class','feed');
                newfeed.setAttribute('id', data[x].feed_id);
                newfeed.innerHTML = data[x].discussion_content;

                newedited.textContent = 'Edited';

                newdate.setAttribute('id','postDate_'+data[x].discussion_id+'');
                newdate.insertAdjacentHTML('beforeend','<i class="fa fa-calendar"></i><a href="javascript:;"> '+data[x].modified_at+' </a>');
                
                newedited.setAttribute('id','edited_'+data[x].discussion_id);

                feedParent.replaceChild(newfeed, feed);
                dateParent.replaceChild(newdate, date);
                editedParent.replaceChild(newedited, edited);

                
                playSound();
                toastr.options = {
                    "closeButton": true,
                    "newestOnTop": false,
                    "positionClass": "toast-bottom-right",
                };
                toastr.info('Edited a posts in this class', data[x].username);

                
            }else{

            // swal('Success','Post has been edited.','success');
            swal({   title: "Success",   text: "Your post has been edited.", type:"success", timer: 1000,   showConfirmButton: false }); 


                var newfeed = document.createElement('span'),
                    feedParent = feed.parentNode,
                    newdate = document.createElement('li'),
                    dateParent = date.parentNode,
                    newedited = document.createElement('sup'),
                    editedParent = edited.parentNode;

                newfeed.setAttribute('class','feed');
                newfeed.setAttribute('id', data[x].feed_id);
                newfeed.innerHTML = data[x].discussion_content;

                newedited.textContent = 'Edited';

                newdate.setAttribute('id','postDate_'+data[x].discussion_id+'');
                newdate.insertAdjacentHTML('beforeend','<i class="fa fa-calendar"></i><a href="javascript:;"> '+data[x].modified_at+' </a>');
                
                newedited.setAttribute('id','edited_'+data[x].discussion_id);

                feedParent.replaceChild(newfeed, feed);
                dateParent.replaceChild(newdate, date);
                editedParent.replaceChild(newedited, edited);

            saveButton.replaceWith("<i class='fa fa-pencil postEdit' title='Edit'></i>");
            cancelButton.remove();
            }
        }

    }
}
});

$(document).on('click','.postDelete',function(){
        discussion_id = $(this).closest('.item').find('.pid').val();
        school_id = $(this).closest('.item').find('.sid').val();

        var deletePost = function (sure){
            socket.emit('setDeletedPost',{
            discussion_id: discussion_id, username: username, school_id: school_id, sure: sure
            });
        };
        if (session_school_id == school_id) {

        swal({  title: "Are you sure?",   
                text: "You will not be able to recover this post!",   
                type: "warning",   showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Delete Post",   
                closeOnConfirm: false,
                closeOnCancel: false
            }, 
                function(isConfirm){  
                if (isConfirm == true) {  
                        deletePost(true);
                        // swal("Deleted", "Your post has been deleted.", "success");
                        swal({   title: "Deleted",   text: "Your post has been deleted.", type:"success", timer: 1000,   showConfirmButton: false }); 
   
                    } else if (isConfirm == false){
                        deletePost(false);
                        // swal("Cancelled", "Your post is safe.", "error");
                        swal({   title: "Cancelled",   text: "Your post is safe.", type:"error", timer: 1000,   showConfirmButton: false }); 

                    }
                });

        }

});

socket.on('postDeleted', function(data){
    if (data.success == true) {
        // console.log(data.discussion_id);
            feed = document.getElementById('feed_'+data.discussion_id);

            feedParent = feed.parentNode;
            feedParentx2 = feedParent.parentNode;
            feedParentx3 = feedParentx2.parentNode;
            feedParentx4 = feedParentx3.parentNode;
            feedParentx5 = feedParentx4.parentNode;

            feedParentx5.remove();
        if (session_school_id != data.school_id) {
            playSound();
                toastr.options = {
                    "closeButton": true,
                    "newestOnTop": false,
                    "positionClass": "toast-bottom-right",
                };
                toastr.info('Removed a post in this class', data.username);
        }
    }      
});


})();


// ----------------End of edit post -------------------------