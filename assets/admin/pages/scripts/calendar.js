var Calendar = function() {


    return {
        //main function to initiate the module
        init: function() {
            Calendar.initCalendar();
        },

        initCalendar: function() {

            if (!jQuery().fullCalendar) {
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};

            if (Metronic.isRTL()) {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        right: 'title, prev, next',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today'
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        right: 'title',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today, prev,next'
                    };
                }
            } else {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        left: 'title, prev, next',
                        center: '',
                        right: 'today,month,agendaWeek,agendaDay'
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }

            var initDrag = function(el) {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim(el.text()) // use the element's text as the event title
                };
                // store the Event Object in the DOM element so we can get to it later
                el.data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                el.draggable({
                    zIndex: 999,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0 //  original position after the drag
                });
            };

            var addEvent = function(title) {
                title = title.length === 0 ? "Untitled Event" : title;
                var html = $('<div class="external-event label label-default">' + title + '</div>');
                jQuery('#event_box').append(html);
                initDrag(html);
            };

            $('#external-events div.external-event').each(function() {
                initDrag($(this));
            });

            /*----------store all events --------------*/
            
            $('#event_add').unbind('click').click(function() {
                var title = $('#event_title').val();
                addEvent(title);
                /*
                $.ajax({
                      url: base_url("class/event"),
                      method: "POST",
                      data: {
                        title: title
                      },
                      success: function(data){
                        console.log(data);
                        addEvent(title);
                      }
                  });
                
                */
            });
            
            /*----------get all events -------------*/
            //predefined events
            $('#event_box').html("");
            /*
            $.ajax({
                  url: base_url("class/event"),
                  method: "GET",
                  success: function(data){
                    $.each(data,function(index,value){
                        addEvent(value.title);
                    });
                  }
              });
            */
            addEvent("test");

            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({ //re-initialize the calendar
                header: h,
                defaultView: 'month', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
                slotMinutes: 15,
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function(date, allDay) { // this function is called when something is dropped
                    
                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');
                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.className = $(this).attr("data-class");
                    var new_date = copiedEventObject.start.format();
                    var new_event = copiedEventObject.title;
                    /*ajax request to server*/
                    $.ajax({
                        url: BASE_URL+'calendar/addHoliday',
                        data: {'type': 'new','title':new_event, 'startdate':new_date},
                        type: 'POST',
                        dataType: 'json',
                        success: function(){
                            /*alert success*/
                            sweetAlert('Success!','You have successfully added a holiday','success');
                        
                        },
                        error: function(e){
                        console.log(e.responseText);
                        }
                        });
                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        console.log($(this))
                        $(this).remove();
                    }
                },
                /* once naay event nga ma drop sa actual calendar mao ni function */
                eventReceive: function(event){
                    
                    var title = event.title;
                    var start = event.start.format("YYYY-MM-DD[T]HH:MM:SS");
                    $.ajax({
                        url: BASE_URL+'calendar/addHoliday',
                        data: 'type=new&title='+title+'&startdate='+start,
                        type: 'POST',
                        dataType: 'json',
                        success: function(response){
                            event.id = response.eventid;
                            $('#calendar').fullCalendar('updateEvent',event);
                        },
                        error: function(e){
                        console.log(e.responseText);
                        }
                        });
                    $('#calendar').fullCalendar('updateEvent',event);
                },
                /* change title of an event (ika click kay mo gawas ang sweetalert) */
                eventClick: function(event, jsEvent, view) {
                    swal({   
                        title: "Edit Title",   
                        text: "Enter new title for this event",   
                        type: "input",   
                        showCancelButton: true,   
                        closeOnConfirm: false,   
                        animation: "slide-from-top",   
                        inputPlaceholder: event.title }, 
                        function(inputValue){   
                            if (inputValue === false) 
                                return false;      
                            if (inputValue === "") {     
                                swal.showInputError("You need to write something!");    
                                 return false   
                             }   
                                var title = inputValue;
                                if (title){
                                    event.title = title;
                                    $.ajax({
                                    url: BASE_URL+'calendar/updateHoliday',
                                    data: 'type=changetitle&title='+title+'&eventid='+event.id,
                                    type: 'POST',
                                    dataType: 'json',
                                    success: function(response){
                                        if(response.status == 'success') {
                                            $('#calendar').fullCalendar('updateEvent',event);
                                            swal("Success!", "Title update to: " + title, "success"); 
                                        }
                                        
                                    },
                                    error: function(e){
                                        swal('Error', 'Error processing your request: '+e.responseText, 'error');
                                    }
                                });
                                }   
                                 
                             });
                    //prompt('Event Title:', event.title, { buttons: { Ok: true, Cancel: false} });
                    
                },
                /*resize events*/
                eventResize: function (event, delta, revertFunc) {
                    swal({   
                        title: "Are you sure?",   
                        text: "This event will be modified",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Yes, delete it!",   
                        cancelButtonText: "No, cancel plx!",   
                        closeOnConfirm: false,   
                        closeOnCancel: false }, 
                        function(isConfirm){   
                            if (isConfirm) {     
                                swal("Deleted!", "Your imaginary file has been deleted.", "success");   
                            } else {     
                                swal("Cancelled", "Your imaginary file is safe :)", "error");   
                            } 
                        });

                    if (!confirm(event.title + " end is now " + (event.end.format()) + ".\n\nIs this okay?")) {
                        revertFunc();
                    } else {
                        alert("Date was changed to: " + event.end.format());
                    }
                },
                
                events: BASE_URL+'calendar/getHoliday'
            });

        }

    };

}();