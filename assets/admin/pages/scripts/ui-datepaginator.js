var UIDatepaginator = function () {

    return {

        //main function to initiate the module
        init: function () {

            //sample #1
            $('#datepaginator_sample_1').datepaginator();

            //sample #2
            $('#datepaginator_sample_2').datepaginator({
                size: "large"
            });

            //sample #3
            $('#datepaginator_sample_3').datepaginator({
                size: "small"

            });

            //sample #3
            var selectedDate;
            $('#timeline_datepignator').datepaginator({
                size: 'small',
                itemWidth: 40,
                onSelectedDateChanged: function(event, date) {
                selectedDate = moment(date).format("YYYY-MM-DD");
                swal({   title: "Go back in time?",   
                    text: "Do you want to load feeds from "+moment(date).format('MMMM Do YYYY')+"",   
                    type: "info",   showCancelButton: true,   
                    closeOnConfirm: false,
                      closeOnCancel: false,
                    showLoaderOnConfirm: true, }, 
                    function(isConfirm){
                    if (isConfirm) {   
                        setTimeout(function(){     
                        window.location = BASE_URL+"timeline?date="+selectedDate+"";
                        }, 2000);
                    }else{
                        location.reload();
                    }
                    });
                }
            });


    $( window ).load(function() {
        $( ".dp-item" ).each(function( index ) {
         if ($(this).attr('data-moment') === timelinedate) {
            $(this).attr('class', 'dp-selected dp-item-sm');
         }
        });
    }); 
    

        $(document).on('click','.dp-today',function(){
        swal({   title: "Load present feeds?",   
            text: "Do you want to load feeds from "+moment().format('MMMM Do YYYY')+"",   
            type: "info",   showCancelButton: true,   
            closeOnConfirm: false,   
            showLoaderOnConfirm: true, }, 
            function(){   
                setTimeout(function(){     
                window.location = BASE_URL+"timeline";
                }, 2000); 
            });
        });
        } // end init

    };

}();