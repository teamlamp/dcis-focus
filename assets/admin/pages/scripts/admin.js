/**
Custom module for you to write your own javascript functions
**/
var base_url = function(url){
  return "http://www.dcis-focus.dev/"+url;
}

  $(".ajaxChangePass").click(function(){
    var old_password = $(".changePassword .old_password").val();
    var new_password = $(".changePassword .new_password").val();
    var confirm_password = $(".changePassword .confirm_password").val();
    var request = {
                  'old_password': old_password,
                  'new_password': new_password,
                  'confirm_password': confirm_password
                };
      $.ajax({
                type: "POST",
                url: base_url("settings/changePassword"),
                data: request,
                success: function(response,status){
                  var data = JSON.parse(response); 
                  console.log(data.message)
                  $('.changePassword .message').html(data.message);
                  $('.changePassword .message').show();
                  
                  if(data.status === 200 ){
                    $(".changePassword .old_password").val('');
                    $(".changePassword .new_password").val('');
                    $(".changePassword .confirm_password").val('');
                    $(".alert-message-change-password").removeClass("alert alert-danger");
                    $(".alert-message-change-password").addClass("alert alert-success");
                    $(".alert-message-change-password .message").html(data.message);
                  }else{
                    $(".alert-message-change-password").removeClass("alert alert-success");
                    $(".alert-message-change-password").addClass("alert alert-danger");
                    $(".alert-message-change-password .message").html(data.message);
                  }
                },
                error: function(response){
                  alert('Error');
                  console.log(response)
                }
            });  
  
  });


$(".btn-show-add-course").on("click",function(){
  $("#addcourse").modal("show");
  $.get(
    base_url("programs/getAllPrograms"),
    function(data){                     
      $.each(data,function(index,value){
        console.log(value)
       $(".ajaxPrograms").append(new Option(value.program_code, value.program_id));
      });
    },
    'json'
  );
});

$(".getClasses").on("change",function(){
  var instructor_id = $(this).val();
  $.get(
    base_url("class/getHandledClasses?id="+instructor_id),
    function(data){                     
      $.each(data,function(index,value){
        //console.log(value.class_id)
       $(".handledClasses").append(new Option(value.course_code, value.class_id));
      });
    },
    'json'
  );
});

$(".handledClasses").on("change",function(){
  var class_id = $(this).val();
  console.log(class_id)
  $.get(
    base_url("class/getHandledStudents?id="+class_id),
    function(data){                     
      if(data && data.length){
        var toAppend ='';
        $.each(json, function(index, value) {
            toAppend += '<option value="'+value.id+'">'+value.name+'</option>';
        });
        $('#consultees').append(toAppend);
      }
    },
    'json'
  );
});

$(".btn-attendance").on('click',function(){
  var classMemberID = $(this).attr("data-classmemberID");
  var attendanceType = $(this).attr("data-attendanceType");
  $.ajax({
      url: base_url("class/checkAttendance"),
      method: "POST",
      data: {
        class_member_id: classMemberID,
        type: attendanceType
      },
      success: function(data){
        $.notify("Marked as "+attendanceType,"danger"); //danger, info , success , warning
        if(attendanceType == "Absent"){
          $("#"+classMemberID+"_color").css("background","red");
        }else if( attendanceType == "Late"){
          $("#"+classMemberID+"_color").css("background","orange");
        
        }else if(attendanceType == "Present"){
           $("#"+classMemberID+"_color").css("background","green");
        
        }

      }
  });
})

var AdminPage = function () {

    // private functions & variables

    var handleSpinner = function () {

        $('#class_groupnumber').spinner({value:0, min: 0, max: 50});
        $('#class_maxabsences').spinner({value:0, min: 0, max: 10});
        $('#class_requiredyears').spinner({value:0, min: 1, max: 4});
        $('#class_requiredsemester').spinner({value:0, min: 1, max: 2});

    }

    var checkboxDays = function () {

        $([1,2,3,4,5,6]).each(function(idx, x) { 
        
        $('#div' + x)[($('#checkbox' + x).is(":checked")? "show" : "hide")](); // synchronize
        $('#div' + x).filter(":hidden").children("input[type='text']").prop("disabled", true);
        $('#checkbox' + x).change(function() {      // toggle on change     
           $('#div' + x).filter(":hidden").children("input[type='text']").prop("disabled", false);
           $('#div' + x).toggle('fade',800);
         });
        });
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.acadyear').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true,
                startDate: '+0d',
                endDate: '+2y'
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

        /* Workaround to restrict daterange past date select: http://stackoverflow.com/questions/11933173/how-to-restrict-the-selectable-date-ranges-in-bootstrap-datepicker */
    }

    var handleGridster = function () {
        var gridster;

      $(function(){

        gridster = $(".grid").gridster({
          widget_selector: "div",
          widget_base_dimensions: [100, 100],
          widget_margins: [5, 5],
          helper: 'clone',
          serialize_params: function($w, wgd)
          {
            console.log('==classinfo==');
            console.log("student id: "+$($w).attr('data-studentID'));
            console.log("class id: "+$($w).attr('data-classID'));
            
            console.log($w);
           return {
            studentID: $($w).attr('data-studentID'),
            classID: $($w).attr('data-classID'),
            col: wgd.col,
            row: wgd.row,
            class_id : wgd.classID,
            student_id: wgd.studentID
           };
          },
          draggable: 
          {
           stop: function(event, ui, $widget) {    
              var data = JSON.stringify(this.serialize($widget));
              console.log(data);
              $.ajax({
                  url: base_url("class/updateSeatPlan?data="+data),
                  method: "GET",
                  data: data,
                  success: function(data){
                    console.log(data);
                  }
              });
            }
          } 
        }).data('gridster');

        // resize widgets on hover
        gridster.$el
          .on('dblclick', '> div', function() {
              gridster.resize_widget($(this), 2, 2);
              $(this).find('div .yearlvl').fadeIn(200);
              
          })
          .on('mouseleave', '> div', function() {
              gridster.resize_widget($(this), 1, 1);
              $('.yearlvl').fadeOut(200);
          });

      });
    }

    var handleSelect2 = function () {
      
    }
    // public functions
    return {

        //main function
        init: function () {

            handleSpinner();
            checkboxDays();
            handleDatePickers();
            handleGridster();
        }

    };



}();




/***
Usage
***/
//Custom.init();
//Custom.doSomeStuff();