$(document).ready(function(){

/* RENDER CONSULTATION MODAL CONTENT*/
$('#viewconsultation').modal({
        keyboard: true,
        backdrop: "static",
        show:false,
        
    }).on('show.bs.modal', function(){
          var id = $(event.target).closest('tr').data('id');
          var content = $(event.target).closest('tr').data('content');
          var topic = $(event.target).closest('tr').find('.topic').html();
          var date = $(event.target).closest('tr').find('.consultation_date').html();
        //make your ajax call populate items or what even you need
        $.ajax({
            type: 'POST',
            url: BASE_URL+'consultation/getConsultees',
            dataType: 'json',
            data: {id:id},
            success: function (data){
                var htmlStr = '';
                $.each(data, function(k, v){
                htmlStr +='<a href="' + BASE_URL + 'profile/'+ v.id + '">' + v.name + '</a><br />';
            });

            $("#consultees").html(htmlStr);
            },
            error: function(xhr, textStatus, errorThrown){
                alert('request failed');
            }
        });

        $.fn.editable.defaults.mode = 'inline';
        $('#consultation_topic').editable({
          tpl: "<input type='text' class='form-control'>"
        });
        $('#consultation_date').editable({
            inputclass: 'form-control'
        });
        $('#consultation_content').editable({
            showbuttons: 'bottom'
        });
        $(this).find('#consultation_id').html(id)
        $(this).find('#consultation_content').html(content)
        $(this).find('#consultation_date').html(date)
        $(this).find('#consultation_topic').html(topic)
    });

    /*  DELETE SINGLE CONSULTATION AND ALL CONSULTEES   */
    $('#delete_consultation').on('click', function() {
    var id = $('#consultation_id').text();
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this record!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: 'btn-danger',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false
        },
     function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: BASE_URL+'consultation/deleteConsultation',
                type: 'get',
                data: {id:id},
                success: function () {
                    swal("Deleted!", "This record has been successfully deleted!", "success");
                    window.location.reload();
                },
                error: function () {
                    alert('ajax failure');
                }
            });
            
            } else {
                swal("Cancelled", "No queries executed", "error");
            }
        });
    });

/*****************ADD CONSULTATION VALIDATION*********************/
            var form3 = $('#consultation');
            var error3 = $('.alert-danger', form3);
            var success3 = $('.alert-success', form3);

            //IMPORTANT: update CKEDITOR textarea with actual content before submit
            form3.on('submit', function() {
            })

            form3.validate({
                onkeyup: true,
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                rules: {
                    conultation_topic: {
                        minlength: 3,
                        required: true
                    },
                    instructor_id: {
                      required: true
                    },
                    classes: {
                        required: true
                    },
                    'consultees[]': {
                        required: true
                    },
                    consultation_date: {
                        required: true
                    },
                    consultation_content: {
                      required: true
                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
                    /*membership: {
                        required: "Please select a Membership type"
                    },
                    service: {
                        required: "Please select  at least 2 types of Service",
                        minlength: jQuery.validator.format("Please select  at least {0} types of Service")
                    }*/
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.parent(".input-group").size() > 0) {
                        error.insertAfter(element.parent(".input-group"));
                    } else if (element.attr("data-error-container")) { 
                        error.appendTo(element.attr("data-error-container"));
                    } else if (element.parents('.radio-list').size() > 0) { 
                        error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                    } else if (element.parents('.radio-inline').size() > 0) { 
                        error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                    } else if (element.parents('.checkbox-list').size() > 0) {
                        error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                    } else if (element.parents('.checkbox-inline').size() > 0) { 
                        error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success3.hide();
                    error3.show();
                    Metronic.scrollTo(error3, -200);
                },

                highlight: function (element) { // hightlight error inputs
                   $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success3.show();
                    error3.hide();
                    $.ajax({
                      url: form.action,
                      type: form.method,
                      data: $(form).serialize(),
                      success: function(response) {
                          swal({   
                            title: "Your consultation has been successfully added!",   
                            text: "I will close in 2 seconds.", 
                            type: "success", 
                            timer: 2000,   
                            showConfirmButton: false 
                          });
                          setTimeout(function () {
                             window.location.href = BASE_URL+"consultation"; //will redirect to your blog page (an ex: blog.html)
                          }, 3000); //will call the function after 2 secs.
                      },
                      error: function(e){
                        swal({   
                            title: "Oooops! We ran into a problem. Please contact the support group.",   
                            text: "I will close in 2 seconds.", 
                            type: "error", 
                            timer: 2000,   
                            showConfirmButton: false 
                          });
                          setTimeout(function () {
                             window.location.href = BASE_URL+"consultation"; //will redirect to your blog page (an ex: blog.html)
                          }, 3000); //will call the function after 2 secs.
                        console.log(e.responseText);
                        }         
                  }); // submit the form
                }

            });

             //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            $('.select2me', form3).change(function () {
                form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

            //initialize datepicker
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                autoclose: true,
                endDate: '+0d'
            });
            $('.date-picker .form-control').change(function() {
                form3.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input 
            })

/**************************************************************************************/
    /*remove options*/
    function removeOptions(selectbox) {
        var i;
        for(i=selectbox.options.length-1;i>=0;i--)
        {
            selectbox.remove(i);
        }
    }
    //using the function:

    /*  ADD CONSULTATION DYNAMIC SELECT2 OPTIONS    */

    $('#classes').on('change', function(){
    var id = $(this).val();
      $('.students').prop("disabled", false);
      $('#consultees').select2('val', '');
      removeOptions(document.getElementById("consultees"));
      $.ajax ({
        url: BASE_URL+'consultation/getClassMembers',
        data: {id:id},
        dataType: "json",
        type: "get",
        success: function( json ) {
          var toAppend ='<option value=""></option>';
        $.each(json, function(i, o) {
            toAppend += '<option value="'+o.id+'">'+o.name+'</option>';
        });
        $('#consultees').append(toAppend);
        }
      });
   });

    $('#instructor').on('change', function(){
    var id = $(this).val();
      $('#classes').prop("placeholder", "Select class...");
      $('#classes').select2('val', '');
      $('#consultees').select2('val', '');
      removeOptions(document.getElementById("classes"));
      $.ajax ({
        url: BASE_URL+'consultation/getHandledClasses',
        data: {id:id},
        dataType: "json",
        type: "get",
        success: function( arr ) {
          var toAppend ='<option value=""></option>';
        $.each(arr, function(i, o) {
            toAppend += '<option value="'+o.id+'">'+o.text+'</option>';
        });
        $('#classes').append(toAppend);
        }
      });
   });

/*END READY FUNCTION*/
});